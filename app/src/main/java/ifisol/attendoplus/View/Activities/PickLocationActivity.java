package ifisol.attendoplus.View.Activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;

import ifisol.attendoplus.R;
import ifisol.attendoplus.Utilities.Utility;

public class PickLocationActivity extends Activity implements View.OnClickListener, OnMapReadyCallback {

    Context mContext;
    Activity mActivity;
    GoogleMap googleMap;
    private Marker mMarker;
    ImageView mImgBack;
    TextView mTvDone;
    EditText mEdSearch;
    View mViewSearch;
    Handler handler;
    EditText tvAdress;

    String ltlng;
    String address;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pick_location);

        Init();

        mTvDone.setOnClickListener(this);
        mImgBack.setOnClickListener(this);

        LocationManager mLocationManager = (LocationManager) mContext.getSystemService(LOCATION_SERVICE);

            if (!mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
        {
            Toast.makeText(mContext, "Please turn onn  GPS", Toast.LENGTH_LONG).show();
            startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
        }

        mEdSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


            }
            @Override
            public void afterTextChanged(Editable s) {

                if (!mEdSearch.getText().toString().equalsIgnoreCase(""))
                {
                    if (handler!=null)
                        handler.removeMessages(0);
                    handler=new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                                 SearchPlace(mEdSearch.getText().toString().trim());
                        }
                    }, 2000);
                }
            }
        });
    }

    public void Init()
    {

        mActivity=this;
        mContext=this;

        mImgBack    = (ImageView) findViewById(R.id.img_back);
        mTvDone     = (TextView)  findViewById(R.id.tv_done);
        mEdSearch   = (EditText)  findViewById(R.id.ed_search);
        tvAdress    = (EditText) findViewById(R.id.tv_address);
        mViewSearch = (View)      findViewById(R.id.view_search);

        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.tv_done:
                Intent intent = new Intent();
                intent.putExtra("LAT_LNG",ltlng);
                intent.putExtra("ADDRESS",address);
                setResult(RESULT_OK, intent);
                finish();
                break;
            case R.id.img_back:
                onBackPressed();
                break;

        }
    }



    private void updateMarker(LatLng position) {
        mMarker.setPosition(position);

    }

    private void drawMarker(LatLng position){
        mMarker = googleMap.addMarker(new MarkerOptions().position(position).visible(true).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)).title("Selected Location"));

    }




    @Override
    public void onBackPressed() {

        finish();
        mActivity.overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
    }

    private void SearchPlace(String name)
    {

        if (name==null || name.isEmpty())
            return;

        tvAdress.setText(name);
        InputMethodManager inputManager = (InputMethodManager)
                mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(mActivity.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

        Geocoder geocoder = new Geocoder(mContext);
        try {
            //Place your latitude and longitude
            List<Address> addresses = geocoder.getFromLocationName(name, 1);
            if (addresses != null && addresses.size() > 0) {
                double latti = addresses.get(0).getLatitude();
                double longi = addresses.get(0).getLongitude();

                ltlng = String.valueOf(latti)+","+String.valueOf(longi);
                address = name;

                LatLng latLng = new LatLng(latti,longi);
                if (mMarker == null){
                    drawMarker(latLng);
                }else{
                    updateMarker(latLng);
                }
                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latti, longi), 15));

            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            //  Toast.makeText(getApplicationContext(), "Could not get address..!", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onMapReady(GoogleMap map) {


        googleMap = map;
        googleMap.clear();
        googleMap.setMyLocationEnabled(true);
        googleMap.getUiSettings().setMyLocationButtonEnabled(true);
        googleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);


        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {

                mEdSearch.setText("");
                ltlng = String.valueOf(latLng.latitude) + "," + String.valueOf(latLng.longitude);
                new getAddress().execute(ltlng);

                if (mMarker == null) {
                    drawMarker(latLng);
                } else {
                    updateMarker(latLng);
                }
                mViewSearch.setVisibility(View.VISIBLE);
                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));

            }
        });

    }



    private class getAddress extends AsyncTask<String,String,String>
    {

        @Override
        protected String doInBackground(String... params) {

            address = Utility.GetAddress(mContext,ltlng.split(",")[0],ltlng.split(",")[1]);
            return address;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            tvAdress.setText(s.split("|||")[0]);


        }
    }
}


