package ifisol.attendoplus.View.Activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;

import ifisol.attendoplus.R;

public class SplashActivity extends Activity {
    //Change the duration of the splash

    private final int SPLASH_DISPLAY_LENGTH = 2000;
    private Context mContext;
    private SplashActivity mActivity;
    Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        setContentView(R.layout.activity_splash);
        Init();
        handler=new Handler();
        handler.postDelayed(run,SPLASH_DISPLAY_LENGTH);

    }
    public Runnable run=new Runnable() {
        @Override
        public void run()
        {
            Intent intent=new Intent (mActivity,LandingActivity.class);
            startActivity(intent);
            finish();
        }
    };
    public void Init()
    {
        this.mContext=this;
        this.mActivity=this;
    }
}
