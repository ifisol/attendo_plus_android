package ifisol.attendoplus.View.Activities;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.IOException;

import fr.arnaudguyon.smartfontslib.FontButton;
import fr.arnaudguyon.smartfontslib.FontEditText;
import ifisol.attendoplus.Global.Globals;
import ifisol.attendoplus.R;
import ifisol.attendoplus.Utilities.Constants;
import ifisol.attendoplus.Utilities.HttpHelper;
import ifisol.attendoplus.Utilities.Utility;
import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.RequestBody;

public class ActivityForgotPassword extends Activity implements View.OnClickListener, HttpHelper.HttpCallback {

    Context mContext;
    Activity mActivity;
    Globals mGlobal;

    private ImageView    imgClose;
    private FontEditText edEmail;

    private FontButton   btnSubmit;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_pass);
        Init();
    }



    private void Init() {
        mContext  = this;
        mActivity = this;
        mGlobal   = (Globals) mActivity.getApplicationContext();
        imgClose        = (ImageView) findViewById(R.id.img_close);
        edEmail         = (FontEditText) findViewById(R.id.ed_email);
        btnSubmit       = (FontButton) findViewById(R.id.btn_submit);

        btnSubmit.setOnClickListener(this);
        imgClose.setOnClickListener(this);

    }




    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btn_submit:
                SendEmail();
                break;

            case R.id.img_close:
                finish();
                break;
        }
    }




    private void SendEmail() {

        if (edEmail.getText().toString().trim().isEmpty())
            Utility.ShowAlert(mActivity, "Email", "Please enter your Email address");
        else if (!Utility.EmailValidator(edEmail.getText().toString().trim()))
            Utility.ShowAlert(mActivity, "Invalid Email", "Please enter a valid Email address");
        else
        {

            RequestBody formBody = new FormBody.Builder()
                    .add("email", edEmail.getText().toString().trim())
                    .build();

            try {
                HttpHelper.CallApi(mActivity,Constants.forgot_password, formBody, this, true);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void onFailure(Call call, final IOException e) {

        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                Toast.makeText(mContext,"Request Timeout", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public void onSuccess(Call call, String response) {

        ParseRes(response);

    }




    private void ParseRes(String s)
    {
        try {
            Log.d("Response........",s);
            final JSONObject jsonObj = new JSONObject(s);
            final String message = jsonObj.getString("message");
            final String status = jsonObj.getString("status");
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (status.equals("true"))
                            finish();

                         Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
                    }
                });



        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
