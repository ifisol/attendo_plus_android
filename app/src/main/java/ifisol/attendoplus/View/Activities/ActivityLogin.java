package ifisol.attendoplus.View.Activities;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.IOException;

import fr.arnaudguyon.smartfontslib.FontButton;
import fr.arnaudguyon.smartfontslib.FontEditText;
import fr.arnaudguyon.smartfontslib.FontTextView;
import ifisol.attendoplus.Global.Globals;
import ifisol.attendoplus.Model.User;
import ifisol.attendoplus.R;
import ifisol.attendoplus.Utilities.Constants;
import ifisol.attendoplus.Utilities.HttpHelper;
import ifisol.attendoplus.Utilities.Utility;
import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.RequestBody;

public class ActivityLogin extends Activity implements View.OnClickListener, HttpHelper.HttpCallback {

    Context mContext;
    Activity mActivity;
    Globals mGlobal;

    private ImageView    imgClose;
    private FontEditText edEmail;
    private FontEditText edPassword;
    private FontButton   btnLogin;
    private View         viewNoAccount;
    FontTextView         tvForgotPass;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Init();
    }



    private void Init() {
        mContext  = this;
        mActivity = this;
        mGlobal   = (Globals) mActivity.getApplicationContext();
        imgClose        = (ImageView) findViewById(R.id.img_close);
        edEmail         = (FontEditText) findViewById(R.id.ed_email);
        edPassword      = (FontEditText) findViewById(R.id.ed_password);
        btnLogin        = (FontButton) findViewById(R.id.btn_login);
        viewNoAccount   = (View) findViewById(R.id.view_no_account);
        tvForgotPass    = (FontTextView) findViewById(R.id.tv_forgot_pass);
        btnLogin.setOnClickListener(this);
        viewNoAccount.setOnClickListener(this);
        tvForgotPass.setOnClickListener(this);
    }




    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btn_login:
                Login();
                break;
            case R.id.view_no_account:
                Utility.OpenActivity(mActivity,ActivitySignUp.class);
                break;
            case R.id.tv_forgot_pass:
                Utility.OpenActivity(mActivity,ActivityForgotPassword.class);
                break;
        }
    }




    private void Login() {

        if (edEmail.getText().toString().trim().isEmpty())
            Utility.ShowAlert(mActivity, "Email/UserName", "Please enter Email/UserName");
        else if (edPassword.getText().toString().trim().isEmpty())
            Utility.ShowAlert(mActivity, "Password", "Please enter password");
        else
        {
            RequestBody formBody = new FormBody.Builder()
                    .add("email", edEmail.getText().toString().trim())
                    .add("password", edPassword.getText().toString().trim())
                    .add("device_type", "Android")
                    .add("device_token", "").build();

            try {
                HttpHelper.CallApi(mActivity,Constants.Login, formBody, this, true);
            } catch (Exception e) {
                e.printStackTrace();
                mGlobal.mLoadingDia.dismiss();
            }

        }
    }

    @Override
    public void onFailure(Call call, final IOException e) {

        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                Toast.makeText(mContext,"Request Timeout", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public void onSuccess(Call call, String response) {


        ParseLoginRes(response);

    }




    private void ParseLoginRes(String s)
    {
        try {
            Log.d("Response........",s);
            final JSONObject jsonObj = new JSONObject(s);
            final String message = jsonObj.getString("message");

            if (jsonObj.getString("status").equals("true"))
            {
                Gson gson = new Gson();
                User user = gson.fromJson(jsonObj.getString("response"),User.class);
                ((Globals)mContext.getApplicationContext()).SaveActiveUser(user);
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Utility.OpenActivity(mActivity,ActivityMain.class);
                        finish();
                    }
                });

            }
            else
            {
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Utility.ShowAlert(mActivity,"Login Failed",message);
                    }
                });
            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
