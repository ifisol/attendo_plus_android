package ifisol.attendoplus.View.Activities;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabReselectListener;
import com.roughike.bottombar.OnTabSelectListener;

import java.util.TimeZone;

import fr.arnaudguyon.smartfontslib.FontTextView;
import ifisol.attendoplus.Global.Globals;
import ifisol.attendoplus.R;
import ifisol.attendoplus.Utilities.Constants;
import ifisol.attendoplus.Utilities.Utility;
import ifisol.attendoplus.View.Fragments.Analytic_MainFragment;
import ifisol.attendoplus.View.Fragments.Cpd_MainFragment;
import ifisol.attendoplus.View.Fragments.CreateEventFragment_3;
import ifisol.attendoplus.View.Fragments.CreateEventFragment_4;
import ifisol.attendoplus.View.Fragments.CreateEventMainFragment;
import ifisol.attendoplus.View.Fragments.EventQrScannerFragment;
import ifisol.attendoplus.View.Fragments.EventsPagerFragment;
import ifisol.attendoplus.View.Fragments.ExhibitorsFragment;
import ifisol.attendoplus.View.Fragments.LogBookMainFragment;
import ifisol.attendoplus.View.Fragments.NotificationsFragment;
import ifisol.attendoplus.View.Fragments.Notifications_MainFragment;
import ifisol.attendoplus.View.Fragments.OngoingEventsFragment;
import ifisol.attendoplus.View.Fragments.OrganisationsFragment;
import ifisol.attendoplus.View.Fragments.ProfileFragment;
import ifisol.attendoplus.View.Fragments.QRScannerFragment;

public class ActivityMain extends FragmentActivity implements OnTabSelectListener, View.OnClickListener, OnTabReselectListener {

    Context  mContext;
    Activity mActivity;
    Globals  mGlobal;

    private View         bottomMenu;
    private View         bottomSettings;
    private View         viewClose;
    private View         viewExibitors;
    private View         viewSettings;
    private View         viewOrganisation;
    private View         viewContac;
    public static BottomBar    bottomBarGuest;
    public static BottomBar    bottomBarOrg;
    private View         viewSettingsClose;
    private View         viewNotification;
    private View         viewProfile;
    private SwitchCompat imgNotification;
    private SwitchCompat imgLocation;
    FontTextView         tvLogout;

    public static FontTextView mTvABTitle;

    BottomSheetBehavior mBottomSheetMenuBehavior;
    BottomSheetBehavior mBottomSheetSettingsBehavior;
    String[] PERMISSIONS_CONTACT;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Init();

    }

    private void Init() {

        mContext  = this;
        mActivity = this;

        mGlobal   = (Globals) mActivity.getApplicationContext();

        mGlobal.timezone = TimeZone.getDefault().getID();

        bottomBarGuest    =  (BottomBar) findViewById(R.id.bottomBar_guest);
        bottomBarOrg      =  (BottomBar) findViewById(R.id.bottomBar_org);
        bottomMenu        =  findViewById(R.id.bottom_sheet );
        bottomSettings    =  findViewById(R.id.bottom_sheet_settings);
        viewClose         =  findViewById(R.id.view_close);
        viewExibitors     =  findViewById(R.id.view_exibitors);
        viewSettings      =  findViewById(R.id.view_settings);
        viewOrganisation  =  findViewById(R.id.view_organisation);
        viewContac        =  findViewById(R.id.view_contac);
        viewSettingsClose =  findViewById(R.id.view_settings_close);
        viewNotification  =  findViewById(R.id.view_notifications);
        viewProfile       =  findViewById(R.id.view_profile);
        imgNotification   =  (SwitchCompat) findViewById(R.id.img_notification);
        imgLocation       =  (SwitchCompat) findViewById(R.id.img_location);
        mTvABTitle        =  (FontTextView) findViewById(R.id.tv_actionbar_title);
        tvLogout          =  (FontTextView) findViewById(R.id.tv_logout);

        mBottomSheetMenuBehavior     = BottomSheetBehavior.from(bottomMenu);
        mBottomSheetSettingsBehavior = BottomSheetBehavior.from(bottomSettings);

        viewClose.setOnClickListener(this);
        viewExibitors.setOnClickListener(this);
        viewSettings.setOnClickListener(this);
        viewOrganisation.setOnClickListener(this);
        viewContac.setOnClickListener(this);
        imgNotification.setOnClickListener(this);
        imgLocation.setOnClickListener(this);
        viewSettingsClose.setOnClickListener(this);
        viewNotification.setOnClickListener(this);
        viewProfile.setOnClickListener(this);
        tvLogout.setOnClickListener(this);


        if (mGlobal.mActiveUser.getUser_type().equals(Constants.TAG_ORGANISER))
        {
            bottomBarGuest.setVisibility(View.GONE);
            bottomBarOrg.setOnTabSelectListener(this);
            bottomBarOrg.setOnTabReselectListener(this);
            bottomBarOrg.setVisibility(View.VISIBLE);
        }
        else
        {
            bottomBarOrg.setVisibility(View.GONE);
            bottomBarGuest.setOnTabSelectListener(this);
            bottomBarGuest.setOnTabReselectListener(this);
            bottomBarGuest.setVisibility(View.VISIBLE);
        }



        mBottomSheetMenuBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(View bottomSheet, int newState) {

            }

            @Override
            public void onSlide(View bottomSheet, float slideOffset) {

            }
        });

        mBottomSheetSettingsBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(View bottomSheet, int newState) {

            }

            @Override
            public void onSlide(View bottomSheet, float slideOffset) {

            }
        });

    }

    @Override
    public void onTabSelected(int tabId) {

        switch (tabId)
        {
            case R.id.tab_more_guest:

                mBottomSheetMenuBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

                break;

            case R.id.tab_more_org:

                mBottomSheetMenuBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                break;

            case R.id.tab_events_guest:

                Bundle bundle1 = new Bundle();
                bundle1.putInt("POSITION", 0);
                Utility.ReplaceFragment(new EventsPagerFragment(),getSupportFragmentManager(),bundle1);

                break;

            case R.id.tab_cpd:

                Bundle bundle = new Bundle();
                bundle.putInt("POSITION", 0);
                Utility.ReplaceFragment(new Cpd_MainFragment(),getSupportFragmentManager(),bundle);

                break;

            case R.id.tab_logbook:
                Bundle bundl = new Bundle();
                bundl.putInt("POSITION", 0);
                Utility.ReplaceFragment(new LogBookMainFragment(),getSupportFragmentManager(),bundl);

                break;

            case R.id.tab_qr:

                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                    checkCameraPermission();
                else
                    Utility.ReplaceFragment(new QRScannerFragment(),getSupportFragmentManager());

                break;

            case R.id.tab_events_org:
                Bundle bundle2 = new Bundle();
                bundle2.putInt("POSITION", 0);
                Utility.ReplaceFragment(new EventsPagerFragment(),getSupportFragmentManager(),bundle2);

                break;

            case R.id.tab_analytic:

                Utility.ReplaceFragment(new Analytic_MainFragment(),getSupportFragmentManager());

                break;

            case R.id.tab_add_event:

                Fragment fragment = new CreateEventMainFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.contentContainer, fragment ,fragment.getClass().getName()).addToBackStack("").commit();

                break;

            case R.id.tab_ongoing_events:

                    Utility.ReplaceFragment(new OngoingEventsFragment(),getSupportFragmentManager());

                break;
        }
    }


    @Override
    public void onBackPressed()

    {
        if (mBottomSheetMenuBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            mBottomSheetMenuBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }
        else if (mBottomSheetSettingsBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            mBottomSheetSettingsBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }
        else {

            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.contentContainer);

            if (fragment instanceof EventsPagerFragment || fragment instanceof Cpd_MainFragment ||
                    fragment instanceof LogBookMainFragment || fragment instanceof QRScannerFragment || fragment instanceof OrganisationsFragment
                     || fragment instanceof NotificationsFragment || fragment instanceof OngoingEventsFragment
                    || fragment instanceof Analytic_MainFragment|| fragment instanceof EventQrScannerFragment)
            {

            }
            else
            {
                if (fragment instanceof ProfileFragment)
                {
                    if (!((ProfileFragment)fragment).mProfileType.equals("OWN"))
                        getSupportFragmentManager().popBackStack();
                }
               else if (fragment instanceof CreateEventMainFragment)
                {
                    Fragment childFragment = ((CreateEventMainFragment)fragment).getChildFragmentManager().findFragmentById(R.id.create_event_container);

                   if (childFragment instanceof CreateEventFragment_3)
                    {
                        if (((CreateEventFragment_3) childFragment).isFullScreen)
                        {
                            CreateEventMainFragment.header.setVisibility(View.VISIBLE);
                            CreateEventMainFragment.viewSteps.setVisibility(View.VISIBLE);
                            bottomBarOrg.setVisibility(View.VISIBLE);
                            ((CreateEventFragment_3) childFragment).viewHeader.setVisibility(View.VISIBLE);
                            ((CreateEventFragment_3) childFragment).tvExitFull.setVisibility(View.GONE);

                            ((CreateEventFragment_3) childFragment).isFullScreen=false;
                        }

                    }
                    else if (childFragment instanceof CreateEventFragment_4)
                    {
                        if (((CreateEventFragment_4) childFragment).isFullScreen)
                        {
                            CreateEventMainFragment.header.setVisibility(View.VISIBLE);
                            CreateEventMainFragment.viewSteps.setVisibility(View.VISIBLE);
                            bottomBarOrg.setVisibility(View.VISIBLE);
                            ((CreateEventFragment_4) childFragment).viewHeader.setVisibility(View.VISIBLE);
                            ((CreateEventFragment_4) childFragment).tvExitFull.setVisibility(View.GONE);

                            ((CreateEventFragment_4) childFragment).isFullScreen=false;
                        }

                    }

                }
                else
                    getSupportFragmentManager().popBackStack();
            }


        }
    }


    @Override public boolean dispatchTouchEvent(MotionEvent event){
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            if (mBottomSheetMenuBehavior.getState()==BottomSheetBehavior.STATE_EXPANDED) {

                Rect outRect = new Rect();
                bottomMenu.getGlobalVisibleRect(outRect);

                if(!outRect.contains((int)event.getRawX(), (int)event.getRawY()))
                    mBottomSheetMenuBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
            else if (mBottomSheetSettingsBehavior.getState()==BottomSheetBehavior.STATE_EXPANDED)
            {
                Rect outRect = new Rect();
                bottomSettings.getGlobalVisibleRect(outRect);

                if(!outRect.contains((int)event.getRawX(), (int)event.getRawY()))
                    mBottomSheetSettingsBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }

        }

        return super.dispatchTouchEvent(event);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.view_close:

                mBottomSheetMenuBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

                break;


            case R.id.view_settings:

                mBottomSheetMenuBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                mBottomSheetSettingsBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

                break;

            case R.id.view_settings_close:

                mBottomSheetSettingsBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                mBottomSheetMenuBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

                break;
            case R.id.img_notification:

                if (imgNotification.isSelected())
                    imgNotification.setSelected(false);
                else
                    imgNotification.setSelected(true);

                break;

            case R.id.img_location:

                if (imgLocation.isSelected())
                    imgLocation.setSelected(false);
                else
                    imgLocation.setSelected(true);
                break;

            case R.id.view_organisation:

                mBottomSheetMenuBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                Utility.ReplaceFragment(new OrganisationsFragment(),getSupportFragmentManager());

                break;

            case R.id.view_notifications:

                mBottomSheetMenuBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                if (mGlobal.mActiveUser.getUser_type().equals(Constants.TAG_GUEST))
                {
                    Bundle bundle = new Bundle();
                    bundle.putString("NOTIFICATION_TYPE","Received");
                    Utility.ReplaceFragment(new NotificationsFragment(),getSupportFragmentManager(),bundle);
                }

                else
                {
                    Bundle bundle = new Bundle();
                    bundle.putInt("POSITION", 0);
                    Utility.ReplaceFragment(new Notifications_MainFragment(),getSupportFragmentManager(), bundle);
                }


                break;

            case R.id.view_profile:

                mBottomSheetMenuBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                Bundle bundle = new Bundle();
                bundle.putString("PROFILE_TYPE","OWN");
                Utility.ReplaceFragment(new ProfileFragment(),getSupportFragmentManager(),bundle);

                break;

            case R.id.view_exibitors:

                mBottomSheetMenuBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                Utility.ReplaceFragment(new ExhibitorsFragment()     ,getSupportFragmentManager());

                break;

            case R.id.tv_logout:

            mGlobal.SaveActiveUser(null);
                Utility.OpenActivity(mActivity,ActivityLogin.class);
                finishAffinity();

            break;

        }
    }

    @Override
    public void onTabReSelected(@IdRes int tabId) {

        switch (tabId) {
            case R.id.tab_more_guest:
                if (mBottomSheetMenuBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED)
                    mBottomSheetMenuBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                else
                   mBottomSheetMenuBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                break;
            case R.id.tab_more_org:
                if (mBottomSheetMenuBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED)
                    mBottomSheetMenuBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                else
                    mBottomSheetMenuBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                break;
            case R.id.tab_qr:

                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                    checkCameraPermission();
                else
                    Utility.ReplaceFragment(new QRScannerFragment(),getSupportFragmentManager());

                break;
        }
    }


    private void checkCameraPermission()
    {

        PERMISSIONS_CONTACT = new String[]{android.Manifest.permission.CAMERA};
        boolean is = false;

            for (String permission : PERMISSIONS_CONTACT)
            {
                if (!(mActivity.checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED))
                {
                    is = true;
                }
            }


           if (is)
            {
            requestPermissions(PERMISSIONS_CONTACT, 10);
            } else
            {
                Utility.ReplaceFragment(new QRScannerFragment(),getSupportFragmentManager());
            }

    }




    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    Utility.ReplaceFragment(new QRScannerFragment(),getSupportFragmentManager());
                }
            }, 200);
        } else if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
            Log.d("Permissions", "Permission Denied: " + permissions[0]);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (!shouldShowRequestPermissionRationale(permissions[0])) {
                    Utility.ShowAlert(mActivity,"Camera Permission", "You selected 'Never ask again' for camera permission but this permission is compulsory to scan Qr code. You can still allow permission from settings. Go to applications settings select Attendo and then select permissions");
                }
            }
        }
    }



}
