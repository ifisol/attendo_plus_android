package ifisol.attendoplus.View.Activities;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import org.json.JSONObject;

import java.io.IOException;

import fr.arnaudguyon.smartfontslib.FontButton;
import fr.arnaudguyon.smartfontslib.FontEditText;
import ifisol.attendoplus.Global.Globals;
import ifisol.attendoplus.R;
import ifisol.attendoplus.Utilities.Constants;
import ifisol.attendoplus.Utilities.HttpHelper;
import ifisol.attendoplus.Utilities.Utility;
import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.RequestBody;

public class ActivityChangePassword extends Activity implements View.OnClickListener, HttpHelper.HttpCallback {

    Context mContext;
    Activity mActivity;
    Globals mGlobal;

    private ImageView    imgClose;
    private FontEditText edCurrPass;
    private FontEditText edNewPass;
    private FontEditText edConfmNewPass;

    private FontButton   btnSubmit;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_pass);
        Init();
    }



    private void Init() {
        mContext        = this;
        mActivity       = this;
        mGlobal         = (Globals) mActivity.getApplicationContext();
        imgClose        = (ImageView) findViewById(R.id.img_close);
        edCurrPass      = (FontEditText) findViewById(R.id.ed_old_password);
        edNewPass       = (FontEditText) findViewById(R.id.ed_new_password);
        edConfmNewPass  = (FontEditText) findViewById(R.id.ed_confirm_password);
        btnSubmit       = (FontButton) findViewById(R.id.btn_submit);

        btnSubmit.setOnClickListener(this);
        imgClose.setOnClickListener(this);
    }




    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btn_login:
                ChangePassword();
                break;
            case R.id.img_close:
                finish();
                break;
        }
    }




    private void ChangePassword() {

        if (edCurrPass.getText().toString().trim().isEmpty())
            Utility.ShowAlert(mActivity, "Current Passwor", "Please enter your current password");
        else if (edNewPass.getText().toString().trim().isEmpty())
            Utility.ShowAlert(mActivity, "New Password", "Please enter new password");
        else if (edConfmNewPass.getText().toString().trim().isEmpty())
            Utility.ShowAlert(mActivity, "Confirm Password", "Please confirm your new password");
        else if (!edConfmNewPass.getText().toString().trim().equals(edNewPass.getText().toString().trim()))
            Utility.ShowAlert(mActivity, "Password Not Matched", "Password did not match");
        else if (!mGlobal.mActiveUser.getPassword().equals(Utility.getMD5(edCurrPass.getText().toString().trim())))
            Utility.ShowAlert(mActivity, "Wrong Current Password", "Please provide correct current password");
        else
        {
            RequestBody formBody = new FormBody.Builder()
                    .add("user_id", mGlobal.mActiveUser.getUser_id())
                    .add("old_password", edCurrPass.getText().toString().trim())
                    .add("new_password", edNewPass.getText().toString().trim())
                    .build();

            try {
                HttpHelper.CallApi(mActivity,Constants.ChangePassword, formBody, this, true);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void onFailure(Call call, IOException e) {

        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

            }
        });

    }

    @Override
    public void onSuccess(Call call, String response) {

        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
            }
        });

        ParseRes(response);

    }

    private void ParseRes(String s) {

        try {
            Log.d("Response........", s);
            final JSONObject jsonObj = new JSONObject(s);
            final String message = jsonObj.getString("message");
            if (jsonObj.getString("status").equals("true")) {
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        Utility.ShowAlert(mActivity, "Password Changed", message);
                        finish();
                    }
                });
            } else {
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        Utility.ShowAlert(mActivity, "Failed", message);
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
