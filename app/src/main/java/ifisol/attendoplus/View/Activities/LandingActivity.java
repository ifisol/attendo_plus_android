package ifisol.attendoplus.View.Activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import fr.arnaudguyon.smartfontslib.FontButton;
import ifisol.attendoplus.Global.Globals;
import ifisol.attendoplus.R;
import ifisol.attendoplus.Utilities.Utility;

public class LandingActivity extends Activity implements View.OnClickListener{

    Context  mContext;
    Activity mActivity;
    Globals  mGlobal;

    private FontButton btnLogin;
    private FontButton btnSignup;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing);

        Init();

        if (mGlobal.mActiveUser!=null)
        {
            Utility.OpenActivity(mActivity,ActivityMain.class);
            finish();
        }

        btnLogin.setOnClickListener(this);
        btnSignup.setOnClickListener(this);

    }

    private void Init() {

        mContext  = this;
        mActivity = this;
        mGlobal   = (Globals) mActivity.getApplicationContext();

        btnLogin = (FontButton) findViewById(R.id.btn_login);
        btnSignup = (FontButton) findViewById(R.id.btn_signup);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.btn_login:
                Intent in = new Intent(v.getContext(),ActivityLogin.class);
                startActivity(in);

                break;
            case R.id.btn_signup:
                Intent intnt = new Intent(v.getContext(),ActivitySignUp.class);
                startActivity(intnt);
                break;
        }
    }

}

