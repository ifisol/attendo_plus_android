package ifisol.attendoplus.View.Activities;

import android.app.Activity;
import android.app.usage.UsageEvents;
import android.content.Context;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.nostra13.universalimageloader.core.ImageLoader;

import fr.arnaudguyon.smartfontslib.FontTextView;
import ifisol.attendoplus.Global.Globals;
import ifisol.attendoplus.Model.Events;
import ifisol.attendoplus.R;
import ifisol.attendoplus.Utilities.Constants;
import ifisol.attendoplus.Utilities.Utility;

public class FeedbackActivity extends Activity implements View.OnClickListener{


    Activity mActivity;
    Globals  mGlobal;
    CircularImageView eventImg;
    FontTextView      tvName;
    FontTextView      tvTime;
    View feedbackView;
    View feedDialog;

    boolean isToReschedule=true;
    Context mContext;
    private WebView wvFdbck;
    Events event;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.give_feedback_activity);
        this.setFinishOnTouchOutside(false);

        mContext=this;
        mActivity=this;
        mGlobal = (Globals) this.getApplicationContext();

        event = (Events) getIntent().getSerializableExtra("EVENT");

        wvFdbck = (WebView) findViewById(R.id.wbv_feedback);
        wvFdbck.getSettings().setJavaScriptEnabled(true);
        wvFdbck.loadUrl(Constants.Base_URL+event.getFeedback_url()+getIntent().getStringExtra("USER_ID"));


        try {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }

        TextView mTvClose = (TextView)findViewById(R.id.tv_close);
        TextView mTvFeedBack = (TextView) findViewById(R.id.tv_feedback);
        TextView mTvLater = (TextView) findViewById(R.id.tv_later);
        TextView mTvName = (TextView) findViewById(R.id.txt_name);
        eventImg = (CircularImageView) findViewById(R.id.img_event);
        tvName = (FontTextView) findViewById(R.id.tv_event_name);
        tvTime = (FontTextView) findViewById(R.id.tv_time);
        feedbackView = findViewById(R.id.view_feedback);
        feedDialog = findViewById(R.id.feedback_dialog);



        mTvClose.setOnClickListener(this);
        mTvFeedBack.setOnClickListener(this);
        mTvLater.setOnClickListener(this);

        tvName.setText(event.getEvent_name());
        tvTime.setText(event.getEvent_start_time()+" - "+event.getEvent_end_time());
        ImageLoader.getInstance().displayImage(Constants.Img_Base_URL+event.getEvent_photo(),eventImg);

        mTvName.setText(event.getEvent_name());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.tv_close:
                Utility.CancelNotification();
                isToReschedule=false;
                finish();
                break;
            case R.id.tv_feedback:
                isToReschedule=false;
                feedbackView.setVisibility(View.VISIBLE);
                feedDialog.setVisibility(View.GONE);
                break;
            case R.id.tv_later:
                Utility.ScheduleNotification(mContext,1800000,event);
                isToReschedule=false;
                finish();
                break;
        }

    }

    @Override
    protected void onDestroy() {

        super.onDestroy();
        if (isToReschedule)
            Utility.ScheduleNotification(mContext,1800000,event);
    }



    @Override
    public void onBackPressed() {
       finish();
    }
}

