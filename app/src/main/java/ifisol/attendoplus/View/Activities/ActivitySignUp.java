package ifisol.attendoplus.View.Activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.gson.Gson;
import com.sandrios.sandriosCamera.internal.SandriosCamera;
import com.sandrios.sandriosCamera.internal.configuration.CameraConfiguration;
import com.yalantis.taurus.PullToRefreshView;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import co.lujun.androidtagview.TagContainerLayout;
import fr.arnaudguyon.smartfontslib.FontButton;
import fr.arnaudguyon.smartfontslib.FontCheckBox;
import fr.arnaudguyon.smartfontslib.FontEditText;
import fr.arnaudguyon.smartfontslib.FontRadioButton;
import fr.arnaudguyon.smartfontslib.FontTextView;
import ifisol.attendoplus.Controller.Adapters.OrganisationsAdapter;
import ifisol.attendoplus.Global.Globals;
import ifisol.attendoplus.Model.Organisations;
import ifisol.attendoplus.Model.User;
import ifisol.attendoplus.R;
import ifisol.attendoplus.Utilities.Constants;
import ifisol.attendoplus.Utilities.HttpHelper;
import ifisol.attendoplus.Utilities.ResizeImage;
import ifisol.attendoplus.Utilities.Utility;
import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.RequestBody;

public class ActivitySignUp extends Activity implements View.OnClickListener, HttpHelper.HttpCallback {

    Context mContext;
    Activity mActivity;
    Globals mGlobal;

    private ImageView imgClose;
    private FontTextView tvSignup;
    private ImageView imgSelectImg;
    private CircularImageView imgUser;
    private FontEditText edUserName;
    private FontEditText edFirstName;
    private FontEditText edProfession;
    private FontEditText edLastName;
    private FontEditText edEmail;
    private FontEditText edPassword;
    private FontEditText edConfirmPassword;
    private FontEditText edAddress;
    private RadioGroup   userTypeRGroup;
    private FontRadioButton rGuest;
    private FontRadioButton rOrg;
    private FontTextView tvOrganisation;
    private FontEditText edPhone;
    private FontCheckBox chTerms;
    private FontButton btnSignup;
    private FontTextView tvLogin;
    private ImageView imgInfo;
    private FontTextView tvSecOrg;

    ArrayList<Organisations> mOrgList;
    public Dialog dialog;

    private static final int CAPTURE_MEDIA = 368;

    public String OrganisationId="";
    String UserType="";
    Bitmap UserImage;
    String imgBase64;
    boolean pullRef;
    OrganisationsAdapter org_adapter;
    PullToRefreshView mPullToRefreshView;
    TagContainerLayout mTagContainerLayout;
    private View viewSecOrg;
    StringBuilder secOrgIds;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        Init();

        FetchOrganisations(false);
        pullRef=false;


        rGuest.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked)
                {
                    rOrg.setChecked(false);
                    UserType=Constants.TAG_GUEST;
                    viewSecOrg.setVisibility(View.VISIBLE);
                }
            }
        });


        rOrg.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked)
                {
                    rGuest.setChecked(false);
                    UserType=Constants.TAG_ORGANISER;
                    viewSecOrg.setVisibility(View.GONE);
                }
            }
        });

    }





    private void FetchOrganisations(boolean loader) {
        try {
            RequestBody formBody = new FormBody.Builder().add("","").build();
            HttpHelper.CallApi(mActivity,Constants.Fetch_Organisations,formBody,this,loader);
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("Exception...........",e.getMessage());
        }
    }







    private void Init() {
        mContext  = this;
        mActivity = this;
        mGlobal   = (Globals) mActivity.getApplicationContext();

        imgClose          = (ImageView) findViewById(R.id.img_close);
        tvSignup          = (FontTextView) findViewById(R.id.tv_signup);
        imgSelectImg      = (ImageView) findViewById(R.id.img_select_img);
        imgUser           = (CircularImageView) findViewById(R.id.img_user);
        edUserName        = (FontEditText) findViewById(R.id.ed_user_name);
        edFirstName       = (FontEditText) findViewById(R.id.ed_first_name);
        edLastName        = (FontEditText) findViewById(R.id.ed_last_name);
        edAddress         = (FontEditText) findViewById(R.id.ed_address);
        edEmail           = (FontEditText) findViewById(R.id.ed_email);
        edPassword        = (FontEditText) findViewById(R.id.ed_password);
        edConfirmPassword = (FontEditText) findViewById(R.id.ed_confirm_password);
        edProfession      = (FontEditText) findViewById(R.id.ed_profession);
        userTypeRGroup    = (RadioGroup) findViewById(R.id.user_type_r_group);
        rGuest            = (FontRadioButton) findViewById(R.id.r_guest);
        rOrg              = (FontRadioButton) findViewById(R.id.r_org);
        tvOrganisation    = (FontTextView) findViewById(R.id.tv_organisation);
        edPhone           = (FontEditText) findViewById(R.id.ed_phone);
        chTerms           = (FontCheckBox) findViewById(R.id.ch_terms);
        btnSignup         = (FontButton) findViewById(R.id.btn_signup);
        tvLogin           = (FontTextView) findViewById(R.id.tv_login);
        imgInfo           = (ImageView) findViewById(R.id.img_info);
        tvSecOrg          = (FontTextView) findViewById(R.id.tv_sec_organisations);
        viewSecOrg        = findViewById(R.id.view_secondary);
        mTagContainerLayout = (TagContainerLayout) findViewById(R.id.tag_layout);
     //  mTagContainerLayout.setTags();

        tvOrganisation.setOnClickListener(this);
        imgSelectImg.setOnClickListener(this);
        btnSignup.setOnClickListener(this);
        tvLogin.setOnClickListener(this);
        imgClose.setOnClickListener(this);
        imgInfo.setOnClickListener(this);
        tvSecOrg.setOnClickListener(this);
    }



    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.img_select_img:
                launchCamera();
                break;
            case R.id.tv_organisation:
                if (mOrgList==null || mOrgList.size()==0)
                    Utility.ShowAlert(mActivity,"Organisations", "Organisations are not available. Please try again later. Thanks");
                else
                   ShowOrganisations("primary");
                break;
            case R.id.tv_login:
                finish();
                break;
            case R.id.img_close:
                finish();
                break;
            case R.id.btn_signup:
                SignUp();
                break;
            case R.id.img_info:
                Utility.ShowInfo(mActivity,"User Type","Guest","Register as a Guest with an Organisation to see upcoming events of your Organisations. Attend events, check your CPD points, Create your own Journals and many more","Organizer","Register as an Organizer to create your own events on the go. See who is interested in your event and who attended your event. You can check your ongoing events and the peoples joining or attending them");
                break;
            case R.id.tv_sec_organisations:
                if (mOrgList==null || mOrgList.size()==0)
                    Utility.ShowAlert(mActivity,"Organisations", "Organisations are not available. Please try again later. Thanks");
                else if (OrganisationId.equals(""))
                        Utility.ShowAlert(mActivity,"Primary Organisations", "Please select primary organisation first. Thanks");
                else
                    ShowOrganisations("secondary");
                break;
        }

    }



    public void ShowOrganisations(String organisationType) {
        dialog = new Dialog(mActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        dialog.setContentView(R.layout.organisations_fragment);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        DisplayMetrics dm = new DisplayMetrics();
        mActivity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER | Gravity.START);
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        //lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        RecyclerView mOrg_rv;
        FontTextView doneTv = (FontTextView) dialog.findViewById(R.id.tv_done);
        if (organisationType.equals("secondary"))
            doneTv.setVisibility(View.VISIBLE);
        else
            doneTv.setVisibility(View.GONE);
        mPullToRefreshView = (PullToRefreshView) dialog.findViewById(R.id.pull_to_refresh);
        FloatingActionButton fab_add   = (FloatingActionButton) dialog.findViewById(R.id.fab_add);
        mOrg_rv = (RecyclerView) dialog.findViewById(R.id.rv_organisations);
        mOrg_rv.setHasFixedSize(true);
        mOrg_rv.setLayoutManager(new LinearLayoutManager(mContext));
        org_adapter = new OrganisationsAdapter(mActivity,null,mOrgList,organisationType);
        mOrg_rv.setAdapter(org_adapter);

        org_adapter.setOnClickListener(new OrganisationsAdapter.OnItemClick() {
            @Override
            public void onItemClicked(Organisations obj) {

                OrganisationId = obj.getOrganisation_id();
                tvOrganisation.setText(obj.getOrganisation_name());
                tvOrganisation.setTextColor(getResources().getColor(R.color.black));
                dialog.dismiss();
            }
        });
        doneTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String sep = "";
                secOrgIds = new StringBuilder();
                List<String> mTagList = new ArrayList<String>();

                for (Organisations org : org_adapter.mFilterArray )
                {
                    if (org.isSelected() && org.getOrganisation_id()!=OrganisationId)
                    {
                        mTagList.add(org.getOrganisation_name());
                        secOrgIds.append(sep);
                        secOrgIds.append(org.getOrganisation_id());
                        sep = ",";
                    }

                }
                mTagContainerLayout.setTags(mTagList);
                dialog.dismiss();
            }

        });

        fab_add.setVisibility(View.GONE);
        fab_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CreateOrganisations();
            }
        });


        FontEditText mTxtSearch = (FontEditText) dialog.findViewById(R.id.ed_search);
        mTxtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                org_adapter.FilterList(s.toString());

            }
        });








        mPullToRefreshView.setOnRefreshListener(new PullToRefreshView.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPullToRefreshView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        FetchOrganisations(false);
                        pullRef=true;
                    }
                },500);
            }
        });


        mOrg_rv.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                super.onScrolled(recyclerView, dx, dy);

                if (dy<=0)
                    mPullToRefreshView.setEnabled(true);
                else
                    mPullToRefreshView.setEnabled(false);
            }
        });








        dialog.show();
    }



    public void CreateOrganisations() {

        dialog = new Dialog(mActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        dialog.setContentView(R.layout.create_organisation_fragment);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        DisplayMetrics dm = new DisplayMetrics();
        mActivity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER | Gravity.START);
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        window.setAttributes(lp);

        dialog.show();
    }



    @Override
    public void onFailure(Call call, final IOException e) {

 //       e.printStackTrace();

        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                Toast.makeText(mContext,"Request Timeout", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public void onSuccess(Call call, String response) {

        switch (call.request().url().toString())
        {
            case Constants.Fetch_Organisations:
            ParseOrganisations(response);
            break;
            case Constants.Register:
            ParseRegisterRes(response);
            break;
        }

    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CAPTURE_MEDIA && resultCode == RESULT_OK) {
            try {

                ResizeImage resizeImage = new ResizeImage(mContext);
                String uri = resizeImage.compressImage(data.getStringExtra(CameraConfiguration.Arguments.FILE_PATH));
                File imgFile = new File(uri);
                if (imgFile.exists()) {
                    UserImage = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                    imgUser.setImageBitmap(UserImage);
                    imgBase64 = Utility.EncodeTobase64(UserImage);
                }


                Log.e("File >>>>>>>>>>>>>", "" + data.getStringExtra(CameraConfiguration.Arguments.FILE_PATH));
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }



    private void ParseOrganisations(String s)
    {
        try {
            JSONObject jsonObj = new JSONObject(s);
            Gson gson = new Gson();
            Organisations[] mOrgs = gson.fromJson(jsonObj.getString("response"),Organisations[].class);
            mOrgList = new ArrayList<>(Arrays.asList(mOrgs));
            if (pullRef)
            {
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mPullToRefreshView.setRefreshing(false);
                        if (org_adapter!=null)
                        {
                            if (mOrgList==null || mOrgList.size()==0)
                                Utility.ShowAlert(mActivity,"Organisations", "No organisations found. Please try again later. Thanks");
                            else
                            {org_adapter.mFilterArray.clear();
                                org_adapter.mOriginalArray.clear();
                                org_adapter.mFilterArray=mOrgList;
                                org_adapter.mOriginalArray=mOrgList;
                                org_adapter.notifyDataSetChanged();}

                        }
                    }
                });
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }


    private void ParseRegisterRes(String s)
    {

        try {
            Log.d("Response........",s);
            final JSONObject jsonObj = new JSONObject(s);
            final String message = jsonObj.getString("message");
            final String status  = jsonObj.getString("status");

            if (status.equals("true"))
            {
                if (UserType.equals(Constants.TAG_GUEST))
                {
                    Gson gson = new Gson();
                    User user = gson.fromJson(jsonObj.getString("response"),User.class);
                    ((Globals)mContext.getApplicationContext()).SaveActiveUser(user);
                    mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            Utility.OpenActivity(mActivity,ActivityMain.class);
                            finish();
                        }
                    });
                }
                else
                {
                    mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ShowRegMsg("Organiser Registration",message);
                        }
                    });
                }
            }
            else
            {
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Utility.ShowAlert(mActivity,"Signup Failed",message);
                    }
                });
            }


        }
        catch (Exception e)
        {
          e.printStackTrace();
        }
    }



    private void launchCamera() {
        new SandriosCamera(mActivity,null, CAPTURE_MEDIA)
                .setShowPicker(true)
                //.setVideoFileSize(15) //File Size in MB: Default is no limit
                .setMediaAction(CameraConfiguration.MEDIA_ACTION_PHOTO) // default is CameraConfiguration.MEDIA_ACTION_BOTH
                .enableImageCropping(true) // Default is false.
                .launchCamera();
    }



    private void SignUp() {
        if (edUserName.getText().toString().trim().isEmpty())
            Utility.ShowAlert(mActivity, "User Name", "Please enter User Name");
        else if (edFirstName.getText().toString().trim().isEmpty())
            Utility.ShowAlert(mActivity, "First Name", "Please enter First Name");
        else if (edEmail.getText().toString().trim().isEmpty())
            Utility.ShowAlert(mActivity, "Email", "Please enter Email");
        else if (!Utility.EmailValidator(edEmail.getText().toString().trim()))
            Utility.ShowAlert(mActivity, "Invalid Email", "Please enter a valid Email address");
        else if (edPassword.getText().toString().trim().isEmpty())
            Utility.ShowAlert(mActivity, "Password", "Please enter password");
        else if (edConfirmPassword.getText().toString().trim().isEmpty())
            Utility.ShowAlert(mActivity, "Confirm Password", "Please confirm your password");
        else if (!edConfirmPassword.getText().toString().trim().equals(edPassword.getText().toString().trim()))
            Utility.ShowAlert(mActivity, "Password Not Matched", "Your password did not match");
        else if (!rGuest.isChecked() && !rOrg.isChecked())
            Utility.ShowAlert(mActivity, "User Type", "Please select a User Type");
        else if (OrganisationId.equals(""))
            Utility.ShowAlert(mActivity, "Organisation", "Please select an Organisation");
        else if (edProfession.getText().toString().trim().isEmpty())
            Utility.ShowAlert(mActivity, "Job Tiltle", "Please enter enter Job Title");
        else if (edAddress.getText().toString().trim().isEmpty())
            Utility.ShowAlert(mActivity, "Address", "Please enter your address");
        else if (edPhone.getText().toString().trim().isEmpty())
            Utility.ShowAlert(mActivity, "Phone Number", "Please enter Phone Number");
        else if (!chTerms.isChecked())
            Utility.ShowAlert(mActivity, "Terms and conditions", "To join Attendo + you must accept terms and conditions");
        else {
            try {
                FormBody.Builder body = new FormBody.Builder();

                        body.add("first_name", edFirstName.getText().toString().trim());
                        body.add("last_name", edLastName.getText().toString().trim());
                        body.add("user_name", edUserName.getText().toString().trim());
                        body.add("email", edEmail.getText().toString().trim());
                        body.add("password", edPassword.getText().toString().trim());
                        body.add("profession", edProfession.getText().toString().trim());
                        body.add("address", edAddress.getText().toString().trim());
                        body.add("phone", edPhone.getText().toString().trim());
                        body.add("user_type", UserType);
                        body.add("organisation_id", OrganisationId);
                        body.add("device_type", "Android");
                        body.add("device_token", "");
                        body.add("picture",imgBase64==null?"":imgBase64);
                        if (UserType.equals(Constants.TAG_GUEST))
                        {
                           if(secOrgIds!=null)
                              body.add("seconday_organisations", secOrgIds.toString());
                            else
                              body.add("seconday_organisations","");
                        }



                RequestBody formBody = body.build();
            HttpHelper.CallApi( mActivity,Constants.Register, formBody, this, true);
            } catch (Exception e) {
                e.printStackTrace();
                Log.d("Exception...........", e.getMessage());
            }
        }
    }

    public void ShowRegMsg(String title, String msg) {
        final Dialog dialog = new Dialog(mActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.alert);
        DisplayMetrics dm = new DisplayMetrics();
        mActivity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER | Gravity.START);
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        TextView header = (TextView) dialog.findViewById(R.id.tv_header);
        TextView message = (TextView) dialog.findViewById(R.id.tv_msg);
        header.setText(title);
        message.setText(msg);
        Button btnOK = (Button) dialog.findViewById(R.id.btn_ok);
        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dialog.dismiss();
                mActivity.finish();
            }
        });

        dialog.show();
    }
}



