package ifisol.attendoplus.View.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import fr.arnaudguyon.smartfontslib.FontTextView;
import ifisol.attendoplus.Controller.Adapters.Analytics_ExVisitorAdapter;
import ifisol.attendoplus.Global.Globals;
import ifisol.attendoplus.Model.Exhibitors;
import ifisol.attendoplus.Model.User;
import ifisol.attendoplus.R;
import ifisol.attendoplus.Utilities.Constants;
import ifisol.attendoplus.Utilities.HttpHelper;
import ifisol.attendoplus.Utilities.Utility;
import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.RequestBody;

public class Analytics_ExhibitorDetailFragment extends Fragment implements HttpHelper.HttpCallback, View.OnClickListener {


    FragmentActivity mActivity;
    Context  mContext;
    Globals  mGlobal;



    private ImageView imgBack;
    private ImageView imgNoRecord;

    private CircularImageView imgUser;
    private FontTextView      tvUsername;
    private FontTextView      tvEmail;
    private FontTextView      tvPhone;
    private FontTextView tvCount;
    private ListView visitorList;

    Exhibitors exhibitor;
    String EventID;
    ArrayList<User> mVisitorList;
    Analytics_ExVisitorAdapter adapter;
    

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return Init(inflater.inflate(R.layout.analytics_exhibitors_detail_fragment, null));
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        exhibitor = (Exhibitors) getArguments().getSerializable("EXHIBITOR");
        EventID     = getArguments().getString("EVENT_ID");

        ImageLoader.getInstance().displayImage(Constants.Img_Base_URL+exhibitor.getExhibitor_picture(),imgUser);
        tvUsername.setText(exhibitor.getExhibitor_name());
        tvEmail.setText(exhibitor.getExhibitor_email());
        tvPhone.setText(exhibitor.getExhibitor_phone());

        imgBack.setOnClickListener(this);

        FetchVisitors();

    }

    private View Init(View view)
    {

        mActivity = getActivity();
        mContext  = getActivity();
        mGlobal   = (Globals) mActivity.getApplicationContext();

        imgBack = (ImageView) view.findViewById(R.id.img_back);
        imgNoRecord = (ImageView) view.findViewById(R.id.img_no_record);
        imgUser = (CircularImageView)     view.findViewById(R.id.img_user);
        tvUsername = (FontTextView)       view.findViewById(R.id.tv_username);
        tvEmail = (FontTextView)          view.findViewById(R.id.tv_email);
        tvPhone = (FontTextView)          view.findViewById(R.id.tv_phone);
        tvCount = (FontTextView)          view.findViewById(R.id.tv_count);
        visitorList = (ListView)view.findViewById(R.id.visitors_list);

        return view;
    }






    private void SetAdapter(ArrayList<User> mVisitorList) {
        if (mVisitorList == null || mVisitorList.size() == 0)
        {
            imgNoRecord.setVisibility(View.VISIBLE);
            tvCount.setText("0");
        }
        else {
            imgNoRecord.setVisibility(View.GONE);
            tvCount.setText(String.valueOf(mVisitorList.size()));
            adapter = new Analytics_ExVisitorAdapter(mContext, mActivity, mVisitorList);
            visitorList.setAdapter(adapter);

        }


        visitorList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {

                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        User obj = (User) adapter.getItem(position);
                        if (obj.getProfile_visibility_status()!=null && obj.getProfile_visibility_status().equals("public"))
                        {
                            Bundle bundle = new Bundle();
                            bundle.putString("USER_ID", obj.getUser_id());
                            bundle.putString("PROFILE_TYPE","OTHER");
                            Utility.ReplaceFragment(new ProfileFragment(),mActivity.getSupportFragmentManager(),bundle);
                        }
                        else
                            Utility.ShowAlert(mActivity,"Private Profile","This user has a private profile. You cannot view private profiles");
                    }
                });
            }
        });

    }






    private void FetchVisitors() {
        RequestBody formBody = new FormBody.Builder()
                .add("user_id",mGlobal.mActiveUser.getUser_id())
                .add("event_id", EventID)
                .add("exhibitor_id", exhibitor.getExhibitor_id())
                .build();
        /*if (mGlobal.mActiveUser.getUser_type().equals(Constants.TAG_GUEST)) */try {
            HttpHelper.CallApi(mActivity, Constants.Get_exhibitor_visitors,formBody,this,true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }









    @Override
    public void onFailure(Call call, IOException e) {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                SetAdapter(null);
            }
        });
    }

    @Override
    public void onSuccess(Call call, String response) {
        try {
            Log.d("Response . . . . . .", response);
            Gson gson = new Gson();
            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.getString("status").equals("true"))
            {
                final User[] users = gson.fromJson(jsonObject.getString("response"), User[].class);
                mVisitorList = new ArrayList<>(Arrays.asList(users));
            }

            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    SetAdapter(mVisitorList);
                }
            });


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {

        if (v==imgBack)
            mActivity.getSupportFragmentManager().popBackStack();
    }
}
