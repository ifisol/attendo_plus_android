package ifisol.attendoplus.View.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import fr.arnaudguyon.smartfontslib.FontTextView;
import ifisol.attendoplus.Global.Globals;
import ifisol.attendoplus.Model.Events;
import ifisol.attendoplus.R;
import ifisol.attendoplus.Utilities.Constants;
import ifisol.attendoplus.Utilities.CreateEventStepsListener;
import ifisol.attendoplus.Utilities.HttpHelper;
import ifisol.attendoplus.Utilities.Utility;
import ifisol.attendoplus.View.Activities.ActivityMain;
import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.RequestBody;

public class CreateEventFragment_4 extends Fragment implements View.OnClickListener, HttpHelper.HttpCallback {

    FragmentActivity mActivity;
    Context  mContext;
    Globals mGlobal;

    CreateEventStepsListener stepsListener;

    public FontTextView btnSkip;
    public FontTextView tvExitFull;
    public View viewHeader;
    private FontTextView tvViewFull;
    private WebView wvFeedback;

    public boolean isFullScreen;

    ImageView imgCreateFeedBack;
    ImageView imgSkip;
    private View viewOptions;
    String eventID;
    String status;
    boolean editing;
    Events event;

    private TextView btnNext;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.create_event_fragment_4, null);

        return Init(v);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        stepsListener = (CreateEventStepsListener) getParentFragment();
        stepsListener.UpdateSteps(3);

        event = (Events) getArguments().getSerializable("EVENT");
        eventID = event.getEvent_id();
        editing = getArguments().getBoolean("EDITING");





        wvFeedback.setWebViewClient(new WebViewClient() {
    //        ProgressDialog progressDialog;

            //If you will not use this method url links are opeen in new brower not in webview
            public boolean shouldOverrideUrlLoading(WebView view, String url) {

                view.loadUrl(url);
                return true;
            }

            public void onPageFinished(WebView view, String url) {

            //    try {
         //           if (progressDialog.isShowing()) {
         //               progressDialog.dismiss();
         //               progressDialog = null;
         //           }
          //      } catch (Exception exception) {
          //          exception.printStackTrace();
         //       }
            }

            //Show loader on url load
            public void onLoadResource(WebView view, String url) {

               /* if (progressDialog == null) {
                    // in standard case YourActivity.this
                    progressDialog = new ProgressDialog(mContext);
                    progressDialog.setMessage("Loading...");
                    progressDialog.show();
                }*/
            }

        });
        wvFeedback.getSettings().setJavaScriptEnabled(true);
        btnSkip.setOnClickListener(this);
        tvViewFull.setOnClickListener(this);
        tvExitFull.setOnClickListener(this);
        tvExitFull.setOnClickListener(this);
        imgSkip.setOnClickListener(this);
        btnNext.setOnClickListener(this);
        imgCreateFeedBack.setOnClickListener(this);
        CreateEventMainFragment.imgBack.setOnClickListener(this);

        if (editing)
        {
            if (event.getHas_feedback().equals("yes"))
            {
                wvFeedback.setVisibility(View.VISIBLE);
                viewOptions.setVisibility(View.GONE);
                tvViewFull.setVisibility(View.VISIBLE);
                btnSkip.setVisibility(View.GONE);
                btnNext.setVisibility(View.VISIBLE);
                wvFeedback.loadUrl(Constants.Base_URL+event.getFeedback_url());
            }
            else
            {
                viewOptions.setVisibility(View.VISIBLE);
                btnSkip.setVisibility(View.GONE);
                btnNext.setVisibility(View.GONE);
            }

        }

    }

    private View Init(View v)
    {
        mActivity = getActivity();
        mContext  = getActivity();
        mGlobal = (Globals) mActivity.getApplicationContext();

        btnSkip       = (FontTextView) v.findViewById(R.id.btn_skip);
        wvFeedback     = (WebView) v.findViewById(R.id.wv_logbook);
        tvExitFull = (FontTextView) v.findViewById(R.id.tv_exit_full);
        viewHeader = (RelativeLayout) v.findViewById(R.id.view_header);
        tvViewFull = (FontTextView) v.findViewById(R.id.tv_view_full);
        imgCreateFeedBack = (ImageView) v.findViewById(R.id.img_create_feedback);
        imgSkip = (ImageView) v.findViewById(R.id.img_skip);
        btnNext       = (TextView) v.findViewById(R.id.btn_next);
        viewOptions = v.findViewById(R.id.view_options);
        final JavaScriptInterface myJavaScriptInterface = new JavaScriptInterface(mContext);
        wvFeedback.getSettings().setLightTouchEnabled(true);
        wvFeedback.getSettings().setJavaScriptEnabled(true);
        wvFeedback.addJavascriptInterface(myJavaScriptInterface, "AppFunction");

        return v;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.btn_skip:
                status = "skip";
                AddSkipFeedback();
                break;
            case R.id.tv_view_full:
                ShowFullScreen(true);
                break;
            case R.id.tv_exit_full:
                ShowFullScreen(false);
                break;
            case R.id.btn_next:
                Bundle bundle = new Bundle();
                bundle.putSerializable("EVENT", event);
                bundle.putBoolean("EDITING", editing);
                Utility.ReplaceFragment_create_event(new CreateEventFragment_5(),getFragmentManager(),bundle);
                break;
            case R.id.img_skip:
                status = "skip";
                AddSkipFeedback();
                event.setHas_feedback("no");
                break;
            case R.id.img_create_feedback:
                wvFeedback.setVisibility(View.VISIBLE);
                viewOptions.setVisibility(View.GONE);
                tvViewFull.setVisibility(View.VISIBLE);
                status = "add";
                AddSkipFeedback();
                event.setHas_feedback("yes");
                btnSkip.setVisibility(View.VISIBLE);
                break;
            case R.id.img_back:
                Bundle bundle1 = new Bundle();
                bundle1.putSerializable("EVENT", event);
                bundle1.putBoolean("EDITING", true);
                Utility.ReplaceFragment_create_event(new CreateEventFragment_3(),getFragmentManager(),bundle1);
                break;
        }
    }

    private void ShowFullScreen (boolean isFulscreen) {

        isFullScreen = isFulscreen;

        if (isFulscreen)
        {
            CreateEventMainFragment.header.setVisibility(View.GONE);
            CreateEventMainFragment.viewSteps.setVisibility(View.GONE);
            ActivityMain.bottomBarOrg.setVisibility(View.GONE);
            viewHeader.setVisibility(View.GONE);
            btnSkip.setVisibility(View.GONE);
            tvExitFull.setVisibility(View.VISIBLE);

        }
        else
        {

            CreateEventMainFragment.header.setVisibility(View.VISIBLE);
            CreateEventMainFragment.viewSteps.setVisibility(View.VISIBLE);
            ActivityMain.bottomBarOrg.setVisibility(View.VISIBLE);
            viewHeader.setVisibility(View.VISIBLE);
            btnSkip.setVisibility(View.VISIBLE);
            tvExitFull.setVisibility(View.GONE);
        }

    }




    private void AddSkipFeedback() {
        RequestBody formBody = new FormBody.Builder()
                .add("user_id",mGlobal.mActiveUser.getUser_id())
                .add("event_id",eventID)
                .add("status", status)
                .build();
        try {
            HttpHelper.CallApi(mActivity, Constants.Add_event_feedback,formBody,this,true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onFailure(Call call, IOException e) {

    }

    @Override
    public void onSuccess(Call call, String response) {
        Log.d("res~~~~~~~~~~~~~",response);
        try {
            final JSONObject jsonObject = new JSONObject(response);
            if (status.equals("add"))
            {
                if (jsonObject.getString("status").equals("true"))
                {
                    final String url = jsonObject.getString("feedback_url");
                    mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            wvFeedback.loadUrl(Constants.Base_URL+url);
                            event.setFeedback_url(url);
                        }
                    });

                }
                else
                {
                    mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            try {
                                Toast.makeText(mActivity, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }


            }
            else if (status.equals("skip"))
            {
                if (jsonObject.getString("status").equals("true"))
                {
                    mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Bundle bundle = new Bundle();
                            bundle.putSerializable("EVENT", event);
                            Utility.ReplaceFragment_create_event(new CreateEventFragment_5(),getFragmentManager(),bundle);
                        }
                    });

                }
                else
                {
                    mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            try {
                                Toast.makeText(mActivity, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    public class JavaScriptInterface {
        Context mContext;

        public  JavaScriptInterface(Context c) {
            mContext = c;
        }
        @JavascriptInterface
        public void CreateForm(boolean status, String mesg){
            if (status)
            {
                ShowFullScreen(false);
                Bundle bundle = new Bundle();
                bundle.putBoolean("EDITING", editing);
                bundle.putSerializable("EVENT", event);
                Utility.ReplaceFragment_create_event(new CreateEventFragment_5(),getFragmentManager(),bundle);
            }
            else
                Toast.makeText(mContext, mesg, Toast.LENGTH_SHORT).show();
        }
    }

}