package ifisol.attendoplus.View.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


import fr.arnaudguyon.smartfontslib.FontTextView;
import ifisol.attendoplus.Controller.Adapters.EventsDetailPagerAdapter;
import ifisol.attendoplus.Global.Globals;
import ifisol.attendoplus.Model.Events;
import ifisol.attendoplus.R;
import ifisol.attendoplus.Utilities.Constants;
import ifisol.attendoplus.Utilities.Utility;

public class



EventsDetailPagerFragment extends Fragment implements View.OnClickListener {

    FragmentActivity mActivity;
    Context mContext;
    Globals mGlobal;

    private TabLayout tabs;
    private ViewPager detail_VP;
    private EventsDetailPagerAdapter eventsDetailPagerAdapter;
    FontTextView mTvTitle;
    String mEventType;
    ImageView imgBack;
    Events mEvent;
    public static ImageView imgQr;




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.events_detail_main_fragment, null);
    }





    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Init(view);

    }




    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mEventType = getArguments().getString(Constants.EVENT_TYPE);
        mEvent = (Events) getArguments().getSerializable("EVENT");
        SetUpViewPager(mEvent);
        if (getArguments().getString(Constants.TAG_VIEW_TO_GO).equals(Constants.TAG_EXHIBITOR))
            detail_VP.setCurrentItem(1);
        else detail_VP.setCurrentItem(0);
        tabs.setOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(detail_VP) {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                super.onTabSelected(tab);
                View tabView = (View) tab.getCustomView();
                ImageView tabIcon = (ImageView) tabView.findViewById(R.id.img_tab);
                tabIcon.setColorFilter(getResources().getColor(R.color.cpd_green));
                mTvTitle.setText(eventsDetailPagerAdapter.getPageTitle(tab.getPosition()));

                if (tab.getPosition()==0)
                    showhideQR();
                else
                    imgQr.setVisibility(View.GONE);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

                super.onTabUnselected(tab);
                View tabView = (View) tab.getCustomView();
                ImageView tabIcon = (ImageView) tabView.findViewById(R.id.img_tab);
                tabIcon.setColorFilter(getResources().getColor(R.color.black));
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

                super.onTabReselected(tab);
            }
        });
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mActivity.getSupportFragmentManager().popBackStack();
            }
        });
    }






    private View Init(View view) {
        mActivity = getActivity();
        mContext = getActivity();
        mGlobal = (Globals) mActivity.getApplicationContext();
        tabs = (TabLayout) view.findViewById(R.id.tabs);
        detail_VP = (ViewPager) view.findViewById(R.id.detail_pager);
        mTvTitle = (FontTextView) view.findViewById(R.id.tv_title);
        imgBack = (ImageView) view.findViewById(R.id.img_back);
        imgQr = (ImageView) view.findViewById(R.id.img_qr);

        imgQr.setOnClickListener(this);
        return view;
    }





    private void SetUpViewPager(Events event) {
        eventsDetailPagerAdapter = new EventsDetailPagerAdapter(getChildFragmentManager(), mContext, mEventType, event);
        detail_VP.setAdapter(eventsDetailPagerAdapter);
        tabs.setupWithViewPager(detail_VP);
        for (int i = 0; i < tabs.getTabCount(); i++) {
            tabs.getTabAt(i).setCustomView(eventsDetailPagerAdapter.getTabView(i));
        }
        TabLayout.Tab tab = tabs.getTabAt(0);
        View tabView = (View) tab.getCustomView();
        ImageView tabIcon = (ImageView) tabView.findViewById(R.id.img_tab);
        tabIcon.setColorFilter(getResources().getColor(R.color.cpd_green));
        mTvTitle.setText(eventsDetailPagerAdapter.getPageTitle(0));

        showhideQR();

    }


    private void showhideQR()
    {
         if (mEventType.equals(Constants.TAG_ONGOING_EVENTS) || mEventType.equals(Constants.TAG_UPCOMING))
        {
            if (mGlobal.mActiveUser.getUser_type().equals(Constants.TAG_ORGANISER) && mEvent.getUser_id().equals(mGlobal.mActiveUser.getUser_id()))
                imgQr.setVisibility(View.VISIBLE);
            else
                imgQr.setVisibility(View.GONE);
        }
        else
            imgQr.setVisibility(View.GONE);
    }









    @Override
    public void onClick(View v) {
    if (v==imgQr)
    {
        Bundle bundle = new Bundle();
        bundle.putSerializable("EVENT",mEvent);
        Utility.ReplaceFragment(new QrListing_fragment(),mActivity.getSupportFragmentManager(),bundle);
    }
    }
}
