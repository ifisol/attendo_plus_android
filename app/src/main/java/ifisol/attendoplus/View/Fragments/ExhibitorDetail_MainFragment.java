package ifisol.attendoplus.View.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import fr.arnaudguyon.smartfontslib.FontTextView;
import ifisol.attendoplus.Controller.Adapters.ExhibitorPagerAdapter;
import ifisol.attendoplus.Global.Globals;
import ifisol.attendoplus.R;

public class ExhibitorDetail_MainFragment extends Fragment  {


    FragmentActivity mActivity;
    Context  mContext;
    Globals  mGlobal;

    private TabLayout tabs;
    private ViewPager exh_vp;
    private ExhibitorPagerAdapter exhibitorPagerAdapter;
    private CardView tabView;
    FontTextView mTvTitle;
    ImageView imgBack;
    String exhibitorID;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return Init(inflater.inflate(R.layout.exhibitor_detail_main_fragment, null));
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        exhibitorID = getArguments().getString("EXHIBITOR_ID");
        SetUpViewPager();

        tabs.setOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(exh_vp) {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                super.onTabSelected(tab);
                View tabView = (View) tab.getCustomView();
                ImageView tabIcon = (ImageView) tabView.findViewById(R.id.img_tab);
                tabIcon.setColorFilter(getResources().getColor(R.color.cpd_green));
                mTvTitle.setText(exhibitorPagerAdapter.getPageTitle(tab.getPosition()));
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

                super.onTabUnselected(tab);
                View tabView = (View) tab.getCustomView();
                ImageView tabIcon = (ImageView) tabView.findViewById(R.id.img_tab);
                tabIcon.setColorFilter(getResources().getColor(R.color.black));
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

                super.onTabReselected(tab);
            }
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mActivity.getSupportFragmentManager().popBackStack();
            }
        });

    }

    private View Init(View view)
    {

        mActivity = getActivity();
        mContext  = getActivity();
        mGlobal   = (Globals) mActivity.getApplicationContext();

        tabs            = (TabLayout) view.findViewById(R.id.tabs);
        exh_vp = (ViewPager) view.findViewById(R.id.exhibitor_view_pager);
        tabView         = (CardView) view.findViewById(R.id.tab_view);
        mTvTitle        = (FontTextView)  view.findViewById(R.id.tv_actionbar_title);
        imgBack  = (ImageView) view.findViewById(R.id.img_back);

        return view;
    }

    private void SetUpViewPager() {

        exhibitorPagerAdapter = new ExhibitorPagerAdapter(getChildFragmentManager(),mContext, exhibitorID);
        exh_vp.setAdapter(exhibitorPagerAdapter);

        tabs.setupWithViewPager(exh_vp);

        for (int i = 0; i < tabs.getTabCount(); i++) {
            tabs.getTabAt(i).setCustomView(exhibitorPagerAdapter.getTabView(i));
        }

        TabLayout.Tab tab = tabs.getTabAt(0);
        View tabView = (View) tab.getCustomView();
        ImageView tabIcon = (ImageView) tabView.findViewById(R.id.img_tab);
        tabIcon.setColorFilter(getResources().getColor(R.color.cpd_green));
        mTvTitle.setText(exhibitorPagerAdapter.getPageTitle(0));




    }

}
