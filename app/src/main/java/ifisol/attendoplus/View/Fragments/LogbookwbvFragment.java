package ifisol.attendoplus.View.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import fr.arnaudguyon.smartfontslib.FontTextView;
import ifisol.attendoplus.Global.Globals;
import ifisol.attendoplus.Model.Events;
import ifisol.attendoplus.R;
import ifisol.attendoplus.Utilities.Constants;

public class LogbookwbvFragment extends Fragment{


    FragmentActivity mActivity;
    Context  mContext;
    Globals  mGlobal;

    private WebView wvLogbook;

    Events event;
    String mEventType;
    FontTextView tvNoRecord;




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.webview, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Init(view);
    }



    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);
        event = (Events) getArguments().getSerializable("EVENT");
        mEventType = getArguments().getString(Constants.EVENT_TYPE);
        SetData();


    }

    private View Init(View view)
    {
        mActivity = getActivity();
        mContext  = getActivity();
        mGlobal   = (Globals) mActivity.getApplicationContext();
        wvLogbook = (WebView) view.findViewById(R.id.wbv);
        tvNoRecord = (FontTextView) view.findViewById(R.id.tv_no_record);


        wvLogbook.getSettings().setJavaScriptEnabled(true);

        return view;
    }




    private void SetData() {


        wvLogbook.setWebViewClient(new WebViewClient() {


            //If you will not use this method url links are opeen in new brower not in webview
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }


            public void onPageFinished(WebView view, String url) {

                if (Globals.mLoadingDia!=null && Globals.mLoadingDia.isShowing())
                {
                    Globals.mLoadingDia.dismiss();
                }
            }

        });

        if (event.getHas_logbook().equals("yes") && !event.getLogbook_url().equals(""))
        {
            wvLogbook.loadUrl(Constants.Base_URL+event.getLogbook_url());
            tvNoRecord.setVisibility(View.GONE);
        }

        else
        {
            tvNoRecord.setVisibility(View.VISIBLE);
            tvNoRecord.setText("It seems there is no Logbook form\n attached to this event");
        }


    }

    public class JavaScriptInterface {
        Context mContext;

        public  JavaScriptInterface(Context c) {
            mContext = c;
        }
        @JavascriptInterface
        public void SaveLogbook(boolean status, String mesg){
            if (status)
            {

            }
            else
                Toast.makeText(mContext, mesg, Toast.LENGTH_SHORT).show();
        }
    }

}
