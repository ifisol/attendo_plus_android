package ifisol.attendoplus.View.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.yalantis.taurus.PullToRefreshView;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import fr.arnaudguyon.smartfontslib.FontEditText;
import ifisol.attendoplus.Controller.Adapters.OrganisationsAdapter;
import ifisol.attendoplus.Global.Globals;
import ifisol.attendoplus.Model.Organisations;
import ifisol.attendoplus.R;
import ifisol.attendoplus.Utilities.Constants;
import ifisol.attendoplus.Utilities.HttpHelper;
import ifisol.attendoplus.Utilities.Utility;
import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.RequestBody;

public class OrganisationsFragment extends Fragment implements HttpHelper.HttpCallback, OrganisationsAdapter.OnItemClick {

    FragmentActivity mActivity;
    Context  mContext;
    Globals mGlobal;

    RecyclerView mOrg_rv;
    OrganisationsAdapter org_adapter;
    ArrayList<Organisations> mOrgList;
    FloatingActionButton fab_add;
    ImageView imgNoRecord;
    FontEditText mTxtSearch;
    PullToRefreshView mPullToRefreshView;
    View view;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        mActivity = getActivity();
        mContext  = getActivity();
        mGlobal = (Globals) mActivity.getApplicationContext();

        FetchOrganisations(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (view==null) view = Init(inflater.inflate(R.layout.organisations_fragment, null));

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        fab_add.setVisibility(View.GONE);






        mTxtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                org_adapter.FilterList(s.toString());

            }
        });





        mPullToRefreshView.setOnRefreshListener(new PullToRefreshView.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPullToRefreshView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        FetchOrganisations(false);
                    }
                },500);
            }
        });


        mOrg_rv.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                super.onScrolled(recyclerView, dx, dy);

                if (dy<=0)
                    mPullToRefreshView.setEnabled(true);
                else
                    mPullToRefreshView.setEnabled(false);
            }
        });







    }










    private void FetchOrganisations(boolean b) {
        try {
            RequestBody formBody = new FormBody.Builder().add("user_id",mGlobal.mActiveUser.getUser_id()).build();
            HttpHelper.CallApi(mActivity,Constants.Fetch_Organisations,formBody,this,b);
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("Exception...........",e.getMessage());
            if (e.getMessage()!=null) Toast.makeText(mActivity, e.getMessage().toString(), Toast.LENGTH_SHORT).show();
        }
    }











    private View Init(View v)
    {


        mOrg_rv = (RecyclerView) v.findViewById(R.id.rv_organisations);
        fab_add   = (FloatingActionButton) v.findViewById(R.id.fab_add);
        imgNoRecord = (ImageView) v.findViewById(R.id.img_no_record);
        mPullToRefreshView = (PullToRefreshView) v.findViewById(R.id.pull_to_refresh);


        mTxtSearch = (FontEditText) v.findViewById(R.id.ed_search);

        return v;
    }

    private void SetAdapter()
    {
        mPullToRefreshView.setRefreshing(false);
        mOrg_rv.setHasFixedSize(true);
        mOrg_rv.setLayoutManager(new LinearLayoutManager(mContext));
        org_adapter = new OrganisationsAdapter(mActivity,null, mOrgList,"");
        mOrg_rv.setAdapter(org_adapter);

        org_adapter.setOnClickListener(this);

    }






    @Override
    public void onFailure(Call call, IOException e) {

        e.printStackTrace();

        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                imgNoRecord.setVisibility(View.VISIBLE);

            }
        });

    }

    @Override
    public void onSuccess(Call call, String response) {
        ParseOrganisations(response);
    }





    private void ParseOrganisations(String s)
    {
        try {
            JSONObject jsonObj = new JSONObject(s);
            Gson gson = new Gson();
            Organisations[] mOrgs = gson.fromJson(jsonObj.getString("response"),Organisations[].class);
            mOrgList = new ArrayList<>(Arrays.asList(mOrgs));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mPullToRefreshView.setRefreshing(false);
                if (mOrgList!=null && mOrgList.size()>0)
                {
                    SetAdapter();
                    imgNoRecord.setVisibility(View.GONE);
                }
                else
                imgNoRecord.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void onItemClicked(Organisations obj) {

        Bundle bundle = new Bundle();
        bundle.putString("ORGANISATION_ID", obj.getOrganisation_id());
        Utility.ReplaceFragment(new Organisation_MainFragment(),mActivity.getSupportFragmentManager(),bundle);
    }
}