package ifisol.attendoplus.View.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.yalantis.taurus.PullToRefreshView;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import ifisol.attendoplus.Controller.Adapters.GuestsAdapter;
import ifisol.attendoplus.Global.Globals;
import ifisol.attendoplus.Model.Attendees;
import ifisol.attendoplus.Model.Events;
import ifisol.attendoplus.R;
import ifisol.attendoplus.Utilities.Constants;
import ifisol.attendoplus.Utilities.HttpHelper;
import ifisol.attendoplus.Utilities.Utility;
import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.RequestBody;

public class GuestsFragment extends Fragment implements HttpHelper.HttpCallback {

    FragmentActivity mActivity;
    Context  mContext;
    Globals  mGlobal;

    RecyclerView GuestRV;
    GuestsAdapter guestsAdapter;
    String mGuestType;
    FloatingActionButton fab;
    Events event;
    String eventType;
    ImageView imgNoRecord;
    ArrayList<Attendees> mGuestList;
    String orgID;
    PullToRefreshView mPullToRefreshView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.guests_fragment, null);

        return Init(v);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mGuestType = getArguments().getString(Constants.USER_TYPE);

        FetchGuest(true);











        mPullToRefreshView.setOnRefreshListener(new PullToRefreshView.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPullToRefreshView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        FetchGuest(false);
                    }
                },500);
            }
        });


        GuestRV.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                super.onScrolled(recyclerView, dx, dy);
                if (dy<=0)
                    mPullToRefreshView.setEnabled(true);
                else
                    mPullToRefreshView.setEnabled(false);
            }
        });











    }





    private void FetchGuest(boolean b) {
        if (mGuestType.equals("Organisation Members"))
        {
            fab.setVisibility(View.GONE);
            orgID =  getArguments().getString("ORGANISATION_ID");
            FetchOrganisationGuest();
        }
        else {
            event = (Events) getArguments().getSerializable("EVENT");
            eventType = getArguments().getString(Constants.EVENT_TYPE);
            if (mGuestType.equals(Constants.TAG_ATTENDED_GUEST) && eventType.equals(Constants.TAG_ONGOING_EVENTS))
                fab.setVisibility(View.VISIBLE);
            else
                fab.setVisibility(View.GONE);

            FetchEventGuest(b);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Bundle bundle = new Bundle();
                    bundle.putSerializable("EVENT", event);
                    Utility.ReplaceFragment(new MarkAttendanceMainFragment(), mActivity.getSupportFragmentManager(),bundle);
                }
            });
        }
    }





    private View Init(View v)
    {
        mActivity = getActivity();
        mContext  = getActivity();
        mGlobal   = (Globals) mActivity.getApplicationContext();
        imgNoRecord = (ImageView) v.findViewById(R.id.img_no_record);

        GuestRV = (RecyclerView) v.findViewById(R.id.rv_guest);
        fab     =  (FloatingActionButton) v.findViewById(R.id.fab_add);
        mPullToRefreshView = (PullToRefreshView) v.findViewById(R.id.pull_to_refresh);

        return v;
    }





    private void SetAdapter()
    {
        mPullToRefreshView.setRefreshing(false);
        GuestRV.setHasFixedSize(true);
        GuestRV.setLayoutManager(new LinearLayoutManager(mContext));
        guestsAdapter = new GuestsAdapter(mContext, mActivity, this, mGuestList);
        GuestRV.setAdapter(guestsAdapter);

        guestsAdapter.setItemClick(new GuestsAdapter.ItemClick() {
            @Override
            public void onItemClick(int position, Object user) {

                if (((Attendees)user).getProfile_visibility_status()!=null && ((Attendees)user).getProfile_visibility_status().equals("public"))
                {
                    Bundle bundle = new Bundle();
                    bundle.putString("USER_ID", ((Attendees)user).getUser_id());
                    bundle.putString("PROFILE_TYPE","OTHER");
                    Utility.ReplaceFragment(new ProfileFragment(),mActivity.getSupportFragmentManager(),bundle);
                }
                else
                    Utility.ShowAlert(mActivity,"Private Profile","This user has a private profile. You cannot view private profiles");



            }
        });
    }








    private void FetchEventGuest(boolean loader) {

        if (mGuestType.equals(Constants.TAG_INTERESTED_GUEST)) {
            RequestBody body = new FormBody.Builder().add("user_id", mGlobal.mActiveUser.getUser_id()).add("event_id", event.getEvent_id()).add("tag", "interested_guests").build();
            try {
                HttpHelper.CallApi(mActivity, Constants.Other_guests, body, this, loader);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (mGuestType.equals(Constants.TAG_ATTENDED_GUEST)) {
            RequestBody body = new FormBody.Builder().add("user_id", mGlobal.mActiveUser.getUser_id()).add("event_id", event.getEvent_id()).add("tag", "attended_guests").build();
            try {
                HttpHelper.CallApi(mActivity, Constants.Other_guests, body, this, false);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }





    private void FetchOrganisationGuest() {
            RequestBody body = new FormBody.Builder().add("user_id", mGlobal.mActiveUser.getUser_id()).add("organisation_id", orgID).add("tag", "organisation_associated_guests").build();
            try {
                HttpHelper.CallApi(mActivity, Constants.Organisation_detail, body, this, true);
            } catch (Exception e) {
                e.printStackTrace();
            }
    }






    @Override
    public void onFailure(Call call, IOException e) {

        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                imgNoRecord.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void onSuccess(Call call, String response) {

        try {
            ParseGuests(response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void ParseGuests(String res) throws Exception {

        Log.d("Res~~~~~~~~~~~~~~~~~~~~", res);
        JSONObject jsonObj = new JSONObject(res);
        if (jsonObj.getString("status").equals("true"))
        {
            Gson gson = new Gson();
            Attendees[] attendez = gson.fromJson(jsonObj.getString("response"),Attendees[].class);
            mGuestList = new ArrayList<>(Arrays.asList(attendez));

        }

        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (mGuestList!=null && mGuestList.size()>0)
                {
                    SetAdapter();
                    imgNoRecord.setVisibility(View.GONE);
                }
                else
                    imgNoRecord.setVisibility(View.VISIBLE);
            }
        });
    }
}