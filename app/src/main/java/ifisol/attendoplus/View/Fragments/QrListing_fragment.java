package ifisol.attendoplus.View.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.yalantis.taurus.PullToRefreshView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import fr.arnaudguyon.smartfontslib.FontEditText;
import fr.arnaudguyon.smartfontslib.FontTextView;
import ifisol.attendoplus.Controller.Adapters.QrListAdapter;
import ifisol.attendoplus.Global.Globals;
import ifisol.attendoplus.Model.Events;
import ifisol.attendoplus.Model.Exhibitors;
import ifisol.attendoplus.R;
import ifisol.attendoplus.Utilities.Constants;
import ifisol.attendoplus.Utilities.HttpHelper;
import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.RequestBody;

public class QrListing_fragment extends Fragment implements View.OnClickListener, HttpHelper.HttpCallback {

    FragmentActivity mActivity;
    Context mContext;
    Globals mGlobal;


    ArrayList<Exhibitors> mExhibitors;
    PullToRefreshView mPullToRefreshView;
    QrListAdapter adapter;

    private ListView qrList;
    private FontTextView tvNoRecord;

    Events event;
    View header;

    FontTextView eventName;
    ImageView imgEventQrEmail;
    FontEditText edSearch;
    ImageView imgBack;

    String OrgEmail;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return Init(inflater.inflate(R.layout.qr_listing_fragment, null));
    }





    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        event = (Events) getArguments().getSerializable("EVENT");

        eventName.setText(event.getEvent_name());
    }





    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        FetchQrs(true);




        mPullToRefreshView.setOnRefreshListener(new PullToRefreshView.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPullToRefreshView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        FetchQrs(false);
                    }
                },500);
            }
        });


        qrList.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem == 0) {
                    mPullToRefreshView.setEnabled(true);
                } else mPullToRefreshView.setEnabled(false);
            }
        });
    }





    private View Init(View view) {
        mActivity           = getActivity();
        mContext            = getActivity();
        mGlobal             = (Globals) mActivity.getApplicationContext();
        qrList              = (ListView) view.findViewById(R.id.qr_list);
        tvNoRecord          = (FontTextView) view.findViewById(R.id.tv_no_record);
        mPullToRefreshView  = (PullToRefreshView) view.findViewById(R.id.pull_to_refresh);
        imgBack             = (ImageView) view.findViewById(R.id.img_back);
        eventName = (FontTextView) view.findViewById(R.id.tv_event_name);
        imgEventQrEmail = (ImageView) view.findViewById(R.id.img_send_email);

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(mContext.LAYOUT_INFLATER_SERVICE);
        header = inflater.inflate(R.layout.qr_list_header,null);
        qrList.addHeaderView(header);

        edSearch = (FontEditText) header.findViewById(R.id.ed_search);

        imgEventQrEmail.setOnClickListener(this);
        imgBack.setOnClickListener(this);

        return view;
    }





    private void FetchQrs(boolean loader) {

        FormBody.Builder builder = new FormBody.Builder();
               builder.add("user_id",mGlobal.mActiveUser.getUser_id());
               builder.add("event_id",event.getEvent_id()).build();
                  RequestBody  body = builder.build();


        try {
            HttpHelper.CallApi(mActivity,Constants.Qr_code_list, body, this, loader);
        } catch (Exception e) {
            e.printStackTrace();
        }


}






    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }






    @Override
    public void onClick(View v) {
        if (v==imgEventQrEmail)
        {
           sendQr(mGlobal.mActiveUser.getUser_id(),"to_org");
        }
        else if (v==imgBack)
            mActivity.getSupportFragmentManager().popBackStack();
    }






    private void SetQrAdapter(ArrayList<Exhibitors> list) {
        mPullToRefreshView.setRefreshing(false);
        if (list==null||list.size()==0) {
            tvNoRecord.setVisibility(View.VISIBLE);
            edSearch.setVisibility(View.GONE);
       //     qrList.setVisibility(View.GONE);
            return;
        }
        tvNoRecord.setVisibility(View.GONE);
        edSearch.setVisibility(View.VISIBLE);
     //   qrList.setVisibility(View.VISIBLE);

       adapter  = new QrListAdapter(mActivity, list);
       qrList.setAdapter(adapter);


        adapter.setViewClickListener(new QrListAdapter.viewClickListener() {
            @Override
            public void click(View v, Exhibitors obj) {

                switch (v.getId())
                {
                    case R.id.tv_request:
                        break;
                    case R.id.img_email:
                        sendQr(obj.getExhibitor_id(), "to_exhib");
                        break;
                }
            }
        });

        edSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                adapter.FilterList(s.toString());
            }
        });
    }





    public void sendQr(String user_id, String emailTo) {
        FormBody.Builder builder = new FormBody.Builder();
        builder.add("event_id",event.getEvent_id()).build();
        try {
            String url = "";
            if (emailTo.equals("to_org"))
            {
                builder.add("user_id",user_id);
                url = Constants.Send_qr_organiser;
            }

            else
            {
                url = Constants.Send_qr_to_exhibitor;
                builder.add("exhibitor_id",user_id);
            }
            RequestBody  body = builder.build();
            HttpHelper.CallApi(mActivity,url, body, this, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }






    @Override
    public void onFailure(Call call, final IOException e) {
        e.printStackTrace();
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (e.getMessage()!=null) Toast.makeText(mActivity, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
    @Override
    public void onSuccess(Call call, final String response) {
        try {
            Log.d("Response ......." ,response);
            if (call.request().url().toString().equals(Constants.Send_qr_organiser) || call.request().url().toString().equals(Constants.Send_qr_to_exhibitor) )
            {
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            JSONObject json = new JSONObject(response);
                                Toast.makeText(mActivity, json.getString("message"), Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

            }
            else
                ParseQrs(response);
        } catch (final Exception e) {
            e.printStackTrace();
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.d("Exception======>>>>>>>>",e.getMessage());
                    SetQrAdapter(mExhibitors); //To show no record found//
                }
            });
        }
    }





    private void ParseQrs(String s) throws Exception{
        Log.d("Response........",s);
        final JSONObject jsonObj = new JSONObject(s);
        final String message = jsonObj.getString("message");

        if (jsonObj.getString("status").equals("true"))
        {
            OrgEmail = jsonObj.getString("event_organiser_email");
            Gson gson = new Gson();
            Exhibitors[] exhibitors = gson.fromJson(jsonObj.getString("response"),Exhibitors[].class);
            mExhibitors = new ArrayList<>(Arrays.asList(exhibitors));
        }
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                SetQrAdapter(mExhibitors);
            }
        });
    }




}