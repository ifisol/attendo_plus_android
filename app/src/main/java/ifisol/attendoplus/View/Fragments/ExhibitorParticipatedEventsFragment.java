package ifisol.attendoplus.View.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.internal.NavigationMenu;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import com.google.gson.Gson;
import com.yalantis.taurus.PullToRefreshView;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import fr.arnaudguyon.smartfontslib.FontEditText;
import ifisol.attendoplus.Controller.Adapters.ExhibitorEventsAdapter;
import ifisol.attendoplus.Global.Globals;
import ifisol.attendoplus.Model.Events;
import ifisol.attendoplus.R;
import ifisol.attendoplus.Utilities.Constants;
import ifisol.attendoplus.Utilities.HttpHelper;
import ifisol.attendoplus.Utilities.Utility;
import io.github.yavski.fabspeeddial.FabSpeedDial;
import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.RequestBody;

public class ExhibitorParticipatedEventsFragment extends Fragment implements HttpHelper.HttpCallback, AdapterView.OnItemClickListener {


    FragmentActivity mActivity;
    Context  mContext;
    Globals  mGlobal;

    private ListView eventsList;
    FabSpeedDial fabSort;
    ImageView imgNoRecord;
    String ExhibitorID;

    ArrayList<Events> mEvents;
    ExhibitorEventsAdapter adapter;
    View viewBlur;
    PullToRefreshView mPullToRefreshView;
    View header;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.exhibitors_participated_events, null);
    }




    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Init(view);
    }






    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ExhibitorID = getArguments().getString("EXHIBITOR_ID");

        FetchEvents();







        fabSort.setMenuListener(new FabSpeedDial.MenuListener() {
            @Override
            public boolean onPrepareMenu(NavigationMenu navigationMenu) {
                viewBlur.setVisibility(View.VISIBLE);
                return true;}
            @Override
            public boolean onMenuItemSelected(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.sort_az:
                        Collections.sort(adapter.mFilterArray, Utility.TitleComparator);
                        break;
                    case R.id.sort_09:
                        Collections.sort(adapter.mFilterArray, Utility.DateComparator);
                        break;}
                adapter.notifyDataSetChanged();
                viewBlur.setVisibility(View.GONE);
                return true;}
            @Override
            public void onMenuClosed() {
                viewBlur.setVisibility(View.GONE);}});





        mPullToRefreshView.setOnRefreshListener(new PullToRefreshView.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPullToRefreshView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        FetchEvents();
                    }
                },500);
            }
        });


        eventsList.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem == 0) {
                    mPullToRefreshView.setEnabled(true);
                } else mPullToRefreshView.setEnabled(false);
            }
        });
        
        
        
    }

    private void FetchEvents() {
        RequestBody body = new FormBody.Builder().add("user_id",mGlobal.mActiveUser.getUser_id())
                .add("exhibitor_id",ExhibitorID)
                .add("tag","exhibitor_participated_events").build();
        FetchEvents(Constants.Exhibitor_detail,body,false);
    }

    private View Init(View view) {
        mActivity          = getActivity();
        mContext           = getActivity();
        mGlobal            = (Globals) mActivity.getApplicationContext();
        eventsList         = (ListView) view.findViewById(R.id.lv_exhib_events);
        fabSort            = (FabSpeedDial) view.findViewById(R.id.fab_sort);
        imgNoRecord        = (ImageView) view.findViewById(R.id.img_no_record);
        viewBlur            = view.findViewById(R.id.view_blur);
        mPullToRefreshView = (PullToRefreshView) view.findViewById(R.id.pull_to_refresh);

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(mContext.LAYOUT_INFLATER_SERVICE);
        header = inflater.inflate(R.layout.search_view,null);
        eventsList.addHeaderView(header);

        return view;
    }







    @Override
    public void onDestroyView() {
        super.onDestroyView();
        for(Call call : Globals.okHttpClient.dispatcher().runningCalls()) {
            call.cancel();
        }
        if (Globals.mLoadingDia != null && Globals.mLoadingDia.isShowing())
            Globals.mLoadingDia.dismiss();
    }






    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Bundle bundle = new Bundle();
                bundle.putString(Constants.TAG_VIEW_TO_GO, "");
                bundle.putString(Constants.EVENT_TYPE, Constants.TAG_EXHIBITOR_PARTICIPATED_EVENTS);
                bundle.putSerializable("EVENT", adapter.getItm(position-1));
                Utility.ReplaceFragment(new EventsDetailPagerFragment(), mActivity.getSupportFragmentManager(), bundle);
    }






    private void FetchEvents(String url, RequestBody body, boolean ShowLoading) {
        try {
            HttpHelper.CallApi(mActivity,url, body, this, ShowLoading);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }





    private void SetEventsAdapter(ArrayList<Events> list) {
        mPullToRefreshView.setRefreshing(false);
        if (list==null||list.size()==0) {
            imgNoRecord.setVisibility(View.VISIBLE);
            fabSort.setVisibility(View.GONE);
            return;
        }
        imgNoRecord.setVisibility(View.GONE);
        fabSort.setVisibility(View.VISIBLE);



            adapter = new ExhibitorEventsAdapter(mContext, mActivity, Constants.TAG_EXHIBITOR_PARTICIPATED_EVENTS,list);
            eventsList.setAdapter(adapter);
            eventsList.setOnItemClickListener(this);




        FontEditText mTxtSearch = (FontEditText) header.findViewById(R.id.ed_search);
        mTxtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                adapter.FilterList(s.toString());

            }
        });





        }








    @Override
    public void onFailure(Call call, IOException e) {
        e.printStackTrace();
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                SetEventsAdapter(mEvents);
            }
        });
    }
    @Override
    public void onSuccess(Call call, String response) {

        try {
            Log.d("Response ......." ,response);
            ParseEvents(response);
        } catch (Exception e) {
            e.printStackTrace();
            SetEventsAdapter(mEvents); //To show no record found//
        }
    }





    private void ParseEvents(String s) throws Exception{
        Log.d("Response........",s);
        final JSONObject jsonObj = new JSONObject(s);
        final String message = jsonObj.getString("message");
        mEvents = new ArrayList<>();

        if (jsonObj.getString("status").equals("true"))
        {
            Gson gson = new Gson();
            Events[] events = gson.fromJson(jsonObj.getString("response"),Events[].class);
            mEvents = new ArrayList<>(Arrays.asList(events));
        }
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                SetEventsAdapter(mEvents);
            }
        });
    }


}
