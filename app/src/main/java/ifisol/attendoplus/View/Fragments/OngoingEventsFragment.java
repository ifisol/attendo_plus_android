package ifisol.attendoplus.View.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.internal.NavigationMenu;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.yalantis.taurus.PullToRefreshView;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import fr.arnaudguyon.smartfontslib.FontEditText;
import ifisol.attendoplus.Controller.Adapters.OngoingEventsAdapter;
import ifisol.attendoplus.Global.Globals;
import ifisol.attendoplus.Model.Events;
import ifisol.attendoplus.R;
import ifisol.attendoplus.Utilities.Constants;
import ifisol.attendoplus.Utilities.HttpHelper;
import ifisol.attendoplus.Utilities.Utility;
import io.github.yavski.fabspeeddial.FabSpeedDial;
import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.RequestBody;

public class OngoingEventsFragment extends Fragment implements AdapterView.OnItemClickListener, HttpHelper.HttpCallback {

    FragmentActivity mActivity;
    Context mContext;
    Globals mGlobal;
    ListView mEventslv;

    OngoingEventsAdapter ongoingEventsAdapter;

    View imgNoRecord;

    FabSpeedDial fab_EventsSort;
    RequestBody body;
    ArrayList<Events> mEvents;
    View viewBlur;
    PullToRefreshView mPullToRefreshView;
    View header;
    View view;



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mActivity           = getActivity();
        mContext            = getActivity();
        mGlobal             = (Globals) mActivity.getApplicationContext();

 //       SetUpEvents(true);
    }





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (view==null)
              view = Init(inflater.inflate(R.layout.ongoing_events, null));

        return view;
    }





    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }





    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);



        fab_EventsSort.setMenuListener(new FabSpeedDial.MenuListener() {
            @Override
            public boolean onPrepareMenu(NavigationMenu navigationMenu) {

                viewBlur.setVisibility(View.VISIBLE);
                return true;
            }

            @Override
            public boolean onMenuItemSelected(MenuItem menuItem) {

                switch (menuItem.getItemId())
                {
                    case R.id.sort_az:
                        Collections.sort(ongoingEventsAdapter.mFilterArray,Utility.TitleComparator);
                        break;
                }
                ongoingEventsAdapter.notifyDataSetChanged();
                viewBlur.setVisibility(View.GONE);
                return true;
            }

            @Override
            public void onMenuClosed() {
                viewBlur.setVisibility(View.GONE);
            }
        });





        mPullToRefreshView.setOnRefreshListener(new PullToRefreshView.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPullToRefreshView.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        SetUpEvents(false);
                    }
                },500);
            }
        });






        mEventslv.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem == 0) {
                    mPullToRefreshView.setEnabled(true);
                } else mPullToRefreshView.setEnabled(false);
            }
        });




    }





    private View Init(View view) {
        mEventslv           = (ListView) view.findViewById(R.id.events_list);
        fab_EventsSort      = (FabSpeedDial) view.findViewById(R.id.fab_sort);
        imgNoRecord         =  view.findViewById(R.id.img_no_record);
        viewBlur            = view.findViewById(R.id.view_blur);
        mPullToRefreshView = (PullToRefreshView) view.findViewById(R.id.pull_to_refresh);

        mEventslv.setOnItemClickListener(this);

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(mContext.LAYOUT_INFLATER_SERVICE);
        header = inflater.inflate(R.layout.search_view,null);
        mEventslv.addHeaderView(header);

        return view;
    }





    private void SetUpEvents(boolean loader) {

                body = new FormBody.Builder().add("user_id",mGlobal.mActiveUser.getUser_id())
                        .add("tag","on_going_events")
                        .add("timezone",mGlobal.timezone).build();
                FetchEvents(Constants.Get_Organiser_events,body,loader);
            }







    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (ongoingEventsAdapter!=null)
            ongoingEventsAdapter.CancelTimers();

        if (Globals.mLoadingDia != null && Globals.mLoadingDia.isShowing())
            Globals.mLoadingDia.dismiss();

        mPullToRefreshView.setRefreshing(false);



    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                Bundle bundle = new Bundle();
                bundle.putString(Constants.TAG_VIEW_TO_GO, "");
                bundle.putString(Constants.EVENT_TYPE, Constants.TAG_ONGOING_EVENTS);
                bundle.putSerializable("EVENT", (Events)ongoingEventsAdapter.getItem(position-1));
                Utility.ReplaceFragment(new EventsDetailPagerFragment(), mActivity.getSupportFragmentManager(), bundle);

 }





    private void FetchEvents(String url, RequestBody body, boolean ShowLoading) {
        try {
            HttpHelper.CallApi(mActivity,url, body, this, ShowLoading);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }







    private void SetEventsAdapter(ArrayList<Events> list) {
        mPullToRefreshView.setRefreshing(false);
        if (list==null||list.size()==0) {
            imgNoRecord.setVisibility(View.VISIBLE);
            fab_EventsSort.setVisibility(View.GONE);
            return;
        }
        imgNoRecord.setVisibility(View.GONE);
        if (list.size()>1)
            fab_EventsSort.setVisibility(View.VISIBLE);
        else
            fab_EventsSort.setVisibility(View.GONE);



            ongoingEventsAdapter = new OngoingEventsAdapter(mContext,mActivity, list);
            mEventslv.setAdapter(ongoingEventsAdapter);


        FontEditText mTxtSearch = (FontEditText) header.findViewById(R.id.ed_search);
        mTxtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                ongoingEventsAdapter.FilterList(s.toString());

            }
        });


    }

    @Override
    public void onResume() {

        super.onResume();

        SetUpEvents(true);
    }

    @Override
    public void onFailure(Call call, final IOException e) {
        e.printStackTrace();

        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mPullToRefreshView.setRefreshing(false);
                Toast.makeText(mActivity,"Request Timeout", Toast.LENGTH_SHORT).show();
            }
        });
    }
    @Override
    public void onSuccess(Call call, String response) {

        try {
            Log.d("Response ......." ,response);
            ParseEvents(response);
        } catch (Exception e) {
            e.printStackTrace();
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    imgNoRecord.setVisibility(View.VISIBLE);
                    fab_EventsSort.setVisibility(View.GONE);
                    mPullToRefreshView.setRefreshing(false);
                }
            });
        }
    }





    private void ParseEvents(String s) throws Exception{
        Log.d("Response........",s);
        final JSONObject jsonObj = new JSONObject(s);
        final String message = jsonObj.getString("message");
        mEvents = new ArrayList<>();

        if (jsonObj.getString("status").equals("true"))
        {
            Gson gson = new Gson();
            Events[] events = gson.fromJson(jsonObj.getString("response"),Events[].class);
            mEvents = new ArrayList<>(Arrays.asList(events));
        }
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                SetEventsAdapter(mEvents);
            }
        });
    }


}