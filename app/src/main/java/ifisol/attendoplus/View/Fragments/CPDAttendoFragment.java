package ifisol.attendoplus.View.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.internal.NavigationMenu;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ListView;

import com.google.gson.Gson;
import com.yalantis.taurus.PullToRefreshView;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;

import fr.arnaudguyon.smartfontslib.FontTextView;
import ifisol.attendoplus.Controller.Adapters.CPD_AllEvents_Adapter;
import ifisol.attendoplus.Controller.Adapters.CpdEventsAdapter;
import ifisol.attendoplus.Global.Globals;
import ifisol.attendoplus.Model.Events;
import ifisol.attendoplus.Model.Non_attendo_events;
import ifisol.attendoplus.R;
import ifisol.attendoplus.Utilities.Constants;
import ifisol.attendoplus.Utilities.HttpHelper;
import ifisol.attendoplus.Utilities.Utility;
import io.github.yavski.fabspeeddial.FabSpeedDial;
import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.RequestBody;

public class CPDAttendoFragment extends Fragment implements AdapterView.OnItemClickListener,HttpHelper.HttpCallback {

    FragmentActivity mActivity;
    Context mContext;
    Globals mGlobal;
    ListView mEventslv;

    CpdEventsAdapter eventsAdapter;

    String mEventType;
    ImageView imgNoRecord;
    ArrayList<Events> mAttendoEvents ;
    FabSpeedDial fab_EventsSort;

    RequestBody body;
    String cpdCount;
    View viewBlur;
    PullToRefreshView mPullToRefreshView;
    View header;
    View view;




    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mActivity           = getActivity();
        mContext            = getActivity();
        mGlobal             = (Globals) mActivity.getApplicationContext();

        mEventType = getArguments().getString(Constants.EVENT_TYPE);
        FetchEvents(true);
    }





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (view==null)
            view = Init(inflater.inflate(R.layout.cpd_att_events, null));
        return view;
    }







    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);





        fab_EventsSort.setMenuListener(new FabSpeedDial.MenuListener() {
            @Override
            public boolean onPrepareMenu(NavigationMenu navigationMenu) {

                viewBlur.setVisibility(View.VISIBLE);
                return true;
            }

            @Override
            public boolean onMenuItemSelected(MenuItem menuItem) {

                switch (menuItem.getItemId())
                {
                    case R.id.sort_az:
                        sort("name");
                        break;
                    case R.id.sort_09:
                        sort("date");
                        break;
                }
                eventsAdapter.notifyDataSetChanged();
                viewBlur.setVisibility(View.GONE);
                return true;
            }

            @Override
            public void onMenuClosed() {
                viewBlur.setVisibility(View.GONE);
            }
        });








        mPullToRefreshView.setOnRefreshListener(new PullToRefreshView.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPullToRefreshView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        FetchEvents(false);
                    }
                },500);
            }
        });


        mEventslv.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem == 0) {
                    mPullToRefreshView.setEnabled(true);
                } else mPullToRefreshView.setEnabled(false);
            }
        });







    }



    private void sort(String type)
    {

                if (type.equals("date"))
                Collections.sort(((ArrayList<Events>)eventsAdapter.mEvents),Utility.DateComparator);
                else
                    Collections.sort(((ArrayList<Events>)eventsAdapter.mEvents),Utility.TitleComparator);


    }




    private View Init(View view) {

        mEventslv           = (ListView) view.findViewById(R.id.events_list);
        fab_EventsSort      = (FabSpeedDial) view.findViewById(R.id.fab_sort);
        imgNoRecord         = (ImageView) view.findViewById(R.id.img_no_record);
        mPullToRefreshView = (PullToRefreshView) view.findViewById(R.id.pull_to_refresh);
        viewBlur            = view.findViewById(R.id.view_blur);

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(mContext.LAYOUT_INFLATER_SERVICE);
        header = inflater.inflate(R.layout.cpd_listing_header,null);
        mEventslv.addHeaderView(header);

        return view;
    }



    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                Bundle bundle = new Bundle();
                bundle.putString(Constants.TAG_VIEW_TO_GO, "");
                bundle.putString(Constants.EVENT_TYPE, mEventType);
                bundle.putSerializable("EVENT", (Events)eventsAdapter.getItem(position-1));
                Utility.ReplaceFragment(new EventsDetailPagerFragment(), mActivity.getSupportFragmentManager(), bundle);
 }






    private void FetchEvents(boolean loader) {

        RequestBody body = new FormBody.Builder().add("user_id", mGlobal.mActiveUser.getUser_id())
                .add("tag","attendo_events").build();
        FetchEvents(Constants.Guest_Cpd_events,body,true);
    }





    private void FetchEvents(String url, RequestBody body, boolean ShowLoading) {
        try {
            HttpHelper.CallApi(mActivity,url, body, this, ShowLoading);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }







    private void SetEventsAdapter(ArrayList<?> list) {
        mPullToRefreshView.setRefreshing(false);
        if (list==null||list.size()==0) {
            imgNoRecord.setVisibility(View.VISIBLE);
            fab_EventsSort.setVisibility(View.GONE);
            return;
        }
        imgNoRecord.setVisibility(View.GONE);
        if (list.size()>1)
            fab_EventsSort.setVisibility(View.VISIBLE);
        else
            fab_EventsSort.setVisibility(View.GONE);


        mEventslv.setVisibility(View.VISIBLE);


        FontTextView tv = (FontTextView) header.findViewById(R.id.tv_header);
        tv.setText("Attendo Events CPD : "+cpdCount);



            eventsAdapter = new CpdEventsAdapter(mContext, mActivity, mEventType,list);
            mEventslv.setAdapter(eventsAdapter);
            mEventslv.setOnItemClickListener(this);

        }







    @Override
    public void onFailure(Call call, IOException e) {
        e.printStackTrace();

        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                imgNoRecord.setVisibility(View.VISIBLE);
                fab_EventsSort.setVisibility(View.GONE);
            }
        });
    }
    @Override
    public void onSuccess(Call call, String response) {

        try {
            Log.d("Response ......." ,response);
            ParseEvents(response);
        } catch (Exception e) {
            e.printStackTrace();
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    imgNoRecord.setVisibility(View.VISIBLE);
                    fab_EventsSort.setVisibility(View.GONE);
                }
            });
        }
    }





    private void ParseEvents(String s) throws Exception{
        Log.d("Response........",s);

        final JSONObject jsonObj = new JSONObject(s);
        if (jsonObj.getString("status").equals("true"))
        {
            Gson gson = new Gson();

                cpdCount = jsonObj.getString("attendo_cpd_count");
                Events[] events = gson.fromJson(jsonObj.getString("response"),Events[].class);
                mAttendoEvents = new ArrayList<>(Arrays.asList(events));




        }
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                    SetEventsAdapter(mAttendoEvents);

            }
        });
    }







}