package ifisol.attendoplus.View.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.github.siyamed.shapeimageview.RoundedImageView;
import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import co.lujun.androidtagview.TagContainerLayout;
import fr.arnaudguyon.smartfontslib.FontTextView;
import ifisol.attendoplus.Global.Globals;
import ifisol.attendoplus.Model.Organisations;
import ifisol.attendoplus.Model.User;
import ifisol.attendoplus.R;
import ifisol.attendoplus.Utilities.Constants;
import ifisol.attendoplus.Utilities.HttpHelper;
import ifisol.attendoplus.Utilities.Utility;
import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.RequestBody;

public class ProfileFragment extends Fragment implements View.OnClickListener, HttpHelper.HttpCallback {


    FragmentActivity mActivity;
    Context  mContext;
    Globals  mGlobal;


    private FloatingActionButton fabEdit;
    private RoundedImageView imgUser;
    private FontTextView     tvUserName;
    private FontTextView     tvCpd;
    private FontTextView     tvFirstName;
    private FontTextView     tvLastName;
    private FontTextView     tvMemNo;
    private FontTextView     tvEmail;
    private FontTextView     tvJobTitle;
    private FontTextView     tvOrganisation;
    private FontTextView     tvAddress;
    private FontTextView     tvPhone;
    private ImageView        imgBack;
    TagContainerLayout       mTagContainerLayout;
    private View             viewSecOrg;
    private View             lineTags;
    User user;

    String UserID;
    public String mProfileType;




    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.profile, null);
    }



    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Init(view);
    }





    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mProfileType = getArguments().getString("PROFILE_TYPE");


        if (mProfileType.equals("OWN"))
        {
          //  SetData(mGlobal.mActiveUser);
            fabEdit.setVisibility(View.VISIBLE);
            imgBack.setVisibility(View.GONE);

            RequestBody body = new FormBody.Builder()
                    .add("user_id",mGlobal.mActiveUser.getUser_id()).build();

            String url ="";

            if (mGlobal.mActiveUser.getUser_type().equals(Constants.TAG_GUEST))
                url = Constants.Get_guest_Profile;
            else
                url = Constants.Get_org_Profile;

            try {
                HttpHelper.CallApi(mActivity,url,body,this,true);
            } catch (Exception e) {
                e.printStackTrace();
            }



        }
        else
        {
            fabEdit.setVisibility(View.GONE);
            imgBack.setVisibility(View.VISIBLE);
            UserID = getArguments().getString("USER_ID");
            RequestBody body = new FormBody.Builder()
                    .add("public_user_id", UserID)
                    .add("user_id",mGlobal.mActiveUser.getUser_id()).build();
            try {
                HttpHelper.CallApi(mActivity,Constants.Get_guest_public_Profile,body,this,true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }





    private View Init(View view)
    {
        mActivity = getActivity();
        mContext  = getActivity();
        mGlobal   = (Globals) mActivity.getApplicationContext();
        fabEdit = (FloatingActionButton) view.findViewById(R.id.fab_edit);
        imgUser = (RoundedImageView) view.findViewById(R.id.img_user);
        tvUserName = (FontTextView) view.findViewById(R.id.tv_user_name);
        tvCpd = (FontTextView) view.findViewById(R.id.tv_cpd);
        tvFirstName = (FontTextView) view.findViewById(R.id.tv_first_name);
        tvLastName = (FontTextView) view.findViewById(R.id.tv_last_name);
        tvMemNo = (FontTextView) view.findViewById(R.id.tv_mem_no);
        tvEmail = (FontTextView) view.findViewById(R.id.tv_email);
        tvJobTitle = (FontTextView) view.findViewById(R.id.tv_job_title);
        tvOrganisation = (FontTextView) view.findViewById(R.id.tv_organisation);
        tvAddress = (FontTextView) view.findViewById(R.id.tv_address);
        tvPhone = (FontTextView) view.findViewById(R.id.tv_phone);
        imgBack = (ImageView) view.findViewById(R.id.img_back);
        mTagContainerLayout = (TagContainerLayout) view.findViewById(R.id.tag_layout);
        viewSecOrg        = view.findViewById(R.id.view_secondary);
        lineTags              = view.findViewById(R.id.view);
        fabEdit.setOnClickListener(this);
        tvOrganisation.setOnClickListener(this);
        imgBack.setOnClickListener(this);
        return view;
    }





    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab_edit:
                Bundle bundle = new Bundle();
                bundle.putSerializable("USER", user);
                Utility.ReplaceFragment(new EditProfileFragment(), mActivity.getSupportFragmentManager(),bundle);
                break;
            case R.id.tv_organisation:
                //              Utility.ReplaceFragment(new Organisation_MainFragment(), getFragmentManager());
                break;
            case R.id.img_back:
                mActivity.getSupportFragmentManager().popBackStack();
                break;
        }
    }






    @Override
    public void onFailure(Call call, final IOException e) {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                Toast.makeText(mActivity,"Request Timeout", Toast.LENGTH_SHORT).show();
            }
        });
    }
    @Override
    public void onSuccess(Call call, String response) {

        try {
            ParseProfileData(response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }




    private void SetData(User user)
    {
        ImageLoader.getInstance().displayImage(Constants.Img_Base_URL+user.getPicture(),imgUser);
        tvUserName.setText(user.getUser_name());
        tvFirstName.setText(user.getFirst_name());
        tvLastName.setText(user.getLast_name());
        tvMemNo.setText(user.getUser_id());
        tvEmail.setText(user.getEmail());
        tvJobTitle.setText(user.getProfession());
        tvOrganisation.setText(user.getOrganisation_name());
        tvAddress.setText(user.getAddress());
        tvPhone.setText(user.getPhone());

        if (mGlobal.mActiveUser.getUser_type().equals(Constants.TAG_ORGANISER))
        {
            tvCpd.setVisibility(View.GONE);
            viewSecOrg.setVisibility(View.GONE);
            lineTags.setVisibility(View.GONE);
        }

        else
        {
            tvCpd.setVisibility(View.VISIBLE);
            if (user.getCpd_count()!=null && !user.getCpd_count().equals(""))
                tvCpd.setText(user.getCpd_count());
            else
                tvCpd.setText("0");

            if (user.getSeconday_organisations()!=null && user.getSeconday_organisations().size()>0)
            {
                viewSecOrg.setVisibility(View.VISIBLE);
                lineTags.setVisibility(View.VISIBLE);
                List<String> mTagList = new ArrayList<>();
                for (Organisations org : user.getSeconday_organisations() )
                {
                    mTagList.add(org.getOrganisation_name());
                }
                mTagContainerLayout.setTags(mTagList);
            }
            else
            {
                viewSecOrg.setVisibility(View.GONE);
                lineTags.setVisibility(View.GONE);
            }



        }

    }




    private void ParseProfileData(String response) throws Exception {

        Log.d("response>>>>>>", response);
        final JSONObject json = new JSONObject(response);
        if (json.getString("status").equals("true"))
        {
            Gson gson = new Gson();
            user = gson.fromJson(json.getString("response"),User.class);
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    SetData(user);
                }
            });
        }
        else
        {
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    try {
                        Toast.makeText(mActivity,json.getString("message"), Toast.LENGTH_SHORT).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }

    }


}
