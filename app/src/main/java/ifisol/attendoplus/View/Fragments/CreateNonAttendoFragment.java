package ifisol.attendoplus.View.Fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.sandrios.sandriosCamera.internal.SandriosCamera;
import com.sandrios.sandriosCamera.internal.configuration.CameraConfiguration;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;

import fr.arnaudguyon.smartfontslib.FontButton;
import fr.arnaudguyon.smartfontslib.FontEditText;
import fr.arnaudguyon.smartfontslib.FontRadioButton;
import fr.arnaudguyon.smartfontslib.FontTextView;
import ifisol.attendoplus.Global.Globals;
import ifisol.attendoplus.Model.Non_attendo_events;
import ifisol.attendoplus.R;
import ifisol.attendoplus.Utilities.Constants;
import ifisol.attendoplus.Utilities.HttpHelper;
import ifisol.attendoplus.Utilities.ResizeImage;
import ifisol.attendoplus.Utilities.Utility;
import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.RequestBody;

public class CreateNonAttendoFragment extends Fragment implements View.OnClickListener, CompoundButton.OnCheckedChangeListener, HttpHelper.HttpCallback {

    FragmentActivity mActivity;
    Context mContext;
    Globals mGlobal;

    private View header;
    private FontTextView tvActionbarTitle;
    private ImageView imgBack;
    private ImageView img1;
    private ImageView img2;
    private FontEditText edTitle;
    private FontEditText edSubject;
    private FontRadioButton radioStructured;
    private FontRadioButton radioUnstructured;
    private FontEditText edHours;
    private FontEditText edReff;
    private FontEditText edWebsite;
    private View viewStartTime;
    private FontTextView tvStartTime;
    private View viewEndTime;
    private FontTextView tvEndTime;
    private FontTextView tvDate;
    private FontEditText edDescription;
    private FontTextView tvExample;
    private FontButton btnCreateEvent;

    private static final int CAPTURE_IMG1 = 368;
    private static final int CAPTURE_IMG2 = 369;
    Bitmap bmImg1;
    Bitmap bmImg2;
    String ActivityType="structured";
    String status = "";
    String msg = "";

    String Img1base64;
    String Img2base64;

    String mAction;
    Non_attendo_events nonAtEvent;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        return inflater.inflate(R.layout.create_gournal_fragment, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);

        Init(view);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);

        imgBack.setOnClickListener(this);
        img1.setOnClickListener(this);
        img2.setOnClickListener(this);
        viewStartTime.setOnClickListener(this);
        viewEndTime.setOnClickListener(this);

        radioStructured.setOnCheckedChangeListener(this);
        radioUnstructured.setOnCheckedChangeListener(this);

        mAction = getArguments().getString("ACTION");
        if (mAction.equals("EDIT"))
        {
            nonAtEvent = (Non_attendo_events) getArguments().getSerializable("EVENT");
            btnCreateEvent.setText("Update Event");
            tvActionbarTitle.setText("Edit Non-Attendo Event");

          SetData();
        }
        else
        {
            btnCreateEvent.setText("Create Event");
            tvActionbarTitle.setText("Create Non-Attendo Event");
        }


    }


    private View Init(View view) {

        mActivity = getActivity();
        mContext = getActivity();
        mGlobal = (Globals) mActivity.getApplicationContext();

        header = view.findViewById(R.id.header);
        tvActionbarTitle = (FontTextView) view.findViewById(R.id.tv_actionbar_title);
        imgBack = (ImageView) view.findViewById(R.id.img_back);
        img1 = (ImageView) view.findViewById(R.id.img_1);
        img2 = (ImageView) view.findViewById(R.id.img_2);
        edTitle = (FontEditText) view.findViewById(R.id.ed_title);
        edSubject = (FontEditText) view.findViewById(R.id.ed_subject);
        radioStructured = (FontRadioButton) view.findViewById(R.id.radio_structured);
        radioUnstructured = (FontRadioButton) view.findViewById(R.id.radio_unstructured);
        edHours = (FontEditText) view.findViewById(R.id.ed_hours);
        edReff = (FontEditText) view.findViewById(R.id.ed_reff);
        edWebsite = (FontEditText) view.findViewById(R.id.ed_website);
        viewStartTime =  view.findViewById(R.id.view_start_time);
        tvStartTime = (FontTextView) view.findViewById(R.id.tv_start_time);
        viewEndTime =  view.findViewById(R.id.view_end_time);
        tvEndTime = (FontTextView) view.findViewById(R.id.tv_end_time);
        edDescription = (FontEditText) view.findViewById(R.id.ed_description);
        tvExample = (FontTextView) view.findViewById(R.id.tv_example);
        btnCreateEvent = (FontButton) view.findViewById(R.id.btn_create_event);
        tvDate = (FontTextView) view.findViewById(R.id.tv_date);

        btnCreateEvent.setOnClickListener(this);
        tvDate.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.img_back:
                mActivity.getSupportFragmentManager().popBackStack();
                break;
            case R.id.img_1:
                launchCamera(CAPTURE_IMG1);
                break;
            case R.id.img_2:
                launchCamera(CAPTURE_IMG2);
                break;
            case R.id.view_start_time:
                Utility.PickTime(tvStartTime,mActivity);
                break;
            case R.id.view_end_time:
                Utility.PickTime(tvEndTime,mActivity);
                break;
            case R.id.tv_date:
                Utility.PickDate(tvDate,mActivity);
                break;
            case R.id.btn_create_event:
                CreateEvent();
                break;
        }
    }




    private void CreateEvent() {

             String title= edTitle.getText().toString().trim();
             String subject= edSubject.getText().toString().trim();
             String description= edDescription.getText().toString().trim();
             String date= tvDate.getText().toString().trim();
             String cpd= edHours.getText().toString().trim();
             String reff= edReff.getText().toString().trim();
             String website= edWebsite.getText().toString().trim();


        if (!mAction.equals("EDIT")&& bmImg1==null && bmImg2==null)
            Utility.ShowAlert(mActivity,"Event Picture","Please provide atleast one Picture for your event");
        else if (title.isEmpty())
            Utility.ShowAlert(mActivity,"Event title","Please provide a suitable title for you event");
        else if (subject.isEmpty())
            Utility.ShowAlert(mActivity,"Event Subject","Please provide a event subject");
        else if (date.trim().isEmpty())
            Utility.ShowAlert(mActivity,"Event Date","Please enter Event date");
        else if (description.trim().isEmpty())
            Utility.ShowAlert(mActivity,"Event Description","Please provide a brief description that what your event is about");
        else
        {

            try {
                FormBody.Builder builder = new FormBody.Builder();


                        builder.add("non_event_name",title);
                        builder.add("non_event_subject",subject);
                        builder.add("non_event_description",description);
                        builder.add("non_event_date",date);
                        builder.add("non_event_cpd",cpd);
                        builder.add("non_event_reff",reff);
                        builder.add("non_event_website",website);
                        builder.add("user_id",mGlobal.mActiveUser.getUser_id());
                        builder.add("non_event_type",ActivityType);

                if (Img1base64!=null)
                    builder.add("non_event_pic1",Img1base64);
                if (Img2base64!=null)
                    builder.add("non_event_pic2", Img2base64);

                String url = Constants.Create_non_attendo_events;
                if (mAction.equals("EDIT"))
                {
                //    if (nonAtEvent.getStatus().equals("complete"))
                        url = Constants.Edit_non_attendo_events;

                    builder.add("non_event_id",nonAtEvent.getNon_event_id());

                }
                
                RequestBody body = builder.build();
               HttpHelper.CallApi(mActivity, url,body,this,true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }




    private void launchCamera(int code) {
        new SandriosCamera(mActivity,this, code)
                .setShowPicker(true)
                //.setVideoFileSize(15) //File Size in MB: Default is no limit
                .setMediaAction(CameraConfiguration.MEDIA_ACTION_PHOTO) // default is CameraConfiguration.MEDIA_ACTION_BOTH
                .enableImageCropping(true) // Default is false.
                .launchCamera();
    }




    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAPTURE_IMG1 || requestCode == CAPTURE_IMG2 && resultCode == mActivity.RESULT_OK) {

            new decodeImage().execute(String.valueOf(requestCode),data.getStringExtra(CameraConfiguration.Arguments.FILE_PATH));

        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

        switch (buttonView.getId())
        {
            case R.id.radio_structured:
                if (isChecked)
                {
                    radioUnstructured.setChecked(false);
                    ActivityType = "structured";
                }
                break;
            case R.id.radio_unstructured:
                if (isChecked)
                {
                    radioStructured.setChecked(false);
                    ActivityType = "unstructured";
                }
                break;
        }
    }




    @Override
    public void onFailure(Call call, final IOException e) {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (e.getMessage()!=null) Toast.makeText(mActivity, e.getMessage().toString(), Toast.LENGTH_SHORT).show();
                else
                Toast.makeText(mActivity,"Request Timeout",Toast.LENGTH_LONG).show();
            }
        });

    }

    @Override
    public void onSuccess(Call call, String response) {
        Log.d("Res~~~~~~~~", response);
        try {
            JSONObject json = new JSONObject(response);
            status = json.getString("status");
            msg = json.getString("message");
        } catch (final JSONException e) {
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(mActivity,e.getMessage(),Toast.LENGTH_LONG).show();
                }
            });
        }
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (status.equals("true"))
                {
                    Bundle bundle = new Bundle();
                    bundle.putInt("POSITION", 2);
                    Utility.ReplaceFragment(new Cpd_MainFragment(),mActivity.getSupportFragmentManager(),bundle);
                }


                Toast.makeText(mActivity,msg,Toast.LENGTH_LONG).show();
            }
        });
    }




    private void SetData() {

        if (nonAtEvent.getPictures()!=null&&nonAtEvent.getPictures().size()>0)
        {
            ImageLoader.getInstance().displayImage(Constants.Img_Base_URL+nonAtEvent.getPictures().get(0).getNon_event_picture(),img1);
            img1.setScaleType(ImageView.ScaleType.CENTER_CROP);
        }

        if (nonAtEvent.getPictures()!=null&&nonAtEvent.getPictures().size()>1)
        {
            img2.setScaleType(ImageView.ScaleType.CENTER_CROP);
            ImageLoader.getInstance().displayImage(Constants.Img_Base_URL+nonAtEvent.getPictures().get(1).getNon_event_picture(),img2);
        }


        edTitle.setText(nonAtEvent.getNon_event_name());
        edSubject.setText(nonAtEvent.getNon_event_subject());

        if (nonAtEvent.getNon_event_type()!=null && nonAtEvent.getNon_event_type().equals("structured"))
        {
            radioStructured.setChecked(true);
            ActivityType = nonAtEvent.getNon_event_type();
            radioUnstructured.setChecked(false);
        }
        else if (nonAtEvent.getNon_event_type()!=null && nonAtEvent.getNon_event_type().equals("unstructured"))
        {
            radioStructured.setChecked(false);
            ActivityType = nonAtEvent.getNon_event_type();
            radioUnstructured.setChecked(true);
        }

        edHours.setText(nonAtEvent.getNon_event_cpd());
        edReff.setText(nonAtEvent.getNon_event_reff());
        edWebsite.setText(nonAtEvent.getNon_event_website());
        tvDate.setText(nonAtEvent.getNon_event_date());
        tvStartTime.setText(nonAtEvent.getNon_event_start_time());
        tvEndTime.setText(nonAtEvent.getNon_event_end_time());
        edDescription.setText(nonAtEvent.getNon_event_description());
    }



    private class decodeImage extends AsyncTask<String,Void,String>
    {



        @Override
        protected String doInBackground(String... params) {
            try {
                if (Integer.parseInt(params[0]) == CAPTURE_IMG1) {

                    ResizeImage resizeImage = new ResizeImage(mContext);
                    String uri = resizeImage.compressImage(params[1]);
                    File imgFile = new File(uri);
                    if (imgFile.exists()) {
                        bmImg1 = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                        Img1base64 = Utility.EncodeTobase64(bmImg1);
                    }

                } else {

                    ResizeImage resizeImage = new ResizeImage(mContext);
                    String uri = resizeImage.compressImage(params[1]);
                    File imgFile = new File(uri);
                    if (imgFile.exists()) {
                        bmImg2 = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                        Img2base64 = Utility.EncodeTobase64(bmImg2);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return params[0];
        }

        @Override
        protected void onPostExecute(String resCode) {
            super.onPostExecute(resCode);
            if (Integer.parseInt(resCode) == CAPTURE_IMG1)
                    img1.setImageBitmap(bmImg1);
                else
                    img2.setImageBitmap(bmImg2);


            }
        }
    }


