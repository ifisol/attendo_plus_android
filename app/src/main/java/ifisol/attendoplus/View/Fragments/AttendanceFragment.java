package ifisol.attendoplus.View.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import fr.arnaudguyon.smartfontslib.FontTextView;
import ifisol.attendoplus.Controller.Adapters.AttendancePagerAdapter;
import ifisol.attendoplus.Global.Globals;
import ifisol.attendoplus.Model.Events;
import ifisol.attendoplus.R;
import ifisol.attendoplus.Utilities.Constants;

public class AttendanceFragment extends Fragment  {


    FragmentActivity mActivity;
    Context  mContext;
    Globals  mGlobal;

    private TabLayout tabs;
    private ViewPager att_vp;
    private AttendancePagerAdapter attendancePagerAdapter;
    FontTextView mTvTitle;

    ImageView imgBack;
    Events event;
    String mEventType;
    int pos;




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.attendance_fragment, null);
    }




    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Init(view);
    }






    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        pos = getArguments().getInt("POSITION");
        event = (Events) getArguments().getSerializable("EVENT");
        mEventType = getArguments().getString(Constants.EVENT_TYPE);
        SetUpViewPager();
        att_vp.setCurrentItem(pos);
        tabs.setOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(att_vp) {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                super.onTabSelected(tab);
                View tabView = (View) tab.getCustomView();
                ImageView tabIcon = (ImageView) tabView.findViewById(R.id.img_tab);
                tabIcon.setImageResource(attendancePagerAdapter.GetActIcon(tab.getPosition()));
                mTvTitle.setText(attendancePagerAdapter.getPageTitle(tab.getPosition()));
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                super.onTabUnselected(tab);
                View tabView = (View) tab.getCustomView();
                ImageView tabIcon = (ImageView) tabView.findViewById(R.id.img_tab);
                tabIcon.setImageResource(attendancePagerAdapter.GetNormalIcon(tab.getPosition()));
            }
            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                super.onTabReselected(tab);
            }
        });







        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivity.getSupportFragmentManager().popBackStack();
            }
        });
    }










    private View Init(View view)
    {
        mActivity = getActivity();
        mContext  = getActivity();
        mGlobal   = (Globals) mActivity.getApplicationContext();
        tabs            = (TabLayout) view.findViewById(R.id.tabs);
        att_vp = (ViewPager) view.findViewById(R.id.attendance_view_pager);
        mTvTitle        = (FontTextView)  view.findViewById(R.id.tv_title);
        imgBack         = (ImageView) view.findViewById(R.id.img_back);
        return view;
    }







    private void SetUpViewPager() {
        attendancePagerAdapter = new AttendancePagerAdapter(getChildFragmentManager(),mContext, event, mEventType);
        att_vp.setAdapter(attendancePagerAdapter);
        tabs.setupWithViewPager(att_vp);
        for (int i = 0; i < tabs.getTabCount(); i++) {
            tabs.getTabAt(i).setCustomView(attendancePagerAdapter.getTabView(i));
        }
        TabLayout.Tab tab = tabs.getTabAt(pos);
        View tabView = (View) tab.getCustomView();
        ImageView tabIcon = (ImageView) tabView.findViewById(R.id.img_tab);
        tabIcon.setImageResource(attendancePagerAdapter.GetActIcon(pos));
        mTvTitle.setText(attendancePagerAdapter.getPageTitle(pos));
    }




}
