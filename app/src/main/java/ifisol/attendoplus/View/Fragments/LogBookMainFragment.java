package ifisol.attendoplus.View.Fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import fr.arnaudguyon.smartfontslib.FontTextView;
import ifisol.attendoplus.Controller.Adapters.LogbookPagerAdapter;
import ifisol.attendoplus.Global.Globals;
import ifisol.attendoplus.R;
import ifisol.attendoplus.View.Activities.ActivityMain;

public class LogBookMainFragment extends Fragment  {


    Activity mActivity;
    Context  mContext;
    Globals  mGlobal;

    private TabLayout tabs;
    private ViewPager logBookPager;
    private LogbookPagerAdapter logbookPagerAdapter;
    private FontTextView mTvTitle;
    int pos;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return Init(inflater.inflate(R.layout.logbook_main_fragment, null));
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        pos = getArguments().getInt("POSITION");
        SetUpViewPager();


        tabs.setOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(logBookPager) {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                super.onTabSelected(tab);
                View tabView = (View) tab.getCustomView();
                ImageView tabIcon = (ImageView) tabView.findViewById(R.id.img_tab);
                tabIcon.setImageResource(logbookPagerAdapter.GetActIcon(tab.getPosition()));
                mTvTitle.setText(logbookPagerAdapter.getPageTitle(tab.getPosition()));
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

                super.onTabUnselected(tab);
                View tabView = (View) tab.getCustomView();
                ImageView tabIcon = (ImageView) tabView.findViewById(R.id.img_tab);
                tabIcon.setImageResource(logbookPagerAdapter.GetNormIcon(tab.getPosition()));
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

                super.onTabReselected(tab);
            }
        });


    }

    private View Init(View view)
    {

        mActivity = getActivity();
        mContext  = getActivity();
        mGlobal   = (Globals) mActivity.getApplicationContext();

        ActivityMain.mTvABTitle.setText("Log Book");

        tabs            = (TabLayout) view.findViewById(R.id.tabs);
        logBookPager    = (ViewPager) view.findViewById(R.id.logbook_pager);
        mTvTitle        = (FontTextView) view.findViewById(R.id.tv_actionbar_title);


        return view;
    }

    private void SetUpViewPager() {

        logbookPagerAdapter = new LogbookPagerAdapter(getChildFragmentManager(),mContext);
        logBookPager.setAdapter(logbookPagerAdapter);

        tabs.setupWithViewPager(logBookPager);

        logBookPager.setCurrentItem(pos);

        for (int i = 0; i < tabs.getTabCount(); i++) {
            tabs.getTabAt(i).setCustomView(logbookPagerAdapter.getTabView(i));
        }

        TabLayout.Tab tab = tabs.getTabAt(pos);
        View tabView = (View) tab.getCustomView();
        ImageView tabIcon = (ImageView) tabView.findViewById(R.id.img_tab);
        tabIcon.setImageResource(logbookPagerAdapter.GetActIcon(pos));
        mTvTitle.setText(logbookPagerAdapter.getPageTitle(pos));

    }

}
