package ifisol.attendoplus.View.Fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.nostra13.universalimageloader.core.ImageLoader;

import fr.arnaudguyon.smartfontslib.FontTextView;
import ifisol.attendoplus.Global.Globals;
import ifisol.attendoplus.Model.User;
import ifisol.attendoplus.R;
import ifisol.attendoplus.Utilities.Constants;

public class Analytics_logbookDetailFragment extends Fragment {


    Activity mActivity;
    Context  mContext;
    Globals  mGlobal;

    private CircularImageView imgUser;
    private FontTextView      tvUsername;
    private FontTextView      tvEmail;
    private FontTextView      tvPhone;
    private WebView           wbvLogbook;

    User user;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.analytics_logbook_detail_fragment, null);
    }



    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Init(view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);

        user = (User) getArguments().getSerializable("USER");
        setData();
    }





    private void setData() {

        ImageLoader.getInstance().displayImage(Constants.Img_Base_URL+user.getPicture(),imgUser);
        tvUsername.setText(user.getUser_name());
        tvEmail.setText(user.getEmail());
        tvPhone.setText(user.getPhone());

        wbvLogbook.loadUrl(Constants.Base_URL+user.getLogbook_url());

    }





    private View Init(View view)
    {

        mActivity = getActivity();
        mContext  = getActivity();
        mGlobal   = (Globals) mActivity.getApplicationContext();

        imgUser = (CircularImageView) view.findViewById(R.id.img_user);
        tvUsername = (FontTextView) view.findViewById(R.id.tv_username);
        tvEmail = (FontTextView) view.findViewById(R.id.tv_email);
        tvPhone = (FontTextView) view.findViewById(R.id.tv_phone);
        wbvLogbook = (WebView) view.findViewById(R.id.wbv_logbook);
        wbvLogbook.getSettings().setJavaScriptEnabled(true);

        wbvLogbook.setWebViewClient(new WebViewClient() {
            ProgressDialog progressDialog;

            //If you will not use this method url links are opeen in new brower not in webview
            public boolean shouldOverrideUrlLoading(WebView view, String url) {

                view.loadUrl(url);
                return true;
            }

            public void onPageFinished(WebView view, String url) {

                try {
                    if (progressDialog.isShowing()) {
                        progressDialog.dismiss();
                        progressDialog = null;
                    }
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            }

            //Show loader on url load
            public void onLoadResource(WebView view, String url) {

                if (progressDialog == null) {
                    // in standard case YourActivity.this
                    progressDialog = new ProgressDialog(mContext);
                    progressDialog.setMessage("Loading...");
                    progressDialog.show();
                }
            }
        });

        return view;
    }

}
