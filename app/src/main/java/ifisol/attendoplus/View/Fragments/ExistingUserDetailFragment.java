package ifisol.attendoplus.View.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.github.siyamed.shapeimageview.RoundedImageView;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import fr.arnaudguyon.smartfontslib.FontButton;
import fr.arnaudguyon.smartfontslib.FontTextView;
import ifisol.attendoplus.Global.Globals;
import ifisol.attendoplus.Model.User;
import ifisol.attendoplus.R;
import ifisol.attendoplus.Utilities.Constants;
import ifisol.attendoplus.Utilities.HttpHelper;
import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.RequestBody;

public class ExistingUserDetailFragment extends Fragment implements View.OnClickListener, HttpHelper.HttpCallback {


    FragmentActivity mActivity;
    Context  mContext;
    Globals  mGlobal;


    private RoundedImageView imgUser;
    private FontTextView     tvUserName;
    private FontTextView     tvCpd;
    private FontTextView     tvFirstName;
    private FontTextView     tvLastName;
    private FontTextView     tvMemNo;
    private FontTextView     tvEmail;
    private FontTextView     tvJobTitle;
    private FontTextView     tvOrganisation;
    private FontTextView     tvAddress;
    private FontTextView     tvPhone;
    private FontButton       btnAttendance;
    ImageView imgBack;

    User user;
    String eventID;




    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.existing_user_detail, null);
    }



    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Init(view);
    }





    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        user = (User) getArguments().getSerializable("USER");
        eventID = getArguments().getString("EVENT_ID");
        SetData(user);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mActivity.getSupportFragmentManager().popBackStack();
            }
        });

    }





    private View Init(View view)
    {
        mActivity = getActivity();
        mContext  = getActivity();
        mGlobal   = (Globals) mActivity.getApplicationContext();
        btnAttendance = (FontButton) view.findViewById(R.id.btn_attendance);
        imgUser = (RoundedImageView) view.findViewById(R.id.img_user);
        tvUserName = (FontTextView) view.findViewById(R.id.tv_user_name);
        tvCpd = (FontTextView) view.findViewById(R.id.tv_cpd);
        tvFirstName = (FontTextView) view.findViewById(R.id.tv_first_name);
        tvLastName = (FontTextView) view.findViewById(R.id.tv_last_name);
        tvMemNo = (FontTextView) view.findViewById(R.id.tv_mem_no);
        tvEmail = (FontTextView) view.findViewById(R.id.tv_email);
        tvJobTitle = (FontTextView) view.findViewById(R.id.tv_job_title);
        tvOrganisation = (FontTextView) view.findViewById(R.id.tv_organisation);
        tvAddress = (FontTextView) view.findViewById(R.id.tv_address);
        tvPhone = (FontTextView) view.findViewById(R.id.tv_phone);
        imgBack  = (ImageView) view.findViewById(R.id.img_back);
        btnAttendance.setOnClickListener(this);
        tvOrganisation.setOnClickListener(this);
        return view;
    }





    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_attendance:
                markAttendance();
                break;

        }
    }






    @Override
    public void onFailure(Call call, IOException e) {
    }
    @Override
    public void onSuccess(Call call, String response) {

        try {
            ParseResponse(response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }




    private void SetData(User user)
    {
        ImageLoader.getInstance().displayImage(Constants.Img_Base_URL+user.getPicture(),imgUser);
        tvUserName.setText(user.getUser_name());
        tvFirstName.setText(user.getFirst_name());
        tvLastName.setText(user.getLast_name());
        tvMemNo.setText(user.getUser_id());
        tvEmail.setText(user.getEmail());
        tvJobTitle.setText(user.getProfession());
        tvOrganisation.setText(user.getOrganisation_name());
        tvAddress.setText(user.getAddress());
        tvPhone.setText(user.getPhone());

        if (mGlobal.mActiveUser.getUser_type().equals(Constants.TAG_ORGANISER))
            tvCpd.setVisibility(View.GONE);
        else
        {
            tvCpd.setVisibility(View.VISIBLE);
            tvCpd.setText(user.getCpd_count());
        }

    }




    private void ParseResponse(String response) throws Exception {

        Log.d("response>>>>>>", response);
        final JSONObject json = new JSONObject(response);
        if (json.getString("status").equals("true"))
        {
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                   mActivity.getSupportFragmentManager().popBackStack();
                }
            });
        }

        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    Toast.makeText(mActivity,json.getString("message"), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }


    private void markAttendance()
    {

        RequestBody body = new FormBody.Builder()
                .add("user_id", mGlobal.mActiveUser.getUser_id())
                .add("guest_id", user.getUser_id())
                .add("event_id", eventID)
                .add("device_type", "Android")
                .add("tag","existing").build();
        try {
            HttpHelper.CallApi(mActivity,Constants.Mark_attendance,body,this,true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
