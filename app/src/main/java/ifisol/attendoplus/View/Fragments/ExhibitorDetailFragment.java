package ifisol.attendoplus.View.Fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONObject;

import java.io.IOException;

import fr.arnaudguyon.smartfontslib.FontTextView;
import ifisol.attendoplus.Global.Globals;
import ifisol.attendoplus.Model.Exhibitors;
import ifisol.attendoplus.R;
import ifisol.attendoplus.Utilities.Constants;
import ifisol.attendoplus.Utilities.HttpHelper;
import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.RequestBody;

public class ExhibitorDetailFragment extends Fragment implements View.OnClickListener, HttpHelper.HttpCallback {


    Activity mActivity;
    Context  mContext;
    Globals  mGlobal;

    String ExhibitorID;
    Exhibitors exhibitor;

    private CircularImageView imgUser;
    private FontTextView      tvUsername;
    private FontTextView      tvPhone;
    private FontTextView      tvScanCount;
    private FontTextView      tvWebsite;
    private FontTextView      tvEmail;
    private FontTextView      tvDescription;
    View blankView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.exhibitors_detail, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Init(view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ExhibitorID = getArguments().getString("EXHIBITOR_ID");

        RequestBody body = new FormBody.Builder().add("user_id",mGlobal.mActiveUser.getUser_id())
                .add("exhibitor_id",ExhibitorID)
                .add("tag","exhibitor_detail").build();
        try {
            HttpHelper.CallApi(mActivity,Constants.Exhibitor_detail,body,this,true);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private View Init(View view)
    {

        mActivity = getActivity();
        mContext  = getActivity();
        mGlobal   = (Globals) mActivity.getApplicationContext();

        imgUser = (CircularImageView) view.findViewById(R.id.img_user);
        tvUsername = (FontTextView) view.findViewById(R.id.tv_username);
        tvPhone = (FontTextView) view.findViewById(R.id.tv_phone);
        tvScanCount = (FontTextView) view.findViewById(R.id.tv_scan_count);
        tvWebsite   = (FontTextView) view.findViewById(R.id.tv_website);
        tvEmail  = (FontTextView) view.findViewById(R.id.tv_email);
        tvDescription = (FontTextView) view.findViewById(R.id.tv_description);
        blankView = view.findViewById(R.id.view_blank);

        return view;
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onFailure(Call call, IOException e) {

    }

    @Override
    public void onSuccess(Call call, String response) {

        try {
            ParseDetail(response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }





    private void ParseDetail(String s) throws Exception {
        Log.d("Response........",s);
        final JSONObject jsonObj = new JSONObject(s);

        if (jsonObj.getString("status").equals("true"))
        {
            Gson gson = new Gson();
            exhibitor = gson.fromJson(jsonObj.getString("response"),Exhibitors.class);
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    SetData(exhibitor);
                }
            });
        }

    }




    private void SetData(Exhibitors exhibitor) {
        blankView.setVisibility(View.GONE);
        ImageLoader.getInstance().displayImage(Constants.Img_Base_URL+exhibitor.getExhibitor_picture(),imgUser);
        tvUsername.setText(exhibitor.getExhibitor_name());
        tvPhone.setText(exhibitor.getExhibitor_phone());
        tvScanCount.setText(exhibitor.getExhb_visit_count()+" Scans");
        tvWebsite.setText(exhibitor.getExhibitor_website());
        tvEmail.setText(exhibitor.getExhibitor_email());
        tvDescription.setText(exhibitor.getExhibitor_description());
    }
}

