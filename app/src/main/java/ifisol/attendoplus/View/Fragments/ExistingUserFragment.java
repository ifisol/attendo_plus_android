package ifisol.attendoplus.View.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import ifisol.attendoplus.Controller.Adapters.GuestsAdapter;
import ifisol.attendoplus.Global.Globals;
import ifisol.attendoplus.Model.Events;
import ifisol.attendoplus.Model.User;
import ifisol.attendoplus.R;
import ifisol.attendoplus.Utilities.Constants;
import ifisol.attendoplus.Utilities.HttpHelper;
import ifisol.attendoplus.Utilities.Utility;
import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.RequestBody;

public class ExistingUserFragment extends Fragment implements HttpHelper.HttpCallback {

    FragmentActivity mActivity;
    Context  mContext;
    Globals  mGlobal;

    RecyclerView GuestRV;
    GuestsAdapter guestsAdapter;

    Events event;
    ImageView imgNoRecord;
    ArrayList<User> mGuestList;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.existing_users_fragment, null);

        return Init(v);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        event = (Events) getArguments().getSerializable("EVENT");

            FetchEventGuest();



    }





    private View Init(View v)
    {
        mActivity = getActivity();
        mContext  = getActivity();
        mGlobal   = (Globals) mActivity.getApplicationContext();
        imgNoRecord = (ImageView) v.findViewById(R.id.img_no_record);

        GuestRV = (RecyclerView) v.findViewById(R.id.rv_guest);

        return v;
    }





    private void SetAdapter()
    {
        GuestRV.setHasFixedSize(true);
        GuestRV.setLayoutManager(new LinearLayoutManager(mContext));
        guestsAdapter = new GuestsAdapter(mContext, mActivity, this, mGuestList);
        GuestRV.setAdapter(guestsAdapter);

        guestsAdapter.setItemClick(new GuestsAdapter.ItemClick() {
            @Override
            public void onItemClick(int position, Object user) {
                Bundle bundle = new Bundle();
                bundle.putSerializable("USER",(User) user);
                bundle.putSerializable("EVENT_ID",event.getEvent_id());
                Utility.ReplaceFragment(new ExistingUserDetailFragment(),mActivity.getSupportFragmentManager(),bundle);

            }
        });
    }








    private void FetchEventGuest() {

        RequestBody body = new FormBody.Builder()
                .add("user_id", mGlobal.mActiveUser.getUser_id())
                .add("event_id", event.getEvent_id())
                .add("org_id",event.getEvent_organisation_id()).build();
        try {
            HttpHelper.CallApi(mActivity, Constants.Get_existing_guests, body, this, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }










    @Override
    public void onFailure(Call call, IOException e) {

        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                Toast.makeText(mActivity, "Request time out", Toast.LENGTH_SHORT).show();
                imgNoRecord.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void onSuccess(Call call, String response) {

        try {
            ParseGuests(response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void ParseGuests(String res) throws Exception {

        Log.d("Res~~~~~~~~~~~~~~~~~~~~", res);
        JSONObject jsonObj = new JSONObject(res);
        if (jsonObj.getString("status").equals("true"))
        {
            Gson gson = new Gson();
            User[] users = gson.fromJson(jsonObj.getString("response"),User[].class);
            mGuestList = new ArrayList<>(Arrays.asList(users));

        }

        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (mGuestList!=null && mGuestList.size()>0)
                {
                    SetAdapter();
                    imgNoRecord.setVisibility(View.GONE);
                }
                else
                    imgNoRecord.setVisibility(View.VISIBLE);
            }
        });
    }
}