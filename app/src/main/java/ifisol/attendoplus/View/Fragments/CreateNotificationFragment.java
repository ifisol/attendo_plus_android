package ifisol.attendoplus.View.Fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.gson.Gson;
import com.sandrios.sandriosCamera.internal.SandriosCamera;
import com.sandrios.sandriosCamera.internal.configuration.CameraConfiguration;
import com.yalantis.taurus.PullToRefreshView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import co.lujun.androidtagview.TagContainerLayout;
import fr.arnaudguyon.smartfontslib.FontButton;
import fr.arnaudguyon.smartfontslib.FontEditText;
import fr.arnaudguyon.smartfontslib.FontTextView;
import ifisol.attendoplus.Controller.Adapters.NotificationRecepientsAdapter;
import ifisol.attendoplus.Controller.Adapters.NotificationTypeAdapter;
import ifisol.attendoplus.Global.Globals;
import ifisol.attendoplus.Model.NotificationRecepients;
import ifisol.attendoplus.Model.Notification_types;
import ifisol.attendoplus.Model.Organisations;
import ifisol.attendoplus.R;
import ifisol.attendoplus.Utilities.Constants;
import ifisol.attendoplus.Utilities.HttpHelper;
import ifisol.attendoplus.Utilities.ResizeImage;
import ifisol.attendoplus.Utilities.Utility;
import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.RequestBody;

public class CreateNotificationFragment extends Fragment implements View.OnClickListener, HttpHelper.HttpCallback {


    FragmentActivity mActivity;
    Context  mContext;
    Globals  mGlobal;



    private ImageView imgBack;
    private ImageView imgSelectImg;
    private CircularImageView imgUser;
    private FontEditText edTitle;
    private FontEditText edSubject;
    private FontTextView tvCategory;
    private FontEditText edDescription;
    private FontButton   btnSend;
    ArrayList<Notification_types> typeList;
    ArrayList<NotificationRecepients> recepients;
    StringBuilder recepientsIDs;
    String typeId="";

    TagContainerLayout mTagContainerLayout;
    private View       viewRecepients;
    FontTextView       tvRecepients;

    Bitmap UserImage;
    String imgBase64;

    private static final int CAPTURE_MEDIA = 368;





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return Init(inflater.inflate(R.layout.create_notification_fragment, null));
    }





    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }





    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);

        getNotiTypes();
        FetchRecepients(false);

        imgBack.setOnClickListener(this);
        btnSend.setOnClickListener(this);
        tvCategory.setOnClickListener(this);
        imgSelectImg.setOnClickListener(this);
        tvRecepients.setOnClickListener(this);
    }

    private View Init(View view)
    {
        mActivity = getActivity();
        mContext  = getActivity();
        mGlobal   = (Globals) mActivity.getApplicationContext();
        imgBack   = (ImageView) view.findViewById(R.id.img_back);

        imgSelectImg      = (ImageView) view.findViewById(R.id.img_select_img);
        imgUser           = (CircularImageView) view.findViewById(R.id.img_user);
        edTitle = (FontEditText) view.findViewById(R.id.ed_title);
        tvCategory = (FontTextView) view.findViewById(R.id.tv_category);
        edDescription = (FontEditText) view.findViewById(R.id.ed_description);
        edSubject = (FontEditText) view.findViewById(R.id.ed_subject);
        btnSend = (FontButton) view.findViewById(R.id.btn_send);

        mTagContainerLayout = (TagContainerLayout)view.findViewById(R.id.tag_layout);
        viewRecepients = view.findViewById(R.id.view_recepients);
        tvRecepients = (FontTextView)view.findViewById(R.id.tv_recepients);

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btn_send:
                sendNotification();
                break;
            case R.id.img_back:
                mActivity.getSupportFragmentManager().popBackStack();
                break;
            case R.id.tv_category:
                if (typeList==null || typeList.size()==0)
                    Utility.ShowAlert(mActivity, "Notification Categories","Notification Categories are not available please try again later");
                else
                    ShowTypes();
                break;
            case R.id.img_select_img:
                launchCamera();
                break;
            case R.id.tv_recepients:
                if (recepients==null || recepients.size()==0)
                    Utility.ShowAlert(mActivity, "Recepients","Recepients are not available please try again later");
                else
                   recepients();
                break;
        }
    }



    private void getNotiTypes()
    {
        RequestBody body = new FormBody.Builder()
                .add("user_id",mGlobal.mActiveUser.getUser_id()).build();
        try {
            HttpHelper.CallApi(mActivity, Constants.Get_notification_type,body,this,false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    private void sendNotification() {


        String title = edTitle.getText().toString().trim();
        String subject = edSubject.getText().toString().trim();
        String desc = edDescription.getText().toString().trim();

        if (title.isEmpty())
            Utility.ShowAlert(mActivity,"Notification Title","Please enter notification title");
        else if (typeId.isEmpty())
            Utility.ShowAlert(mActivity,"Notification Category","Please select a notification category");
        else if (desc.isEmpty())
            Utility.ShowAlert(mActivity,"Description","Please briefly describe your notification");
        else
        {
            FormBody.Builder builder = new FormBody.Builder();
                    builder.add("user_id",mGlobal.mActiveUser.getUser_id());
                    builder.add("notification_name",title);
                    builder.add("notification_subject",subject);
                    builder.add("notification_type_id",typeId);
                    builder.add("notification_description",desc);
                  if (imgBase64!=null)
                        builder.add("notification_picture",imgBase64);
                    builder.add("guest_id",recepientsIDs==null?"":recepientsIDs.toString());
            RequestBody body = builder.build();
            try {
                HttpHelper.CallApi(mActivity, Constants.create_notification,body,this,true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }








    public void ShowTypes() {

        final Dialog dialog = new Dialog(mActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        dialog.setContentView(R.layout.event_types_fragment);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        DisplayMetrics dm = new DisplayMetrics();
        mActivity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER | Gravity.START);
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        //lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        FontTextView tvTitle = (FontTextView) dialog.findViewById(R.id.tv_title);
        tvTitle.setText("Notification Types");
        RecyclerView mEventType_rv;
        mEventType_rv = (RecyclerView) dialog.findViewById(R.id.rv_event_type);
        mEventType_rv.setHasFixedSize(true);
        mEventType_rv.setLayoutManager(new LinearLayoutManager(mContext));

        final NotificationTypeAdapter TypeAdapter = new NotificationTypeAdapter(mContext, mActivity, typeList);
            mEventType_rv.setAdapter(TypeAdapter);
         TypeAdapter.setOnItemClick(new NotificationTypeAdapter.OnItemClicked() {
                @Override
                public void onClick(int pos) {

                    for (int i = 0; i < TypeAdapter.mFilterArray.size(); i++) {
                        TypeAdapter.mFilterArray.get(i).setSelected(false);
                    }
                    for (int i = 0; i < TypeAdapter.mOriginalArray.size(); i++) {
                        TypeAdapter.mOriginalArray.get(i).setSelected(false);
                    }
                    if (pos==0)
                        viewRecepients.setVisibility(View.GONE);
                    else
                        viewRecepients.setVisibility(View.VISIBLE);
                    TypeAdapter.mFilterArray.get(pos).setSelected(true);
                    TypeAdapter.notifyDataSetChanged();
                    typeId = TypeAdapter.mFilterArray.get(pos).getNotification_type_id();
                    tvCategory.setText(TypeAdapter.mFilterArray.get(pos).getNotification_type_name());
                    dialog.dismiss();
                }
            });


        FontEditText mTxtSearch = (FontEditText) dialog.findViewById(R.id.ed_search);
        mTxtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                TypeAdapter.FilterList(s.toString());

            }
        });


        dialog.show();
    }




    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CAPTURE_MEDIA && resultCode == mActivity.RESULT_OK) {
            try {

                ResizeImage resizeImage = new ResizeImage(mContext);
                String uri = resizeImage.compressImage(data.getStringExtra(CameraConfiguration.Arguments.FILE_PATH));
                File imgFile = new File(uri);
                if (imgFile.exists()) {
                    UserImage = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                    imgUser.setImageBitmap(UserImage);
                    imgBase64 = Utility.EncodeTobase64(UserImage);
                }


                Log.e("File >>>>>>>>>>>>>", "" + data.getStringExtra(CameraConfiguration.Arguments.FILE_PATH));
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }


    private void launchCamera() {
        new SandriosCamera(mActivity,this, CAPTURE_MEDIA)
                .setShowPicker(true)
                //.setVideoFileSize(15) //File Size in MB: Default is no limit
                .setMediaAction(CameraConfiguration.MEDIA_ACTION_PHOTO) // default is CameraConfiguration.MEDIA_ACTION_BOTH
                .enableImageCropping(true) // Default is false.
                .launchCamera();
    }


    public void recepients() {
       final Dialog dialog = new Dialog(mActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        dialog.setContentView(R.layout.recepient_dialog);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        DisplayMetrics dm = new DisplayMetrics();
        mActivity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER | Gravity.START);
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        //lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        RecyclerView recpient_rv;
        FontTextView doneTv = (FontTextView) dialog.findViewById(R.id.tv_done);

        recpient_rv = (RecyclerView) dialog.findViewById(R.id.rv_recep);
        recpient_rv.setHasFixedSize(true);
        recpient_rv.setLayoutManager(new LinearLayoutManager(mContext));
        final NotificationRecepientsAdapter recp_adapter = new NotificationRecepientsAdapter(mActivity,recepients);
        recpient_rv.setAdapter(recp_adapter);


        doneTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String sep = "";
                recepientsIDs = new StringBuilder();
                List<String> mTagList = new ArrayList<String>();

                for (NotificationRecepients recepient : recp_adapter.mFilterArray )
                {
                    if (recepient.isSelect())
                    {
                        mTagList.add(recepient.getFirst_name()+" "+recepient.getLast_name());
                        recepientsIDs.append(sep);
                        recepientsIDs.append(recepient.getUser_id());
                        sep = ",";
                    }

                }
                mTagContainerLayout.setTags(mTagList);
                dialog.dismiss();
            }

        });


        FontEditText mTxtSearch = (FontEditText) dialog.findViewById(R.id.ed_search);
        mTxtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                recp_adapter.FilterList(s.toString());

            }
        });


        dialog.show();
    }



    private void FetchRecepients(boolean loader) {
        try {
            RequestBody formBody = new FormBody.Builder().add("org_id",mGlobal.mActiveUser.getOrganisation_id()).build();
            HttpHelper.CallApi(mActivity,Constants.Organisation_guest,formBody,this,loader);
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("Exception...........",e.getMessage());
        }
    }



    @Override
    public void onFailure(Call call, final IOException e) {

        if (call.request().url().toString().equals(Constants.create_notification))
        {
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    Toast.makeText(mActivity,"Request Timeout", Toast.LENGTH_SHORT).show();
                }
            });
        }

    }



    @Override
    public void onSuccess(Call call, final String response) {

        if (call.request().url().toString().equals(Constants.create_notification)) {
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    try {
                        JSONObject json = new JSONObject(response);
                        if (json.getString("status").equals("true"))
                        {
                            Bundle bundle = new Bundle();
                            bundle.putInt("POSITION", 1);
                            Utility.ReplaceFragment(new Notifications_MainFragment(),mActivity.getSupportFragmentManager(), bundle);
                        }


                        Toast.makeText(mActivity, json.getString("message"), Toast.LENGTH_SHORT).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });
        }
        else if (call.request().url().toString().equals(Constants.Get_notification_type))
            parseTypes(response);
        else if (call.request().url().toString().equals(Constants.Organisation_guest))
                parseRecepients(response);

    }

    private void parseTypes(String res) {
        try {
            JSONObject json = new JSONObject(res);
            Gson gson =new Gson();
            Notification_types[] types = gson.fromJson(json.getString("response"),Notification_types[].class);
            typeList = new ArrayList<>(Arrays.asList(types));

        }
        catch (Exception e)
        {

        }
    }



    private void parseRecepients(String res) {
        try {
            JSONObject json = new JSONObject(res);
            Gson gson =new Gson();
            NotificationRecepients[] recepient = gson.fromJson(json.getString("response"),NotificationRecepients[].class);
            recepients = new ArrayList<>(Arrays.asList(recepient));

        }
        catch (Exception e)
        {

        }
    }


}
