package ifisol.attendoplus.View.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import fr.arnaudguyon.smartfontslib.FontTextView;
import ifisol.attendoplus.Global.Globals;
import ifisol.attendoplus.Model.Events;
import ifisol.attendoplus.R;
import ifisol.attendoplus.Utilities.CreateEventStepsListener;
import ifisol.attendoplus.Utilities.Utility;

public class CreateEventMainFragment extends Fragment implements CreateEventStepsListener {


    FragmentActivity mActivity;
    Context  mContext;
    Globals  mGlobal;

    ArrayList<FontTextView> mTvList;
    ArrayList<View> mLines;

    private FontTextView tv1;
    private FontTextView tv2;
    private FontTextView tv3;
    private FontTextView tv4;
    private FontTextView tv5;
    private View line1;
    private View line2;
    private View line3;
    private View line4;

    public static View viewSteps;
    public static View header;

    public static ImageView imgBack;
    View view;
    public static boolean exitEditing;
    TextView tvTitle;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        mActivity = getActivity();
        mContext  = getActivity();
        mGlobal   = (Globals) mActivity.getApplicationContext();

        if (getArguments()!=null)
        {
            Bundle bundle = new Bundle();
            bundle.putSerializable("EVENT", (Events) getArguments().getSerializable("EVENT"));
            Utility.ReplaceFragment_create_event(new CreateEventFragment_1(),getChildFragmentManager(),bundle);

        }
        else
        {
            Utility.ReplaceFragment_create_event(new CreateEventFragment_1(),getChildFragmentManager());
        }



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view==null)
        {
            view = Init(inflater.inflate(R.layout.create_event_main_fragment, null));
        }
        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);

        if (getArguments()!=null)
            tvTitle.setText("Edit Event");
        else
            tvTitle.setText("Create Event");


    }


    private View Init(View view)
    {
        tv1       = (FontTextView) view.findViewById(R.id.tv_1);
        tv2       = (FontTextView) view.findViewById(R.id.tv_2);
        tv3       = (FontTextView) view.findViewById(R.id.tv_3);
        tv4       = (FontTextView) view.findViewById(R.id.tv_4);
        tv5       = (FontTextView) view.findViewById(R.id.tv_5);
        tvTitle   = (FontTextView) view.findViewById(R.id.tv_actionbar_title);
        line1     = (View) view.findViewById(R.id.line_1);
        line2     = (View) view.findViewById(R.id.line_2);
        line3     = (View) view.findViewById(R.id.line_3);
        line4     = (View) view.findViewById(R.id.line_4);
        imgBack   = (ImageView) view.findViewById(R.id.img_back);

        header    = view.findViewById(R.id.header);
        viewSteps = view.findViewById(R.id.view_steps);

        mTvList =new ArrayList<>();
        mTvList.add(tv1);
        mTvList.add(tv2);
        mTvList.add(tv3);
        mTvList.add(tv4);
        mTvList.add(tv5);

        mLines = new ArrayList<>();
        mLines.add(line1);
        mLines.add(line2);
        mLines.add(line3);
        mLines.add(line4);


        return view;
    }

    @Override
    public void UpdateSteps(int pageNo) {

        if (pageNo==-1) // used while editing event to provide back option from editing
        {
            imgBack.setVisibility(View.VISIBLE);
            pageNo=0;
        }
        else
        {
            if (pageNo==0)
                imgBack.setVisibility(View.GONE);
            else
                imgBack.setVisibility(View.VISIBLE  );
        }


        mTvList.get(pageNo).setSelected(true);
        for (int i = pageNo + 1; i < mTvList.size(); i++) {
            mTvList.get(i).setSelected(false);
        }
        if (pageNo != 0) {
            pageNo = pageNo - 1;
            mLines.get(pageNo).setBackgroundColor(getResources().getColor(R.color.cpd_green));
            for (int i = pageNo + 1; i < mLines.size(); i++) {
                    mLines.get(i).setBackgroundColor(getResources().getColor(R.color.light_grey));
            }
        }
        else
        {
            for (int i = pageNo ; i < mLines.size(); i++) {
                    mLines.get(i).setBackgroundColor(getResources().getColor(R.color.light_grey));
            }
        }


    }
}
