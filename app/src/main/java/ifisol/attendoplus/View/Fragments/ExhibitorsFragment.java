package ifisol.attendoplus.View.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.yalantis.taurus.PullToRefreshView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import ifisol.attendoplus.Controller.Adapters.ExhibitorsGridAdapter;
import ifisol.attendoplus.Global.Globals;
import ifisol.attendoplus.Model.Events;
import ifisol.attendoplus.Model.Exhibitors;
import ifisol.attendoplus.R;
import ifisol.attendoplus.Utilities.Constants;
import ifisol.attendoplus.Utilities.HttpHelper;
import ifisol.attendoplus.Utilities.ItemDecorationAlbumColumns;
import ifisol.attendoplus.Utilities.Utility;
import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.RequestBody;

public class ExhibitorsFragment extends Fragment implements HttpHelper.HttpCallback, ExhibitorsGridAdapter.clickListener {

    FragmentActivity mActivity;
    Context  mContext;
    Globals mGlobal;

    RecyclerView mExhibitorsRV;
    ExhibitorsGridAdapter exhibitorsGridAdapter;
    ImageView imgNoRecord;
    View HeaderView;

    String mEventType ;
    Events mEvent ;

    ArrayList<Exhibitors> mExhibList;
    PullToRefreshView mPullToRefreshView;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.exhibitors_fragment, null);

        return Init(v);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (getArguments()!=null)
        {
            HeaderView.setVisibility(View.GONE);
            mEventType = getArguments().getString(Constants.EVENT_TYPE);
            mEvent     = (Events)getArguments().getSerializable("EVENT");
            FetchEventExhibitors(false);
        }
        else
        {
          HeaderView.setVisibility(View.VISIBLE);
            FetchAllExhibitors(true);
        }




        mPullToRefreshView.setOnRefreshListener(new PullToRefreshView.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPullToRefreshView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (getArguments()!=null)
                            FetchEventExhibitors(false);
                        else
                            FetchAllExhibitors(false);

                    }
                },500);
            }
        });



        mExhibitorsRV.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                super.onScrolled(recyclerView, dx, dy);
                if (dy<=0)
                mPullToRefreshView.setEnabled(true);
                else
                    mPullToRefreshView.setEnabled(false);
            }
        });





    }






    private void FetchEventExhibitors(boolean loader) {
        String url="";
        FormBody.Builder builder = new FormBody.Builder();
        builder.add("user_id",mGlobal.mActiveUser.getUser_id());
        builder.add("event_id", mEvent.getEvent_id());
        builder.add("status", "mm");
        if (mGlobal.mActiveUser.getUser_type().equals(Constants.TAG_ORGANISER))
        {
            builder.add("tag", "org_event_exhibitors");
            url = Constants.Org_event_detail;
        }
        else
        {
            builder.add("tag", "guest_event_exhibitors");
            url = Constants.Guest_event_detail;
        }

        RequestBody formBody = builder.build();
       try {
            HttpHelper.CallApi(mActivity,url,formBody,this,loader);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }




    private void FetchAllExhibitors(boolean loader) {
        RequestBody formBody = new FormBody.Builder()
                .add("user_id",mGlobal.mActiveUser.getUser_id())
                .build();
       try {
            HttpHelper.CallApi(mActivity,Constants.Get_Exhibitors,formBody,this,loader);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }




    private View Init(View v)
    {
        mActivity = getActivity();
        mContext  = getActivity();
        mGlobal   = (Globals)mContext.getApplicationContext();

        mExhibitorsRV = (RecyclerView) v.findViewById(R.id.rv_exhibitors);
        imgNoRecord   = (ImageView) v.findViewById(R.id.img_no_record);
        mPullToRefreshView = (PullToRefreshView) v.findViewById(R.id.pull_to_refresh);
        HeaderView    = v.findViewById(R.id.header);

        mExhibitorsRV.setHasFixedSize(true);
        mExhibitorsRV.setLayoutManager(new GridLayoutManager(mContext,2));
        exhibitorsGridAdapter = new ExhibitorsGridAdapter(mActivity,this,new ArrayList<Exhibitors>());
        mExhibitorsRV.setAdapter(exhibitorsGridAdapter);
        mExhibitorsRV.addItemDecoration(new ItemDecorationAlbumColumns(getResources().
                getDimensionPixelSize(R.dimen.dp_1), 2));
        exhibitorsGridAdapter.setClickListener(this);

        return v;
    }




    private void SetAdapter(ArrayList<Exhibitors> mList)
    {
        mPullToRefreshView.setRefreshing(false);
        if (mList!=null && mList.size()>0)
        {
            imgNoRecord.setVisibility(View.GONE);
            exhibitorsGridAdapter.mExList.clear();
            exhibitorsGridAdapter.mExList=mList;
            exhibitorsGridAdapter.notifyDataSetChanged();

        }
        else
            imgNoRecord.setVisibility(View.VISIBLE);

    }





    @Override
    public void onFailure(Call call, IOException e) {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                SetAdapter(mExhibList);
            }
        });
    }

    @Override
    public void onSuccess(Call call, String response) {
        try {
            Log.d("Response . . . . . .", response);
            Gson gson = new Gson();
            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.getString("status").equals("true"))
            {
                final Exhibitors[] exhibitors = gson.fromJson(jsonObject.getString("response"), Exhibitors[].class);
                mExhibList = new ArrayList<>(Arrays.asList(exhibitors));
            }

            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    SetAdapter(mExhibList);
                }
            });


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onItemClick(int pos, Exhibitors obj) {

        if (mEventType!=null && mEventType.equals(Constants.TAG_ANALYTIC_EVENTS))
        {
            Bundle bundle = new Bundle();
            bundle.putSerializable("EXHIBITOR",obj);
            bundle.putSerializable("EVENT_ID",mEvent.getEvent_id());
            Utility.ReplaceFragment(new Analytics_ExhibitorDetailFragment(), mActivity.getSupportFragmentManager(),bundle);
        }
        else
        {
            Bundle bundle = new Bundle();
            bundle.putString("EXHIBITOR_ID",obj.getExhibitor_id());
            Utility.ReplaceFragment(new ExhibitorDetail_MainFragment(), mActivity.getSupportFragmentManager(),bundle);
        }
    }
}