package ifisol.attendoplus.View.Fragments;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.os.Bundle;
import android.widget.AbsListView;
import android.widget.ImageView;

import ifisol.attendoplus.Controller.Adapters.NotificationsAdapter;
import ifisol.attendoplus.Global.Globals;
import ifisol.attendoplus.Model.Notifications;
import ifisol.attendoplus.R;
import ifisol.attendoplus.Utilities.Constants;
import ifisol.attendoplus.Utilities.HttpHelper;
import ifisol.attendoplus.Utilities.Utility;
import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.RequestBody;

import android.widget.ExpandableListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.yalantis.taurus.PullToRefreshView;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import static ifisol.attendoplus.R.id.map;

public class NotificationsFragment extends Fragment implements View.OnClickListener, HttpHelper.HttpCallback {


    FragmentActivity mActivity;
    Context mContext;
    Globals mGlobal;

    ImageView imgNoRecord;
    private ExpandableListView notificationList;

    NotificationsAdapter listAdapter;
    FloatingActionButton fab_add;

    HashMap<String, ArrayList<Notifications>> NotificationRecHM;
    HashMap<String, ArrayList<Notifications>> NotificationSentHM;
    ArrayList<String> RecheaderList;
    ArrayList<String> SentheaderList;

    PullToRefreshView mPullToRefreshView;
    View view;
    String notiType;
    View header;




    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        mActivity = getActivity();
        mContext  = getActivity();
        mGlobal   = (Globals) mActivity.getApplicationContext();
        notiType = getArguments().getString("NOTIFICATION_TYPE");

        if (notiType.equals("Received"))
            FetchNotification(true);
        else
            FetchNotification(false);
    }





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view==null)
            view = Init(inflater.inflate(R.layout.notifications_fragment, null));

        return view;
    }










    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        fab_add.setOnClickListener(this);
        if (mGlobal.mActiveUser.getUser_type().equals(Constants.TAG_GUEST))
        {
            header.setVisibility(View.VISIBLE);
            fab_add.setVisibility(View.GONE);
        }

        else
        {
            fab_add.setVisibility(View.VISIBLE);
            header.setVisibility(View.GONE);
        }





        mPullToRefreshView.setOnRefreshListener(new PullToRefreshView.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPullToRefreshView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        FetchNotification(false);
                    }
                },500);
            }
        });


        notificationList.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem == 0) {
                    mPullToRefreshView.setEnabled(true);
                } else mPullToRefreshView.setEnabled(false);
            }
        });




    }








    private View Init(View view)
    {
        imgNoRecord      = (ImageView) view.findViewById(R.id.img_no_record);
        notificationList = (ExpandableListView) view.findViewById(R.id.notification_list);
        fab_add          = (FloatingActionButton) view.findViewById(R.id.fab_add);
        mPullToRefreshView = (PullToRefreshView) view.findViewById(R.id.pull_to_refresh);
        header = view.findViewById(R.id.header);


        return view;
    }





    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.fab_add:
                Utility.ReplaceFragment(new CreateNotificationFragment(), mActivity.getSupportFragmentManager());
                break;
        }
    }







    private void FetchNotification(boolean loader)
    {RequestBody body = new FormBody.Builder().add("user_id", mGlobal.mActiveUser.getUser_id()).build();
        try {
            HttpHelper.CallApi(mActivity,Constants.Get_Notifications,body,this,loader);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }






    @Override
    public void onFailure(Call call, final IOException e) {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                imgNoRecord.setVisibility(View.VISIBLE);
                Toast.makeText(mActivity,"Request Timeout", Toast.LENGTH_SHORT).show();
            }
        });
    }







    @Override
    public void onSuccess(Call call, String response) {
        try {

            ParseNotifications(response);

            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mPullToRefreshView.setRefreshing(false);
                }
            });
        } catch (final Exception e) {
            e.printStackTrace();
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    imgNoRecord.setVisibility(View.VISIBLE);
                    Toast.makeText(mActivity, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }






    private void ParseNotifications(String response) throws Exception {
        JSONObject json = new JSONObject(response);

        if (json.getString("status").equals("true"))
        {
            Gson gson = new Gson();
            Notifications[] notifications = gson.fromJson(json.getString("all_notifications"),Notifications[].class);
            ArrayList<Notifications> mNotiList = new ArrayList<>(Arrays.asList(notifications));

            if (notiType.equals("Received"))
            {
                NotificationRecHM = new HashMap<>();
                RecheaderList = new ArrayList<>();

                for (Notifications obj : mNotiList)
                {
                    HashMap<String, ArrayList<Notifications>> termHM = new HashMap<>();
                    for (Notifications obj1 : obj.getNotifications())
                    {
                        if (obj1.getIs_my_created().equals("no"))
                        {
                            termHM.put(obj.getNotification_type_name(),obj.getNotifications());
                        }
                    }

                    if (termHM.size()>0)
                    {
                        RecheaderList.add(obj.getNotification_type_name());
                        NotificationRecHM.putAll(termHM);
                    }


                }
            }
            else if (notiType.equals("Sent"))
            {
                NotificationSentHM = new HashMap<>();
                SentheaderList = new ArrayList<>();

                for (Notifications obj : mNotiList)
                {
                    HashMap<String, ArrayList<Notifications>> termHM = new HashMap<>();
                    for (Notifications obj1 : obj.getNotifications())
                    {
                        if (obj1.getIs_my_created().equals("yes"))
                        {
                            termHM.put(obj.getNotification_type_name(),obj.getNotifications());
                        }
                    }

                    if (termHM.size()>0)
                    {
                        SentheaderList.add(obj.getNotification_type_name());
                        NotificationSentHM.putAll(termHM);
                    }


                }
            }

        }


        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (notiType.equals("Received"))
                {
                    if (NotificationRecHM.size()>0 )
                    {
                        SetAdapter(RecheaderList, NotificationRecHM);
                        imgNoRecord.setVisibility(View.GONE);
                    }

                    else
                    {

                        imgNoRecord.setVisibility(View.VISIBLE);
                    }
                }
                else if (notiType.equals("Sent"))
                {
                    if (NotificationSentHM.size()>0 )
                    {
                        SetAdapter(SentheaderList, NotificationSentHM);
                        imgNoRecord.setVisibility(View.GONE);
                    }

                    else
                    {

                        imgNoRecord.setVisibility(View.VISIBLE);
                    }
                }


            }
        });
    }







    private void SetAdapter(ArrayList<String> headerList, HashMap<String, ArrayList<Notifications>> notificationHM) {
        mPullToRefreshView.setRefreshing(false);
        listAdapter = new NotificationsAdapter(mContext, headerList, notificationHM);
        notificationList.setAdapter(listAdapter);
        for (int i=0 ;i<listAdapter.getGroupCount();i++)
            notificationList.expandGroup(i);
        notificationList.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                Bundle bundle = new Bundle();
                bundle.putSerializable("NOTIFICATION",(Notifications)listAdapter.getChild(groupPosition,childPosition));
                Utility.ReplaceFragment(new NotificationDetailFragment(), mActivity.getSupportFragmentManager(),bundle);
                return true;
            }
        });
    }
}
