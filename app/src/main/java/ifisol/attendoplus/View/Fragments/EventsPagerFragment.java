package ifisol.attendoplus.View.Fragments;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.widget.ImageView;

import fr.arnaudguyon.smartfontslib.FontTextView;
import ifisol.attendoplus.Controller.Adapters.EventsPagerAdapter;
import ifisol.attendoplus.Global.Globals;
import ifisol.attendoplus.R;

public class EventsPagerFragment extends Fragment{


    Activity mActivity;
    Context  mContext;
    Globals  mGlobal;

    private TabLayout tabs;
    private ViewPager eventsViewPager;
    public EventsPagerAdapter eventsPagerAdapter;
    FontTextView mTvTitle;
    int pos;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.events_fragment, null);
    }



    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Init(view);
    }



    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        pos = getArguments().getInt("POSITION");
        SetUpViewPager();
    }



    private View Init(View view)
    {
        mActivity       = getActivity();
        mContext        = getActivity();
        mGlobal         = (Globals) mActivity.getApplicationContext();
        tabs            = (TabLayout) view.findViewById(R.id.tabs);
        eventsViewPager = (ViewPager) view.findViewById(R.id.events_view_pager);
        mTvTitle        = (FontTextView)  view.findViewById(R.id.tv_title);
        return view;
    }




    private void SetUpViewPager() {
        eventsPagerAdapter = new EventsPagerAdapter(getChildFragmentManager(),mContext);
        eventsViewPager.setAdapter(eventsPagerAdapter);
        eventsViewPager.setCurrentItem(pos);
        tabs.setupWithViewPager(eventsViewPager);
        for (int i = 0; i < tabs.getTabCount(); i++) {
            tabs.getTabAt(i).setCustomView(eventsPagerAdapter.getTabView(i));
        }
        TabLayout.Tab tab = tabs.getTabAt(pos);
        View tabView = (View) tab.getCustomView();
        ImageView tabIcon = (ImageView) tabView.findViewById(R.id.img_tab);
        tabIcon.setImageResource(eventsPagerAdapter.GetActIcon(pos));
        mTvTitle.setText(eventsPagerAdapter.getPageTitle(pos));
        tabs.setOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(eventsViewPager) {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                super.onTabSelected(tab);
                View tabView = (View) tab.getCustomView();
                ImageView tabIcon = (ImageView) tabView.findViewById(R.id.img_tab);
                tabIcon.setImageResource(eventsPagerAdapter.GetActIcon(tab.getPosition()));
                mTvTitle.setText(eventsPagerAdapter.getPageTitle(tab.getPosition()));
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                super.onTabUnselected(tab);
                View tabView = (View) tab.getCustomView();
                ImageView tabIcon = (ImageView) tabView.findViewById(R.id.img_tab);
                tabIcon.setImageResource(eventsPagerAdapter.GetNormalIcon(tab.getPosition()));
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                super.onTabReselected(tab);
            }
        });
    }

}
