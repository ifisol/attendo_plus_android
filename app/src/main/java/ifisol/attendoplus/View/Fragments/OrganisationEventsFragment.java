package ifisol.attendoplus.View.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.internal.NavigationMenu;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import com.google.gson.Gson;
import com.yalantis.taurus.PullToRefreshView;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import ifisol.attendoplus.Controller.Adapters.EventsAdapter;
import ifisol.attendoplus.Global.Globals;
import ifisol.attendoplus.Model.Events;
import ifisol.attendoplus.R;
import ifisol.attendoplus.Utilities.Constants;
import ifisol.attendoplus.Utilities.HttpHelper;
import ifisol.attendoplus.Utilities.Utility;
import io.github.yavski.fabspeeddial.FabSpeedDial;
import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.RequestBody;

public class OrganisationEventsFragment extends Fragment implements AdapterView.OnItemClickListener, View.OnClickListener, HttpHelper.HttpCallback {

    FragmentActivity mActivity;
    Context mContext;
    Globals mGlobal;
    ListView mEventslv;

    EventsAdapter eventsAdapter;

    String orgID;
    ImageView imgNoRecord;
    FabSpeedDial fab_EventsSort;
    ArrayList<Events> mEvents;
    View viewBlur;
    PullToRefreshView mPullToRefreshView;
    View header;
    View v;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        orgID =  getArguments().getString("ORGANISATION_ID");
        mActivity           = getActivity();
        mContext            = getActivity();
        mGlobal             = (Globals) mActivity.getApplicationContext();
        FetchOrganisationEvents();
    }





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (v==null)
            v = Init(inflater.inflate(R.layout.organisation_events_listing, null));

        return v;
    }





    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }





    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);





        mPullToRefreshView.setOnRefreshListener(new PullToRefreshView.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPullToRefreshView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        FetchOrganisationEvents();
                    }
                },500);
            }
        });


        mEventslv.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem == 0) {
                    mPullToRefreshView.setEnabled(true);
                } else mPullToRefreshView.setEnabled(false);
            }
        });



        fab_EventsSort.setMenuListener(new FabSpeedDial.MenuListener() {
            @Override
            public boolean onPrepareMenu(NavigationMenu navigationMenu) {
                viewBlur.setVisibility(View.VISIBLE);
                return true;}
            @Override
            public boolean onMenuItemSelected(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.sort_az:
                        Collections.sort(eventsAdapter.mFilterArray, Utility.TitleComparator);
                        break;
                    case R.id.sort_09:
                        Collections.sort(eventsAdapter.mFilterArray, Utility.DateComparator);
                        break;}
                eventsAdapter.notifyDataSetChanged();
                viewBlur.setVisibility(View.GONE);
                return true;}
            @Override
            public void onMenuClosed() {
                viewBlur.setVisibility(View.GONE);}});



    }





    private View Init(View view) {
        mEventslv           = (ListView) view.findViewById(R.id.events_list);
        fab_EventsSort      = (FabSpeedDial) view.findViewById(R.id.fab_sort);
        imgNoRecord         = (ImageView) view.findViewById(R.id.img_no_record);
        mPullToRefreshView = (PullToRefreshView) view.findViewById(R.id.pull_to_refresh);
        viewBlur            = view.findViewById(R.id.view_blur);

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(mContext.LAYOUT_INFLATER_SERVICE);
         header = inflater.inflate(R.layout.search_view,null);
        mEventslv.addHeaderView(header);

        return view;
    }





    private void FetchOrganisationEvents() {
        RequestBody body = new FormBody.Builder().add("user_id", mGlobal.mActiveUser.getUser_id()).add("organisation_id", orgID).add("tag", "organisation_created_events").build();
        try {
            HttpHelper.CallApi(mActivity, Constants.Organisation_detail, body, this, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }






    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Bundle bundle = new Bundle();
        bundle.putString(Constants.TAG_VIEW_TO_GO, "");
        bundle.putString(Constants.EVENT_TYPE,Constants.TAG_ORGANISATION_EVENTS);
        bundle.putSerializable("EVENT", eventsAdapter.getItm(position-1));
        Utility.ReplaceFragment(new EventsDetailPagerFragment(), mActivity.getSupportFragmentManager(), bundle);
 }






    @Override
    public void onClick(View v) {
        Utility.ReplaceFragment(new CreateNonAttendoFragment(), mActivity.getSupportFragmentManager());
    }







    private void SetEventsAdapter(ArrayList<Events> list) {
        mPullToRefreshView.setRefreshing(false);
        if (list==null||list.size()==0) {
            imgNoRecord.setVisibility(View.VISIBLE);
            fab_EventsSort.setVisibility(View.GONE);
            return;
        }
        imgNoRecord.setVisibility(View.GONE);
        fab_EventsSort.setVisibility(View.VISIBLE);

       eventsAdapter = new EventsAdapter(mContext, mActivity, Constants.TAG_ORGANISATION_EVENTS,list);
       mEventslv.setAdapter(eventsAdapter);
       mEventslv.setOnItemClickListener(this);
    }







    @Override
    public void onFailure(Call call, IOException e) {
        e.printStackTrace();

        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mPullToRefreshView.setRefreshing(false);
                imgNoRecord.setVisibility(View.VISIBLE);
            }
        });
    }
    @Override
    public void onSuccess(Call call, String response) {

        try {
            Log.d("Response ......." ,response);
            ParseEvents(response);
        } catch (Exception e) {
            e.printStackTrace();
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    SetEventsAdapter(mEvents); //To show no record found//
                    mPullToRefreshView.setRefreshing(false);
                }
            });

        }
    }





    private void ParseEvents(String s) throws Exception{
        Log.d("Response........",s);
        final JSONObject jsonObj = new JSONObject(s);
        final String message = jsonObj.getString("message");
        mEvents = new ArrayList<>();

        if (jsonObj.getString("status").equals("true"))
        {
            Gson gson = new Gson();
            Events[] events = gson.fromJson(jsonObj.getString("response"),Events[].class);
            mEvents = new ArrayList<>(Arrays.asList(events));
        }
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                SetEventsAdapter(mEvents);
                mPullToRefreshView.setRefreshing(false);
            }
        });
    }




}