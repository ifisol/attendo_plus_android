package ifisol.attendoplus.View.Fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import fr.arnaudguyon.smartfontslib.FontTextView;
import ifisol.attendoplus.Controller.Adapters.CPDPagerAdapter;
import ifisol.attendoplus.Global.Globals;
import ifisol.attendoplus.R;

public class Cpd_MainFragment extends Fragment  {


    Activity mActivity;
    Context  mContext;
    Globals  mGlobal;

    private TabLayout tabs;
    private ViewPager CPD_vp;
    private CPDPagerAdapter cpdPagerAdapter;
    FontTextView mTvTitle;
    View v;
    int pos;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        mActivity = getActivity();
        mContext  = getActivity();
        mGlobal   = (Globals) mActivity.getApplicationContext();


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (v==null)
            v = inflater.inflate(R.layout.cpd_main_fragment, null);
        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Init(view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);
        pos = getArguments().getInt("POSITION");
        SetUpViewPager();


    }

    private View Init(View view)
    {

        tabs            = (TabLayout) view.findViewById(R.id.tabs);
        CPD_vp          = (ViewPager) view.findViewById(R.id.cpd_view_pager);
        mTvTitle        = (FontTextView)  view.findViewById(R.id.tv_actionbar_title);


        return view;
    }

    private void SetUpViewPager() {

        cpdPagerAdapter = new CPDPagerAdapter(getChildFragmentManager(),mContext);
        CPD_vp.setAdapter(cpdPagerAdapter);
        CPD_vp.setCurrentItem(pos);
        tabs.setupWithViewPager(CPD_vp);

        for (int i = 0; i < tabs.getTabCount(); i++) {
            tabs.getTabAt(i).setCustomView(cpdPagerAdapter.getTabView(i));
        }

        TabLayout.Tab tab = tabs.getTabAt(pos);
        View tabView = (View) tab.getCustomView();
        ImageView tabIcon = (ImageView) tabView.findViewById(R.id.img_tab);
        tabIcon.setImageResource(cpdPagerAdapter.GetActIcon(pos));
        mTvTitle.setText(cpdPagerAdapter.getPageTitle(pos));

        tabs.setOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(CPD_vp) {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                super.onTabSelected(tab);
                View tabView = (View) tab.getCustomView();
                ImageView tabIcon = (ImageView) tabView.findViewById(R.id.img_tab);
                tabIcon.setImageResource(cpdPagerAdapter.GetActIcon(tab.getPosition()));
                mTvTitle.setText(cpdPagerAdapter.getPageTitle(tab.getPosition()));
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

                super.onTabUnselected(tab);
                View tabView = (View) tab.getCustomView();
                ImageView tabIcon = (ImageView) tabView.findViewById(R.id.img_tab);
                tabIcon.setImageResource(cpdPagerAdapter.GetNormalIcon(tab.getPosition()));
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

                super.onTabReselected(tab);
            }
        });




    }

}
