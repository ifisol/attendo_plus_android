package ifisol.attendoplus.View.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ExpandableListView;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.yalantis.taurus.PullToRefreshView;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import fr.arnaudguyon.smartfontslib.FontTextView;
import ifisol.attendoplus.Controller.Adapters.CPD_AllEvents_Adapter;
import ifisol.attendoplus.Global.Globals;
import ifisol.attendoplus.Model.Events;
import ifisol.attendoplus.Model.Non_attendo_events;
import ifisol.attendoplus.R;
import ifisol.attendoplus.Utilities.Constants;
import ifisol.attendoplus.Utilities.HttpHelper;
import ifisol.attendoplus.Utilities.Utility;
import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.RequestBody;

public class CpdAllEventsFragment extends Fragment implements  HttpHelper.HttpCallback {

    FragmentActivity mActivity;
    Context mContext;
    Globals mGlobal;
    ExpandableListView mCpdAllLv;

    CPD_AllEvents_Adapter cpd_allEvents_adapter;
    String mEventType;
    ImageView imgNoRecord;
    HashMap<String, ArrayList<?>> nonAttendoData;
    ArrayList<Events> mAttendoEvents ;
    ArrayList<Non_attendo_events> mNonAttendoEvents;
    RequestBody body;
    String cpdCount;
    View viewBlur;
    PullToRefreshView mPullToRefreshView;
    View header;
    View view;




    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mActivity           = getActivity();
        mContext            = getActivity();
        mGlobal             = (Globals) mActivity.getApplicationContext();
        mEventType = getArguments().getString(Constants.EVENT_TYPE);
        FetchEvents(true);
    }





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (view==null)
            view = Init(inflater.inflate(R.layout.cpd_all_events, null));
        return view;
    }







    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);







        mPullToRefreshView.setOnRefreshListener(new PullToRefreshView.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPullToRefreshView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        FetchEvents(false);
                    }
                },500);
            }
        });






        mCpdAllLv.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem == 0) {
                    mPullToRefreshView.setEnabled(true);
                } else mPullToRefreshView.setEnabled(false);
            }
        });


        mCpdAllLv.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

                Object object = cpd_allEvents_adapter.getChild(groupPosition,childPosition);

                if (object instanceof Events)
                {
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.TAG_VIEW_TO_GO, "");
                    bundle.putString(Constants.EVENT_TYPE, mEventType);
                    bundle.putSerializable("EVENT", (Events)object);
                    Utility.ReplaceFragment(new EventsDetailPagerFragment(), mActivity.getSupportFragmentManager(), bundle);
                }
                else if (object instanceof Non_attendo_events)
                {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("EVENT", (Non_attendo_events)object);
                    Utility.ReplaceFragment(new NotAttendoEventDetailFragment(), mActivity.getSupportFragmentManager(), bundle);
                }



                return true;
            }
        });






    }






    private View Init(View view) {

        mCpdAllLv           = (ExpandableListView) view.findViewById(R.id.cpd_all_exp_list);
        imgNoRecord         = (ImageView) view.findViewById(R.id.img_no_record);
        mPullToRefreshView = (PullToRefreshView) view.findViewById(R.id.pull_to_refresh);
        viewBlur            = view.findViewById(R.id.view_blur);

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(mContext.LAYOUT_INFLATER_SERVICE);
        header = inflater.inflate(R.layout.cpd_listing_header,null);
        mCpdAllLv.addHeaderView(header);

        return view;
    }









    private void FetchEvents(boolean loader) {

        RequestBody body = new FormBody.Builder().add("user_id", mGlobal.mActiveUser.getUser_id())
                .add("tag","all_events").build();
        FetchEvents(Constants.Guest_Cpd_events,body,false);
    }





    private void FetchEvents(String url, RequestBody body, boolean ShowLoading) {
        try {
            HttpHelper.CallApi(mActivity,url, body, this, ShowLoading);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }










    @Override
    public void onFailure(Call call, IOException e) {
        e.printStackTrace();

        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                imgNoRecord.setVisibility(View.VISIBLE);

            }
        });
    }
    @Override
    public void onSuccess(Call call, String response) {

        try {
            Log.d("Response ......." ,response);
            ParseEvents(response);
        } catch (Exception e) {
            e.printStackTrace();
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    imgNoRecord.setVisibility(View.VISIBLE);

                }
            });
        }
    }





    private void ParseEvents(String s) throws Exception{
        Log.d("Response........",s);

        final JSONObject jsonObj = new JSONObject(s);
        if (jsonObj.getString("status").equals("true"))
        {
            Gson gson = new Gson();

                cpdCount = jsonObj.getString("total_cpd_count");
                JSONObject Json2 = new JSONObject(jsonObj.getString("response"));
                Non_attendo_events[]  NonEvents = gson.fromJson(Json2.getString("non_attendo_events"),Non_attendo_events[].class);
                Events[] events = gson.fromJson(Json2.getString("attendo_events"),Events[].class);
                mAttendoEvents = new ArrayList<>(Arrays.asList(events));
                mNonAttendoEvents = new ArrayList<>(Arrays.asList(NonEvents));


        }
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                    SetAllEventsAdapter(mAttendoEvents,mNonAttendoEvents);


            }
        });
    }


    private void SetAllEventsAdapter(ArrayList<Events> attendoList, ArrayList<Non_attendo_events> NonAttendoList ) {
        mPullToRefreshView.setRefreshing(false);
        nonAttendoData = new HashMap<String, ArrayList<?>>();
        ArrayList<String> headers = new ArrayList<>();
        headers.add("Attendo Events");
        headers.add("Non-Attendo Events");
        nonAttendoData.put(headers.get(0),attendoList);
        nonAttendoData.put(headers.get(1), NonAttendoList);


        if (attendoList!=null && attendoList.size()>0 || NonAttendoList!=null && NonAttendoList.size()>0)
            imgNoRecord.setVisibility(View.GONE);
        else
        {
            imgNoRecord.setVisibility(View.VISIBLE);
            return;
        }


        FontTextView tv = (FontTextView) header.findViewById(R.id.tv_header);
        tv.setText("Total Events CPD : "+cpdCount);

        cpd_allEvents_adapter = new CPD_AllEvents_Adapter(mContext, headers, nonAttendoData, mActivity);
        mCpdAllLv.setAdapter(cpd_allEvents_adapter);
        mCpdAllLv.expandGroup(0);
        mCpdAllLv.expandGroup(1);
    }




}