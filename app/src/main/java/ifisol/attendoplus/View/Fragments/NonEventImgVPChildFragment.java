package ifisol.attendoplus.View.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;

import ifisol.attendoplus.Global.Globals;
import ifisol.attendoplus.R;
import ifisol.attendoplus.Utilities.Constants;

public class NonEventImgVPChildFragment extends Fragment{

    FragmentActivity mActivity;
    Context mContext;
    Globals mGlobal;

    ImageView mImgV;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        return Init(inflater.inflate(R.layout.vp_image_item, null));
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);
        Init(view);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);
        ImageLoader.getInstance().displayImage(Constants.Img_Base_URL+getArguments().getString("IMAGE"), mImgV);

    }

    private View Init(View view) {

        mActivity = getActivity();
        mContext = getActivity();
        mGlobal = (Globals) mActivity.getApplicationContext();
        mImgV   = (ImageView) view.findViewById(R.id.img_item);

        return view;


    }



}