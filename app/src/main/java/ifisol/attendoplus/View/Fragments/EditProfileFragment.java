package ifisol.attendoplus.View.Fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.sandrios.sandriosCamera.internal.SandriosCamera;
import com.sandrios.sandriosCamera.internal.configuration.CameraConfiguration;
import com.yalantis.taurus.PullToRefreshView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import co.lujun.androidtagview.TagContainerLayout;
import fr.arnaudguyon.smartfontslib.FontButton;
import fr.arnaudguyon.smartfontslib.FontEditText;
import fr.arnaudguyon.smartfontslib.FontTextView;
import ifisol.attendoplus.Controller.Adapters.OrganisationsAdapter;
import ifisol.attendoplus.Global.Globals;
import ifisol.attendoplus.Model.Organisations;
import ifisol.attendoplus.Model.User;
import ifisol.attendoplus.R;
import ifisol.attendoplus.Utilities.Constants;
import ifisol.attendoplus.Utilities.HttpHelper;
import ifisol.attendoplus.Utilities.ResizeImage;
import ifisol.attendoplus.Utilities.Utility;
import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.RequestBody;

public class EditProfileFragment extends Fragment implements View.OnClickListener, HttpHelper.HttpCallback {


    FragmentActivity mActivity;
    Context  mContext;
    Globals  mGlobal;


    private FontTextView tvUserName;
    private FontEditText edFirstName;
    private FontEditText edLastName;
    private FontTextView tvEmail;
    private FontTextView tvOrganisation;
    private FontEditText edAddress;
    private FontEditText edPhone;
    private FontEditText edJobTitle;
    private FontButton   btnUpdate;
    private RelativeLayout header;
    private FontTextView tvActionbarTitle;
    CircularImageView    imgUser;
    private ImageView    imgBack;
    PullToRefreshView mPullToRefreshView;
    SwitchCompat switchPrivacy;
    ArrayList<Organisations> mOrgList;
    OrganisationsAdapter org_adapter;
    String base64Img;
    private static final int CAPTURE_MEDIA = 368;
    Bitmap UserImage;
    StringBuilder secOrgIds;
    TagContainerLayout mTagContainerLayout;
    private View viewSecOrg;
    FontTextView tvSecOrg;
    public String OrganisationId="";
    User user;




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return Init(inflater.inflate(R.layout.edit_profile, null));
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);

        imgBack.setOnClickListener(this);
        btnUpdate.setOnClickListener(this);
        tvUserName.setOnClickListener(this);
        tvEmail.setOnClickListener(this);
        switchPrivacy.setOnClickListener(this);
        tvOrganisation.setOnClickListener(this);
        imgUser.setOnClickListener(this);
        tvSecOrg.setOnClickListener(this);

        user = (User)getArguments().getSerializable("USER");
        FetchOrganisations(false);
        SetData(user);

    }

    private View Init(View view)
    {

        mActivity = getActivity();
        mContext  = getActivity();
        mGlobal   = (Globals) mActivity.getApplicationContext();

        tvUserName = (FontTextView) view.findViewById(R.id.tv_user_name);
        edFirstName = (FontEditText) view.findViewById(R.id.ed_first_name);
        edLastName = (FontEditText) view.findViewById(R.id.ed_last_name);
        tvSecOrg = (FontTextView) view.findViewById(R.id.tv_sec_organisations);
        tvEmail = (FontTextView) view.findViewById(R.id.tv_email);
        tvOrganisation = (FontTextView) view.findViewById(R.id.tv_organisation);
        edAddress = (FontEditText) view.findViewById(R.id.ed_address);
        edJobTitle = (FontEditText) view.findViewById(R.id.ed_jobtitle);
        edPhone = (FontEditText) view.findViewById(R.id.ed_phone);
        btnUpdate = (FontButton) view.findViewById(R.id.btn_update);
        header = (RelativeLayout) view.findViewById(R.id.header);
        tvActionbarTitle = (FontTextView) view.findViewById(R.id.tv_actionbar_title);
        imgBack = (ImageView) view.findViewById(R.id.img_back);
        imgUser = (CircularImageView) view.findViewById(R.id.img_user);
        switchPrivacy = (SwitchCompat) view.findViewById(R.id.img_privacy);
        mTagContainerLayout = (TagContainerLayout) view.findViewById(R.id.tag_layout);
        viewSecOrg        = view.findViewById(R.id.view_secondary);
        tvSecOrg = (FontTextView) view.findViewById(R.id.tv_sec_organisations);

        return view;
    }



    private void SetData(User user)
    {
        OrganisationId = user.getOrganisation_id();
        ImageLoader.getInstance().displayImage(Constants.Img_Base_URL+user.getPicture(),imgUser);
        tvUserName.setText(user.getUser_name());
        edFirstName.setText(user.getFirst_name());
        edLastName.setText(user.getLast_name());
        tvEmail.setText(user.getEmail());
        tvOrganisation.setText(user.getOrganisation_name());
        edAddress.setText(user.getAddress());
        edJobTitle.setText(user.getProfession());
        edPhone.setText(user.getPhone());

        if (user.getProfile_visibility_status().equals("private"))
            switchPrivacy.setChecked(true);
        else switchPrivacy.setChecked(false);

        if (mGlobal.mActiveUser.getUser_type().equals(Constants.TAG_ORGANISER))
            viewSecOrg.setVisibility(View.GONE);


        else
        {
            viewSecOrg.setVisibility(View.VISIBLE);
            if (user.getSeconday_organisations()!=null && user.getSeconday_organisations().size()>0)
            {
                List<String> mTagList = new ArrayList<>();
                for (Organisations org : user.getSeconday_organisations() )
                {
                    mTagList.add(org.getOrganisation_name());
                }
                mTagContainerLayout.setTags(mTagList);
            }

        }


    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.img_back:
                mActivity.getSupportFragmentManager().popBackStack();
                break;
            case R.id.img_user:
                launchCamera();
                break;
            case R.id.tv_user_name:
                Utility.ShowAlert(mActivity, "User Name", "You cannot change User Name");
                break;
            case R.id.tv_email:
                Utility.ShowAlert(mActivity, "Email", "You cannot change Email");
                break;
            case R.id.tv_organisation:
                Utility.ShowAlert(mActivity, "Organisation", "You cannot change your Organisation");
                break;
            case R.id.btn_update:
                UpdateProfile();
                break;
            case R.id.img_privacy:

                if (switchPrivacy.isSelected())
                    switchPrivacy.setSelected(false);
                else
                    switchPrivacy.setSelected(true);

                break;
            case R.id.tv_sec_organisations:
                if (mOrgList==null || mOrgList.size()==0)
                    Utility.ShowAlert(mActivity,"Organisations", "Organisations are not available. Please try again later. Thanks");
                else
                ShowOrganisations("secondary");
                break;
        }

    }







    private void UpdateProfile() {

        String fName = edFirstName.getText().toString().trim();
        String lName = edLastName.getText().toString().trim();
        String Job = edJobTitle.getText().toString().trim();
        String Address = edAddress.getText().toString().trim();
        String Phone = edPhone.getText().toString().trim();

        if (fName.isEmpty())
            Utility.ShowAlert(mActivity,"First Name","Please enter your First name");
        else if (Job.isEmpty())
            Utility.ShowAlert(mActivity,"Job Title","Please enter your Job Title");
        else
        {
            FormBody.Builder builder =new FormBody.Builder();
                    builder.add("user_id",mGlobal.mActiveUser.getUser_id());
                    builder.add("first_name",fName);
                    builder.add("user_name",tvUserName.getText().toString());
                    builder.add("last_name",lName);
                    builder.add("email",tvEmail.getText().toString());
                    builder.add("organisation_id", mGlobal.mActiveUser.getOrganisation_id());
                    builder.add("profession",Job);
                    builder.add("address",Address);
                    builder.add("phone",Phone);
                    builder.add("device_type","Android");
                    builder.add("device_token","");
                    builder.add("picture",UserImage!=null?base64Img:"");
                    builder.add("visibility_status", switchPrivacy.isChecked()?"private":"public");
            try {
                String url = "";
                if (mGlobal.mActiveUser.getUser_type().equals(Constants.TAG_GUEST))
                {
                    url = Constants.Update_Guest_profile;
                    if (secOrgIds!=null)
                        builder.add("seconday_organisations", secOrgIds.toString());
                }

                else
                   url = Constants.Update_org_profile;

                RequestBody body = builder.build();

                HttpHelper.CallApi(mActivity,url,body,this,true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }



    }







    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CAPTURE_MEDIA && resultCode == mActivity.RESULT_OK) {
            try {
                ResizeImage resizeImage = new ResizeImage(mContext);
                String uri = resizeImage.compressImage(data.getStringExtra(CameraConfiguration.Arguments.FILE_PATH));
                File imgFile = new File(uri);
                if (imgFile.exists()) {
                    UserImage = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                    imgUser.setImageBitmap(UserImage);
                    base64Img = Utility.EncodeTobase64(UserImage);
                }

                Log.e("File >>>>>>>>>>>>>", "" + data.getStringExtra(CameraConfiguration.Arguments.FILE_PATH));
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }










    private void launchCamera() {
        new SandriosCamera(mActivity,this, CAPTURE_MEDIA)
                .setShowPicker(true)
                //.setVideoFileSize(15) //File Size in MB: Default is no limit
                .setMediaAction(CameraConfiguration.MEDIA_ACTION_PHOTO) // default is CameraConfiguration.MEDIA_ACTION_BOTH
                .enableImageCropping(true) // Default is false.
                .launchCamera();
    }

    @Override
    public void onFailure(Call call, final IOException e) {

        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                Toast.makeText(mActivity,"Request Timeout", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onSuccess(Call call, final String response) {


        if (call.request().url().toString().equals(Constants.Fetch_Organisations))
        {
            ParseOrganisations(response);
        }
        else
        {
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        JSONObject json = new JSONObject(response);
                        Toast.makeText(mActivity, json.getString("message"), Toast.LENGTH_SHORT).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });
        }
    }


    public void ShowOrganisations(String organisationType) {
        final Dialog dialog = new Dialog(mActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        dialog.setContentView(R.layout.organisations_fragment);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        DisplayMetrics dm = new DisplayMetrics();
        mActivity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER | Gravity.START);
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        //lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        RecyclerView mOrg_rv;
        FontTextView doneTv = (FontTextView) dialog.findViewById(R.id.tv_done);
        if (organisationType.equals("secondary"))
            doneTv.setVisibility(View.VISIBLE);
        else
            doneTv.setVisibility(View.GONE);
        mPullToRefreshView = (PullToRefreshView) dialog.findViewById(R.id.pull_to_refresh);
        mPullToRefreshView.setEnabled(false);
        FloatingActionButton fab_add   = (FloatingActionButton) dialog.findViewById(R.id.fab_add);
        mOrg_rv = (RecyclerView) dialog.findViewById(R.id.rv_organisations);
        mOrg_rv.setHasFixedSize(true);
        mOrg_rv.setLayoutManager(new LinearLayoutManager(mContext));
        org_adapter = new OrganisationsAdapter(mActivity,this,mOrgList,organisationType);
        mOrg_rv.setAdapter(org_adapter);


        doneTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String sep = "";
                secOrgIds = new StringBuilder();
                List<String> mTagList = new ArrayList<String>();

                for (Organisations org : org_adapter.mFilterArray )
                {
                    if (org.isSelected()&& org.getOrganisation_id()!=OrganisationId)
                    {
                        mTagList.add(org.getOrganisation_name());
                        secOrgIds.append(sep);
                        secOrgIds.append(org.getOrganisation_id());
                        sep = ",";
                    }

                }
                mTagContainerLayout.setTags(mTagList);
                dialog.dismiss();
            }

        });

        fab_add.setVisibility(View.GONE);
       ;


        FontEditText mTxtSearch = (FontEditText) dialog.findViewById(R.id.ed_search);
        mTxtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                org_adapter.FilterList(s.toString());

            }
        });

        dialog.show();
    }

    private void FetchOrganisations(boolean loader) {
        try {
            RequestBody formBody = new FormBody.Builder().add("","").build();
            HttpHelper.CallApi(mActivity,Constants.Fetch_Organisations,formBody,this,loader);
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("Exception...........",e.getMessage());
        }
    }


    private void ParseOrganisations(String s)
    {
        try {
            JSONObject jsonObj = new JSONObject(s);
            Gson gson = new Gson();
            Organisations[] mOrgs = gson.fromJson(jsonObj.getString("response"),Organisations[].class);
            mOrgList = new ArrayList<>(Arrays.asList(mOrgs));

            if (mOrgList !=null && user.getSeconday_organisations()!=null && user.getSeconday_organisations().size()>0)
            {
                for (Organisations obj :user.getSeconday_organisations())
                {
                    for (int i=0;i<mOrgList.size();i++)
                    {
                     if (obj.getOrganisation_id().equals(mOrgList.get(i).getOrganisation_id()))
                         mOrgList.get(i).setSelected(true);
                    }

                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
