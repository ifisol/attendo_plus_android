package ifisol.attendoplus.View.Fragments;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Locale;

import fr.arnaudguyon.smartfontslib.FontButton;
import fr.arnaudguyon.smartfontslib.FontTextView;
import ifisol.attendoplus.Controller.Adapters.ExhibitorsGridAdapter;
import ifisol.attendoplus.Global.Globals;
import ifisol.attendoplus.Model.Events;
import ifisol.attendoplus.Model.Exhibitors;
import ifisol.attendoplus.R;
import ifisol.attendoplus.Utilities.Constants;
import ifisol.attendoplus.Utilities.HttpHelper;
import ifisol.attendoplus.Utilities.Utility;
import ifisol.attendoplus.View.Activities.ActivityMain;
import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.RequestBody;

public class EventQrScannerFragment extends Fragment implements View.OnClickListener, HttpHelper.HttpCallback {


    FragmentActivity mActivity;
    Context  mContext;
    Globals  mGlobal;

    private RecyclerView rvExhibitors;
    private ExhibitorsGridAdapter exhibitor_ra;

    private FontButton btnAttend;
    private FontButton btnCancel;
    private FontTextView tvNoRecord;
    private FontTextView tvEventName;
    private FontTextView tvStartEndTime;
    private FontTextView tvAddressStreet;
    ArrayList<Exhibitors> mExhibitorList;

    Events event;





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return Init(inflater.inflate(R.layout.qr_code_event_scaner, null));
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Init(view);
        ActivityMain.bottomBarGuest.setVisibility(View.GONE);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);

        event = (Events) getArguments().getSerializable("EVENT");
        FetchEventExhibitors();

        setData();

        btnAttend.setOnClickListener(this);
        btnCancel.setOnClickListener(this);

    }


    private View Init(View view)
    {

        mActivity = getActivity();
        mContext  = getActivity();
        mGlobal   = (Globals) mActivity.getApplicationContext();

        tvEventName      = (FontTextView) view.findViewById(R.id.tv_event_name);
        tvStartEndTime   = (FontTextView) view.findViewById(R.id.tv_start_end_time);
        tvAddressStreet  = (FontTextView) view.findViewById(R.id.tv_address_street);
        rvExhibitors     = (RecyclerView) view.findViewById(R.id.rv_exhibitors);
        btnAttend        = (FontButton) view.findViewById(R.id.btn_attend);
        btnCancel        = (FontButton) view.findViewById(R.id.btn_cancel);
        tvNoRecord       = (FontTextView) view.findViewById(R.id.rv_no_record);

        rvExhibitors         = (RecyclerView) view.findViewById(R.id.rv_exhibitors);

        return view;
    }


    private void SetAdapter()
    {

        if (mExhibitorList!=null && mExhibitorList.size()>0)
        {
            tvNoRecord.setVisibility(View.GONE);
            LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
            rvExhibitors.setHasFixedSize(true);
            rvExhibitors.setLayoutManager(layoutManager);
            exhibitor_ra = new ExhibitorsGridAdapter(mActivity, this, mExhibitorList);
            rvExhibitors.setAdapter(exhibitor_ra);
        }
        else
        {
            tvNoRecord.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btn_attend:
                markAttendance();
                break;
            case R.id.btn_cancel:
                mActivity.getSupportFragmentManager().popBackStack();
                ActivityMain.bottomBarGuest.setVisibility(View.VISIBLE);
                break;
        }
    }


    private void FetchEventExhibitors() {

        FormBody.Builder builder = new FormBody.Builder();
        builder.add("user_id",mGlobal.mActiveUser.getUser_id());
        builder.add("event_id", event.getEvent_id());
        builder.add("status", "upcoming");
            builder.add("tag", "guest_event_exhibitors");

        RequestBody formBody = builder.build();
        try {
            HttpHelper.CallApi(mActivity,Constants.Guest_event_detail,formBody,this,false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }




    @Override
    public void onFailure(Call call, IOException e) {
    }

    @Override
    public void onSuccess(Call call, String response) {

        if (call.request().url().toString().equals(Constants.Guest_event_detail))
        {
            try {
                Log.d("Response . . . . . .", response);
                Gson gson = new Gson();
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.getString("status").equals("true"))
                {
                    final Exhibitors[] exhibitors = gson.fromJson(jsonObject.getString("response"), Exhibitors[].class);
                    mExhibitorList = new ArrayList<>(Arrays.asList(exhibitors));
                }

                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        SetAdapter();
                    }
                });

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        else if (call.request().url().toString().equals(Constants.Mark_qr_attendance))
        {
            try {
                final JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.getString("status").equals("true"))
                {
                    mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                           if (event.getHas_feedback().equals("yes"))
                           {
                               Calendar cal = Calendar.getInstance();
                               SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a", Locale.US);
                               String current_time = sdf.format(cal.getTime());
                               String end_time = event.getEvent_end_time();
                               Utility.ScheduleNotification(mContext,current_time,end_time, event);
                           }


                            ShowCongratsDialog();
                        }
                    });
                }
                else
                {
                    mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            try {
                                Toast.makeText(mActivity, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }

            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }

    }


    private void setData() {
        tvEventName.setText(event.getEvent_name());
        tvStartEndTime.setText(event.getEvent_start_time()+" - "+event.getEvent_end_time());
        tvAddressStreet.setText(event.getEvent_street_address()+" "+event.getEvent_city()+" "+event.getEvent_country());
    }


    private void markAttendance()
    {

        RequestBody body = new FormBody.Builder()
                .add("guest_id", mGlobal.mActiveUser.getUser_id())
                .add("event_id", event.getEvent_id())
                .add("device_type", "Android").build();;
        try {
            HttpHelper.CallApi(mActivity,Constants.Mark_qr_attendance,body,this,true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void ShowCongratsDialog() {

        final Dialog dialog = new Dialog(mActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.qr_code_congrats_view);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        DisplayMetrics dm = new DisplayMetrics();
        mActivity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER | Gravity.START);
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        //    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);

        FontTextView tvDone= (FontTextView) dialog.findViewById(R.id.tv_close);
        FontTextView tvTxt= (FontTextView) dialog.findViewById(R.id.tv_text);

        tvTxt.setText("Your attendance has been marked successfully \nfor the event "+event.getEvent_name()+" ");

        tvDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                Utility.ReplaceFragment(new QRScannerFragment(),mActivity.getSupportFragmentManager());
                ActivityMain.bottomBarGuest.setVisibility(View.VISIBLE);
            }
        });


        dialog.show();
    }

}
