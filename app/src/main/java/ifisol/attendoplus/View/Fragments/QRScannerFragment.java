package ifisol.attendoplus.View.Fragments;

import android.content.Context;
import android.graphics.PointF;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dlazaro66.qrcodereaderview.QRCodeReaderView;

import org.json.JSONObject;

import java.util.HashMap;

import ifisol.attendoplus.Global.Globals;
import ifisol.attendoplus.Model.Events;
import ifisol.attendoplus.R;
import ifisol.attendoplus.Utilities.Utility;
import ifisol.attendoplus.View.Activities.ActivityMain;

public class QRScannerFragment extends Fragment implements QRCodeReaderView.OnQRCodeReadListener {


    FragmentActivity mActivity;
    Context  mContext;
    Globals  mGlobal;
    private QRCodeReaderView qrCodeReaderView;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.qr_scanner_fragment, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Init(view);
    }

    private View Init(View view)
    {

        mActivity = getActivity();
        mContext  = getActivity();
        mGlobal   = (Globals) mActivity.getApplicationContext();

        ActivityMain.mTvABTitle.setText("QR Scanner");

        qrCodeReaderView = (QRCodeReaderView) view.findViewById(R.id.qrdecoderview);
        qrCodeReaderView.setOnQRCodeReadListener(this);

        qrCodeReaderView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!qrCodeReaderView.isActivated())
                     onResume();
            }
        });


        return view;
    }



    @Override
    public void onStop() {

        super.onStop();
        qrCodeReaderView.stopCamera();

    }

    @Override
    public void onResume() {

        super.onResume();
        qrCodeReaderView.setQRDecodingEnabled(true);
        qrCodeReaderView.startCamera();
    }

    @Override
    public void onQRCodeRead(String s, PointF[] points) {

        qrCodeReaderView.stopCamera();
        qrCodeReaderView.setQRDecodingEnabled(false);

        try {
            MediaPlayer mp = MediaPlayer.create(mActivity, R.raw.shutter);
            mp.start();
            JSONObject jsonObject = new JSONObject(s);
            if (!jsonObject.has("qr_type"))
                Utility.ShowAlert(mActivity,"Invalid Qr code", "This is an invalid Qr code please try again by scaning a valid Qr code. Thanks");
            else
            {
                if (jsonObject.getString("qr_type").equals("event_qr"))
                    eventScanner(jsonObject);
                else if (jsonObject.getString("qr_type").equals("exhibitor_qr"))
                    exhibitorScanner(jsonObject);
            }


        } catch (Exception e) {
            e.printStackTrace();
            Utility.ShowAlert(mActivity,"Invalid Qr code", "This is an invalid Qr code please try again by scaning a valid Qr code. Thanks");
        }
    }




    private void exhibitorScanner(JSONObject jsonObject) throws Exception
    {


        HashMap<String, String> scanData = new HashMap<>();

        scanData.put("exhibitor_id",jsonObject.getString("exhibitor_id"));
        scanData.put("event_id",jsonObject.getString("event_id"));
        scanData.put("exhibitor_name",jsonObject.getString("exhibitor_name"));
        scanData.put("exhibitor_email",jsonObject.getString("exhibitor_email"));
        scanData.put("exhibitor_phone",jsonObject.getString("exhibitor_phone"));
        scanData.put("exhibitor_website",jsonObject.getString("exhibitor_website"));
        scanData.put("exhibitor_company",jsonObject.getString("exhibitor_company"));
        scanData.put("exhibitor_description",jsonObject.getString("exhibitor_description"));
        scanData.put("exhibitor_picture",jsonObject.getString("exhibitor_picture"));
        scanData.put("exhibitor_status",jsonObject.getString("exhibitor_status"));
        scanData.put("event_name",jsonObject.getString("event_name"));
        scanData.put("event_start_time",jsonObject.getString("event_start_time"));
        scanData.put("event_end_time",jsonObject.getString("event_end_time"));
        scanData.put("event_organisation_id",jsonObject.getString("event_organisation_id"));



        if (Utility.isBefore(scanData.get("event_start_time")))
            Utility.ShowAlert(mActivity,"Event not attended", "You have not attended the respective event of exhibitor yet. To scan any exhibitor you must have to attend the event first. Thaks");
        else if (Utility.isAfter(scanData.get("event_end_time")))
             Utility.ShowAlert(mActivity,"Event has been finished", "You are too late to scan this Qr. Event has ended now");
        else
        {
            Bundle bundle = new Bundle();
            bundle.putSerializable("SCAN_DATA", scanData);
            Utility.ReplaceFragment(new ExibitorQrScannerFragment(),mActivity.getSupportFragmentManager(),bundle);
        }

    }





    private void eventScanner(JSONObject jsonObject) throws Exception
    {
        Events event = new Events();
        event.setEvent_id(jsonObject.getString("event_id"));
        event.setUser_id(jsonObject.getString("user_id"));
        event.setEvent_name(jsonObject.getString("event_name"));
        event.setEvent_description(jsonObject.getString("event_description"));
        event.setIs_public_event(jsonObject.getString("is_public"));
        event.setEvent_country(jsonObject.getString("event_country"));
        event.setEvent_city(jsonObject.getString("event_city"));
        event.setEvent_street_address(jsonObject.getString("event_street_address"));
        event.setEvent_lat(jsonObject.getString("event_lat"));
        event.setEvent_lng(jsonObject.getString("event_lng"));
        event.setEvent_cpd(jsonObject.getString("event_cpd"));
        event.setEvent_date(jsonObject.getString("event_date"));
        event.setEvent_start_time(jsonObject.getString("event_start_time"));
        event.setEvent_end_time(jsonObject.getString("event_end_time"));
        event.setEvent_photo(jsonObject.getString("event_picture"));
        event.setEvent_web_url(jsonObject.getString("event_web_url"));
        event.setEvent_status(jsonObject.getString("event_status"));

        event.setHas_logbook(jsonObject.getString("has_logbook"));
        event.setHas_feedback(jsonObject.getString("has_feedback"));
        if (event.getHas_feedback().equals("yes"))
            event.setFeedback_url(jsonObject.getString("feedback_url"));
        event.setHas_exhibitor(jsonObject.getString("has_exhibitor"));
        event.setEvent_organisation_id(jsonObject.getString("organisation_id"));
        event.setEvent_organisation_name(jsonObject.getString("organisation_name"));
        event.setEvent_type_id(jsonObject.getString("event_type_id"));
        event.setEvent_type_name(jsonObject.getString("event_type_name"));

        /*if (event.getIs_public_event().equals("yes") && !event.getEvent_organisation_id().equals(mGlobal.mActiveUser.getOrganisation_id()))
            Utility.ShowAlert(mActivity,"Invalid Event", "Sorry your organisation not hosting this event. You can only attend event that is hosted by your organisation");*/
         if (!Utility.checkDateValidity(event.getEvent_date()))
            Utility.ShowAlert(mActivity,"Past Event", "Sorry you cannot attend past events");
        else if (Utility.isBefore(event.getEvent_start_time()))
            Utility.ShowAlert(mActivity,"Event not started", "You are earlier to scan this Qr code. Please wait till event starts");
        else if (Utility.isAfter(event.getEvent_end_time()))
            Utility.ShowAlert(mActivity,"Event ended", "You are too late to scan this Qr. Event has ended now");
        else
        {
            Bundle bundle = new Bundle();
            bundle.putSerializable("EVENT", event);
            Utility.ReplaceFragment(new EventQrScannerFragment(),mActivity.getSupportFragmentManager(),bundle);
        }
    }
}
