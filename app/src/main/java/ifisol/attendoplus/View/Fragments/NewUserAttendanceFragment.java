package ifisol.attendoplus.View.Fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.gson.Gson;
import com.sandrios.sandriosCamera.internal.SandriosCamera;
import com.sandrios.sandriosCamera.internal.configuration.CameraConfiguration;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import fr.arnaudguyon.smartfontslib.FontButton;
import fr.arnaudguyon.smartfontslib.FontEditText;
import fr.arnaudguyon.smartfontslib.FontTextView;
import ifisol.attendoplus.Controller.Adapters.OrganisationsAdapter;
import ifisol.attendoplus.Global.Globals;
import ifisol.attendoplus.Model.Events;
import ifisol.attendoplus.Model.Organisations;
import ifisol.attendoplus.R;
import ifisol.attendoplus.Utilities.Constants;
import ifisol.attendoplus.Utilities.HttpHelper;
import ifisol.attendoplus.Utilities.ResizeImage;
import ifisol.attendoplus.Utilities.Utility;
import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.RequestBody;

public class NewUserAttendanceFragment extends Fragment implements View.OnClickListener, HttpHelper.HttpCallback {

    FragmentActivity mActivity;
    Context mContext;
    Globals mGlobal;



    private ImageView    imgSelectPhoto;
    private ImageView    imgUser;
    private FontTextView tvOrganisation;
    private FontEditText edUserName;
    private FontEditText edFirstName;
    private FontEditText edLastName;
    private FontEditText edEmail;
    private FontEditText edJobTitle;
    private FontEditText edAddress;
    private FontEditText edPhone;
    private FontButton   btnAttendance;

    ArrayList<Organisations> mOrgList;
    private static final int CAPTURE_MEDIA = 368;

    String OrganisationId="";
    Bitmap UserImage;
    String base64Img;

    Events event;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.new_user_attendance_fragment, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Init(view);

        event = (Events) getArguments().getSerializable("EVENT");
        tvOrganisation.setText(event.getEvent_organisation_name());
        OrganisationId = event.getEvent_organisation_id();

        try {
            RequestBody formBody = new FormBody.Builder().add("","").build();
            HttpHelper.CallApi(mActivity,Constants.Fetch_Organisations,formBody,this,false);
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("Exception...........",e.getMessage());
        }
    }



    private View Init(View view) {

        mActivity = getActivity();
        mContext = getActivity();
        mGlobal = (Globals) mActivity.getApplicationContext();

        imgSelectPhoto = (ImageView) view.findViewById(R.id.img_select_photo);
        imgUser = (CircularImageView) view.findViewById(R.id.img_user);
        edUserName = (FontEditText) view.findViewById(R.id.ed_user_name);
        edFirstName = (FontEditText) view.findViewById(R.id.ed_first_name);
        edLastName = (FontEditText) view.findViewById(R.id.ed_last_name);
        edEmail = (FontEditText) view.findViewById(R.id.ed_email);
        tvOrganisation = (FontTextView) view.findViewById(R.id.tv_organisation);
        edJobTitle = (FontEditText) view.findViewById(R.id.ed_job_title);
        edAddress = (FontEditText) view.findViewById(R.id.ed_address);
        edPhone = (FontEditText) view.findViewById(R.id.ed_phone);
        btnAttendance = (FontButton) view.findViewById(R.id.btn_attendance);

      //  tvOrganisation.setOnClickListener(this);
        imgSelectPhoto.setOnClickListener(this);
        btnAttendance.setOnClickListener(this);


        return view;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            /*case R.id.tv_organisation:
                ShowOrganisations();
                break;*/
            case R.id.img_select_photo:
                launchCamera();
                break;
            case R.id.btn_attendance:
                markAttendace();
                break;
        }

    }








    public void ShowOrganisations() {
        final Dialog dialog = new Dialog(mActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        dialog.setContentView(R.layout.organisations_fragment);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        DisplayMetrics dm = new DisplayMetrics();
        mActivity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER | Gravity.START);
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        //lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        RecyclerView mOrg_rv;
        final OrganisationsAdapter org_adapter;
        FloatingActionButton fab_add   = (FloatingActionButton) dialog.findViewById(R.id.fab_add);
        fab_add.setVisibility(View.GONE);
        mOrg_rv = (RecyclerView) dialog.findViewById(R.id.rv_organisations);
        mOrg_rv.setHasFixedSize(true);
        mOrg_rv.setLayoutManager(new LinearLayoutManager(mContext));
        org_adapter = new OrganisationsAdapter(mActivity,this,mOrgList,"");
        mOrg_rv.setAdapter(org_adapter);

        /*org_adapter.setOnClickListener(new OrganisationsAdapter.OnItemClick() {
            @Override
            public void onItemClicked(Organisations obj) {

                OrganisationId = obj.getOrganisation_id();
                tvOrganisation.setText(obj.getOrganisation_name());
                dialog.dismiss();
            }
        });*/

        FontEditText mTxtSearch = (FontEditText) dialog.findViewById(R.id.ed_search);
        mTxtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                org_adapter.FilterList(s.toString());

            }
        });


        dialog.show();
    }





    private void ParseOrganisations(String s)
    {
        try {
            JSONObject jsonObj = new JSONObject(s);
            Gson gson = new Gson();
            Organisations[] mOrgs = gson.fromJson(jsonObj.getString("response"),Organisations[].class);
            mOrgList = new ArrayList<>(Arrays.asList(mOrgs));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }




    private void launchCamera() {
        new SandriosCamera(mActivity,this, CAPTURE_MEDIA)
                .setShowPicker(true)
                //.setVideoFileSize(15) //File Size in MB: Default is no limit
                .setMediaAction(CameraConfiguration.MEDIA_ACTION_PHOTO) // default is CameraConfiguration.MEDIA_ACTION_BOTH
                .enableImageCropping(true) // Default is false.
                .launchCamera();
    }



    private void markAttendace() {

        String username = edUserName.getText().toString().trim();
        String firstname = edFirstName.getText().toString().trim();
        String lastname = edLastName.getText().toString().trim();
        String email = edEmail.getText().toString().trim();
        String jobtitle = edJobTitle.getText().toString().trim();
        String address = edAddress.getText().toString().trim();
        String phone = edPhone.getText().toString().trim();

        if (username.isEmpty())
            Utility.ShowAlert(mActivity, "User Name", "Please enter User Name");
        else if (firstname.isEmpty())
            Utility.ShowAlert(mActivity, "First Name", "Please enter First Name");
        else if (email.isEmpty())
            Utility.ShowAlert(mActivity, "Email", "Please enter Email");
        else if (!Utility.EmailValidator(email))
            Utility.ShowAlert(mActivity, "Invalid Email", "Please enter a valid Email address");

        else if (jobtitle.isEmpty())
            Utility.ShowAlert(mActivity, "Job Tiltle", "Please enter enter Job Title");
        else if (address.isEmpty())
            Utility.ShowAlert(mActivity, "Address", "Please enter your address");
        else if (phone.isEmpty())
            Utility.ShowAlert(mActivity, "Phone Number", "Please enter Phone Number");

        else {
            try {
                RequestBody formBody = new FormBody.Builder()
                        .add("tag","new")
                        .add("event_id", event.getEvent_id())
                        .add("user_id", mGlobal.mActiveUser.getUser_id())
                        .add("first_name", firstname)
                        .add("last_name", lastname)
                        .add("user_name", username)
                        .add("email", email)
                        .add("address", address)
                        .add("phone", phone)
                        .add("profession", jobtitle)
                        .add("organisation_id", OrganisationId)
                        .add("device_type", "Android")
                        .add("device_token", "")
                        .add("picture",base64Img).build();
                HttpHelper.CallApi( mActivity, Constants.Mark_attendance, formBody, this, true);
            } catch (Exception e) {
                e.printStackTrace();
                Log.d("Exception...........", e.getMessage());
            }
        }
    }


    @Override
    public void onFailure(Call call, IOException e) {

        e.printStackTrace();

        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
            }
        });

    }

    @Override
    public void onSuccess(Call call, String response) {

        switch (call.request().url().toString())
        {
            case Constants.Fetch_Organisations:
                ParseOrganisations(response);
                break;
            case Constants.Mark_attendance:
                try {
                    parseResponse(response);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }

    }




    private void parseResponse(String response) throws Exception {

        Log.d("response>>>>>>", response);
        final JSONObject json = new JSONObject(response);
        if (json.getString("status").equals("true"))
        {
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mActivity.getSupportFragmentManager().popBackStack();
                }
            });
        }

        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    Toast.makeText(mActivity,json.getString("message"), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }






    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CAPTURE_MEDIA && resultCode == mActivity.RESULT_OK) {
            try {

                ResizeImage resizeImage = new ResizeImage(mContext);
                String uri = resizeImage.compressImage(data.getStringExtra(CameraConfiguration.Arguments.FILE_PATH));
                File imgFile = new File(uri);
                if (imgFile.exists()) {
                    UserImage = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                    imgUser.setImageBitmap(UserImage);
                    base64Img = Utility.EncodeTobase64(UserImage);
                }


                Log.e("File >>>>>>>>>>>>>", "" + data.getStringExtra(CameraConfiguration.Arguments.FILE_PATH));
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

}