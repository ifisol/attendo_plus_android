package ifisol.attendoplus.View.Fragments;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

import fr.arnaudguyon.smartfontslib.FontButton;
import fr.arnaudguyon.smartfontslib.FontTextView;
import ifisol.attendoplus.Global.Globals;
import ifisol.attendoplus.R;
import ifisol.attendoplus.Utilities.Constants;
import ifisol.attendoplus.Utilities.HttpHelper;
import ifisol.attendoplus.Utilities.Utility;
import ifisol.attendoplus.View.Activities.ActivityMain;
import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.RequestBody;

public class ExibitorQrScannerFragment extends Fragment implements View.OnClickListener, HttpHelper.HttpCallback {


    FragmentActivity mActivity;
    Context  mContext;
    Globals  mGlobal;


    HashMap<String, String> mScanData;


    private FontButton btnYes;
    private FontButton btnCancel;
    private CircularImageView imgExhibitor;
    private FontTextView      tvEmail;
    private FontTextView      tvName;
    private FontTextView      tvPhone;
    private FontTextView      tvWebsitw;
    private FontTextView      tvCompany;
    private FontTextView      tvDescription;





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return Init(inflater.inflate(R.layout.qr_code_exhibitor_scaner, null));
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Init(view);
        ActivityMain.bottomBarGuest.setVisibility(View.GONE);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);

        mScanData = (HashMap<String, String>) getArguments().getSerializable("SCAN_DATA");

        setData();

        btnYes.setOnClickListener(this);
        btnCancel.setOnClickListener(this);

    }


    private View Init(View view)
    {

        mActivity = getActivity();
        mContext  = getActivity();
        mGlobal   = (Globals) mActivity.getApplicationContext();


        btnYes = (FontButton) view.findViewById(R.id.btn_yes);
        btnCancel = (FontButton) view.findViewById(R.id.btn_cancel);
        imgExhibitor = (CircularImageView) view.findViewById(R.id.img_exhibitor);
        tvEmail = (FontTextView) view.findViewById(R.id.tv_email);
        tvPhone = (FontTextView) view.findViewById(R.id.tv_phone);
        tvWebsitw = (FontTextView) view.findViewById(R.id.tv_websitw);
        tvCompany = (FontTextView) view.findViewById(R.id.tv_company);
        tvName = (FontTextView) view.findViewById(R.id.exh_name);
        tvDescription = (FontTextView) view.findViewById(R.id.tv_description);

        return view;
    }



    private void setData() {

        tvName.setText(mScanData.get("exhibitor_name"));
        tvEmail.setText(mScanData.get("exhibitor_email"));
        tvPhone.setText(mScanData.get("exhibitor_phone"));
        tvWebsitw.setText(mScanData.get("exhibitor_website"));
        tvCompany.setText(mScanData.get("exhibitor_company"));
        tvDescription.setText(mScanData.get("exhibitor_description"));
        ImageLoader.getInstance().displayImage(Constants.Img_Base_URL+mScanData.get("exhibitor_picture"),imgExhibitor);

    }




    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btn_yes:
                markAttendance();
                break;
            case R.id.btn_cancel:
                mActivity.getSupportFragmentManager().popBackStack();
                ActivityMain.bottomBarGuest.setVisibility(View.VISIBLE);
                break;
        }
    }



    @Override
    public void onFailure(Call call, IOException e) {
    }

    @Override
    public void onSuccess(Call call, String response) {

            try {
                final JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.getString("status").equals("true"))
                {
                    mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ShowCongratsDialog();
                        }
                    });
                }
                else
                {
                    mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            try {
                                Toast.makeText(mActivity, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }

            }
            catch (Exception e)
            {
                e.printStackTrace();
            }


    }




    private void markAttendance()
    {

        RequestBody body = new FormBody.Builder()
                .add("user_id", mGlobal.mActiveUser.getUser_id())
                .add("event_id", mScanData.get("event_id"))
                .add("exhibitor_id",mScanData.get("exhibitor_id")).build();
        try {
            HttpHelper.CallApi(mActivity,Constants.Scan_exhibitor_qr,body,this,true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void ShowCongratsDialog() {

        final Dialog dialog = new Dialog(mActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.qr_code_congrats_view);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        DisplayMetrics dm = new DisplayMetrics();
        mActivity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER | Gravity.START);
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        //    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);

        FontTextView tvDone= (FontTextView) dialog.findViewById(R.id.tv_close);
        FontTextView tvTxt= (FontTextView) dialog.findViewById(R.id.tv_text);

        tvTxt.setText("Your attendance has been marked successfully \nfor the Exhibitor "+mScanData.get("exhibitor_name")+"");

        tvDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                Utility.ReplaceFragment(new QRScannerFragment(),mActivity.getSupportFragmentManager());
                ActivityMain.bottomBarGuest.setVisibility(View.VISIBLE);
            }
        });


        dialog.show();
    }



}
