package ifisol.attendoplus.View.Fragments;

import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import fr.arnaudguyon.smartfontslib.FontButton;
import fr.arnaudguyon.smartfontslib.FontTextView;
import ifisol.attendoplus.Controller.Adapters.NonEventDetailImgPagerAdapter;
import ifisol.attendoplus.Global.Globals;
import ifisol.attendoplus.Model.Non_attendo_events;
import ifisol.attendoplus.R;
import ifisol.attendoplus.Utilities.Utility;

import android.support.v4.view.ViewPager;

import java.text.SimpleDateFormat;
import java.util.Date;

public class NotAttendoEventDetailFragment extends Fragment implements View.OnClickListener {

    FragmentActivity mActivity;
    Context mContext;
    Globals mGlobal;

    private RelativeLayout header;
    private FontButton btnEdit;

    private FontTextView tvActionbarTitle;
    private FontTextView tvCategory;
    private FontTextView tvTitle;
    private FontTextView tvDate;
    private FontTextView tvCPD;
    private FontTextView tvWebsite;
    private ViewPager    imgVp;
    private FontTextView tvDscription;
    View view1;
    View view2;

    ImageView imgBack;

    NonEventDetailImgPagerAdapter nonEventDetailImgPagerAdapter;

    Non_attendo_events event;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.not_attendo_event_detail_fragment, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Init(view);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);

        event = (Non_attendo_events) getArguments().getSerializable("EVENT");

        nonEventDetailImgPagerAdapter = new NonEventDetailImgPagerAdapter(getChildFragmentManager(),mContext, event.getPictures());
        imgVp.setAdapter(nonEventDetailImgPagerAdapter);

        imgVp.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {



            }

            @Override
            public void onPageSelected(int position) {

                switch (position)
                {
                    case 0:
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            view1.setBackground(getResources().getDrawable(R.drawable.circle_red1));
                            view2.setBackground(getResources().getDrawable(R.drawable.circle_grey));
                        }
                        else
                        {
                            view1.setBackgroundDrawable(getResources().getDrawable(R.drawable.circle_red1));
                            view2.setBackgroundDrawable(getResources().getDrawable(R.drawable.circle_grey));
                        }

                        break;
                    case 1:

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            view1.setBackground(getResources().getDrawable(R.drawable.circle_grey));
                            view2.setBackground(getResources().getDrawable(R.drawable.circle_red1));
                        }
                        else
                        {
                            view1.setBackgroundDrawable(getResources().getDrawable(R.drawable.circle_grey));
                            view2.setBackgroundDrawable(getResources().getDrawable(R.drawable.circle_red1));
                        }

                        break;
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               mActivity.getSupportFragmentManager().popBackStack();
            }
        });

        SetData();

    }

    private View Init(View view)
    {

        mActivity = getActivity();
        mContext  = getActivity();
        mGlobal   = (Globals) mActivity.getApplicationContext();

        header = (RelativeLayout) view.findViewById(R.id.header);
        tvActionbarTitle = (FontTextView) view.findViewById(R.id.tv_actionbar_title);
        btnEdit = (FontButton) view.findViewById(R.id.btn_edit);
        tvCategory   = (FontTextView) view.findViewById(R.id.tv_category);
        tvTitle      = (FontTextView) view.findViewById(R.id.tv_title);
        tvDate      = (FontTextView) view.findViewById(R.id.tv_date_day);
        tvCPD      = (FontTextView) view.findViewById(R.id.tv_points);
        tvWebsite      = (FontTextView) view.findViewById(R.id.tv_web);
        imgVp        = (ViewPager) view.findViewById(R.id.img_vp);
        tvDscription = (FontTextView) view.findViewById(R.id.tv_dscription);
        view1        = view.findViewById(R.id.view_1);
        view2        = view.findViewById(R.id.view_2);
        imgBack         = (ImageView) view.findViewById(R.id.img_back);
        btnEdit.setOnClickListener(this);
        return view;
    }


    private void SetData() {

        try {

            tvCategory.setText(event.getNon_event_subject());
            tvTitle.setText(event.getNon_event_name());
            tvDscription.setText(event.getNon_event_description());
            tvWebsite.setText(event.getNon_event_website());
            tvCPD.setText(event.getNon_event_cpd());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date dt1=sdf.parse(event.getNon_event_date());
            SimpleDateFormat sdf1 = new SimpleDateFormat("EEEE");
            String day=sdf1.format(dt1);
            tvDate.setText(day+" "+event.getNon_event_date());
        }
        catch (Exception e)
        {

        }

    }

    @Override
    public void onClick(View v) {

        if (v==btnEdit)
        {
            Bundle bundle = new Bundle();
            bundle.putString("ACTION","EDIT");
            bundle.putSerializable("EVENT",event);
            Utility.ReplaceFragment(new CreateNonAttendoFragment(), mActivity.getSupportFragmentManager(),bundle);
        }
    }
}
