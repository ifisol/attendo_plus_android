package ifisol.attendoplus.View.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import com.github.siyamed.shapeimageview.RoundedImageView;
import com.nostra13.universalimageloader.core.ImageLoader;

import fr.arnaudguyon.smartfontslib.FontTextView;
import ifisol.attendoplus.Global.Globals;
import ifisol.attendoplus.Model.Events;
import ifisol.attendoplus.R;
import ifisol.attendoplus.Utilities.Constants;
import ifisol.attendoplus.Utilities.Utility;

public class LogbookDetailFragment extends Fragment implements View.OnClickListener {


    FragmentActivity mActivity;
    Context  mContext;
    Globals  mGlobal;

    private WebView wvLogbook;



    private ImageView imgBack;
    private View             eventView;
    private RoundedImageView imgEvent;
    private FontTextView     tvEventName;
    private FontTextView     tvDateDay;
    private FontTextView     tvDateMonth;
    private FontTextView     tvLocation;
    private FontTextView     tvStrtEndTime;
    Events event;
    String mEventType;




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.log_book_event_detail, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Init(view);
    }



    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);
        event = (Events) getArguments().getSerializable("EVENT");
        mEventType = getArguments().getString(Constants.EVENT_TYPE);
        SetData();


    }

    private View Init(View view)
    {
        mActivity = getActivity();
        mContext  = getActivity();
        mGlobal   = (Globals) mActivity.getApplicationContext();
        wvLogbook = (WebView) view.findViewById(R.id.wbv_logbook);
        imgBack         = (ImageView) view.findViewById(R.id.img_back);
        eventView = view.findViewById(R.id.event_view);
        imgEvent = (RoundedImageView) view.findViewById(R.id.img_user);
        tvEventName = (FontTextView) view.findViewById(R.id.tv_eventName);
        tvDateDay = (FontTextView) view.findViewById(R.id.tv_date_day);
        tvDateMonth = (FontTextView) view.findViewById(R.id.tv_date_month);
        tvLocation = (FontTextView) view.findViewById(R.id.tv_location);
        tvStrtEndTime = (FontTextView) view.findViewById(R.id.tv_strt_end_time);

        wvLogbook.getSettings().setJavaScriptEnabled(true);
        imgBack.setOnClickListener(this);
        eventView.setOnClickListener(this);
        return view;
    }




    private void SetData() {
        ImageLoader.getInstance().displayImage(Constants.Img_Base_URL+event.getEvent_photo(),imgEvent);
        tvEventName.setText(event.getEvent_name());
        tvDateDay.setText(event.getEvent_date().split("-")[2]);
        tvDateMonth.setText(Utility.GetMonth(event.getEvent_date().split("-")[1]));
        tvLocation.setText(event.getEvent_street_address());
        tvLocation.setText(event.getEvent_street_address()+" "+event.getEvent_city()+" "+event.getEvent_country());
        tvStrtEndTime.setText(event.getEvent_start_time()+" - "+event.getEvent_end_time());

        wvLogbook.setWebViewClient(new WebViewClient() {


            //If you will not use this method url links are opeen in new brower not in webview
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }


            public void onPageFinished(WebView view, String url) {

                if (Globals.mLoadingDia!=null && Globals.mLoadingDia.isShowing())
                {
                    Globals.mLoadingDia.dismiss();
                }
            }

        });

        wvLogbook.loadUrl(Constants.Base_URL+event.getLogbook_url());
        Utility.ShowLoading(mActivity);



    }




    @Override
    public void onClick(View v) {

        if (v==eventView)
        {
            Bundle bundle = new Bundle();
            bundle.putString(Constants.TAG_VIEW_TO_GO, "");
            bundle.putString(Constants.EVENT_TYPE, mEventType);
            bundle.putSerializable("EVENT",event);
            Utility.ReplaceFragment(new EventsDetailPagerFragment(), mActivity.getSupportFragmentManager(), bundle);
        }
            else if (v==imgBack)
        {
            Bundle bundl = new Bundle();
            if (mEventType.equals(Constants.TAG_PENDING_LOG_EVENTS))
                bundl.putInt("POSITION", 0);
            if (mEventType.equals(Constants.TAG_FILLED_LOG_EVENTS))
                bundl.putInt("POSITION", 1);
            Utility.ReplaceFragment(new LogBookMainFragment(),mActivity.getSupportFragmentManager(),bundl);
        }
    }
}
