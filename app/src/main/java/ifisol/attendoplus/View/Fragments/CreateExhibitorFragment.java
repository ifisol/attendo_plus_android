package ifisol.attendoplus.View.Fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.sandrios.sandriosCamera.internal.SandriosCamera;
import com.sandrios.sandriosCamera.internal.configuration.CameraConfiguration;

import java.io.File;
import java.io.IOException;

import fr.arnaudguyon.smartfontslib.FontButton;
import fr.arnaudguyon.smartfontslib.FontEditText;
import ifisol.attendoplus.Global.Globals;
import ifisol.attendoplus.R;
import ifisol.attendoplus.Utilities.Constants;
import ifisol.attendoplus.Utilities.HttpHelper;
import ifisol.attendoplus.Utilities.ResizeImage;
import ifisol.attendoplus.Utilities.Utility;
import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.RequestBody;

public class CreateExhibitorFragment extends Fragment implements View.OnClickListener, HttpHelper.HttpCallback {


    FragmentActivity mActivity;
    Context  mContext;
    Globals  mGlobal;


    private ImageView imgSelectImg;
    private CircularImageView imgUser;
    private FontEditText edName;
    private FontEditText edAddress;
    private FontEditText edEmail;
    private FontEditText edPhone;
    private FontEditText edWebsite;
    private FontEditText edDescription;
    private FontButton btnCreate;
    private ImageView imgBack;

    private static final int CAPTURE_MEDIA = 368;

    Bitmap UserImage;
    String base64Img;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.create_exhibitor_fragment, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Init(view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);

        imgBack.setOnClickListener(this);
    }

    private View Init(View view)
    {

        mActivity = getActivity();
        mContext  = getActivity();
        mGlobal   = (Globals) mActivity.getApplicationContext();
        imgBack   = (ImageView) view.findViewById(R.id.img_back);

        imgSelectImg = (ImageView) view.findViewById(R.id.img_select_img);
        imgUser = (CircularImageView) view.findViewById(R.id.img_user);
        edName = (FontEditText) view.findViewById(R.id.ed_name);
        edAddress = (FontEditText) view.findViewById(R.id.ed_address);
        edEmail = (FontEditText) view.findViewById(R.id.ed_email);
        edPhone = (FontEditText) view.findViewById(R.id.ed_phone);
        edWebsite = (FontEditText) view.findViewById(R.id.ed_website);
        edDescription = (FontEditText) view.findViewById(R.id.ed_description);
        btnCreate = (FontButton) view.findViewById(R.id.btn_create);



        imgSelectImg.setOnClickListener(this);
        btnCreate.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {


        switch (v.getId())
        {
            case R.id.img_back:
                mActivity.getSupportFragmentManager().popBackStack();
                break;

            case R.id.img_select_photo:
                launchCamera();
                break;

            case R.id.btn_create:
                Create();
                break;

        }

    }




    private void Create() {

        String username = edName.getText().toString().trim();
        String address = edAddress.getText().toString().trim();
        String email = edEmail.getText().toString().trim();
        String phone = edPhone.getText().toString().trim();
        String website = edWebsite.getText().toString().trim();
        String description = edDescription.getText().toString().trim();

        if (username.isEmpty())
            Utility.ShowAlert(mActivity, "Name", "Please enter Exhibitor Name");
        else if (address.isEmpty())
            Utility.ShowAlert(mActivity, "Address", "Please enter address");
        else if (email.isEmpty())
            Utility.ShowAlert(mActivity, "Email", "Please enter Email");
        else if (!Utility.EmailValidator(email))
            Utility.ShowAlert(mActivity, "Invalid Email", "Please enter a valid Email address");
        else if (phone.isEmpty())
            Utility.ShowAlert(mActivity, "Phone Number", "Please enter Phone Number");
        else if (description.equals(""))
            Utility.ShowAlert(mActivity, "Description", "Please provide brief description about Exhibitor");




        else {
            try {
                RequestBody formBody = new FormBody.Builder()
                        .add("user_id", mGlobal.mActiveUser.getUser_id())
                        .add("user_name", username)
                        .add("email", email)
                        .add("address", address)
                        .add("phone", phone)
                        .add("device_type", "Android")
                        .add("picture",base64Img).build();
                HttpHelper.CallApi( mActivity, Constants.Mark_attendance, formBody, this, true);
            } catch (Exception e) {
                e.printStackTrace();
                Log.d("Exception...........", e.getMessage());
            }
        }
    }





    private void launchCamera() {
        new SandriosCamera(mActivity,this, CAPTURE_MEDIA)
                .setShowPicker(true)
                //.setVideoFileSize(15) //File Size in MB: Default is no limit
                .setMediaAction(CameraConfiguration.MEDIA_ACTION_PHOTO) // default is CameraConfiguration.MEDIA_ACTION_BOTH
                .enableImageCropping(true) // Default is false.
                .launchCamera();
    }





    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CAPTURE_MEDIA && resultCode == mActivity.RESULT_OK) {
            try {

                ResizeImage resizeImage = new ResizeImage(mContext);
                String uri = resizeImage.compressImage(data.getStringExtra(CameraConfiguration.Arguments.FILE_PATH));
                File imgFile = new File(uri);
                if (imgFile.exists()) {
                    UserImage = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                    imgUser.setImageBitmap(UserImage);
                    base64Img = Utility.EncodeTobase64(UserImage);
                }



                Log.e("File >>>>>>>>>>>>>", "" + data.getStringExtra(CameraConfiguration.Arguments.FILE_PATH));
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void onFailure(Call call, IOException e) {

    }

    @Override
    public void onSuccess(Call call, String response) {

    }
}
