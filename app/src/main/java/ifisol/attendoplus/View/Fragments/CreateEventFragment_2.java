package ifisol.attendoplus.View.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import ifisol.attendoplus.Controller.Adapters.ExhibitorsGridAdapter;
import ifisol.attendoplus.Global.Globals;
import ifisol.attendoplus.Model.Events;
import ifisol.attendoplus.Model.Exhibitors;
import ifisol.attendoplus.R;
import ifisol.attendoplus.Utilities.Constants;
import ifisol.attendoplus.Utilities.CreateEventStepsListener;
import ifisol.attendoplus.Utilities.HttpHelper;
import ifisol.attendoplus.Utilities.ItemDecorationAlbumColumns;
import ifisol.attendoplus.Utilities.Utility;
import ifisol.attendoplus.View.Activities.ActivityMain;
import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.RequestBody;

public class CreateEventFragment_2 extends Fragment implements View.OnClickListener, HttpHelper.HttpCallback, ExhibitorsGridAdapter.clickListener {

    FragmentActivity mActivity;
    Context  mContext;
    Globals mGlobal;

    RecyclerView mExhibitorsRV;
    ExhibitorsGridAdapter exhibitorsGridAdapter;

    CreateEventStepsListener stepsListener;
    private TextView btnNext;
    FloatingActionButton fab_add;

    ImageView imgNoRecord;
    ArrayList<Exhibitors> mExhibList;
    TextView btnSkip;
    String exhIDs;
    String eventID;
    Events event;
    boolean editing;
    String addSkip="";
    View v;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
         if (v== null)
             v = inflater.inflate(R.layout.create_event_fragment_2, null);

        return Init(v);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        stepsListener = (CreateEventStepsListener) getParentFragment();
        stepsListener.UpdateSteps(1);
        ActivityMain.mTvABTitle.setText("Add Exhibitors");

        event =(Events) getArguments().getSerializable("EVENT");
        eventID = event.getEvent_id();
        editing = getArguments().getBoolean("EDITING");
        if (editing)
        {
            if (event.getHas_exhibitor().equals("yes"))
                btnSkip.setVisibility(View.GONE);
        }


            FetchExhibitors();





        btnNext.setOnClickListener(this);
        fab_add.setOnClickListener(this);
        btnSkip.setOnClickListener(this);
        CreateEventMainFragment.imgBack.setOnClickListener(this);


    }

    private View Init(View v)
    {
        mActivity = getActivity();
        mContext  = getActivity();
        mGlobal = (Globals) mActivity.getApplicationContext();
        imgNoRecord   = (ImageView) v.findViewById(R.id.img_no_record);
        btnSkip       = (TextView) v.findViewById(R.id.btn_skip);

        mExhibitorsRV = (RecyclerView) v.findViewById(R.id.rv_exhibitors);
        mExhibitorsRV.setNestedScrollingEnabled(false);

        btnNext       = (TextView) v.findViewById(R.id.btn_exhibitor_next);
        fab_add       =  (FloatingActionButton) v.findViewById(R.id.fab_add);

        return v;
    }



    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.btn_exhibitor_next:
                addSkip = "add";
                SelectOrganiser();
                break;
            case R.id.fab_add:
                Utility.ReplaceFragment(new CreateExhibitorFragment(), mActivity.getSupportFragmentManager());
                break;
            case R.id.btn_skip:
                addSkip = "skip";
                Skip();
                break;
            case R.id.img_back:

                Bundle bundle = new Bundle();
                bundle.putSerializable("EVENT", event);
                Utility.ReplaceFragment_create_event(new CreateEventFragment_1(),getFragmentManager(),bundle);


                break;
        }
    }






    private void Skip() {
        RequestBody formBody = new FormBody.Builder()
                .add("user_id",mGlobal.mActiveUser.getUser_id())
                .add("event_id",eventID)
                .add("status", addSkip)
                .build();
        try {
            HttpHelper.CallApi(mActivity, Constants.Add_event_exhibitors,formBody,this,true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }








    private void SelectOrganiser() {
        String IDseparator = "";
        StringBuilder sbIDs = new StringBuilder();

        for (Exhibitors obj : exhibitorsGridAdapter.mExList) {

            if (obj.isIs_selected()) {
                {
                    sbIDs.append(IDseparator);
                    IDseparator = ",";
                    sbIDs.append(obj.getExhibitor_id());
                }

            }
        }

        exhIDs  = sbIDs.toString();
        if (exhIDs==null || exhIDs.equals(""))
            Utility.ShowAlert(mActivity,"Exhibitors","No Exhibitors selected");
        else
        {

            RequestBody formBody = new FormBody.Builder()
                    .add("user_id",mGlobal.mActiveUser.getUser_id())
                    .add("event_id",eventID)
                    .add("exhibitors", exhIDs)
                    .add("status", addSkip)
                    .build();
            try {
                HttpHelper.CallApi(mActivity, Constants.Add_event_exhibitors,formBody,this,true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        Log.d("IDs",  exhIDs);
    }





    private void FetchExhibitors() {

        FormBody.Builder builder = new FormBody.Builder();

                builder.add("user_id",mGlobal.mActiveUser.getUser_id());
                builder.add("event_id",event.getEvent_id());

        try {
            RequestBody formBody = builder.build();
            HttpHelper.CallApi(mActivity, Constants.Get_Exhibitors,formBody,this,true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }







    @Override
    public void onFailure(Call call, IOException e) {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

            }
        });
    }

    @Override
    public void onSuccess(Call call, String response) {
        Log.d("Res~~~~~~~~~~~~~", response);
        try {
            if (call.request().url().toString().equals(Constants.Get_Exhibitors))
            {
                Gson gson = new Gson();
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.getString("status").equals("true"))
                {
                    final Exhibitors[] exhibitors = gson.fromJson(jsonObject.getString("response"), Exhibitors[].class);
                    mExhibList = new ArrayList<>(Arrays.asList(exhibitors));
                }

                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        SetAdapter(mExhibList);
                    }
                });
            }
            else if (call.request().url().toString().equals(Constants.Add_event_exhibitors))
            {
                Gson gson = new Gson();
                final JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.getString("status").equals("true"))
                {
                    if (addSkip.equals("skip"))
                        event.setHas_exhibitor("no");
                    else if (addSkip.equals("add"))
                        event.setHas_exhibitor("yes");
                    mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Bundle bundle = new Bundle();
                            bundle.putBoolean("EDITING", editing);
                            bundle.putSerializable("EVENT", event);
                            Utility.ReplaceFragment_create_event(new CreateEventFragment_3(),getFragmentManager(),bundle);

                        }
                    });
                }
                else
                {
                    mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            try {
                                Toast.makeText(mActivity, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }




        } catch (JSONException e) {
            e.printStackTrace();
        }
    }



    private void SetAdapter(ArrayList<Exhibitors> mList)
    {
        if (mList!=null && mList.size()>0)
        {

            imgNoRecord.setVisibility(View.GONE);
            mExhibitorsRV.setHasFixedSize(true);
            mExhibitorsRV.setLayoutManager(new GridLayoutManager(mContext,2));
            exhibitorsGridAdapter = new ExhibitorsGridAdapter( mActivity,this, mExhibList);
            mExhibitorsRV.setAdapter(exhibitorsGridAdapter);
            mExhibitorsRV.addItemDecoration(new ItemDecorationAlbumColumns(getResources().
                    getDimensionPixelSize(R.dimen.dp_1), 2));

            btnNext.setVisibility(View.VISIBLE);

            exhibitorsGridAdapter.setClickListener(this);

        }
        else
        {

            imgNoRecord.setVisibility(View.VISIBLE);
            btnNext.setVisibility(View.GONE);
        }


    }

    @Override
    public void onItemClick(int pos, Exhibitors obj) {
        if (exhibitorsGridAdapter.mExList.get(pos).isIs_selected())
            exhibitorsGridAdapter.mExList.get(pos).setIs_selected(false);
        else
            exhibitorsGridAdapter.mExList.get(pos).setIs_selected(true);

        exhibitorsGridAdapter.notifyDataSetChanged();
    }
}