package ifisol.attendoplus.View.Fragments;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.internal.NavigationMenu;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.github.siyamed.shapeimageview.RoundedImageView;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import fr.arnaudguyon.smartfontslib.FontButton;
import fr.arnaudguyon.smartfontslib.FontTextView;
import ifisol.attendoplus.Global.Globals;
import ifisol.attendoplus.Model.Events;
import ifisol.attendoplus.R;
import ifisol.attendoplus.Utilities.Constants;
import ifisol.attendoplus.Utilities.HttpHelper;
import ifisol.attendoplus.Utilities.Utility;
import io.github.yavski.fabspeeddial.FabSpeedDial;
import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.RequestBody;

public class EventDetailFragment extends Fragment implements View.OnClickListener, HttpHelper.HttpCallback {


    FragmentActivity mActivity;
    EventDetailFragment fragment;
    Context  mContext;
    Globals  mGlobal;

    private FontTextView tvEventName;
    private FontTextView tvCpd;
    private FontTextView tvExhibitors;
    private ImageView    imgLogbook;
    private ImageView    imgPublic;
    private FontTextView tvLogbook;
    private ImageView    imgFeedback;
    private FontTextView tvFeedback;
    private RelativeLayout viewInterested;
    private FontTextView tvInterestedCount;
    private RoundedImageView imgEvent;
    private FontTextView tvDateDay;
    private FontTextView tvStartEndTime;
    private View         viewLocation;
    private FontTextView tvAddressRegion;
    private FontTextView tvAddressStreet;
    private FontTextView tvDescription;
    private View         viewOrganisation;
    private FontTextView tvOrganisation;
    private FontTextView tvType;
    private FontButton btnInterested;
    private FabSpeedDial fabEdit;
    private View viewBlur;
    String  mEventType;
    Events  mEvent;





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return Init(inflater.inflate(R.layout.event_detail_fragment, null));
    }





    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mEventType = getArguments().getString(Constants.EVENT_TYPE);
        mEvent     = (Events)getArguments().getSerializable("EVENT");

        if (mGlobal.mActiveUser.getUser_type().equals(Constants.TAG_GUEST))
        {
            fabEdit.setVisibility(View.GONE);
            if (mEventType.equals(Constants.TAG_UPCOMING)) {
                btnInterested.setVisibility(View.VISIBLE);
                if (mEvent.getUser_is_interested().equals("yes"))
                    btnInterested.setText("Undo Interested");
                else btnInterested.setText("Interested");
            } else btnInterested.setVisibility(View.GONE);

        }
        else
        {
            btnInterested.setVisibility(View.GONE);

            if (mEventType.equals(Constants.TAG_ONGOING_EVENTS))
                fabEdit.setVisibility(View.GONE);
            else if (mEventType.equals(Constants.TAG_AWAITING)|| mEventType.equals(Constants.TAG_UPCOMING))
            {
                if (mEvent.getIs_my_created_event().equals("yes"))
                {
                    fabEdit.setVisibility(View.VISIBLE);
                    fabEdit.setMenuListener(new FabSpeedDial.MenuListener() {
                        @Override
                        public boolean onPrepareMenu(NavigationMenu navigationMenu) {

                            viewBlur.setVisibility(View.VISIBLE);
                            return true;
                        }

                        @Override
                        public boolean onMenuItemSelected(MenuItem menuItem) {

                            switch (menuItem.getItemId())
                            {
                                case R.id.edit_event:
                                    if (mEventType.equals(Constants.TAG_UPCOMING))
                                         editWarning();
                                    else
                                        editEvent();

                                    break;
                                case R.id.delete_event:
                                      deleteEvent();
                                    break;
                            }
                            return true;
                        }

                        @Override
                        public void onMenuClosed() {
                            viewBlur.setVisibility(View.GONE);
                        }
                    });
                }
                else
                    fabEdit.setVisibility(View.GONE);
            }
            else
                fabEdit.setVisibility(View.GONE);

        }

        SetData(mEvent);

    }




    private void editEvent() {
        Bundle bundle = new Bundle();
        bundle.putSerializable("EVENT", mEvent);
        Utility.ReplaceFragment(new CreateEventMainFragment(),mActivity.getSupportFragmentManager(),bundle);
    }




    private View Init(View view)
    {
        mActivity = getActivity();
        mContext  = getActivity();
        fragment  = this;
        mGlobal   = (Globals) mActivity.getApplicationContext();

        tvEventName         = (FontTextView) view.findViewById(R.id.tv_eventName);
        tvCpd               = (FontTextView) view.findViewById(R.id.tv_cpd);
        tvExhibitors        = (FontTextView) view.findViewById(R.id.tv_exhibitors);
        imgLogbook          = (ImageView) view.findViewById(R.id.img_logbook);
        imgPublic           = (ImageView) view.findViewById(R.id.img_public);
        tvLogbook           = (FontTextView) view.findViewById(R.id.tv_logbook);
        imgFeedback         = (ImageView) view.findViewById(R.id.img_feedback);
        tvFeedback          = (FontTextView) view.findViewById(R.id.tv_feedback);
        viewInterested      = (RelativeLayout) view.findViewById(R.id.view_interested);
        tvInterestedCount   = (FontTextView) view.findViewById(R.id.tv_interested_count);
        imgEvent            = (RoundedImageView) view.findViewById(R.id.img_event);
        tvDateDay           = (FontTextView) view.findViewById(R.id.tv_date_day);
        tvStartEndTime      = (FontTextView) view.findViewById(R.id.tv_start_end_time);
        viewLocation        = view.findViewById(R.id.view_location);
        tvAddressRegion     = (FontTextView) view.findViewById(R.id.tv_address_region);
        tvAddressStreet     = (FontTextView) view.findViewById(R.id.tv_address_street);
        tvDescription       = (FontTextView) view.findViewById(R.id.tv_description);
        viewOrganisation    =  view.findViewById(R.id.view_organisation);
        tvOrganisation      = (FontTextView) view.findViewById(R.id.tv_organisation);
        tvType              = (FontTextView) view.findViewById(R.id.tv_type);
        btnInterested       = (FontButton) view.findViewById(R.id.btn_interested);
        fabEdit             = (FabSpeedDial) view.findViewById(R.id.fab_edit);
        viewBlur            =  view.findViewById(R.id.view_blur);

        viewLocation.setOnClickListener(this);
        viewOrganisation.setOnClickListener(this);
        btnInterested.setOnClickListener(this);
        return view;
    }








    @Override
    public void onClick(View v) {
        Bundle bundle=null;
        switch (v.getId())
        {
            case R.id.view_location:
                bundle = new Bundle();
                bundle.putSerializable("EVENT",mEvent );
                Utility.ReplaceFragment(new MapFragment(), mActivity.getSupportFragmentManager(),bundle);
                break;
            case R.id.view_organisation:
                bundle = new Bundle();
                bundle.putString("ORGANISATION_ID", mEvent.getEvent_organisation_id());
                Utility.ReplaceFragment(new Organisation_MainFragment(), mActivity.getSupportFragmentManager(),bundle);
                break;
            case R.id.btn_interested:
                Interested();
                break;
        }
    }






    private void Interested() {
        RequestBody body = new FormBody.Builder()
                .add("user_id", mGlobal.mActiveUser.getUser_id())
                .add("event_id", mEvent.getEvent_id())
                .add("status",mEvent.getUser_is_interested().equals("yes")?"no":"yes").build();
        try {
            HttpHelper.CallApi(mActivity,Constants.Im_interested,body,this,true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }






    private void SetData(Events mEvent) {

        try {
            tvEventName.setText(mEvent.getEvent_name());
            tvCpd.setText("CPD Points: "+ mEvent.getEvent_cpd());
            tvExhibitors.setText(mEvent.getHas_exhibitor().equals("yes")?"Exhibitors : "+mEvent.getExhibitor_count():"No Exhibitors");
            
            if (mEvent.getInterested_guests()>3)
            {
                viewInterested.setVisibility(View.VISIBLE);
                tvInterestedCount.setText(String.valueOf(mEvent.getInterested_guests())+"+");
            }
            else
                viewInterested.setVisibility(View.GONE);
            ImageLoader.getInstance().displayImage(Constants.Img_Base_URL+mEvent.getEvent_photo(),imgEvent);
            tvStartEndTime.setText(mEvent.getEvent_start_time()+" - "+mEvent.getEvent_end_time());
            tvAddressRegion.setText(mEvent.getEvent_city());
            tvAddressStreet.setText(mEvent.getEvent_street_address());
            tvDescription.setText(mEvent.getEvent_description());
            tvOrganisation.setText(mEvent.getEvent_organisation_name());
            tvType.setText(mEvent.getEvent_type_name());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date dt1=sdf.parse(mEvent.getEvent_date());
            SimpleDateFormat sdf1 = new SimpleDateFormat("EEEE");
            String day=sdf1.format(dt1);
            tvDateDay.setText(day+" "+mEvent.getEvent_date());
            if (mEvent.getIs_public_event().equals("yes"))
                imgPublic.setVisibility(View.VISIBLE);
            else
                imgPublic.setVisibility(View.GONE);



            if (mGlobal.mActiveUser.getUser_type().equals(Constants.TAG_GUEST) && mEventType.equals(Constants.TAG_ATTENDED))
                SetAttendedEvents();
            else
                SetUpcomingEvents();
            
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }






    private void SetAttendedEvents() {

        if (mEvent.getHas_logbook().equals("yes"))
        {
            if (mEvent.getHas_filled_logbook().equals("yes"))
                imgLogbook.setImageResource(R.drawable.ic_filled_log_book);
            else
                imgLogbook.setImageResource(R.drawable.ic_not_filled_logbook);

            tvLogbook.setTextColor(mActivity.getResources().getColor(R.color.black));
        }
        else
        {
            imgLogbook.setImageResource(R.drawable.ic_no_logbook);
            tvLogbook.setTextColor(mActivity.getResources().getColor(R.color.trans_black3));
        }

        if (mEvent.getHas_feedback().equals("yes"))
        {
            if (mEvent.getHas_filled_logbook().equals("yes"))
                imgFeedback.setImageResource(R.drawable.ic_filled_feed_back);
            else
                imgFeedback.setImageResource(R.drawable.ic_not_filled_feedback);

            tvFeedback.setTextColor(mActivity.getResources().getColor(R.color.black));
        }
        else
        {
            imgFeedback.setImageResource(R.drawable.ic_no_feedback);
            tvFeedback.setTextColor(mActivity.getResources().getColor(R.color.trans_black3));
        }
    }







    private void SetUpcomingEvents() {
        if (mEvent.getHas_logbook().equals("yes"))
        {
           imgLogbook.setImageResource(R.drawable.ic_has_logbook);
           tvLogbook.setTextColor(mActivity.getResources().getColor(R.color.black));
        }
        else
        {
            imgLogbook.setImageResource(R.drawable.ic_no_logbook);
            tvLogbook.setTextColor(mActivity.getResources().getColor(R.color.trans_black3));
        }

        if (mEvent.getHas_feedback().equals("yes"))
        {
            imgFeedback.setImageResource(R.drawable.ic_has_feedback);
            tvFeedback.setTextColor(mActivity.getResources().getColor(R.color.black));
        }
        else
        {
            imgFeedback.setImageResource(R.drawable.ic_no_feedback);
            tvFeedback.setTextColor(mActivity.getResources().getColor(R.color.trans_black3));
        }
    }







    @Override
    public void onFailure(Call call, final IOException e) {

        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                Toast.makeText(mActivity,"Request Timeout", Toast.LENGTH_SHORT).show();
            }
        });

    }
    @Override
    public void onSuccess(final Call call, String response) {

        Log.d("Response . . . . .", response);


        try {
            final JSONObject json = new JSONObject(response);
            if (json.getString("status").equals("true")) {
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (call.request().url().toString().equals(Constants.Delete_event))
                        {
                            Bundle bundle2 = new Bundle();
                            if (mEventType.equals(Constants.TAG_AWAITING))
                                bundle2.putInt("POSITION", 0);
                            else if (mEventType.equals(Constants.TAG_UPCOMING))
                                bundle2.putInt("POSITION", 1);

                            Utility.ReplaceFragment(new EventsPagerFragment(),mActivity.getSupportFragmentManager(),bundle2);
                        }
                        else
                        {
                            mEvent.setUser_is_interested(mEvent.getUser_is_interested().equals("yes")?"no":"yes");
                            if (mEvent.getUser_is_interested().equals("yes"))
                                btnInterested.setText("Undo Interested");
                            else btnInterested.setText("Interested");
                        }

                    }
                });
            } else {
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (call.request().url().toString().equals(Constants.Delete_event))
                            Toast.makeText(mActivity, json.getString("message"), Toast.LENGTH_SHORT).show();
                           else Utility.ShowAlert(mActivity, "Failed", json.getString("message"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        } catch (JSONException e) {
        }

    }


    public void editWarning() {
        final Dialog dialog = new Dialog(mActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.edit_event_warning);
        DisplayMetrics dm = new DisplayMetrics();
        mActivity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER | Gravity.START);
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        FontButton btnOK = (FontButton) dialog.findViewById(R.id.btn_ok);
        FontButton btnCancel = (FontButton) dialog.findViewById(R.id.btn_cancel);
        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                editEvent();
                dialog.dismiss();
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dialog.dismiss();
            }
        });
        dialog.show();
    }


    private void deleteEvent()
    {
        RequestBody body = new FormBody.Builder()
                .add("user_id", mGlobal.mActiveUser.getUser_id())
                .add("event_id", mEvent.getEvent_id()).build();
        try {
            HttpHelper.CallApi(mActivity,Constants.Delete_event,body,this,true);
            mEvent.setUser_is_interested(mEvent.getUser_is_interested().equals("yes")?"no":"yes");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
