package ifisol.attendoplus.View.Fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONObject;

import java.io.IOException;

import fr.arnaudguyon.smartfontslib.FontTextView;
import ifisol.attendoplus.Global.Globals;
import ifisol.attendoplus.Model.Organisations;
import ifisol.attendoplus.R;
import ifisol.attendoplus.Utilities.Constants;
import ifisol.attendoplus.Utilities.HttpHelper;
import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.RequestBody;

public class OrganisationDetailFragment extends Fragment implements View.OnClickListener, HttpHelper.HttpCallback {


    Activity mActivity;
    Context  mContext;
    Globals  mGlobal;


    private CircularImageView imgOrg;
    private FontTextView      tvName;
    private FontTextView      tvPhone;
    private FontTextView      tvWeb;
    private FontTextView      tvAddressStreet;
    private FontTextView      tvDescription;
    Organisations mOrg;
    View blankView;

    String orgID;
    View v;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        mActivity = getActivity();
        mContext  = getActivity();
        mGlobal   = (Globals) mActivity.getApplicationContext();

        orgID = getArguments().getString("ORGANISATION_ID");

        FetchDetaOrgDetail();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (v==null)
            v = inflater.inflate(R.layout.organisation_detail_fragment, null);

        return v;
    }





    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Init(view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);

    }



    private View Init(View view)
    {
        imgOrg = (CircularImageView) view.findViewById(R.id.img_org);
        tvName = (FontTextView) view.findViewById(R.id.tv_name);
        tvPhone = (FontTextView) view.findViewById(R.id.tv_phone);
        tvWeb = (FontTextView) view.findViewById(R.id.tv_web);
        tvAddressStreet = (FontTextView) view.findViewById(R.id.tv_address_street);
        tvDescription = (FontTextView) view.findViewById(R.id.tv_description);
        blankView = view.findViewById(R.id.view_blank);

        return view;
    }






    private void SetData(Organisations organisation)
    {
        blankView.setVisibility(View.GONE);
        ImageLoader.getInstance().displayImage(Constants.Img_Base_URL+organisation.getOrganisation_image(),imgOrg);
        tvName.setText(organisation.getOrganisation_name());
        tvPhone.setText(organisation.getOrganisation_ph_number());
        tvWeb.setText(organisation.getOrganisation_website());
        tvAddressStreet.setText(organisation.getOrganisation_address());
        tvDescription.setText(organisation.getOrganisation_description());
    }





    @Override
    public void onClick(View v) {

    }







    private void FetchDetaOrgDetail() {
        RequestBody body = new FormBody.Builder().add("user_id", mGlobal.mActiveUser.getUser_id()).add("organisation_id", orgID).add("tag", "organisation_information").build();
        try {
            HttpHelper.CallApi(mActivity, Constants.Organisation_detail, body, this, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onFailure(Call call,final IOException e) {

        e.printStackTrace();

        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                Toast.makeText(mActivity,"Request Timeout", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public void onSuccess(Call call, String response) {
        ParseOrganisations(response);
    }





    private void ParseOrganisations(String s)
    {
        try {
            JSONObject jsonObj = new JSONObject(s);
            Gson gson = new Gson();
            mOrg = gson.fromJson(jsonObj.getString("response"),Organisations.class);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                SetData(mOrg);
            }
        });
    }


}
