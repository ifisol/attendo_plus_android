package ifisol.attendoplus.View.Fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import fr.arnaudguyon.smartfontslib.FontTextView;
import ifisol.attendoplus.Controller.Adapters.ManualAttendancePagerAdapter;
import ifisol.attendoplus.Global.Globals;
import ifisol.attendoplus.Model.Events;
import ifisol.attendoplus.R;

public class MarkAttendanceMainFragment extends Fragment  {


    FragmentActivity mActivity;
    Context  mContext;
    Globals  mGlobal;

    private TabLayout tabs;
    private ViewPager attendanceVP;
    private ManualAttendancePagerAdapter adapter;
    FontTextView mTvTitle;
    ImageView imgBack;
    Events event;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return Init(inflater.inflate(R.layout.mark_attendance_main_fragment, null));
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        event = (Events) getArguments().getSerializable("EVENT");

        SetUpViewPager();

        tabs.setOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(attendanceVP) {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                super.onTabSelected(tab);
                View tabView = (View) tab.getCustomView();
                ImageView tabIcon = (ImageView) tabView.findViewById(R.id.img_tab);
                tabIcon.setImageResource(adapter.GetActIcon(tab.getPosition()));
                mTvTitle.setText(adapter.getPageTitle(tab.getPosition()));
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

                super.onTabUnselected(tab);
                View tabView = (View) tab.getCustomView();
                ImageView tabIcon = (ImageView) tabView.findViewById(R.id.img_tab);
                tabIcon.setImageResource(adapter.GetNormIcon(tab.getPosition()));
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

                super.onTabReselected(tab);
            }
        });

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mActivity.getSupportFragmentManager().popBackStack();
            }
        });


    }

    private View Init(View view)
    {

        mActivity = getActivity();
        mContext  = getActivity();
        mGlobal   = (Globals) mActivity.getApplicationContext();
        mTvTitle = (FontTextView) view.findViewById(R.id.tv_title);
        imgBack  = (ImageView) view.findViewById(R.id.img_back);

        tabs            = (TabLayout) view.findViewById(R.id.tabs);
        attendanceVP    = (ViewPager) view.findViewById(R.id.attendance_view_pager);


        return view;
    }

    private void SetUpViewPager() {

        adapter = new ManualAttendancePagerAdapter(getChildFragmentManager(),mContext,event);
        attendanceVP.setAdapter(adapter);

        tabs.setupWithViewPager(attendanceVP);

        for (int i = 0; i < tabs.getTabCount(); i++) {
            tabs.getTabAt(i).setCustomView(adapter.getTabView(i));
        }

        TabLayout.Tab tab = tabs.getTabAt(0);
        View tabView = (View) tab.getCustomView();
        ImageView tabIcon = (ImageView) tabView.findViewById(R.id.img_tab);
        tabIcon.setImageResource(adapter.GetActIcon(0));
        mTvTitle.setText(adapter.getPageTitle(0));
        
    }

}
