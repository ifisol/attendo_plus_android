package ifisol.attendoplus.View.Fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.sandrios.sandriosCamera.internal.SandriosCamera;
import com.sandrios.sandriosCamera.internal.configuration.CameraConfiguration;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import fr.arnaudguyon.smartfontslib.FontButton;
import fr.arnaudguyon.smartfontslib.FontEditText;
import fr.arnaudguyon.smartfontslib.FontRadioButton;
import fr.arnaudguyon.smartfontslib.FontTextView;
import ifisol.attendoplus.Controller.Adapters.EventsTypeAdapter;
import ifisol.attendoplus.Global.Globals;
import ifisol.attendoplus.Model.Event_Types;
import ifisol.attendoplus.Model.Events;
import ifisol.attendoplus.R;
import ifisol.attendoplus.Utilities.Constants;
import ifisol.attendoplus.Utilities.CreateEventStepsListener;
import ifisol.attendoplus.Utilities.HttpHelper;
import ifisol.attendoplus.Utilities.ResizeImage;
import ifisol.attendoplus.Utilities.Utility;
import ifisol.attendoplus.View.Activities.PickLocationActivity;
import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.RequestBody;


public class CreateEventFragment_1 extends Fragment implements View.OnClickListener, HttpHelper.HttpCallback {

    FragmentActivity mActivity;
    Context mContext;
    Globals mGlobal;


    private LinearLayout    viewInfo;
    private FontEditText    edEventTitle;
    private FontTextView    tvEventType;
    private FontEditText    edLocation;
    private FontTextView    tvDate;
    private FontEditText    edPoints;
    private FontEditText    edNoOfGuests;
    private FontEditText    edEventDetails;
    private FontEditText    edUrl;
    private FontEditText    edCountry;
    private FontEditText    edCity;
    private FontRadioButton radioPrivate;
    private FontRadioButton radioPublic;
    private LinearLayout    viewDate;
    private RelativeLayout  viewStartTime;
    private FontTextView    tvStartTime;
    private RelativeLayout  viewEndTime;
    private FontTextView    tvEndTime;
    private ImageView       imgEvent;
    private ImageView       imgPlaceholder;
    private FontButton      btnInfoNext;
    private ImageView       imgInfo;
    private ImageView       imgMap;


    String typeId;
    Bitmap eventImg;
    String eventImgString="";
    String address="";
    String latlng;

    private static final int CAPTURE_MEDIA = 111;
    private static final int LOCATION_CODE = 222;
    ArrayList<Event_Types> mTypes;
    CreateEventStepsListener stepsListener;
    boolean editing;


    Events mEvent;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        return Init(inflater.inflate(R.layout.create_event_fragment_1, null));
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);
        stepsListener = (CreateEventStepsListener) getParentFragment();
        FetchEventTypes();

        if (getArguments()!=null)
        {
            mEvent = (Events) getArguments().getSerializable("EVENT");
            editing = true;
            stepsListener.UpdateSteps(-1);
            setData();
        }
        else
        {
            stepsListener.UpdateSteps(0);
            editing=false;
        }


    }





    private View Init(View view) {

        mActivity      = getActivity();
        mContext       = getActivity();
        mGlobal        = (Globals) mActivity.getApplicationContext();
        viewInfo       = (LinearLayout) view.findViewById(R.id.view_info);
        edEventTitle   = (FontEditText) view.findViewById(R.id.ed_event_title);
        tvEventType    = (FontTextView) view.findViewById(R.id.tv_event_type);
        edLocation     = (FontEditText) view.findViewById(R.id.ed_location);
        tvDate         = (FontTextView) view.findViewById(R.id.tv_date);
        edPoints       = (FontEditText) view.findViewById(R.id.ed_points);
        edNoOfGuests   = (FontEditText) view.findViewById(R.id.ed_no_of_guests);
        edEventDetails = (FontEditText) view.findViewById(R.id.ed_event_details);
        edUrl          = (FontEditText) view.findViewById(R.id.ed_url);
        edCountry      = (FontEditText) view.findViewById(R.id.ed_country);
        edCity         = (FontEditText) view.findViewById(R.id.ed_city);
        radioPrivate   = (FontRadioButton) view.findViewById(R.id.radio_private);
        radioPublic    = (FontRadioButton) view.findViewById(R.id.radio_public);
        viewDate       = (LinearLayout) view.findViewById(R.id.view_date);
        viewStartTime  = (RelativeLayout) view.findViewById(R.id.view_start_time);
        tvStartTime    = (FontTextView) view.findViewById(R.id.tv_start_time);
        viewEndTime    = (RelativeLayout) view.findViewById(R.id.view_end_time);
        tvEndTime      = (FontTextView) view.findViewById(R.id.tv_end_time);
        imgEvent       = (ImageView) view.findViewById(R.id.img_event);
        imgPlaceholder = (ImageView) view.findViewById(R.id.img_placeholder);
        btnInfoNext    = (FontButton) view.findViewById(R.id.btn_info_next);
        imgInfo        = (ImageView) view.findViewById(R.id.img_info);
        imgMap         = (ImageView) view.findViewById(R.id.img_map);


        tvEventType.setOnClickListener(this);
        btnInfoNext.setOnClickListener(this);
        viewStartTime.setOnClickListener(this);
        viewEndTime.setOnClickListener(this);
        imgMap.setOnClickListener(this);
        tvDate.setOnClickListener(this);
        imgPlaceholder.setOnClickListener(this);
        imgInfo.setOnClickListener(this);
        CreateEventMainFragment.imgBack.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btn_info_next:
                AddBasicInfo();
                break;
            case R.id.tv_event_type:
                if (mTypes==null || mTypes.size()==0)
                    Utility.ShowAlert(mActivity, "Event Types","Event types are not available please try again later");
                else
                   ShowTypes();
                break;
            case R.id.img_map:
                launchLocation();
                break;
            case R.id.view_start_time:
                /*if (editing && mEvent.getEvent_status().equalsIgnoreCase("approved"))
                    Utility.ShowAlert(mActivity,"Event Timings","You cannot change event timings");
                else*/
                   Utility.PickTime(tvStartTime,mActivity);

                break;
            case R.id.view_end_time:
                /*if (editing && mEvent.getEvent_status().equalsIgnoreCase("approved"))
                    Utility.ShowAlert(mActivity,"Event Timings","You cannot change event timings");
                else*/
                Utility.PickTime(tvEndTime,mActivity);

                break;
            case R.id.tv_date:
                /*if (editing && mEvent.getEvent_status().equalsIgnoreCase("approved"))
                    Utility.ShowAlert(mActivity,"Event Date","You cannot change event date");
                else*/
                    Utility.PickDate(tvDate,mActivity);

                break;
            case R.id.img_placeholder:
                launchCamera();
                break;

            case R.id.img_info:
                Utility.ShowInfo(mActivity,"Event Category","Private","If you create a Private Event then it will only be visible to guests of your own organisation. Other organisations cannot see your private event","Public","Public Event will be visible to guests of all organisations and any one can attend that event");
                break;

            case R.id.img_back:
                mActivity.getSupportFragmentManager().popBackStack();
                break;


        }
    }






    private void AddBasicInfo() {

        String title = edEventTitle.getText().toString().trim();
        String newAddress = edLocation.getText().toString().trim();
        String date = tvDate.getText().toString().trim();
        String points = edPoints.getText().toString().trim();
        String guestNo = edNoOfGuests.getText().toString().trim();
        String description = edEventDetails.getText().toString().trim();
        String webUrl = edUrl.getText().toString().trim();
        String startTime = tvStartTime.getText().toString().trim();
        String endTime = tvEndTime.getText().toString().trim();
        String country = edCountry.getText().toString().trim();
        String city = edCity.getText().toString().trim();

        if (latlng!=null && !address.equalsIgnoreCase(newAddress)) // user select location from map but aftr that change text in street address field
            getLatlng();


        if (title.isEmpty())
            Utility.ShowAlert(mActivity, "Event Title", "You must have to provide an Event title");
        else if (typeId == null)
            Utility.ShowAlert(mActivity, "Event Type", "Please select an Event type");
        else if (country.isEmpty())
            Utility.ShowAlert(mActivity, "Country", "Please enter Country name");
        else if (city.isEmpty())
            Utility.ShowAlert(mActivity, "City", "Please enter City name");
        else if (newAddress.isEmpty())
            Utility.ShowAlert(mActivity, "Street Address", "Please provide Event Street address");
        else if (date.isEmpty())
            Utility.ShowAlert(mActivity, "Event Date", "Please provide a valid date for the Event");
        else if (points.isEmpty())
            Utility.ShowAlert(mActivity, "CPD Points", "Please enter CPD Points for the Event");
        else if (!Utility.checkCPDvalidity(points))
            Utility.ShowAlert(mActivity, "CPD Points", "The event only supports CPD values with quarters and halves for eg 0.25 or 0.50 or 0.75");
        else if (description.isEmpty())
            Utility.ShowAlert(mActivity, "Event Description", "Please provide a brief description for your Event");
        else if (startTime.isEmpty())
            Utility.ShowAlert(mActivity, "Event Start Time", "Please provide start time for your Event");
        else if (endTime.isEmpty())
            Utility.ShowAlert(mActivity, "Event End Time", "Please provide end time for your Event");
        else if (!Utility.checktimings(startTime,endTime))
                 Utility.ShowAlert(mActivity, "Invalid Event Duration", "End time should be after start time");

        else if (latlng==null && getLatlng()==null)
               Utility.ShowAlert(mActivity, "Invalid Address", "The address you entered is incorrect. Please review Country name, City name or street address");
        else {


            FormBody.Builder builder = new FormBody.Builder();
                    builder.add("user_id", mGlobal.mActiveUser.getUser_id());
                    builder.add("event_name",title);
                    builder.add("event_description",description);
                    builder.add("is_public",radioPrivate.isChecked()?"no":"yes");
                    builder.add("event_type_id", typeId);
                    builder.add("event_country",country);
                    builder.add("event_city",city);
                    builder.add("event_street_address",newAddress);
                    builder.add("event_cpd",points);
                    builder.add("event_date",date);
                    builder.add("event_start_time",startTime);
                    builder.add("event_end_time",endTime);
                    builder.add("event_web_url",webUrl);
                   if (latlng!=null)
                   {
                       builder.add("event_lat",latlng.split(",")[0]);
                       builder.add("event_lng",latlng.split(",")[1]);
                   }

                   if (!eventImgString.equals(""))
                       builder.add("event_picture",eventImgString);
                   if (editing)
                       builder.add("event_id",mEvent.getEvent_id());


            RequestBody body =builder.build();
            try {
                HttpHelper.CallApi(mActivity,Constants.Add_event_basic_info,body,this,true);
                btnInfoNext.setEnabled(false);


            } catch (Exception e) {
                e.printStackTrace();
            }


        }

    }






    public void ShowTypes() {

        final Dialog dialog = new Dialog(mActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        dialog.setContentView(R.layout.event_types_fragment);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        DisplayMetrics dm = new DisplayMetrics();
        mActivity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER | Gravity.START);
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        //lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        RecyclerView mEventType_rv;
        final EventsTypeAdapter eventsTypeAdapter;
        mEventType_rv = (RecyclerView) dialog.findViewById(R.id.rv_event_type);
        mEventType_rv.setHasFixedSize(true);
        mEventType_rv.setLayoutManager(new LinearLayoutManager(mContext));
        if (mTypes != null && mTypes.size() > 0) {
            eventsTypeAdapter = new EventsTypeAdapter(mContext, mActivity, mTypes);
            mEventType_rv.setAdapter(eventsTypeAdapter);
            eventsTypeAdapter.setOnItemClick(new EventsTypeAdapter.OnItemClicked() {
                @Override
                public void onClick(int pos) {

                    for (int i = 0; i < eventsTypeAdapter.mFilterArray.size(); i++) {
                        eventsTypeAdapter.mFilterArray.get(i).setSelected(false);
                    }
                    for (int i = 0; i < eventsTypeAdapter.mOriginalArray.size(); i++) {
                        eventsTypeAdapter.mOriginalArray.get(i).setSelected(false);
                    }
                    eventsTypeAdapter.mFilterArray.get(pos).setSelected(true);
                    eventsTypeAdapter.notifyDataSetChanged();
                    typeId = eventsTypeAdapter.mFilterArray.get(pos).getEvent_type_id();
                    tvEventType.setText(eventsTypeAdapter.mFilterArray.get(pos).getEvent_type_name());
                    dialog.dismiss();
                }
            });
        } else {
            Utility.ShowAlert(mActivity, "Event Types", "Event types not available please try again later");
            return;
        }


        FontEditText mTxtSearch = (FontEditText) dialog.findViewById(R.id.ed_search);
        mTxtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                eventsTypeAdapter.FilterList(s.toString());

            }
        });


        dialog.show();
    }





    private void FetchEventTypes() {

        RequestBody body = new FormBody.Builder().add("user_id", mGlobal.mActiveUser.getUser_id()).build();
        try {
            HttpHelper.CallApi(mActivity, Constants.Get_event_types, body, this, false);
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(mActivity,e.getMessage().toString(), Toast.LENGTH_SHORT).show();
        }
    }





    @Override
    public void onFailure(final Call call,final IOException e) {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (call.request().url().toString().equals(Constants.Add_event_basic_info))
                    btnInfoNext.setEnabled(true);

                if (e.getMessage()!=null)
                    Toast.makeText(mActivity,e.getMessage().toString(), Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(mActivity,"Request Timeout", Toast.LENGTH_SHORT).show();
            }
        });
    }






    @Override
    public void onSuccess(Call call, String response) {

        Log.d("Res~~~~~~~~~~~~", response);
        if (call.request().url().toString().equals(Constants.Get_event_types)) {
            try {
                ParseTypes(response);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (call.request().url().toString().equals(Constants.Add_event_basic_info))
        {
            try {
                final JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.getString("status").equals("true"))
                {
                    Gson gson = new Gson();
                    Events event = gson.fromJson(jsonObject.getString("response"), Events.class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("EVENT", event);
                    bundle.putBoolean("EDITING", editing);
                    Utility.ReplaceFragment_create_event(new CreateEventFragment_2(),getFragmentManager(),bundle);
                }
                else
                {
                    mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            try {
                                Toast.makeText(mActivity, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                                btnInfoNext.setEnabled(true);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }
            catch (final Exception e)
            {
                e.printStackTrace();
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        btnInfoNext.setEnabled(true);
                        Toast.makeText(mActivity,e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

            }
        }
    }





    private void setData()
    {
        edEventTitle.setText(mEvent.getEvent_name());
        tvEventType.setText(mEvent.getEvent_type_name());
        edLocation.setText(mEvent.getEvent_street_address());
        tvDate.setText(mEvent.getEvent_date());
        edPoints.setText(mEvent.getEvent_cpd());
        //edNoOfGuests.setText(mEvent.get);
        edEventDetails.setText(mEvent.getEvent_description());
        edUrl.setText(mEvent.getEvent_web_url());
        tvStartTime.setText(mEvent.getEvent_start_time());
        tvEndTime.setText(mEvent.getEvent_end_time());
        edCountry.setText(mEvent.getEvent_country());
        edCity.setText(mEvent.getEvent_city());
        ImageLoader.getInstance().displayImage(Constants.Img_Base_URL+mEvent.getEvent_photo(),imgEvent);

        if (mEvent.getIs_public_event().equals("yes"))
        {
            radioPublic.setChecked(true);
            radioPrivate.setChecked(false);
        }
        else
        {
            radioPublic.setChecked(false);
            radioPrivate.setChecked(true);
        }

        typeId = mEvent.getEvent_type_id();
        address = mEvent.getEvent_street_address();

    }




    private void launchCamera() {
        new SandriosCamera(mActivity, this, CAPTURE_MEDIA).setShowPicker(true)
                //.setVideoFileSize(15) //File Size in MB: Default is no limit
                .setMediaAction(CameraConfiguration.MEDIA_ACTION_PHOTO) // default is CameraConfiguration.MEDIA_ACTION_BOTH
                .enableImageCropping(true) // Default is false.
                .launchCamera();
    }





    private void launchLocation() {

        Intent intent = new Intent(mActivity, PickLocationActivity.class);
        this.startActivityForResult(intent,LOCATION_CODE);
    }







    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAPTURE_MEDIA && resultCode == mActivity.RESULT_OK) {

            new decodeImage().execute(data.getStringExtra(CameraConfiguration.Arguments.FILE_PATH));
        }
        else if (requestCode == LOCATION_CODE && resultCode == mActivity.RESULT_OK) {
            address = data.getStringExtra("ADDRESS");
            edLocation.setText(address);
            latlng = data.getStringExtra("LAT_LNG");
        }
    }




    private void ParseTypes(String response) throws Exception {

        JSONObject json = new JSONObject(response);
        Gson gson = new Gson();
        Event_Types[] types = gson.fromJson(json.getString("response"), Event_Types[].class);
        mTypes = new ArrayList<>(Arrays.asList(types));
    }







    private String getLatlng()
    {
        String address = edLocation.getText().toString()+" "+edCity.getText().toString()+" "+edCountry.getText().toString();
        Geocoder geocoder = new Geocoder(mContext);
        try {
            //Place your latitude and longitude
            List<Address> addresses = geocoder.getFromLocationName(address, 1);
            if (addresses != null && addresses.size() > 0) {
                double latti = addresses.get(0).getLatitude();
                double longi = addresses.get(0).getLongitude();
                latlng = String.valueOf(latti)+","+String.valueOf(longi);
            }
            else
                latlng =null;

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            latlng = null;
        }

        return latlng;
    }

    private class decodeImage extends AsyncTask<String,Void,Void>
    {


        @Override
        protected Void doInBackground(String... params) {
            try {
                ResizeImage resizeImage = new ResizeImage(mContext);
                String uri = resizeImage.compressImage(params[0]);
                File imgFile = new File(uri);
                if (imgFile.exists()) {
                    eventImg = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                    eventImgString = Utility.EncodeTobase64(eventImg);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            imgEvent.setImageBitmap(eventImg);
        }
    }
}
