package ifisol.attendoplus.View.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import fr.arnaudguyon.smartfontslib.FontButton;
import ifisol.attendoplus.Model.Events;
import ifisol.attendoplus.R;
import ifisol.attendoplus.Utilities.Constants;
import ifisol.attendoplus.Utilities.CreateEventStepsListener;
import ifisol.attendoplus.Utilities.Utility;
import ifisol.attendoplus.View.Activities.ActivityMain;

public class CreateEventFragment_5 extends Fragment implements View.OnClickListener {

    FragmentActivity mActivity;
    Context  mContext;

    CreateEventStepsListener stepsListener;

    private FontButton btnFinish;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.create_event_fragment_5, null);

        return Init(v);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        stepsListener = (CreateEventStepsListener) getParentFragment();
        stepsListener.UpdateSteps(4);
        ActivityMain.mTvABTitle.setText("Payment");

        btnFinish.setOnClickListener(this);
        CreateEventMainFragment.imgBack.setOnClickListener(this);


    }

    private View Init(View v)
    {
        mActivity = getActivity();
        mContext  = getActivity();
        btnFinish = (FontButton) v.findViewById(R.id.btn_finish);

        return v;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.btn_finish:
                Bundle bundle = new Bundle();
                bundle.putBoolean("EDITING", getArguments().getBoolean("EDITING"));
                Utility.ReplaceFragment(new CongratsFragment(), mActivity.getSupportFragmentManager(),bundle);
                break;

            case R.id.img_back:
                Bundle bundle1 = new Bundle();
                bundle1.putSerializable("EVENT", (Events)getArguments().getSerializable("EVENT"));
                bundle1.putBoolean("EDITING", true);
                Utility.ReplaceFragment_create_event(new CreateEventFragment_4(),getFragmentManager(),bundle1);
                break;
        }
    }
}