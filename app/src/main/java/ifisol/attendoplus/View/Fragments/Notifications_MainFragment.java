package ifisol.attendoplus.View.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import fr.arnaudguyon.smartfontslib.FontTextView;
import ifisol.attendoplus.Controller.Adapters.ExhibitorPagerAdapter;
import ifisol.attendoplus.Controller.Adapters.NotificationsPagerAdapter;
import ifisol.attendoplus.Global.Globals;
import ifisol.attendoplus.R;

public class Notifications_MainFragment extends Fragment  {


    FragmentActivity mActivity;
    Context  mContext;
    Globals  mGlobal;

    private TabLayout tabs;
    private ViewPager noti_vp;
    private NotificationsPagerAdapter notificationsPagerAdapter;
    FontTextView mTvTitle;
    int pos;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return Init(inflater.inflate(R.layout.notification_main_fragment, null));
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pos = getArguments().getInt("POSITION");
        SetUpViewPager();

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);


    }

    private View Init(View view)
    {

        mActivity = getActivity();
        mContext  = getActivity();
        mGlobal   = (Globals) mActivity.getApplicationContext();

        tabs      = (TabLayout) view.findViewById(R.id.tabs);
        noti_vp   = (ViewPager) view.findViewById(R.id.notification_vp);
        mTvTitle  = (FontTextView)  view.findViewById(R.id.tv_actionbar_title);

        return view;
    }

    private void SetUpViewPager() {

        notificationsPagerAdapter = new NotificationsPagerAdapter(getChildFragmentManager(),mContext);
        noti_vp.setAdapter(notificationsPagerAdapter);

        tabs.setupWithViewPager(noti_vp);

        noti_vp.setCurrentItem(pos);

        for (int i = 0; i < tabs.getTabCount(); i++) {
            tabs.getTabAt(i).setCustomView(notificationsPagerAdapter.getTabView(i));
        }
        TabLayout.Tab tab = tabs.getTabAt(pos);
        View tabView = (View) tab.getCustomView();
        ImageView tabIcon = (ImageView) tabView.findViewById(R.id.img_tab);
        tabIcon.setImageResource(notificationsPagerAdapter.GetActIcon(pos));
        mTvTitle.setText(notificationsPagerAdapter.getPageTitle(pos));
        tabs.setOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(noti_vp) {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                super.onTabSelected(tab);
                View tabView = (View) tab.getCustomView();
                ImageView tabIcon = (ImageView) tabView.findViewById(R.id.img_tab);
                tabIcon.setImageResource(notificationsPagerAdapter.GetActIcon(tab.getPosition()));
                mTvTitle.setText(notificationsPagerAdapter.getPageTitle(tab.getPosition()));
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                super.onTabUnselected(tab);
                View tabView = (View) tab.getCustomView();
                ImageView tabIcon = (ImageView) tabView.findViewById(R.id.img_tab);
                tabIcon.setImageResource(notificationsPagerAdapter.GetNormalIcon(tab.getPosition()));
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                super.onTabReselected(tab);
            }
        });



    }

}
