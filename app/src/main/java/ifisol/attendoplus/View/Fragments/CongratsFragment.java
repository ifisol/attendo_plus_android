package ifisol.attendoplus.View.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import fr.arnaudguyon.smartfontslib.FontTextView;
import ifisol.attendoplus.Global.Globals;
import ifisol.attendoplus.R;
import ifisol.attendoplus.Utilities.Constants;
import ifisol.attendoplus.Utilities.Utility;
import ifisol.attendoplus.View.Activities.ActivityMain;

public class CongratsFragment extends Fragment implements View.OnClickListener {

    FragmentActivity mActivity;
    Context mContext;
    Globals mGlobal;

    ImageView imgClose;

    FontTextView tvTxt;
    FontTextView tvDesc;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        return inflater.inflate(R.layout.create_event_success_view, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);

        Init(view);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);

        boolean isEditing = getArguments().getBoolean("EDITING");

        if (isEditing)
        {
            tvTxt.setText("Your event has been updated !");
            tvDesc.setText("Dear "+mGlobal.mActiveUser.getUser_name()+" thank you for being a part of Attendo + . Your Event request has been submitted. Attendo + team will review and approve it shortly. Thanks");
        }
        else
        {
            tvTxt.setText("Your event has been Created !");
            tvDesc.setText("Dear "+mGlobal.mActiveUser.getUser_name()+" thank you for being a part of Attendo + . Your Event request has been submitted. Attendo + team will review and approve it shortly. Thanks");

        }



    }

    private View Init(View view) {

        mActivity = getActivity();
        mContext = getActivity();
        mGlobal = (Globals) mActivity.getApplicationContext();
        imgClose  = (ImageView) view.findViewById(R.id.img_close);
        tvTxt = (FontTextView) view.findViewById(R.id.tv_text);
        tvDesc = (FontTextView) view.findViewById(R.id.tv_description);
        imgClose.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {

        /*if (Constants.USER_TYPE.equals(Constants.TAG_GUEST))
            {
                ActivityMain.bottomBarGuest.onClick(ActivityMain.bottomBarOrg.getTabAtPosition(1));
                Fragment fragment = new Cpd_MainFragment();
                Bundle bundle = new Bundle();
                bundle.putInt("POSITION",2);
                Utility.ReplaceFragment(fragment, mActivity.getSupportFragmentManager(), bundle);
            }

    else
        {*/
            ActivityMain.bottomBarOrg.onClick(ActivityMain.bottomBarOrg.getTabAtPosition(1));
            Bundle bundle1 = new Bundle();
            bundle1.putInt("POSITION", 0);
            Utility.ReplaceFragment(new EventsPagerFragment(),mActivity.getSupportFragmentManager(),bundle1);

      //

   //     }


    }
}