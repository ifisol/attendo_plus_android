package ifisol.attendoplus.View.Fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import ifisol.attendoplus.Controller.Adapters.AnalyticsMainPagerAdapter;
import ifisol.attendoplus.Global.Globals;
import ifisol.attendoplus.R;
import ifisol.attendoplus.View.Activities.ActivityMain;

public class Analytic_MainFragment extends Fragment  {


    Activity mActivity;
    Context  mContext;
    Globals  mGlobal;

    private TabLayout tabs;
    private ViewPager analyticsViewPager;
    private AnalyticsMainPagerAdapter analyticsMainPagerAdapter;
    TextView mTvTitle;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return Init(inflater.inflate(R.layout.analytics_main_fragment, null));
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        SetUpViewPager();

        tabs.setOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(analyticsViewPager) {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                super.onTabSelected(tab);
                View tabView = (View) tab.getCustomView();
                ImageView tabIcon = (ImageView) tabView.findViewById(R.id.img_tab);
                tabIcon.setColorFilter(getResources().getColor(R.color.cpd_green));
                mTvTitle.setText(analyticsMainPagerAdapter.getPageTitle(tab.getPosition()));
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

                super.onTabUnselected(tab);
                View tabView = (View) tab.getCustomView();
                ImageView tabIcon = (ImageView) tabView.findViewById(R.id.img_tab);
                tabIcon.setColorFilter(getResources().getColor(R.color.black));
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

                super.onTabReselected(tab);
            }
        });



    }

    private View Init(View view)
    {

        mActivity = getActivity();
        mContext  = getActivity();
        mGlobal   = (Globals) mActivity.getApplicationContext();

        tabs            = (TabLayout) view.findViewById(R.id.tabs);
        analyticsViewPager = (ViewPager) view.findViewById(R.id.analytics_view_pager);
        mTvTitle           = (TextView) view.findViewById(R.id.tv_title);

        ActivityMain.mTvABTitle.setText("Analytics");

       /* if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
         //   RelativeLayout.LayoutParams p = (RelativeLayout.LayoutParams) tabView.getLayoutParams();
        //    p.setMargins(0, 0, 0, 0); // get rid of margins since shadow area is now the margin
         //   tabView.setLayoutParams(p);
        }*/

        return view;
    }

    private void SetUpViewPager() {

        analyticsMainPagerAdapter = new AnalyticsMainPagerAdapter(getChildFragmentManager(),mContext);
        analyticsViewPager.setAdapter(analyticsMainPagerAdapter);

        tabs.setupWithViewPager(analyticsViewPager);

        for (int i = 0; i < tabs.getTabCount(); i++) {
            tabs.getTabAt(i).setCustomView(analyticsMainPagerAdapter.getTabView(i));
        }

        TabLayout.Tab tab = tabs.getTabAt(0);
        View tabView = (View) tab.getCustomView();
        ImageView tabIcon = (ImageView) tabView.findViewById(R.id.img_tab);
        tabIcon.setColorFilter(getResources().getColor(R.color.cpd_green));
        mTvTitle.setText(analyticsMainPagerAdapter.getPageTitle(0));
        
    }

}
