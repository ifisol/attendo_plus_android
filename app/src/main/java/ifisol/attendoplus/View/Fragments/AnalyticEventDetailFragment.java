package ifisol.attendoplus.View.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import ifisol.attendoplus.Controller.Adapters.AnalyticsEventDetailPagerAdapter;
import ifisol.attendoplus.Global.Globals;
import ifisol.attendoplus.Model.Events;
import ifisol.attendoplus.R;
import ifisol.attendoplus.View.Activities.ActivityMain;

public class AnalyticEventDetailFragment extends Fragment  {


    FragmentActivity mActivity;
    Context  mContext;
    Globals  mGlobal;

    private TabLayout tabs;
    private ViewPager analyticsViewPager;
    private AnalyticsEventDetailPagerAdapter analyticsPagerAdapter;
    TextView tvTitle;
    ImageView imgBack;
    Events event;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return Init(inflater.inflate(R.layout.analytics_event_detail_fragment, null));
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);

        event = (Events) getArguments().getSerializable("EVENT");
        SetUpViewPager();


        tabs.setOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(analyticsViewPager) {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                super.onTabSelected(tab);
                View tabView = (View) tab.getCustomView();
                ImageView tabIcon = (ImageView) tabView.findViewById(R.id.img_tab);
                tabIcon.setColorFilter(getResources().getColor(R.color.cpd_green));
                tvTitle.setText(analyticsPagerAdapter.getPageTitle(tab.getPosition()));

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

                super.onTabUnselected(tab);
                View tabView = (View) tab.getCustomView();
                ImageView tabIcon = (ImageView) tabView.findViewById(R.id.img_tab);
                tabIcon.setColorFilter(getResources().getColor(R.color.black));
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

                super.onTabReselected(tab);
            }
        });

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mActivity.getSupportFragmentManager().popBackStack();
            }
        });


    }

    private View Init(View view)
    {

        mActivity = getActivity();
        mContext  = getActivity();
        mGlobal   = (Globals) mActivity.getApplicationContext();

        tabs            = (TabLayout) view.findViewById(R.id.tabs);
        analyticsViewPager = (ViewPager) view.findViewById(R.id.analytics_view_pager);
        tvTitle         = (TextView) view.findViewById(R.id.tv_title);
        imgBack         = (ImageView) view.findViewById(R.id.img_back);

        ActivityMain.mTvABTitle.setText("Analytics");

       /* if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
         //   RelativeLayout.LayoutParams p = (RelativeLayout.LayoutParams) tabView.getLayoutParams();
        //    p.setMargins(0, 0, 0, 0); // get rid of margins since shadow area is now the margin
         //   tabView.setLayoutParams(p);
        }*/

        return view;
    }

    private void SetUpViewPager() {

        analyticsPagerAdapter = new AnalyticsEventDetailPagerAdapter(getChildFragmentManager(),mContext, event);
        analyticsViewPager.setAdapter(analyticsPagerAdapter);

        tabs.setupWithViewPager(analyticsViewPager);

        for (int i = 0; i < tabs.getTabCount(); i++) {
            tabs.getTabAt(i).setCustomView(analyticsPagerAdapter.getTabView(i));
        }

        TabLayout.Tab tab = tabs.getTabAt(0);
        View tabView = (View) tab.getCustomView();
        ImageView tabIcon = (ImageView) tabView.findViewById(R.id.img_tab);
        tabIcon.setColorFilter(getResources().getColor(R.color.cpd_green));
        tvTitle.setText(analyticsPagerAdapter.getPageTitle(0));




    }

}
