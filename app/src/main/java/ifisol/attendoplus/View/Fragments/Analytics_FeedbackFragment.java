package ifisol.attendoplus.View.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import fr.arnaudguyon.smartfontslib.FontTextView;
import ifisol.attendoplus.Controller.Adapters.Analytics_FeedbackAdapter;
import ifisol.attendoplus.Global.Globals;
import ifisol.attendoplus.Model.User;
import ifisol.attendoplus.R;
import ifisol.attendoplus.Utilities.Constants;
import ifisol.attendoplus.Utilities.HttpHelper;
import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.RequestBody;

public class Analytics_FeedbackFragment extends Fragment implements HttpHelper.HttpCallback {

    FragmentActivity mActivity;
    Context  mContext;
    Globals mGlobal;

    RecyclerView GuestRV;
    Analytics_FeedbackAdapter feedbackAdapter;
    ImageView imgNoRecord;
    FontTextView tvCount;

    String mEventType ;
    String mEventID ;
    ArrayList<User> mLogUsers;

    
    

    
    

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.analytics_feedback_fragment, null);

        return Init(v);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mEventType = getArguments().getString(Constants.EVENT_TYPE);
        mEventID     = getArguments().getString("EVENT_ID");
        FetchFeedBack();

    }

    private View Init(View v)
    {
        mActivity = getActivity();
        mContext  = getActivity();
        mGlobal = (Globals) mActivity.getApplicationContext();

        GuestRV = (RecyclerView) v.findViewById(R.id.rv_guest);
        imgNoRecord = (ImageView) v.findViewById(R.id.img_no_record);
        tvCount     = (FontTextView) v.findViewById(R.id.tv_count);

        return v;
    }

    private void SetAdapter(ArrayList<User> mFeedbckList)
    {
        if (mFeedbckList==null || mFeedbckList.size()==0) {
            imgNoRecord.setVisibility(View.VISIBLE);
            tvCount.setText("0");
        }
        else
        {
            imgNoRecord.setVisibility(View.GONE);
            tvCount.setText(String.valueOf(mFeedbckList.size()));
            GuestRV.setHasFixedSize(true);
            GuestRV.setLayoutManager(new LinearLayoutManager(mContext));
            feedbackAdapter = new Analytics_FeedbackAdapter(mActivity,mFeedbckList);
            GuestRV.setAdapter(feedbackAdapter);

        }



    }






    private void FetchFeedBack() {
        RequestBody formBody = new FormBody.Builder()
                .add("user_id",mGlobal.mActiveUser.getUser_id())
                .add("event_id", mEventID)
                .add("tag", "feedback")
                .build();
       try {
            HttpHelper.CallApi(mActivity,Constants.Analytic_event_detail,formBody, this,false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }









    @Override
    public void onFailure(Call call, IOException e) {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                SetAdapter(null);
            }
        });
    }

    @Override
    public void onSuccess(Call call, String response) {
        try {
            Log.d("Response . . . . . .", response);
            Gson gson = new Gson();
            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.getString("status").equals("true"))
            {
                final User[] users = gson.fromJson(jsonObject.getString("response"), User[].class);
                mLogUsers = new ArrayList<>(Arrays.asList(users));
            }

            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    SetAdapter(mLogUsers);
                }
            });


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }



    /*

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.analytics_feedback_fragment, null);

        return Init(v);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        SetAdapter();

    }

    private View Init(View v)
    {
        mActivity = getActivity();
        mContext  = getActivity();

        GuestRV = (RecyclerView) v.findViewById(R.id.rv_guest);

        return v;
    }

    private void SetAdapter()
    {
        GuestRV.setHasFixedSize(true);
        GuestRV.setLayoutManager(new LinearLayoutManager(mContext));
      //  ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(R.dimen.dp_1);
      //  mMediaRV.addItemDecoration(itemDecoration);
        feedbackAdapter = new Analytics_FeedbackAdapter(mContext, mActivity, this);
        GuestRV.setAdapter(feedbackAdapter);


    }*/

}