package ifisol.attendoplus.View.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import fr.arnaudguyon.smartfontslib.FontTextView;
import ifisol.attendoplus.Controller.Adapters.Viewers_RecyclerAdapter;
import ifisol.attendoplus.Global.Globals;
import ifisol.attendoplus.Model.Notifications;
import ifisol.attendoplus.Model.User;
import ifisol.attendoplus.R;
import ifisol.attendoplus.Utilities.Constants;
import ifisol.attendoplus.Utilities.HttpHelper;
import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.RequestBody;

public class NotificationDetailFragment extends Fragment implements HttpHelper.HttpCallback {


    FragmentActivity mActivity;
    Context  mContext;
    Globals  mGlobal;

    private FontTextView tvActionbarTitle;
    private FontTextView tvTitle;
    private FontTextView tvCategory;
    private FontTextView tvDscription;
    private FontTextView tvNoViewers;
    private RecyclerView rvViewers;
    private CircularImageView imgNoti;
    private Viewers_RecyclerAdapter viewers_recyclerAdapter;
    View mainView;
    ImageView imgBack;
    Notifications mNotification;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return Init(inflater.inflate(R.layout.notification_detail, null));
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Init(view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);

        mNotification = (Notifications) getArguments().getSerializable("NOTIFICATION");

        if (mNotification.getNotification_read_status()!=null && mNotification.getNotification_read_status().equals("no") && mGlobal.mActiveUser.getUser_type().equals(Constants.TAG_GUEST))
            UpdateReadStatus();

        FetchNotiDetail();

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mActivity.getSupportFragmentManager().popBackStack();
            }
        });
    }




    private void FetchNotiDetail() {
        RequestBody body = new FormBody.Builder()
                .add("user_id", mGlobal.mActiveUser.getUser_id())
                .add("notification_id", mNotification.getNotification_id()).build();
        try {
            HttpHelper.CallApi(mActivity, Constants.Get_Notification_detail,body,this,true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }




    private View Init(View view)
    {

        mActivity = getActivity();
        mContext  = getActivity();
        mGlobal   = (Globals) mActivity.getApplicationContext();

        tvActionbarTitle = (FontTextView) view.findViewById(R.id.tv_actionbar_title);
        tvTitle = (FontTextView) view.findViewById(R.id.tv_title);
        tvCategory = (FontTextView) view.findViewById(R.id.tv_category);
        tvDscription = (FontTextView) view.findViewById(R.id.tv_dscription);
        tvNoViewers = (FontTextView) view.findViewById(R.id.tv_no_view);
        rvViewers = (RecyclerView) view.findViewById(R.id.rv_viewers);
        imgNoti = (CircularImageView) view.findViewById(R.id.img_notification);
        imgBack         = (ImageView) view.findViewById(R.id.img_back);
        mainView = view.findViewById(R.id.view_main);

        return view;
    }


    private void SetViewsAdapter(ArrayList<User> mList)
    {

        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        rvViewers.setHasFixedSize(true);
        rvViewers.setLayoutManager(layoutManager);

        viewers_recyclerAdapter = new Viewers_RecyclerAdapter(mContext,mActivity,mList);
        rvViewers.setAdapter(viewers_recyclerAdapter);
    }





    private void UpdateReadStatus() {

        RequestBody body = new FormBody.Builder()
                .add("receiver_id", mGlobal.mActiveUser.getUser_id())
                .add("notification_id", mNotification.getNotification_id()).build();
        try {
            HttpHelper.CallApi(mActivity, Constants.Update_Read_status,body,this,false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }





    @Override
    public void onFailure(Call call, final IOException e) {

        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                Log.d("Exception=======>>>>", e.getMessage());
            }
        });
    }

    @Override
    public void onSuccess(Call call, String response) {

        if (call.request().url().toString().equals(Constants.Get_Notification_detail))
        {
            try {
                final JSONObject json =  new JSONObject(response);
                if (json.getString("status").equals("true"))
                {
                    Gson gson = new Gson();
                    final Notifications notification = gson.fromJson(json.getString("response"),Notifications.class);
                    mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            SetData(notification);
                        }
                    });
                }
                else
                {
                    mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            try {
                                Toast.makeText(mActivity, json.getString("message"), Toast.LENGTH_SHORT).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }

            }
            catch (Exception e)
            {

            }


        }
    }

    private void SetData(Notifications notification) {

        ImageLoader.getInstance().displayImage(Constants.Img_Base_URL+notification.getNotification_picture(),imgNoti);
        tvTitle.setText(notification.getNotification_name());
        tvCategory.setText(notification.getNotification_subject());
        tvDscription.setText(notification.getNotification_description());
        if (notification.getViewed_by()!=null && notification.getViewed_by().size()>0)
        {
            rvViewers.setVisibility(View.VISIBLE);
            SetViewsAdapter(notification.getViewed_by());
            tvNoViewers.setVisibility(View.GONE);
        }
        else
        {
            rvViewers.setVisibility(View.GONE);
            tvNoViewers.setVisibility(View.VISIBLE);
        }

        mainView.setVisibility(View.VISIBLE);
    }
}
