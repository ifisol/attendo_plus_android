package ifisol.attendoplus.Utilities;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.media.ExifInterface;
import android.media.ThumbnailUtils;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import fr.arnaudguyon.smartfontslib.FontButton;
import fr.arnaudguyon.smartfontslib.FontTextView;
import ifisol.attendoplus.Global.Globals;
import ifisol.attendoplus.Model.Events;
import ifisol.attendoplus.Model.Non_attendo_events;
import ifisol.attendoplus.R;
import ifisol.attendoplus.View.Activities.FeedbackActivity;

public class Utility {

    static Intent notificationIntent;
    static PendingIntent pendingIntent;
    static AlarmManager alarmManager;


    public static boolean isConnected(Activity context) {

        boolean available = false;
        try {
            if (context != null) {
                final ConnectivityManager conMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                final NetworkInfo activeNetwork = conMgr.getActiveNetworkInfo();
                if (activeNetwork != null && activeNetwork.getState() == NetworkInfo.State.CONNECTED) {
                    available = true;
                } else {
                  //  Toast.makeText(context, "Internet Connection Lost", Toast.LENGTH_SHORT).show();
                }
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
       //     Toast.makeText(context, "Internet Connection Lost", Toast.LENGTH_SHORT).show();
        }
        return available;
    }



    public static void OpenActivity(Activity activity, Class<?> cls) {

        Intent mIntent = new Intent(activity, cls);
        activity.startActivity(mIntent);
        activity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }




    public static void OpenActivity(Intent mIntent, Activity activity) {

        activity.startActivity(mIntent);
        activity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }




    public static void OpenActivity(Activity activity, Class<?> cls, int anim1, int anim2) {

        Intent mIntent = new Intent(activity, cls);
        activity.startActivity(mIntent);
        activity.overridePendingTransition(anim1, anim2);
    }




    public static String GetMonth(String m) {

        String month = "";
        switch (Integer.parseInt(m)) {
            case 1:
                month = "Jan";
                break;
            case 2:
                month = "Feb";
                break;
            case 3:
                month = "Mar";
                break;
            case 4:
                month = "Apr";
                break;
            case 5:
                month = "May";
                break;
            case 6:
                month = "Jun";
                break;
            case 7:
                month = "Jul";
                break;
            case 8:
                month = "Aug";
                break;
            case 9:
                month = "Sep";
                break;
            case 10:
                month = "Oct";
                break;
            case 11:
                month = "Nov";
                break;
            case 12:
                month = "Dec";
                break;
        }
        return month;
    }




    public static String EncodeTobase64(Bitmap image) {

        Bitmap immagex = image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immagex.compress(Bitmap.CompressFormat.PNG, 50, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);
        return imageEncoded;
    }




    public static void ReplaceFragment(Fragment fragment, FragmentManager manager) {

        Fragment fragment1 = Utility.getFragmentFromStack(fragment.getClass().getName(),manager);
        if (fragment1!=null)
             manager.beginTransaction().remove(fragment1).commit();
    //    else
            manager.beginTransaction().replace(R.id.contentContainer, fragment,fragment.getClass().getName()).addToBackStack("").commit();
    }




    public static Fragment getFragmentFromStack(String tag , FragmentManager manager) {
        return manager.findFragmentByTag(tag);
    }




    public static void ReplaceFragment_create_event(Fragment fragment, FragmentManager manager) {

        FragmentManager fragmentManager = manager;
        fragmentManager.beginTransaction().replace(R.id.create_event_container, fragment).addToBackStack(fragment.getClass().getName()).commit();
    }

    public static void ReplaceFragment_create_event(Fragment fragment, FragmentManager manager, Bundle bundle) {

        fragment.setArguments(bundle);
        FragmentManager fragmentManager = manager;
        fragmentManager.beginTransaction().replace(R.id.create_event_container, fragment).addToBackStack(fragment.getClass().getName()).commit();
    }





    public static void ReplaceFragment(Fragment fragment, FragmentManager manager, Bundle b) {

        Fragment fragment1 = Utility.getFragmentFromStack(fragment.getClass().getName(),manager);
        if (fragment1!=null)
            manager.beginTransaction().remove(fragment1).commit();

        fragment.setArguments(b);
        FragmentManager fragmentManager = manager;
        fragmentManager.beginTransaction().replace(R.id.contentContainer, fragment).addToBackStack(fragment.getClass().getName()).commit();
    }




    public static boolean EmailValidator(String email) {

        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }




    public Bitmap getSquareCropBitmap(Bitmap bitmap) {
        //use the smallest dimension of the image to crop to
        int minCropDimen = Math.min(bitmap.getWidth(), bitmap.getHeight());
        bitmap = ThumbnailUtils.extractThumbnail(bitmap, minCropDimen, minCropDimen);
        return bitmap;
    }




    public static Bitmap decodeScaledBitmap(String filePath) throws IOException {

        int MAX_HEIGHT = 1024;
        int MAX_WIDTH  = 1024;
        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, options);
        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, MAX_HEIGHT, MAX_HEIGHT);
        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        Bitmap bitmap = BitmapFactory.decodeFile(filePath, options);
        bitmap = rotateImageIfRequired(bitmap, filePath);
        return bitmap;

    }




    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;
        if (height > reqHeight || width > reqWidth) {
            // Calculate ratios of height and width to requested height and width
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            // Choose the smallest ratio as inSampleSize value, this will guarantee a final image
            // with both dimensions larger than or equal to the requested height and width.
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
            // This offers some additional logic in case the image has a strange
            // aspect ratio. For example, a panorama may have a much larger
            // width than height. In these cases the total pixels might still
            // end up being too large to fit comfortably in memory, so we should
            // be more aggressive with sample down the image (=larger inSampleSize).
            final float totalPixels = width * height;
            // Anything more than 2x the requested pixels we'll sample down further
            final float totalReqPixelsCap = reqWidth * reqHeight * 2;
            while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
                inSampleSize++;
            }
        }
        return inSampleSize;
    }



    private static Bitmap rotateImageIfRequired(Bitmap img, String path) throws IOException {

        ExifInterface ei = new ExifInterface(path);
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                return rotateImage(img, 90);
            case ExifInterface.ORIENTATION_ROTATE_180:
                return rotateImage(img, 180);
            case ExifInterface.ORIENTATION_ROTATE_270:
                return rotateImage(img, 270);
            default:
                return img;
        }
    }




    private static Bitmap rotateImage(Bitmap img, int degree) {

        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        Bitmap rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
        img.recycle();
        return rotatedImg;
    }




    public static void ShowAlert(Activity mActivity, String title, String msg) {

        final Dialog dialog = new Dialog(mActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.alert);
        DisplayMetrics dm = new DisplayMetrics();
        mActivity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER | Gravity.START);
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        TextView header = (TextView) dialog.findViewById(R.id.tv_header);
        TextView message = (TextView) dialog.findViewById(R.id.tv_msg);
        header.setText(title);
        message.setText(msg);
        Button btnOK = (Button) dialog.findViewById(R.id.btn_ok);
        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dialog.dismiss();
            }
        });
        dialog.show();
    }








    public static void ShowInfo(Activity mActivity, String mainHeading, String heading1, String msg1, String heading2, String msg2) {
        final Dialog dialog = new Dialog(mActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.info);
        DisplayMetrics dm = new DisplayMetrics();
        mActivity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER | Gravity.START);
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
       FontTextView headermain =  (FontTextView) dialog.findViewById(R.id.tv_mainheading);
       FontTextView header1 =      (FontTextView) dialog.findViewById(R.id.tv_heading1);
       FontTextView txt1 =      (FontTextView) dialog.findViewById(R.id.tv_msg1);
       FontTextView header2 =      (FontTextView) dialog.findViewById(R.id.tv_heading2);
       FontTextView txt2 =     (FontTextView) dialog.findViewById(R.id.tv_msg2);
        headermain.setText(mainHeading);
        header1.setText(heading1);
        txt1.setText(msg1);
        header2.setText(heading2);
        txt2.setText(msg2);
        FontButton btnOK = (FontButton) dialog.findViewById(R.id.btn_ok);
        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dialog.dismiss();
            }
        });
        dialog.show();
    }







    public static void ConnectionLost(final Activity mActivity) {

        Globals.mConnectionDia = new Dialog(mActivity);
        Globals.mConnectionDia .requestWindowFeature(Window.FEATURE_NO_TITLE);
        Globals.mConnectionDia .getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        Globals.mConnectionDia .setContentView(R.layout.connection_lost);
        DisplayMetrics dm = new DisplayMetrics();
        mActivity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = Globals.mConnectionDia .getWindow();
        window.setGravity(Gravity.CENTER | Gravity.START);
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        TextView tvClose = (TextView) Globals.mConnectionDia .findViewById(R.id.tv_close);
        tvClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Globals.mConnectionDia .dismiss();
            }
        });



        Globals.mConnectionDia .show();
    }




    public static final String getMD5(final String toEncrypt) {
        try {
            final MessageDigest digest = MessageDigest.getInstance("md5");
            digest.update(toEncrypt.getBytes());
            final byte[] bytes = digest.digest();
            final StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; i++) {
                sb.append(String.format("%02X", bytes[i]));
            }
            return sb.toString().toLowerCase();
        } catch (Exception exc) {
            return ""; // Impossibru!
        }
    }




    public static void ShowLoading(Activity mActivity) {
        Globals.mLoadingDia = new Dialog(mActivity);
        Globals.mLoadingDia.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Globals.mLoadingDia.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        Globals.mLoadingDia.setContentView(R.layout.loading_view);
        Globals.mLoadingDia.setCancelable(false);
        DisplayMetrics dm = new DisplayMetrics();
        mActivity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = Globals.mLoadingDia.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        window.setAttributes(lp);
        ImageView imageView = (ImageView) Globals.mLoadingDia.findViewById(R.id.img_loading) ;
        Animation animation = AnimationUtils.loadAnimation(mActivity, R.anim.rotation);
        imageView.startAnimation(animation);
        Globals.mLoadingDia.show();
    }




    public static void PickTime(final TextView textView,final Activity mActivity)
    {
        int       Hour;
        int       mint;
        int       sec;
        Calendar calendar;

        calendar=Calendar.getInstance();
        Hour=calendar.get(Calendar.HOUR_OF_DAY);
        mint=calendar.get(Calendar.MINUTE);
        sec=calendar.get(Calendar.SECOND);

        TimePickerDialog tpc = TimePickerDialog.newInstance(new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {

                String am_pm = hourOfDay>=12? "PM":"AM";
                if (hourOfDay==0)
                    hourOfDay=+12;
                String hour = hourOfDay>12?String.valueOf(hourOfDay-12):String.valueOf(hourOfDay);
                String Hr = hour.length()>1?hour:"0"+hour;
                String Mn = String.valueOf(minute).length()>1?String.valueOf(minute):"0"+String.valueOf(minute);
                textView.setText(Hr+":"+Mn+" "+am_pm);
            }
        },Hour,mint,sec,false ) ;

        tpc.setAccentColor(mActivity.getResources().getColor(R.color.cpd_green));
        tpc.show(mActivity.getFragmentManager(), "");
    }



    public static void PickDate(final TextView textView,final Activity mActivity)
    {
        int       year;
        int       month;
        final int       day;
        Calendar calendar;

        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dpd = DatePickerDialog.newInstance(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                String month = String.valueOf(monthOfYear + 1).length()>1?String.valueOf(monthOfYear + 1):"0"+String.valueOf(monthOfYear + 1);
                String mDay = String.valueOf(dayOfMonth).length()>1?String.valueOf(dayOfMonth):"0"+String.valueOf(dayOfMonth);
                textView.setText(String.valueOf(year)+"-"+month+"-"+mDay);
            }
        }, year, month, day);
        dpd.setAccentColor(mActivity.getResources().getColor(R.color.orange2));
        dpd.setMinDate(calendar);
        dpd.show(mActivity.getFragmentManager(), "");
    }







    public static String GetAddress(Context mContext, String lat, String lng)
    {
        Geocoder geocoder;
        List<Address> addresses= new ArrayList<>();
        geocoder = new Geocoder(mContext, Locale.getDefault());
        StringBuilder sb =new StringBuilder();
        String city;
        String countruy;

        try {
            addresses = geocoder.getFromLocation(Double.parseDouble(lat), Double.parseDouble(lng), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

            if (addresses.get(0).getAddressLine(0)!=null) //address
                sb.append(addresses.get(0).getAddressLine(0)+" ");
      //      if (addresses.get(0).getLocality()!=null) // city
      //          sb.append(addresses.get(0).getLocality()+" ");
            if (addresses.get(0).getAdminArea()!=null); // state
            sb.append(addresses.get(0).getAdminArea()+" ");
        //    if (addresses.get(0).getCountryName()!=null); // country
        //    sb.append(addresses.get(0).getCountryName());

        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }

        String address = sb.toString().replaceAll("null","");
        return address;
    }







    public static Comparator<Events> TitleComparator
            = new Comparator<Events>() {

        public int compare(Events Events1, Events Events2) {

            String name1 = Events1.getEvent_name().toUpperCase();
            String name2 = Events2.getEvent_name().toUpperCase();

            //ascending order
            return name1.compareTo(name2);

            //descending order
            //return name2.compareTo(name1);
        }

    };
    public static Comparator<Events> DateComparator
            = new Comparator<Events>() {

        public int compare(Events Events1, Events Events2) {

            String date1 = Events1.getEvent_date();
            String date2 = Events2.getEvent_date();

            //ascending order
            return date1.compareTo(date2);

            //descending order
            //return date2.compareTo(date1);
        }

    };

    public static Comparator<Non_attendo_events> nonAttendo_DateComparator
            = new Comparator<Non_attendo_events>() {

        public int compare(Non_attendo_events Events1, Non_attendo_events Events2) {

            String date1 = Events1.getNon_event_date();
            String date2 = Events2.getNon_event_date();

            //ascending order
            return date1.compareTo(date2);

            //descending order
            //return date2.compareTo(date1);
        }

    };

    public static Comparator<Non_attendo_events> nonAttendo_TitleComparator
            = new Comparator<Non_attendo_events>() {

        public int compare(Non_attendo_events Events1, Non_attendo_events Events2) {

            String name1 = Events1.getNon_event_name();
            String name2 = Events2.getNon_event_name();

            //ascending order
            return name1.compareTo(name2);

            //descending order
            //return date2.compareTo(date1);
        }

    };
    
    
    

    public static boolean checkDateValidity(String valid_till)
    {
        boolean isValid=true;
        try {

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date ExpDate = sdf.parse(valid_till);
            String curD = sdf.format(new Date());
            Date today = sdf.parse(curD);

            if (today.after(ExpDate))
                isValid = false;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return isValid;
    }
    

    public static boolean isBefore(String time)
    {
        boolean isBefore=false;
        try {
            DateFormat df = new SimpleDateFormat("hh:mm a", Locale.US);
            Date startTime = df.parse(time);
            String current = df.format(new Date());
            Date currentTime = df.parse(current);

            if (startTime.after(currentTime))
                  isBefore=true;

        }
        catch (Exception e)
        {

        }

        return isBefore;
    }

    public static boolean isAfter(String time)
    {
        boolean isAfter=false;
        try {
            DateFormat df = new SimpleDateFormat("hh:mm a", Locale.US);
            Date endTime = df.parse(time);
            String current = df.format(new Date());
            Date currentTime = df.parse(current);

            if (currentTime.after(endTime))
                isAfter=true;

        }
        catch (Exception e)
        {

        }

        return isAfter;
    }





    public static boolean checktimings(String time, String endtime) {

        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a", Locale.US);

        try {
            Date strt = sdf.parse(time);
            Date end = sdf.parse(endtime);

            if(end.getTime()>strt.getTime()) {
                return true;
            } else {

                return false;
            }
        } catch (ParseException e){
            e.printStackTrace();
            return false;
        }

    }




    public static long getDuration(String starttime, String endtime)
    {
        long difference=10000;
        try
        {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm a", Locale.US);
            Date startDate = simpleDateFormat.parse(starttime);
            Date endDate = simpleDateFormat.parse(endtime);

            difference = endDate.getTime() - startDate.getTime();

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return difference;

    }




    public static void ScheduleNotification(Context mContext, String start, String end, Events event)
    {
        int delay = (int)getDuration(start,end);
        notificationIntent = new Intent(mContext, FeedbackActivity.class);
        notificationIntent.putExtra("EVENT", event);
        notificationIntent.putExtra("USER_ID", ((Globals)mContext.getApplicationContext()).mActiveUser.getUser_id());
        pendingIntent = PendingIntent.getActivity(mContext, (int)System.currentTimeMillis(), notificationIntent, PendingIntent.FLAG_ONE_SHOT);
        long futureInMillis = SystemClock.elapsedRealtime() + delay;
        alarmManager = (AlarmManager)mContext.getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, futureInMillis, pendingIntent);
    }

    public static void ScheduleNotification(Context mContext, int delay, Events event)
    {
        notificationIntent = new Intent(mContext, FeedbackActivity.class);
        notificationIntent.putExtra("EVENT", event);
        notificationIntent.putExtra("USER_ID", ((Globals)mContext.getApplicationContext()).mActiveUser.getUser_id());
        pendingIntent = PendingIntent.getActivity(mContext, (int)System.currentTimeMillis(), notificationIntent, PendingIntent.FLAG_ONE_SHOT);
        long futureInMillis = SystemClock.elapsedRealtime() + delay;
        alarmManager = (AlarmManager)mContext.getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, futureInMillis, pendingIntent);
    }

    public static void CancelNotification()
    {
        alarmManager.cancel(pendingIntent);
    }





    public static boolean checkCPDvalidity(String mPoints)
    {
        boolean isValid=true;

        if(mPoints.contains(".")) {


            String[] Arr = mPoints.split("\\.") ;
            if (Arr.length>1)
            {
                if (Arr[1].length()>2)
                {

                    isValid = false;
                }
                else
                {
                    if(Integer.parseInt(Arr[1]) % 25 !=0)
                    {

                        isValid = false;
                    }
                }

            }
            else
                isValid = false;
        }




        return isValid;
    }

    public static void ShowPieGraph(String value1, String value2, String value3, String label1, String label2, String label3 , WebView webView)
    {
        String summary =
                "<html>"+
                        "<head><title>My First chart using FusionCharts XT - Using JavaScript</title><script type=\"text/javascript\" src=\"file:///android_asset/FusionCharts.js\"></script></head>"+
                        "<body>"+
                        " <div id=\"chartContainer\">FusionCharts XT will load here!</div>"+
                        "<script type=\"text/javascript\">"+
                        " var myChart = new FusionCharts( \"pie3d\",\"myChartId\", \"350\",\"200\", \"0\", \"0\" );"+
                        "myChart.setXMLData(\"<chart   yaxismaxvalue='110'  " +
                        "numbersuffix='' showvalues='1' bgcolor='#FFFFFF' chartleftmargin=''" +
                        " charttopmargin='10' chartrightmargin='' chartbottommargin='' " +
                        "basefontcolor='#400D1B' showalternatehgridcolor='0' enableSmartLabels='0' " +
                        "showcanvasbase='0' labeldisplay='WRAP' divlinecolor='#FFFFFF' " +
                        "canvasbgcolor='#FFFFFF' showcanvasbg='0' showshadow='0' " +
                        "palettecolors='#FF0000,#2EFE2E, #0000ff' " +
                        "showborder='0' borderColor='#000000' xAxisName='' yAxisName='' numberPrefix=''>" +
                        "<set label='"+label1+"' value='"+value1+"' />" +
                        "<set label='"+label2+"' value='"+value2+"' />" +
                        "<set label='"+label3+"' value='"+value3+"'/></chart></linkeddata></chart>\");"+
                        "myChart.render(\"chartContainer\");"+
                        "</script>"+
                        "</body>"+
                        "</html>";

        webView.loadDataWithBaseURL(null, summary, "text/html", "utf8", null);
    }
    public static void ShowLineGraph(String value1, String value2, String value3, String label1, String label2, String label3 , WebView webView)
    {
        String summary =

                "<html>"+
                        "<head><title>My First chart using FusionCharts XT - Using JavaScript</title><script type=\"text/javascript\" src=\"file:///android_asset/FusionCharts.js\"></script></head>"+
                        "<body>"+
                        " <div id=\"chartContainer\">FusionCharts XT will load here!</div>"+
                        "<script type=\"text/javascript\">"+
                        " var myChart = new FusionCharts( \"column3d\",\"myChartId\", \"300\",\"200\", \"0\", \"0\" );"+
                        "myChart.setXMLData(\"<chart   yaxismaxvalue='110'  " +
                        "numbersuffix='' showvalues='1' bgcolor='#FFFFFF' chartleftmargin=''" +
                        " charttopmargin='10' chartrightmargin='' chartbottommargin='' " +
                        "basefontcolor='#400D1B' showalternatehgridcolor='0' enableSmartLabels='0' " +
                        "showcanvasbase='0' labeldisplay='WRAP' divlinecolor='#FFFFFF' " +
                        "canvasbgcolor='#FFFFFF' showcanvasbg='0' showshadow='0' " +
                        "palettecolors='#FF0000,#2EFE2E, #0000ff' " +
                        "showborder='0' borderColor='#000000' xAxisName='' yAxisName='' numberPrefix=''>" +
                        "<set label='"+label1+"' value='"+value1+"' />" +
                        "<set label='"+label2+"' value='"+value2+"'/></chart></linkeddata></chart>\");"+
                        "myChart.render(\"chartContainer\");"+
                        "</script>"+
                        "</body>"+
                        "</html>";



        webView.loadDataWithBaseURL(null, summary, "text/html", "utf8", null);
    }


}
