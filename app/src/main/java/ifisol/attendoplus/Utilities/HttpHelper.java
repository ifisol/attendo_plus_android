package ifisol.attendoplus.Utilities;

import android.app.Activity;

import java.io.IOException;

import ifisol.attendoplus.Global.Globals;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/*

 * Created by Mehtab Ahmad on 2/21/2017.

*/


public class HttpHelper {

    public static void CallApi(final Activity mActivity, String url, RequestBody formBody , final HttpCallback cb, boolean showLoading) throws Exception
    {

        if (!Utility.isConnected(mActivity)) {
            if (Globals.mConnectionDia==null || !Globals.mConnectionDia.isShowing())
                Utility.ConnectionLost(mActivity);

            if (Globals.mLoadingDia != null && Globals.mLoadingDia.isShowing())
                Globals.mLoadingDia.dismiss();
            return;
        }


        if (showLoading)
            Utility.ShowLoading(mActivity);

        Request request = new Request.Builder().url(url).post(formBody).build();

        Globals.okHttpClient.newCall(request).enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {

                        if (Globals.mLoadingDia!=null && Globals.mLoadingDia.isShowing())
                        {
                            mActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Globals.mLoadingDia.dismiss();
                                }
                            });
                        }

                        cb.onFailure(call,e);
                    }

                    @Override
                    public void onResponse(final Call call, final Response response) throws IOException {

                        {
                            mActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    if (Globals.mLoadingDia!=null && Globals.mLoadingDia.isShowing())
                                      Globals.mLoadingDia.dismiss();
                                }
                            });
                        }

                        cb.onSuccess(call,response.body().string());

                    }

                });
    }


    public interface HttpCallback  {


        public void onFailure(Call call,IOException e);

        public void onSuccess(Call call, String response);
    }

}
