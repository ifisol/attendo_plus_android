package ifisol.attendoplus.Utilities;

/**
 * Created by Mehtab Ahmad on 12/20/2016.
 */

public class Constants
{
    public static final String TAG_GUEST = "guest";
    public static final String Base_URL = "http://attendo.plus.ifisol.com/";
    public static final String TAG_ORGANISER = "organiser";
    public static final String TAG_EXHIBITOR= "view_to_go";
    public static String       USER_TYPE = "User_type";
    public static final String TAG_AWAITING = "Awaiting Events";
    public static final String TAG_UPCOMING = "Upcoming Events";
    public static final String TAG_ATTENDED = "Attended Events";
    public static final String TAG_CPD_ALL_EVENTS = "All Events";
    public static final String TAG_CPD_ATTENDO = "Attendo Events";
    public static final String TAG_STATISTICS_EVENTS = "Statistics";
    public static final String TAG_CPD_NON_ATTENDO = "Non-Attendo Events";
    public static final String TAG_EXHIBITOR_PARTICIPATED_EVENTS = "Participated Events";
    public static final String TAG_PENDING_LOG_EVENTS = "Pending Logbook Events";
    public static final String TAG_ONGOING_EVENTS = "Ongoing Events";
    public static final String TAG_FILLED_LOG_EVENTS ="Filled Logbook Events";
    public static final String TAG_ORGANISATION_EVENTS ="Organisation Events";
    public static final String TAG_ANALYTIC_EVENTS ="Analytic Events";
    public static final String TAG_PAST = "Past Events";
    public static final String EVENT_TYPE = "event_type";
    public static final String TAG_VIEW_TO_GO = "view_to_go";
    public static final String TAG_INTERESTED_GUEST = "Interested Guests";
    public static final String TAG_ATTENDED_GUEST = "Attended Guests";
    public static final String API_Base_URL = "http://attendo.plus.ifisol.com/api/v0.1/";
    public static final String Img_Base_URL = "http://attendo.plus.ifisol.com/images/";
    public static final String Fetch_Organisations = API_Base_URL + "organisation_List";
    public static final String Register = API_Base_URL + "register";
    public static final String Login = API_Base_URL + "login";
    public static final String ChangePassword = API_Base_URL + "Change_password";
    public static final String Guest_events = API_Base_URL + "guest_event_listing"; // user_id&tag=(guest_upcoming_events/guest_attended _events)
    public static final String Guest_event_detail = API_Base_URL + "guest_event_detail";
    public static final String Im_interested = API_Base_URL + "guest_interested_in_event";
    public static final String Other_guests = API_Base_URL + "other_guests"; // user_id&event_id&status=(upcoming/attended)
    public static final String Exhibitor_detail = API_Base_URL + "exhibitor_detail";
    public static final String Get_guest_public_Profile = API_Base_URL + "guest_public_profile";
    public static final String Get_org_public_Profile = API_Base_URL + "organiser_public_profile";
    public static final String Get_guest_Profile = API_Base_URL + "get_guest_profile";
    public static final String Get_org_Profile = API_Base_URL + "organiser_profile";
    public static final String Update_Guest_profile = API_Base_URL + "guest_update_profile";
    public static final String Update_org_profile = API_Base_URL + "update_organiser_profile";
    public static final String Guest_Cpd_events = API_Base_URL + "guest_cpd_list";
    public static final String Create_non_attendo_events = API_Base_URL + "guest_create_non_a_event";
    public static final String Edit_non_attendo_events = API_Base_URL + "guest_update_non_a_event";
    public static final String Logbook_events = API_Base_URL + "guest_log_book_list";
    public static final String Organisation_detail = API_Base_URL + "organisation_detail";
    public static final String Get_Exhibitors = API_Base_URL + "get_exhibitors";

    public static final String Get_Notifications = API_Base_URL + "get_notification_list";
    public static final String Update_Read_status = API_Base_URL + "guest_notification_status";
    public static final String Get_Notification_detail = API_Base_URL + "get_notification_detail";
    public static final String Get_Organiser_events = API_Base_URL + "org_events_list";
    public static final String Get_event_types = API_Base_URL + "get_event_types";
    public static final String Add_event_basic_info = API_Base_URL + "event_basic_info";
    public static final String Add_event_exhibitors = API_Base_URL + "event_exhibitor_info";
    public static final String Add_event_logbook = API_Base_URL + "event_logbook_info";
    public static final String Add_event_feedback = API_Base_URL + "event_feedback_info";
    public static final String Analytic_event_detail = API_Base_URL + "get_analytic_event_detail";
    public static final String Get_exhibitor_visitors = API_Base_URL + "get_exhibitor_visitors";
    public static final String Mark_attendance = API_Base_URL + "mark_attendance";
    public static final String Get_existing_guests = API_Base_URL + "get_org_non_attend_guests";
    public static final String Create_organisation = API_Base_URL + "create_orgainisation";
    public static final String Qr_code_list = API_Base_URL + "qr_code_list";
    public static final String Org_event_detail = API_Base_URL + "org_event_detail";
    public static final String Mark_qr_attendance = API_Base_URL + "mark_qr_attendance";
    public static final String Get_notification_type = API_Base_URL + "get_notification_types";
    public static final String create_notification = API_Base_URL + "create_notification";
    public static final String Scan_exhibitor_qr = API_Base_URL + "scan_exhibitor_qr";
    public static final String Delete_event = API_Base_URL + "delete_event";
    public static final String forgot_password = API_Base_URL + "forgot_password";
    public static final String Organisation_guest = API_Base_URL + "get_organisation_guests_list";
    public static final String Send_qr_organiser = API_Base_URL + "send_qr_to_organiser";
    public static final String Send_qr_to_exhibitor = API_Base_URL + "send_qr_to_exhibitor";

}
