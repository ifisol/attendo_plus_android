package ifisol.attendoplus.Global;

import android.app.Application;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import ifisol.attendoplus.Model.Events;
import ifisol.attendoplus.Model.User;
import ifisol.attendoplus.R;
import okhttp3.OkHttpClient;

/**
 * Created by Mehtab Ahmad on 6/14/2016.
 */
public class Globals extends Application
{

    public static SharedPreferences mPreference;
    public static SharedPreferences.Editor mEditor;
    public static OkHttpClient okHttpClient ;
    public User mActiveUser;
    public static Dialog mLoadingDia;
    public static Dialog mConnectionDia;
    public static String timezone;

    @Override
    public void onCreate() {

        super.onCreate();
        mPreference = getSharedPreferences("Attendo_cpd_pref", Context.MODE_PRIVATE);
        mEditor = mPreference.edit();
        okHttpClient = new OkHttpClient.Builder().connectTimeout(35, TimeUnit.SECONDS).build();
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder().cacheOnDisc(true).imageScaleType(ImageScaleType.EXACTLY).bitmapConfig(Bitmap.Config.RGB_565).cacheInMemory(true).showImageForEmptyUri(R.drawable.no_img).showImageOnFail(R.drawable.no_img).showImageOnLoading(R.drawable.loading_placeholder).imageScaleType(ImageScaleType.EXACTLY).displayer(new FadeInBitmapDisplayer(300)).build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext()).defaultDisplayImageOptions(defaultOptions).memoryCache(new WeakMemoryCache()).discCacheSize(100 * 1024 * 1024).build();
        ImageLoader.getInstance().init(config);
        mActiveUser = GetActiveUser();

        timezone = TimeZone.getDefault().getID();
        Log.d("My time zone", timezone);
    }


    public void SaveActiveUser(User user) {
        mActiveUser = user;
        Gson gson = new Gson();
        String mUser = gson.toJson(user);
        mEditor.putString("ACTIVE_USER",mUser);
        mEditor.commit();
    }


    public void SaveLocation(String location) {
        mEditor.putString("LOCATION",location);
        mEditor.commit();
    }


    public String GETLocation() {
        String loc = mPreference.getString("LOCATION",null);
        return loc;
    }


    public User GetActiveUser() {
        Gson gson   = new Gson();
        String user = mPreference.getString("ACTIVE_USER", null);
        User mUser  = gson.fromJson(user,User.class);
        return mUser;
    }



    /*public Events GetEvent()
    {
        String ObjString=mPreference.getString("EVENT", null);
        Gson gson=new Gson();
        Events event=gson.fromJson(ObjString, Events.class);
        return event;
    }*/


   /* public void saveFeedBackList(ArrayList<Events> events) {

        Gson gson = new Gson();
        String ObjString = gson.toJson(events);
        mEditor.putString("FEEDBACK_EVENTS", ObjString);
        mEditor.commit();
    }


    public ArrayList<Events> getFeedBackList() {

        Gson gson=new Gson();
        Type type = new TypeToken<List<Events>>(){}.getType();
        ArrayList<Events> events = gson.fromJson(mPreference.getString("FEEDBACK_EVENTS", null), type);
        return events;
    }*/
}
