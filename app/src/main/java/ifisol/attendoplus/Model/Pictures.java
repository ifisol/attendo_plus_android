package ifisol.attendoplus.Model;

import java.io.Serializable;

/**
 * Created by Mehtab Ahmad on 3/7/2017.
 */

public class Pictures implements Serializable {



   String status;
   String non_event_picture;

    public String getNon_event_picture() {

        return non_event_picture;
    }

    public void setNon_event_picture(String non_event_picture) {

        this.non_event_picture = non_event_picture;
    }

    public String getStatus() {

        return status;
    }

    public void setStatus(String status) {

        this.status = status;
    }
}
