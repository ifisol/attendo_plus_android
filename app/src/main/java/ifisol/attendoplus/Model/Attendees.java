package ifisol.attendoplus.Model;

import java.io.Serializable;

/**
 * Created by Mehtab Ahmad on 2/28/2017.
 */

public class Attendees implements Serializable {

   String user_id;
   String first_name;
   String last_name;
   String user_name;
   String email;
   String profession;
   String address;
   String phone;
   String picture;
   String organisation_id;
   String user_registration_status;
   String profile_visibility_status;

   public String getUser_id() {

      return user_id;
   }

   public void setUser_id(String user_id) {

      this.user_id = user_id;
   }

   public String getFirst_name() {

      return first_name;
   }

   public void setFirst_name(String first_name) {

      this.first_name = first_name;
   }

   public String getLast_name() {

      return last_name;
   }

   public void setLast_name(String last_name) {

      this.last_name = last_name;
   }

   public String getUser_name() {

      return user_name;
   }

   public void setUser_name(String user_name) {

      this.user_name = user_name;
   }

   public String getEmail() {

      return email;
   }

   public void setEmail(String email) {

      this.email = email;
   }

   public String getProfession() {

      return profession;
   }

   public void setProfession(String profession) {

      this.profession = profession;
   }

   public String getAddress() {

      return address;
   }

   public void setAddress(String address) {

      this.address = address;
   }

   public String getPhone() {

      return phone;
   }

   public void setPhone(String phone) {

      this.phone = phone;
   }

   public String getPicture() {

      return picture;
   }

   public void setPicture(String picture) {

      this.picture = picture;
   }

   public String getOrganisation_id() {

      return organisation_id;
   }

   public void setOrganisation_id(String organisation_id) {

      this.organisation_id = organisation_id;
   }

   public String getUser_registration_status() {

      return user_registration_status;
   }

   public void setUser_registration_status(String user_registration_status) {

      this.user_registration_status = user_registration_status;
   }

   public String getProfile_visibility_status() {

      return profile_visibility_status;
   }

   public void setProfile_visibility_status(String profile_visibility_status) {

      this.profile_visibility_status = profile_visibility_status;
   }


}
