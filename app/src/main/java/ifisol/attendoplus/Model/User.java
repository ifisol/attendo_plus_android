package ifisol.attendoplus.Model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Mehtab Ahmad on 2/22/2017.
 */

public class User implements Serializable
{


    String user_id;
    String first_name;
    String last_name;
    String user_name;
    String email;
    String profession;
    String address;
    String phone;
    String password;
    String picture;
    String user_type;
    String organisation_id;
    String organisation_name;
    String device_token;
    String device_type;
    String status;
    String user_registration_status;
    String profile_visibility_status;
    String cpd_count;
    String logbook_url;
    String feedback_url;
    int ratings;
    ArrayList<Organisations> seconday_organisations;

    public String getUser_id() {

        return user_id;
    }

    public void setUser_id(String user_id) {

        this.user_id = user_id;
    }

    public String getFirst_name() {

        return first_name;
    }

    public void setFirst_name(String first_name) {

        this.first_name = first_name;
    }

    public String getLast_name() {

        return last_name;
    }

    public void setLast_name(String last_name) {

        this.last_name = last_name;
    }

    public String getUser_name() {

        return user_name;
    }

    public void setUser_name(String user_name) {

        this.user_name = user_name;
    }

    public String getEmail() {

        return email;
    }

    public void setEmail(String email) {

        this.email = email;
    }

    public String getProfession() {

        return profession;
    }

    public void setProfession(String profession) {

        this.profession = profession;
    }

    public String getAddress() {

        return address;
    }

    public void setAddress(String address) {

        this.address = address;
    }

    public String getPhone() {

        return phone;
    }

    public void setPhone(String phone) {

        this.phone = phone;
    }

    public String getPassword() {

        return password;
    }

    public void setPassword(String password) {

        this.password = password;
    }

    public String getPicture() {

        return picture;
    }

    public void setPicture(String picture) {

        this.picture = picture;
    }

    public String getUser_type() {

        return user_type;
    }

    public void setUser_type(String user_type) {

        this.user_type = user_type;
    }

    public String getOrganisation_id() {

        return organisation_id;
    }

    public void setOrganisation_id(String organisation_id) {

        this.organisation_id = organisation_id;
    }

    public String getOrganisation_name() {

        return organisation_name;
    }

    public void setOrganisation_name(String organisation_name) {

        this.organisation_name = organisation_name;
    }

    public String getDevice_token() {

        return device_token;
    }

    public void setDevice_token(String device_token) {

        this.device_token = device_token;
    }

    public String getDevice_type() {

        return device_type;
    }

    public void setDevice_type(String device_type) {

        this.device_type = device_type;
    }

    public String getStatus() {

        return status;
    }

    public void setStatus(String status) {

        this.status = status;
    }

    public String getUser_registration_status() {

        return user_registration_status;
    }

    public void setUser_registration_status(String user_registration_status) {

        this.user_registration_status = user_registration_status;
    }

    public String getProfile_visibility_status() {

        return profile_visibility_status;
    }

    public void setProfile_visibility_status(String profile_visibility_status) {

        profile_visibility_status = profile_visibility_status;
    }

    public String getCpd_count() {

        return cpd_count;
    }

    public void setCpd_count(String cpd_count) {

        this.cpd_count = cpd_count;
    }

    public String getLogbook_url() {

        return logbook_url;
    }

    public void setLogbook_url(String logbook_url) {

        this.logbook_url = logbook_url;
    }

    public String getFeedback_url() {

        return feedback_url;
    }

    public void setFeedback_url(String feedback_url) {

        this.feedback_url = feedback_url;
    }

    public int getRatings() {

        return ratings;
    }

    public void setRatings(int ratings) {

        this.ratings = ratings;
    }

    public ArrayList<Organisations> getSeconday_organisations() {

        return seconday_organisations;
    }

    public void setSeconday_organisations(ArrayList<Organisations> seconday_organisations) {

        this.seconday_organisations = seconday_organisations;
    }
}
