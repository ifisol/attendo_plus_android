package ifisol.attendoplus.Model;

/**
 * Created by Mehtab Ahmad on 3/20/2017.
 */

public class Event_Types {

   String event_type_id;
   String event_type_name;
   boolean isSelected;

    public String getEvent_type_id() {

        return event_type_id;
    }

    public void setEvent_type_id(String event_type_id) {

        this.event_type_id = event_type_id;
    }

    public String getEvent_type_name() {

        return event_type_name;
    }

    public void setEvent_type_name(String event_type_name) {

        this.event_type_name = event_type_name;
    }

    public boolean isSelected() {

        return isSelected;
    }

    public void setSelected(boolean selected) {

        isSelected = selected;
    }
}
