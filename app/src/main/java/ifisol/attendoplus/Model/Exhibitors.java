package ifisol.attendoplus.Model;

import java.io.Serializable;

/**
 * Created by Mehtab Ahmad on 2/28/2017.
 */

public class Exhibitors implements Serializable
{




    String exhibitor_qr_approval_date;
    String exhibitor_qr_exists;
    String exhibitor_id;
    String exhibitor_name;
    String exhibitor_picture;
    String exhibitor_email;
    String exhibitor_phone;
    String exhibitor_website;
    String exhibitor_company;
    String exhibitor_description;
    String exhb_scanned_status;
    String exhibitor_status;
    String exhb_visit_count;
    String exhb_event_count;

    boolean is_selected;

    public String getExhb_event_count() {

        return exhb_event_count;
    }

    public void setExhb_event_count(String exhb_event_count) {

        this.exhb_event_count = exhb_event_count;
    }

    public boolean isIs_selected() {

        return is_selected;
    }

    public void setIs_selected(boolean is_selected) {

        this.is_selected = is_selected;
    }

    public String getExhibitor_id() {

        return exhibitor_id;
    }

    public void setExhibitor_id(String exhibitor_id) {

        this.exhibitor_id = exhibitor_id;
    }

    public String getExhibitor_name() {

        return exhibitor_name;
    }

    public void setExhibitor_name(String exhibitor_name) {

        this.exhibitor_name = exhibitor_name;
    }

    public String getExhibitor_picture() {

        return exhibitor_picture;
    }

    public void setExhibitor_picture(String exhibitor_picture) {

        this.exhibitor_picture = exhibitor_picture;
    }

    public String getExhibitor_email() {

        return exhibitor_email;
    }

    public void setExhibitor_email(String exhibitor_email) {

        this.exhibitor_email = exhibitor_email;
    }


    public String getExhibitor_website() {

        return exhibitor_website;
    }

    public void setExhibitor_website(String exhibitor_website) {

        this.exhibitor_website = exhibitor_website;
    }

    public String getExhibitor_company() {

        return exhibitor_company;
    }

    public void setExhibitor_company(String exhibitor_company) {

        this.exhibitor_company = exhibitor_company;
    }

    public String getExhibitor_description() {

        return exhibitor_description;
    }

    public void setExhibitor_description(String exhibitor_description) {

        this.exhibitor_description = exhibitor_description;
    }

    public String getExhb_scanned_status() {

        return exhb_scanned_status;
    }

    public void setExhb_scanned_status(String exhb_scanned_status) {

        this.exhb_scanned_status = exhb_scanned_status;
    }

    public String getExhibitor_phone() {

        return exhibitor_phone;
    }

    public void setExhibitor_phone(String exhibitor_phone) {

        this.exhibitor_phone = exhibitor_phone;
    }

    public String getExhibitor_status() {

        return exhibitor_status;
    }

    public void setExhibitor_status(String exhibitor_status) {

        this.exhibitor_status = exhibitor_status;
    }

    public String getExhb_visit_count() {

        return exhb_visit_count;
    }

    public void setExhb_visit_count(String exhb_visit_count) {

        this.exhb_visit_count = exhb_visit_count;
    }

    public String getExhibitor_qr_approval_date() {

        return exhibitor_qr_approval_date;
    }

    public void setExhibitor_qr_approval_date(String exhibitor_qr_approval_date) {

        this.exhibitor_qr_approval_date = exhibitor_qr_approval_date;
    }

    public String getExhibitor_qr_exists() {

        return exhibitor_qr_exists;
    }

    public void setExhibitor_qr_exists(String exhibitor_qr_exists) {

        this.exhibitor_qr_exists = exhibitor_qr_exists;
    }
}
