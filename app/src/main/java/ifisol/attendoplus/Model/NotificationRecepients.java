package ifisol.attendoplus.Model;

/**
 * Created by Mehtab Ahmad on 5/12/2017.
 */

public class NotificationRecepients {

      String user_id;
      String first_name;
      String last_name;
      String user_type;
      String picture;
      boolean isSelect;

    public String getUser_id() {

        return user_id;
    }

    public void setUser_id(String user_id) {

        this.user_id = user_id;
    }

    public boolean isSelect() {

        return isSelect;
    }

    public void setSelect(boolean select) {

        isSelect = select;
    }

    public String getFirst_name() {

        return first_name;
    }

    public void setFirst_name(String first_name) {

        this.first_name = first_name;
    }

    public String getLast_name() {

        return last_name;
    }

    public void setLast_name(String last_name) {

        this.last_name = last_name;
    }

    public String getUser_type() {

        return user_type;
    }

    public void setUser_type(String user_type) {

        this.user_type = user_type;
    }

    public String getPicture() {

        return picture;
    }

    public void setPicture(String picture) {

        this.picture = picture;
    }
}
