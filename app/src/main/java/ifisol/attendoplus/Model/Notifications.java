package ifisol.attendoplus.Model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Mehtab Ahmad on 3/16/2017.
 */

public class Notifications implements Serializable
{

    String notification_type_id;
    String notification_type_name;
    String notification_id;
    String notification_name;
    String notification_subject;
    String notification_picture;
    String notification_description;
    String notification_read_status;
    ArrayList<Notifications> notifications;
    ArrayList<User> viewed_by;
    String is_my_created;

    public ArrayList<User> getViewed_by() {

        return viewed_by;
    }

    public void setViewed_by(ArrayList<User> viewed_by) {

        this.viewed_by = viewed_by;
    }

    public String getNotification_type_id() {

        return notification_type_id;
    }

    public void setNotification_type_id(String notification_type_id) {

        this.notification_type_id = notification_type_id;
    }

    public String getNotification_type_name() {

        return notification_type_name;
    }

    public void setNotification_type_name(String notification_type_name) {

        this.notification_type_name = notification_type_name;
    }

    public String getNotification_id() {

        return notification_id;
    }

    public void setNotification_id(String notification_id) {

        this.notification_id = notification_id;
    }

    public String getNotification_name() {

        return notification_name;
    }

    public void setNotification_name(String notification_name) {

        this.notification_name = notification_name;
    }

    public String getNotification_subject() {

        return notification_subject;
    }

    public void setNotification_subject(String notification_subject) {

        this.notification_subject = notification_subject;
    }

    public String getNotification_picture() {

        return notification_picture;
    }

    public void setNotification_picture(String notification_picture) {

        this.notification_picture = notification_picture;
    }

    public String getNotification_description() {

        return notification_description;
    }

    public void setNotification_description(String notification_description) {

        this.notification_description = notification_description;
    }

    public String getNotification_read_status() {

        return notification_read_status;
    }

    public void setNotification_read_status(String notification_read_status) {

        this.notification_read_status = notification_read_status;
    }

    public ArrayList<Notifications> getNotifications() {

        return notifications;
    }

    public void setNotifications(ArrayList<Notifications> notifications) {

        this.notifications = notifications;
    }

    public String getIs_my_created() {

        return is_my_created;
    }

    public void setIs_my_created(String is_my_created) {

        this.is_my_created = is_my_created;
    }
}
