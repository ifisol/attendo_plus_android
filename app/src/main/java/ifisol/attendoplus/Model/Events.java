package ifisol.attendoplus.Model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Mehtab Ahmad on 2/23/2017.
 */

public class Events implements Serializable {




   String event_status;
   String event_id;
   String event_name;
   String event_description;
   String event_photo;
   String event_cpd;
   String event_date;
   String event_start_time;
   String event_end_time;
   String event_street_address;
   String event_city;
   String event_country;
   String event_lat;
   String event_lng;
   String event_organisation_id;
   String event_organisation_name;
   String event_type_id;
   String event_type_name;
   String is_public_event;
   String user_is_interested;
   int    interested_guests;
   int    attended_guests;
   String has_logbook;
   String has_feedback;
   String has_exhibitor;
   String exhibitor_count;
   String has_filled_logbook;
   String has_filled_feedback;
   String has_scanned_exhibitor;
   String event_web_url;
   ArrayList<Exhibitors> guest_event_exhibitors;
   String logbook_url;
   String on_going;
   int attanded_count;
    String feedback_url;
    String user_id;
    String is_my_created_event;




    public String getEvent_id() {

        return event_id;
    }

    public String getIs_my_created_event() {

        return is_my_created_event;
    }

    public void setIs_my_created_event(String is_my_created_event) {

        this.is_my_created_event = is_my_created_event;
    }

    public void setEvent_id(String event_id) {

        this.event_id = event_id;
    }

    public String getFeedback_url() {

        return feedback_url;
    }

    public void setFeedback_url(String feedback_url) {

        this.feedback_url = feedback_url;
    }

    public String getEvent_name() {

        return event_name;
    }

    public void setEvent_name(String event_name) {

        this.event_name = event_name;
    }

    public String getEvent_photo() {

        return event_photo;
    }

    public void setEvent_photo(String event_photo) {

        this.event_photo = event_photo;
    }

    public String getEvent_cpd() {

        return event_cpd;
    }

    public void setEvent_cpd(String event_cpd) {

        this.event_cpd = event_cpd;
    }

    public String getEvent_date() {

        return event_date;
    }

    public String getLogbook_url() {

        return logbook_url;
    }

    public void setLogbook_url(String logbook_url) {

        this.logbook_url = logbook_url;
    }

    public void setEvent_date(String event_date) {

        this.event_date = event_date;
    }

    public String getEvent_start_time() {

        return event_start_time;
    }

    public String getEvent_status() {

        return event_status;
    }

    public void setEvent_status(String event_status) {

        this.event_status = event_status;
    }

    public void setEvent_start_time(String event_start_time) {

        this.event_start_time = event_start_time;
    }

    public String getEvent_end_time() {

        return event_end_time;
    }

    public void setEvent_end_time(String event_end_time) {

        this.event_end_time = event_end_time;
    }

    public String getEvent_street_address() {

        return event_street_address;
    }

    public void setEvent_street_address(String event_street_address) {

        this.event_street_address = event_street_address;
    }

    public String getEvent_city() {

        return event_city;
    }

    public void setEvent_city(String event_city) {

        this.event_city = event_city;
    }

    public String getEvent_country() {

        return event_country;
    }

    public void setEvent_country(String event_country) {

        this.event_country = event_country;
    }

    public String getEvent_lat() {

        return event_lat;
    }

    public void setEvent_lat(String event_lat) {

        this.event_lat = event_lat;
    }

    public String getEvent_lng() {

        return event_lng;
    }

    public void setEvent_lng(String event_lng) {

        this.event_lng = event_lng;
    }

    public String getIs_public_event() {

        return is_public_event;
    }

    public void setIs_public_event(String is_public_event) {

        this.is_public_event = is_public_event;
    }

    public String getUser_is_interested() {

        return user_is_interested;
    }

    public void setUser_is_interested(String user_is_interested) {

        this.user_is_interested = user_is_interested;
    }

    public int getInterested_guests() {

        return interested_guests;
    }

    public void setInterested_guests(int interested_guests) {

        this.interested_guests = interested_guests;
    }

    public int getAttended_guests() {

        return attended_guests;
    }

    public void setAttended_guests(int attended_guests) {

        this.attended_guests = attended_guests;
    }

    public String getHas_logbook() {

        return has_logbook;
    }

    public void setHas_logbook(String has_logbook) {

        this.has_logbook = has_logbook;
    }

    public String getHas_feedback() {

        return has_feedback;
    }

    public void setHas_feedback(String has_feedback) {

        this.has_feedback = has_feedback;
    }

    public String getHas_exhibitor() {

        return has_exhibitor;
    }

    public void setHas_exhibitor(String has_exhibitor) {

        this.has_exhibitor = has_exhibitor;
    }

    public String getExhibitor_count() {

        return exhibitor_count;
    }

    public void setExhibitor_count(String exhibitor_count) {

        this.exhibitor_count = exhibitor_count;
    }

    public String getHas_filled_logbook() {

        return has_filled_logbook;
    }

    public void setHas_filled_logbook(String has_filled_logbook) {

        this.has_filled_logbook = has_filled_logbook;
    }

    public String getHas_filled_feedback() {

        return has_filled_feedback;
    }

    public void setHas_filled_feedback(String has_filled_feedback) {

        this.has_filled_feedback = has_filled_feedback;
    }

    public String getHas_scanned_exhibitor() {

        return has_scanned_exhibitor;
    }

    public void setHas_scanned_exhibitor(String has_scanned_exhibitor) {

        this.has_scanned_exhibitor = has_scanned_exhibitor;
    }

    public ArrayList<Exhibitors> getGuest_event_exhibitors() {

        return guest_event_exhibitors;
    }

    public void setGuest_event_exhibitors(ArrayList<Exhibitors> guest_event_exhibitors) {

        this.guest_event_exhibitors = guest_event_exhibitors;
    }

    public String getEvent_description() {

        return event_description;
    }

    public void setEvent_description(String event_description) {

        this.event_description = event_description;
    }

    public String getEvent_organisation_id() {

        return event_organisation_id;
    }

    public void setEvent_organisation_id(String event_organisation_id) {

        this.event_organisation_id = event_organisation_id;
    }

    public String getEvent_organisation_name() {

        return event_organisation_name;
    }

    public void setEvent_organisation_name(String event_organisation_name) {

        this.event_organisation_name = event_organisation_name;
    }

    public String getEvent_type_id() {

        return event_type_id;
    }

    public void setEvent_type_id(String event_type_id) {

        this.event_type_id = event_type_id;
    }

    public String getEvent_type_name() {

        return event_type_name;
    }

    public void setEvent_type_name(String event_type_name) {

        this.event_type_name = event_type_name;
    }

    public String getOn_going() {

        return on_going;
    }

    public void setOn_going(String on_going) {

        this.on_going = on_going;
    }

    public int getAttanded_count() {

        return attanded_count;
    }

    public String getUser_id() {

        return user_id;
    }

    public void setUser_id(String user_id) {

        this.user_id = user_id;
    }

    public void setAttanded_count(int attanded_count) {

        this.attanded_count = attanded_count;
    }

    public String getEvent_web_url() {

        return event_web_url;
    }

    public void setEvent_web_url(String event_web_url) {

        this.event_web_url = event_web_url;
    }
}
