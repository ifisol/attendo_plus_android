package ifisol.attendoplus.Model;

import java.io.Serializable;

/**
 * Created by Mehtab Ahmad on 2/21/2017.
 */

public class Organisations implements Serializable{

      String organisation_id;
      String organisation_name;
      String organisation_description;
      String organisation_image;
      String organisation_website;
      String organisation_address;
      String organisation_ph_number;
      String organisation_added_date;
      String organisation_added_user_id;
      String organisation_status;

    boolean isSelected;

    public String getOrganisation_id() {

        return organisation_id;
    }

    public void setOrganisation_id(String organisation_id) {

        this.organisation_id = organisation_id;
    }

    public String getOrganisation_name() {

        return organisation_name;
    }

    public boolean isSelected() {

        return isSelected;
    }

    public void setSelected(boolean selected) {

        isSelected = selected;
    }

    public void setOrganisation_name(String organisation_name) {

        this.organisation_name = organisation_name;
    }

    public String getOrganisation_description() {

        return organisation_description;
    }

    public void setOrganisation_description(String organisation_description) {

        this.organisation_description = organisation_description;
    }

    public String getOrganisation_image() {

        return organisation_image;
    }

    public void setOrganisation_image(String organisation_image) {

        this.organisation_image = organisation_image;
    }

    public String getOrganisation_website() {

        return organisation_website;
    }

    public void setOrganisation_website(String organisation_website) {

        this.organisation_website = organisation_website;
    }

    public String getOrganisation_address() {

        return organisation_address;
    }

    public void setOrganisation_address(String organisation_address) {

        this.organisation_address = organisation_address;
    }

    public String getOrganisation_ph_number() {

        return organisation_ph_number;
    }

    public void setOrganisation_ph_number(String organisation_ph_number) {

        this.organisation_ph_number = organisation_ph_number;
    }

    public String getOrganisation_added_date() {

        return organisation_added_date;
    }

    public void setOrganisation_added_date(String organisation_added_date) {

        this.organisation_added_date = organisation_added_date;
    }

    public String getOrganisation_added_user_id() {

        return organisation_added_user_id;
    }

    public void setOrganisation_added_user_id(String organisation_added_user_id) {

        this.organisation_added_user_id = organisation_added_user_id;
    }

    public String getOrganisation_status() {

        return organisation_status;
    }

    public void setOrganisation_status(String organisation_status) {

        this.organisation_status = organisation_status;
    }
}
