package ifisol.attendoplus.Model;

/**
 * Created by Mehtab Ahmad on 4/18/2017.
 */

public class Notification_types
{
   String notification_type_id;
   String notification_type_name;
   boolean isSelected;

    public String getNotification_type_id() {

        return notification_type_id;
    }

    public void setNotification_type_id(String notification_type_id) {

        this.notification_type_id = notification_type_id;
    }

    public String getNotification_type_name() {

        return notification_type_name;
    }

    public void setNotification_type_name(String notification_type_name) {

        this.notification_type_name = notification_type_name;
    }

    public boolean isSelected() {

        return isSelected;
    }

    public void setSelected(boolean selected) {

        isSelected = selected;
    }
}
