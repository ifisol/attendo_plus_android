package ifisol.attendoplus.Model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Mehtab Ahmad on 2/23/2017.
 */

public class Non_attendo_events implements Serializable {


    String non_event_id;
    String non_event_name;
    String non_event_subject;
    String non_event_description;
    String non_event_date;
    String non_event_start_time;
    String non_event_end_time;
    String non_event_cpd;
    String non_event_reff;
    String non_event_website;
    String status;
    String non_event_type;
    String user_id;
    ArrayList<Pictures> pictures;



    public String getNon_event_id() {

        return non_event_id;
    }

    public void setNon_event_id(String non_event_id) {

        this.non_event_id = non_event_id;
    }

    public String getNon_event_name() {

        return non_event_name;
    }

    public void setNon_event_name(String non_event_name) {

        this.non_event_name = non_event_name;
    }

    public String getNon_event_subject() {

        return non_event_subject;
    }

    public void setNon_event_subject(String non_event_subject) {

        this.non_event_subject = non_event_subject;
    }

    public String getNon_event_description() {

        return non_event_description;
    }

    public void setNon_event_description(String non_event_description) {

        this.non_event_description = non_event_description;
    }

    public String getNon_event_date() {

        return non_event_date;
    }

    public void setNon_event_date(String non_event_date) {

        this.non_event_date = non_event_date;
    }

    public String getNon_event_start_time() {

        return non_event_start_time;
    }

    public void setNon_event_start_time(String non_event_start_time) {

        this.non_event_start_time = non_event_start_time;
    }

    public String getNon_event_end_time() {

        return non_event_end_time;
    }

    public void setNon_event_end_time(String non_event_end_time) {

        this.non_event_end_time = non_event_end_time;
    }

    public String getNon_event_cpd() {

        return non_event_cpd;
    }

    public void setNon_event_cpd(String non_event_cpd) {

        this.non_event_cpd = non_event_cpd;
    }

    public String getNon_event_reff() {

        return non_event_reff;
    }

    public void setNon_event_reff(String non_event_reff) {

        this.non_event_reff = non_event_reff;
    }

    public String getNon_event_website() {

        return non_event_website;
    }

    public void setNon_event_website(String non_event_website) {

        this.non_event_website = non_event_website;
    }

    public String getStatus() {

        return status;
    }

    public void setStatus(String status) {

        this.status = status;
    }

    public String getNon_event_type() {

        return non_event_type;
    }

    public void setNon_event_type(String non_event_type) {

        this.non_event_type = non_event_type;
    }

    public String getUser_id() {

        return user_id;
    }

    public void setUser_id(String user_id) {

        this.user_id = user_id;
    }

    public ArrayList<Pictures> getPictures() {

        return pictures;
    }

    public void setPictures(ArrayList<Pictures> pictures) {

        this.pictures = pictures;
    }
}
