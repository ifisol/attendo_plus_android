/*
package ifisol.attendopluscpd.Controller.HttpUtilis;

import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

public class LocationClass implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener
         {
    Activity mActivity;

    private static final String TAG = "Location Class";
    private static final int INTERVAL = 1000*4;
             private static final int REQUEST_LOCATION = 2;
             private static final int DISTANCE = 5;
             private static final long FASTEST_INTERVAL = 1000 * 5;
             LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
             String TimeOrDistance="";

    public LocationClass(Activity activity, LocationRequest request) {
        mActivity = activity;
        mLocationRequest = request;
        CreateGoogleClientAPI();
        mGoogleApiClient.connect();
        mLocationRequest.setInterval(INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        mLocationRequest.setSmallestDisplacement(DISTANCE);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

//        protected void createLocationRequest(String TimeOrDistance) {
//            mLocationRequest = new LocationRequest();
//
//
//                mLocationRequest.setInterval(INTERVAL);
//                mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
//
//
//                mLocationRequest.setSmallestDisplacement(DISTANCE);
//
//
//            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
//    }

    private void CreateGoogleClientAPI() {
        mGoogleApiClient = new GoogleApiClient.Builder(mActivity)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }


    protected void startLocationUpdates() {
        Intent intent=new Intent("com.switchsolutions.locationemitter.LOCATION_SEND_ACTION");
        PendingIntent pendingIntent= PendingIntent.getBroadcast(mActivity,0,intent,0);

       */
/* LocationRequest request= LocationRequest.create().setInterval(INTERVAL).setFastestInterval(FASTEST_INTERVAL)
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);*//*

        if (ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Check Permissions Now
            ActivityCompat.requestPermissions(mActivity,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_LOCATION);
        } else {
            // permission has been granted, continue as usual
            PendingResult<Status> pendingResult = LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient,mLocationRequest, pendingIntent);

            Log.d(TAG, "Location update started ..............: ");
        }


    }



    @Override
    public void onConnected(Bundle bundle) {

        startLocationUpdates();
        Toast.makeText(mActivity,"onConnected is called",Toast.LENGTH_SHORT).show();

        Log.d(TAG, "Connected ..............: ");
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.d(TAG, "Connection failed .......................");
    }

            */
/* public void stopLocationUpdates() {
        Intent intent=new Intent("com.switchsolutions.locationemitter.LOCATION_SEND_ACTION");


       PendingIntent pendingIntent= PendingIntent.getBroadcast(mActivity,0,intent,0);

        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, pendingIntent);

        Log.d(TAG, "Location update stopped .......................");
    }*//*


         }
*/
