package ifisol.attendoplus.Controller.Adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.github.siyamed.shapeimageview.RoundedImageView;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import fr.arnaudguyon.smartfontslib.FontTextView;
import ifisol.attendoplus.Model.Events;
import ifisol.attendoplus.Model.Non_attendo_events;
import ifisol.attendoplus.Model.Pictures;
import ifisol.attendoplus.R;
import ifisol.attendoplus.Utilities.Constants;
import ifisol.attendoplus.Utilities.Utility;
import ifisol.attendoplus.View.Fragments.AttendanceFragment;
import ifisol.attendoplus.View.Fragments.EventsDetailPagerFragment;

/**
 * Created by Mehtab Ahmad on 1/6/2017.
 */

public class CPD_AllEvents_Adapter extends BaseExpandableListAdapter {

    private Context context;
    FragmentActivity mActivity;
    private List<String> listDataHeader;
    private HashMap<String, ArrayList<?>> EventsData;
    LayoutInflater infalInflater;


    public CPD_AllEvents_Adapter(Context context, List<String> listDataHeader, HashMap<String, ArrayList<?>> listChildData, FragmentActivity activity) {
        this.context = context;
        this.listDataHeader = listDataHeader;
        this.EventsData = listChildData;
        infalInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mActivity = activity;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this.EventsData.get(this.listDataHeader.get(groupPosition))
                .get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {



    /*    if (groupPosition == 0)
        {
            // in tag id we have to give some pre compiled int id thats why i am using R.layout. ... as ID. OTHER CHECKS ARE USED TO GAIN MORE RECYCLING OF VIEWS
            if (convertView == null || !convertView.getTag(R.layout.events_list_item).equals("Attendo Events") ) {
                convertView = infalInflater.inflate(R.layout.events_list_item, null);
                convertView.setTag(R.layout.events_list_item,"Attendo Events");


            }
        }
        else if (groupPosition == 1 )
        {
            if (convertView == null || !convertView.getTag(R.layout.non_attendo_list_item).equals("Non Attendo Events") )
        {
            convertView = infalInflater.inflate(R.layout.non_attendo_list_item, null);
            convertView.setTag(R.layout.non_attendo_list_item, "Non Attendo Events");


        }
        }*/

       //   if (convertView==null)
      //    {
              if (groupPosition == 0)
              {
                  convertView = infalInflater.inflate(R.layout.events_list_item, null);
                  convertView.setTag(new ViewHolder(convertView));
              }
              else if (groupPosition == 1 )
              {
                  convertView = infalInflater.inflate(R.layout.non_attendo_list_item, null);
                  convertView.setTag(new ViewHolder_Non_Attendo(convertView));
              }
     //     }


       InitViews(convertView.getTag(), EventsData.get(listDataHeader.get(groupPosition)).get(childPosition));
        return convertView;

    }



    @Override
    public int getChildrenCount(int groupPosition) {
        return this.EventsData.get(this.listDataHeader.get(groupPosition))
                .size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this.listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.cpd_all_events_list_header_item, null);
        }

        FontTextView lblListHeader = (FontTextView) convertView.findViewById(R.id.tv_header);
        lblListHeader.setText(headerTitle);

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }



    private void InitViews(Object vh, Object obj) {

        if (vh instanceof ViewHolder)
        {
            Initialize((ViewHolder)vh, (Events)obj);
        }
        else if (vh instanceof ViewHolder_Non_Attendo)
        {
            Initialize((ViewHolder_Non_Attendo) vh, (Non_attendo_events) obj);
        }

        
        
    }


    private void Initialize(ViewHolder h, final Events obj)
    {
        h.tvEventName.setText(obj.getEvent_name());
        h.tvCpd.setText("Cpd Points : "+obj.getEvent_cpd());
        if (obj.getHas_exhibitor().equals("yes"))
            h.tvExhibitors.setText("Exhibitors : "+obj.getExhibitor_count());
        else
            h.tvExhibitors.setText("No Exhibitors");
        h.tvDateDay.setText(obj.getEvent_date().split("-")[2]);
        h.tvDateMonth.setText(Utility.GetMonth(obj.getEvent_date().split("-")[1]));
        h.tvLocation.setText(obj.getEvent_street_address()+" "+obj.getEvent_city()+" "+obj.getEvent_country());
        h.tvStrtEndTime.setText(obj.getEvent_start_time()+" - "+obj.getEvent_end_time());
        if (obj.getInterested_guests()>3)
        {
            h.viewInterested.setVisibility(View.VISIBLE);
            h.tvCount.setText(String.valueOf(obj.getInterested_guests())+"+");
        }
        else
            h.viewInterested.setVisibility(View.GONE);

        ImageLoader.getInstance().displayImage(Constants.Img_Base_URL+obj.getEvent_photo(),h.imgEvent);
        if (obj.getIs_public_event().equals("yes"))
            h.imgPublic.setVisibility(View.VISIBLE);
        else
            h.imgPublic.setVisibility(View.GONE);


        SetLogbookFeedback(h,obj);



        h.viewInterested.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Bundle bundle1 = new Bundle();
                bundle1.putInt("POSITION", 0);
                bundle1.putSerializable("EVENT", obj);
                bundle1.putString(Constants.EVENT_TYPE, Constants.TAG_CPD_ALL_EVENTS);
                Utility.ReplaceFragment(new AttendanceFragment(),mActivity.getSupportFragmentManager(),bundle1);
            }
        });


        h.tvExhibitors.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString(Constants.TAG_VIEW_TO_GO, Constants.TAG_EXHIBITOR);
                bundle.putString(Constants.EVENT_TYPE, Constants.TAG_CPD_ALL_EVENTS);
                bundle.putSerializable("EVENT", obj);
                Utility.ReplaceFragment(new EventsDetailPagerFragment(),mActivity.getSupportFragmentManager(),bundle);
            }
        });


    }

    private void SetLogbookFeedback(ViewHolder h, Events obj) {

        if (obj.getHas_logbook().equals("yes"))
        {
            if (obj.getHas_filled_logbook().equals("yes"))
                h.imgLogbook.setImageResource(R.drawable.ic_filled_log_book);
            else
                h.imgLogbook.setImageResource(R.drawable.ic_not_filled_logbook);

            h.tvLogbook.setTextColor(mActivity.getResources().getColor(R.color.black));
        }
        else
        {
            h.imgLogbook.setImageResource(R.drawable.ic_no_logbook);
            h.tvLogbook.setTextColor(mActivity.getResources().getColor(R.color.trans_black3));
        }

        if (obj.getHas_feedback().equals("yes"))
        {
            if (obj.getHas_filled_logbook().equals("yes"))
                h.imgFeedback.setImageResource(R.drawable.ic_filled_feed_back);
            else
                h.imgFeedback.setImageResource(R.drawable.ic_not_filled_feedback);

            h.tvFeedback.setTextColor(mActivity.getResources().getColor(R.color.black));
        }
        else
        {
            h.imgFeedback.setImageResource(R.drawable.ic_no_feedback);
            h.tvFeedback.setTextColor(mActivity.getResources().getColor(R.color.trans_black3));
        }

        /*if (obj.getUser_is_interested().equals("yes"))  // This will be visible in only upcoming events of guest
            h.imgInterested.setVisibility(View.VISIBLE);
        else
            h.imgInterested.setVisibility(View.GONE);*/
    }

    private void Initialize(ViewHolder_Non_Attendo h, Non_attendo_events obj)
    {
        h.tveventName.setText(obj.getNon_event_name());
        h.tvDateDay.setText(obj.getNon_event_date().split("-")[2]);
        h.tvDateMonth.setText(Utility.GetMonth(obj.getNon_event_date().split("-")[1]));
      //h.tvStrtEndTime.setText(obj.getNon_event_start_time()+" - "+obj.getNon_event_end_time());
        h.tvLocation.setText(obj.getNon_event_website());
        h.tvStrtEndTime.setText("CPD Points: "+obj.getNon_event_cpd());

        String img = "";
        for (Pictures pic : obj.getPictures())
        {
            if (pic.getStatus().equals("primary"))
                img = pic.getNon_event_picture();
        }

        ImageLoader.getInstance().displayImage(Constants.Img_Base_URL+img,h.imgEvent);

    }

    protected class ViewHolder_Non_Attendo {
        private RoundedImageView imgEvent;
        private FontTextView tveventName;
        private LinearLayout viewDate;
        private FontTextView tvDateDay;
        private FontTextView tvDateMonth;
        private FontTextView tvLocation;
        private FontTextView tvStrtEndTime;

        public ViewHolder_Non_Attendo(View view) {
            imgEvent = (RoundedImageView) view.findViewById(R.id.img_event);
            tveventName = (FontTextView) view.findViewById(R.id.tvevent_name);
            viewDate = (LinearLayout) view.findViewById(R.id.view_date);
            tvDateDay = (FontTextView) view.findViewById(R.id.tv_date_day);
            tvDateMonth = (FontTextView) view.findViewById(R.id.tv_date_month);
            tvLocation = (FontTextView) view.findViewById(R.id.tv_location);
            tvStrtEndTime = (FontTextView) view.findViewById(R.id.tv_strt_end_time);
        }
    }




    protected class ViewHolder{
        private CardView cardView;
        private FontTextView tvEventName;
        private FontTextView tvCpd;
        private FontTextView tvExhibitors;
        private LinearLayout viewDate;
        private FontTextView tvDateDay;
        private FontTextView tvDateMonth;
        private FontTextView tvLocation;
        private FontTextView tvStrtEndTime;
        private View         viewInterested;
        private CircularImageView img1;
        private CircularImageView img2;
        private CircularImageView img3;
        private FontTextView tvCount;
        private RoundedImageView imgEvent;
        private ImageView imgPublic;
        private ImageView imgLogbook;
        private FontTextView tvLogbook;
        private ImageView imgFeedback;
        private FontTextView tvFeedback;
        private ImageView imgInterested;

        public ViewHolder(View view) {
            cardView = (CardView) view.findViewById(R.id.card_view);
            tvEventName = (FontTextView) view.findViewById(R.id.tv_eventName);
            tvCpd = (FontTextView) view.findViewById(R.id.tv_cpd);
            tvExhibitors = (FontTextView) view.findViewById(R.id.tv_exhibitors);
            viewDate = (LinearLayout) view.findViewById(R.id.view_date);
            tvDateDay = (FontTextView) view.findViewById(R.id.tv_date_day);
            tvDateMonth = (FontTextView) view.findViewById(R.id.tv_date_month);
            tvLocation = (FontTextView) view.findViewById(R.id.tv_location);
            tvStrtEndTime = (FontTextView) view.findViewById(R.id.tv_strt_end_time);
            viewInterested = view.findViewById(R.id.view_interested);
            img1 = (CircularImageView) view.findViewById(R.id.img_1);
            img2 = (CircularImageView) view.findViewById(R.id.img_2);
            img3 = (CircularImageView) view.findViewById(R.id.img_3);
            tvCount = (FontTextView) view.findViewById(R.id.tv_count);
            imgEvent = (RoundedImageView) view.findViewById(R.id.img_event);
            imgPublic = (ImageView) view.findViewById(R.id.img_public);
            imgLogbook = (ImageView) view.findViewById(R.id.img_logbook);
            tvLogbook = (FontTextView) view.findViewById(R.id.tv_logbook);
            imgFeedback = (ImageView) view.findViewById(R.id.img_feedback);
            tvFeedback = (FontTextView) view.findViewById(R.id.tv_feedback);
            imgInterested = (ImageView) view.findViewById(R.id.img_interested);

            imgInterested.setVisibility(View.GONE);  // This will be visible in only upcoming events of guest
        }
    }
}
