package ifisol.attendoplus.Controller.Adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import java.util.ArrayList;

import ifisol.attendoplus.Model.Events;
import ifisol.attendoplus.R;
import ifisol.attendoplus.View.Fragments.ExistingUserFragment;
import ifisol.attendoplus.View.Fragments.NewUserAttendanceFragment;

/**
 * Created by Mehtab Ahmad on 7/28/2016.
 */
public class ManualAttendancePagerAdapter extends FragmentStatePagerAdapter {


    Context mContext;

    String[] tabs;
    int[] tabIcons;
    int[] tabIconsAct;
    ArrayList<Fragment> mFragList;
    Events event;

    public ManualAttendancePagerAdapter(FragmentManager fm, Context context, Events evt) {

        super(fm);
        mContext = context;
        this.event = evt;
        tabs = new String[]{"Existing Users" , "New User"};
        tabIcons = new int[]{R.drawable.ic_logbook_pending,R.drawable.ic_logbook_filled};
        tabIconsAct = new int[]{R.drawable.ic_logbook_pending_active,R.drawable.ic_logbook_filled_activie};
        mFragList = new ArrayList<>();
        mFragList.add(new ExistingUserFragment());
        mFragList.add(new NewUserAttendanceFragment());

    }

    @Override
    public Fragment getItem(int position) {

        Fragment fragment = mFragList.get(position);
        Bundle bundle = new Bundle();
        bundle.putSerializable("EVENT", event);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getCount() {

        return tabs.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        return tabs[position];
    }

    public View getTabView(int position) {

        // Given you have a custom layout in `res/layout/custom_tab.xml` with a TextView and ImageView
        View v = LayoutInflater.from(mContext).inflate(R.layout.custom_tab_layout, null);

        ImageView img = (ImageView) v.findViewById(R.id.img_tab);

        img.setImageResource(tabIcons[position]);

        return v;
    }

    public int GetActIcon(int pos)
    {
       return tabIconsAct[pos];
    }
    public int GetNormIcon(int pos)
    {
        return tabIcons[pos];
    }
}
