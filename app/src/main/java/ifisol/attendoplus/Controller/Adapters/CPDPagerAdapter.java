package ifisol.attendoplus.Controller.Adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import java.util.ArrayList;

import ifisol.attendoplus.R;
import ifisol.attendoplus.Utilities.Constants;
import ifisol.attendoplus.View.Fragments.CPDAttendoFragment;
import ifisol.attendoplus.View.Fragments.CpdAllEventsFragment;
import ifisol.attendoplus.View.Fragments.CpdNonAttEventsFragment;

/**
 * Created by Mehtab Ahmad on 7/28/2016.
 */
public class CPDPagerAdapter extends FragmentPagerAdapter {


    Context mContext;

    String[] tabs;
    int[] tabIcons;
    int[] tabActIcons;
    ArrayList<Fragment> mFragList;


    public CPDPagerAdapter(FragmentManager fm, Context context) {

        super(fm);
        mContext=context;
        mFragList = new ArrayList<>();

        mFragList.add(new CpdAllEventsFragment());
        mFragList.add(new CPDAttendoFragment());
        mFragList.add(new CpdNonAttEventsFragment());

            tabs      = new String[]{Constants.TAG_CPD_ALL_EVENTS,Constants.TAG_CPD_ATTENDO,Constants.TAG_CPD_NON_ATTENDO};
            tabIcons =new int[]{R.drawable.ic_past,R.drawable.ic_upcoming,R.drawable.ic_awaiting};
            tabActIcons =new int[]{R.drawable.ic_past_active,R.drawable.ic_upcoming_active,R.drawable.ic_awaiting_active};

    }

    @Override
    public Fragment getItem(int position) {

        Fragment fragment = mFragList.get(position);
        Bundle bundle = new Bundle();
        bundle.putString(Constants.EVENT_TYPE,tabs[position]);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getCount() {

        return tabs.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        return tabs[position];
    }

    public View getTabView(int position) {

        // Given you have a custom layout in `res/layout/custom_tab.xml` with a TextView and ImageView
        View v = LayoutInflater.from(mContext).inflate(R.layout.custom_tab_layout, null);

        ImageView img = (ImageView) v.findViewById(R.id.img_tab);

        img.setImageResource(tabIcons[position]);

        return v;
    }

    public int GetActIcon(int pos)
    {
        return tabActIcons[pos];
    }
    public int GetNormalIcon(int pos)
    {
        return tabIcons[pos];
    }

}
