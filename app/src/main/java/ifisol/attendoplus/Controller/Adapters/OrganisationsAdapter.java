package ifisol.attendoplus.Controller.Adapters;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

import fr.arnaudguyon.smartfontslib.FontTextView;
import ifisol.attendoplus.Model.Organisations;
import ifisol.attendoplus.R;
import ifisol.attendoplus.Utilities.Constants;
import ifisol.attendoplus.Utilities.Utility;
import ifisol.attendoplus.View.Activities.ActivitySignUp;
import ifisol.attendoplus.View.Fragments.EditProfileFragment;

/**
 * Created by Mehtab Ahmad on 12/15/2016.
 */
public class OrganisationsAdapter extends RecyclerView.Adapter<OrganisationsAdapter.ViewHolder> {

    Activity mActivity;

    public ArrayList<Organisations> mFilterArray;
    public ArrayList<Organisations> mOriginalArray;
    OnItemClick clickListener;
    String orgType;
    Fragment fragment;

    public OrganisationsAdapter(Activity activity, Fragment frgment, ArrayList<Organisations> list, String type)
    {

        mActivity = activity;
        mFilterArray = list;
        mOriginalArray = list;
        this.orgType = type;
        this.fragment = frgment;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v     = LayoutInflater.from(mActivity).inflate(R.layout.organisations_list_item, null);
        ViewHolder vh =new ViewHolder(v);

        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder h, int position) {

        Organisations org = mFilterArray.get(position);
        h.tvOrgName.setText(org.getOrganisation_name());
        h.tvAddress.setText(org.getOrganisation_address());
        h.tvOrgWeb.setText(org.getOrganisation_website());

        ImageLoader.getInstance().displayImage(Constants.Img_Base_URL+org.getOrganisation_image(), h.imgOrg);

        if (mActivity instanceof ActivitySignUp && orgType.equals("secondary"))
        {
            h.tvSelect.setVisibility(View.VISIBLE);

            if (((ActivitySignUp)mActivity).OrganisationId.equals(mFilterArray.get(position).getOrganisation_id()))
                  h.tvSelect.setSelected(true);
            else
            {
                if (mFilterArray.get(position).isSelected())
                    h.tvSelect.setSelected(true);
                else
                    h.tvSelect.setSelected(false);
            }

        }
        else if (fragment!=null && fragment instanceof EditProfileFragment)
        {
            h.tvSelect.setVisibility(View.VISIBLE);

            if (((EditProfileFragment)fragment).OrganisationId.equals(mFilterArray.get(position).getOrganisation_id()))
                h.tvSelect.setSelected(true);
            else
            {
                if (mFilterArray.get(position).isSelected())
                    h.tvSelect.setSelected(true);
                else
                    h.tvSelect.setSelected(false);
            }

        }





    }

    @Override
    public int getItemCount() {

        return mFilterArray.size();
    }

    protected class ViewHolder extends RecyclerView.ViewHolder{
        private CircularImageView imgOrg;
        private FontTextView tvOrgName;
        private FontTextView tvAddress;
        private FontTextView tvOrgWeb;
        private TextView tvSelect;

        public ViewHolder(View view) {

            super(view);
            imgOrg    = (CircularImageView) view.findViewById(R.id.img_org);
            tvOrgName = (FontTextView) view.findViewById(R.id.tv_org_name);
            tvAddress = (FontTextView) view.findViewById(R.id.tv_address);
            tvOrgWeb = (FontTextView) view.findViewById(R.id.tv_org_web);
            tvSelect =  (TextView) view.findViewById(R.id.tv_select);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (mActivity instanceof ActivitySignUp && orgType.equals("secondary"))
                    {
                        if (((ActivitySignUp)mActivity).OrganisationId.equals(mFilterArray.get(getPosition()).getOrganisation_id()))
                             Utility.ShowAlert(mActivity,"Primary Organisations", "This is your primary organisation you can not select it as your secondary organisation. Thanks");
                            else
                        {
                            if (mFilterArray.get(getPosition()).isSelected())
                                mFilterArray.get(getPosition()).setSelected(false);
                            else
                                mFilterArray.get(getPosition()).setSelected(true);

                            notifyDataSetChanged();
                        }

                    }
                    else if (fragment!=null && fragment instanceof EditProfileFragment)
                    {
                        if (((EditProfileFragment)fragment).OrganisationId.equals(mFilterArray.get(getPosition()).getOrganisation_id()))
                            Utility.ShowAlert(mActivity,"Primary Organisations", "This is your primary organisation you can not select it as your secondary organisation. Thanks");
                        else
                        {
                            if (mFilterArray.get(getPosition()).isSelected())
                                mFilterArray.get(getPosition()).setSelected(false);
                            else
                                mFilterArray.get(getPosition()).setSelected(true);

                            notifyDataSetChanged();
                        }
                    }
                    else
                    {
                        if (clickListener!=null)
                            clickListener.onItemClicked(mFilterArray.get(getPosition()));
                    }

                }
            });
        }
    }

    public void setOnClickListener(OnItemClick listener)
    {
        this.clickListener = listener;
    }

    public interface OnItemClick{
        void onItemClicked(Organisations obj);
    }


    public void FilterList(String filterTxt)
    {
        if (filterTxt.isEmpty())
            mFilterArray = mOriginalArray;
        else
        {
            mFilterArray = new ArrayList<>();

            for (Organisations organisation : mOriginalArray)
            {
                if (organisation.getOrganisation_name().toLowerCase().contains(filterTxt.toLowerCase()))
                    mFilterArray.add(organisation);
            }

            if (mFilterArray.size()==0)
                mFilterArray = mOriginalArray;
        }

        notifyDataSetChanged();
    }
}
