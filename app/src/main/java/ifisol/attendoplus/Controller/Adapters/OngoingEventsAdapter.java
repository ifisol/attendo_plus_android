package ifisol.attendoplus.Controller.Adapters;

import android.content.Context;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.github.siyamed.shapeimageview.RoundedImageView;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import fr.arnaudguyon.smartfontslib.FontTextView;
import ifisol.attendoplus.Model.Events;
import ifisol.attendoplus.R;
import ifisol.attendoplus.Utilities.Constants;
import ifisol.attendoplus.Utilities.Utility;
import ifisol.attendoplus.View.Fragments.AttendanceFragment;
import ifisol.attendoplus.View.Fragments.EventsDetailPagerFragment;

public class OngoingEventsAdapter extends BaseAdapter {


    private Context context;
    private LayoutInflater layoutInflater;
    private FragmentActivity      mActivity;
    private ArrayList<CountTimer> mTimerList;
    public ArrayList<Events> mFilterArray;
    ArrayList<Events> mOriginalArray;
    DateFormat dateFormat;
    String formater = "%02d";

    Date start_time;
    Date end_time;



    public OngoingEventsAdapter(Context context, FragmentActivity activity, ArrayList<Events> events) {

        this.context        = context;
        this.layoutInflater = LayoutInflater.from(context);
        mActivity           = activity;
        mTimerList          = new ArrayList<>();
        mFilterArray             = events;
        mOriginalArray             = events;
        dateFormat          = new SimpleDateFormat("hh:mm a", Locale.ENGLISH);
    }

    @Override
    public int getCount() {
        return mFilterArray.size();
    }

    @Override
    public Object getItem(int position) {
        return mFilterArray.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {

                convertView = layoutInflater.inflate(R.layout.ongoing_event_list_item, null);
                convertView.setTag(new ViewHolder(convertView));

        }

        InitEvents((ViewHolder)convertView.getTag(), position);
        return convertView;
    }


    private void InitEvents(ViewHolder h, final int pos)
    {
        final Events obj = mFilterArray.get(pos);

        h.tvEventName.setText(obj.getEvent_name());
        h.tvCpd.setText("Cpd Points : "+obj.getEvent_cpd());
        if (obj.getHas_exhibitor().equals("yes"))
            h.tvExhibitors.setText("Exhibitors : "+obj.getExhibitor_count());
        else
            h.tvExhibitors.setText("No Exhibitors");
        h.tvDateDay.setText(obj.getEvent_date().split("-")[2]);
        h.tvDateMonth.setText(Utility.GetMonth(obj.getEvent_date().split("-")[1]));
        h.tvLocation.setText(obj.getEvent_street_address()+" "+obj.getEvent_city()+" "+obj.getEvent_country());
        h.tvStrtEndTime.setText(obj.getEvent_start_time()+" - "+obj.getEvent_end_time());
        if (obj.getInterested_guests()>3)
        {
            h.viewInterested.setVisibility(View.VISIBLE);
            h.viewInterested.setTag(pos);
            h.tvInterestedCount.setText(String.valueOf(obj.getInterested_guests())+"+");
        }
        else
        {
            if (obj.getInterested_guests()==3)
            {
                h.imgIntr1.setVisibility(View.VISIBLE);
                h.imgIntr2.setVisibility(View.VISIBLE);
                h.imgIntr3.setVisibility(View.VISIBLE);
                h.tvInterestedCount.setText("3");
            }
            else if (obj.getInterested_guests()==2)
            {
                h.imgIntr1.setVisibility(View.GONE);
                h.imgIntr2.setVisibility(View.VISIBLE);
                h.imgIntr3.setVisibility(View.VISIBLE);
                h.tvInterestedCount.setText("2");
            }
            else if (obj.getInterested_guests()==1)
            {
                h.imgIntr1.setVisibility(View.GONE);
                h.imgIntr2.setVisibility(View.GONE);
                h.imgIntr3.setVisibility(View.VISIBLE);
                h.tvInterestedCount.setText("1");
            }
            else
            {
                h.imgIntr1.setVisibility(View.GONE);
                h.imgIntr2.setVisibility(View.GONE);
                h.imgIntr3.setVisibility(View.GONE);
                h.tvInterestedCount.setText("0");
            }

        }

        if (obj.getAttanded_count()>3)
        {
            h.viewAttended.setVisibility(View.VISIBLE);
            h.viewAttended.setTag(pos);
            h.tvAttendedCount.setText(String.valueOf(obj.getAttanded_count())+"+");
        }
        else
        {
            if (obj.getAttanded_count()==3)
            {
                h.imgAtt1.setVisibility(View.VISIBLE);
                h.imgAtt2.setVisibility(View.VISIBLE);
                h.imgAtt3.setVisibility(View.VISIBLE);
                h.tvAttendedCount.setText("3");
            }
            else if (obj.getAttanded_count()==2)
            {
                h.imgAtt1.setVisibility(View.VISIBLE);
                h.imgAtt2.setVisibility(View.VISIBLE);
                h.imgAtt3.setVisibility(View.GONE);
                h.tvAttendedCount.setText("2");
            }
            else if (obj.getAttanded_count()==1)
            {
                h.imgAtt1.setVisibility(View.VISIBLE);
                h.imgAtt2.setVisibility(View.GONE);
                h.imgAtt3.setVisibility(View.GONE);
                h.tvAttendedCount.setText("1");
            }
            else
            {
                h.imgAtt1.setVisibility(View.GONE);
                h.imgAtt2.setVisibility(View.GONE);
                h.imgAtt3.setVisibility(View.GONE);
                h.tvAttendedCount.setText("0");
            }

        }

        ImageLoader.getInstance().displayImage(Constants.Img_Base_URL+obj.getEvent_photo(),h.imgEvent);
        if (obj.getIs_public_event().equals("yes"))
            h.imgPublic.setVisibility(View.VISIBLE);
        else
            h.imgPublic.setVisibility(View.GONE);





            try
            {
                if (h.timerObj==null)
                {
                start_time  = dateFormat.parse(obj.getEvent_start_time());
                end_time    = dateFormat.parse(obj.getEvent_end_time());
                h.timerObj = new CountTimer((long)(end_time.getTime() - GetCurrentTime().getTime() ),1000,h, start_time, end_time);
                h.timerObj.start();
                mTimerList.add(h.timerObj);
                }
                else
                {
                    h.timerObj.start();
                    h.timerObj.onTick((long)(end_time.getTime() - GetCurrentTime().getTime() ));
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }




        h.viewInterested.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

             //   if (obj.getInterested_guests()>0)
              //  {
                    Bundle bundle1 = new Bundle();
                    bundle1.putInt("POSITION", 0);
                    bundle1.putSerializable("EVENT", mFilterArray.get(pos));
                    bundle1.putString(Constants.EVENT_TYPE, Constants.TAG_ONGOING_EVENTS);
                    Utility.ReplaceFragment(new AttendanceFragment(),mActivity.getSupportFragmentManager(),bundle1);
           //     }
            //    else
                //    Utility.ShowAlert(mActivity,"Interested Guests","No guest is Interested in this event yet");

            }
        });

        h.viewAttended.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            //    if (obj.getAttanded_count()>0)
            //    {
                    Bundle bundle1 = new Bundle();
                    bundle1.putInt("POSITION", 1);
                    bundle1.putSerializable("EVENT", mFilterArray.get(pos));
                    bundle1.putString(Constants.EVENT_TYPE, Constants.TAG_ONGOING_EVENTS);
                    Utility.ReplaceFragment(new AttendanceFragment(),mActivity.getSupportFragmentManager(),bundle1);
             //   }
             //   else
             //   Utility.ShowAlert(mActivity,"Attended Guests","No guest has Attend this event yet");
            }
        });


        h.tvExhibitors.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (obj.getHas_exhibitor().equals("yes"))
                {
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.TAG_VIEW_TO_GO, Constants.TAG_EXHIBITOR);
                    bundle.putString(Constants.EVENT_TYPE, Constants.TAG_ONGOING_EVENTS);
                    bundle.putSerializable("EVENT", mFilterArray.get(pos));
                    Utility.ReplaceFragment(new EventsDetailPagerFragment(),mActivity.getSupportFragmentManager(),bundle);
                }

                else
                  Utility.ShowAlert(mActivity,"Exhibitors","No Exhibtors participating in this event");
            }
        });


    }



    protected class ViewHolder {
        private FontTextView     tvEventName;
        private FontTextView     tvCpd;
        private FontTextView     tvExhibitors;
        private LinearLayout     viewDate;
        private FontTextView     tvDateDay;
        private FontTextView     tvDateMonth;
        private FontTextView     tvLocation;
        private FontTextView     tvStrtEndTime;
        private RoundedImageView imgEvent;
        private ImageView        imgPublic;
        private FontTextView     tvHour;
        private FontTextView     tvMint;
        private FontTextView     tvSeconds;
        private ImageView        imgIntr1;
        private ImageView        imgIntr2;
        private ImageView        imgIntr3;
        private ImageView        imgAtt1;
        private ImageView        imgAtt2;
        private ImageView        imgAtt3;
        private ProgressBar      pbEvent;
        private View             viewInterested;
        private FontTextView    tvInterested;
        private FontTextView    tvInterestedCount;
        private View            viewAttended;
        private FontTextView    tvAttended;
        private FontTextView    tvAttendedCount;
        private CountTimer      timerObj;

        public ViewHolder(View view) {
            tvEventName         = (FontTextView) view.findViewById(R.id.tv_eventName);
            tvCpd               = (FontTextView) view.findViewById(R.id.tv_cpd);
            tvExhibitors        = (FontTextView) view.findViewById(R.id.tv_exhibitors);
            viewDate            = (LinearLayout) view.findViewById(R.id.view_date);
            tvDateDay           = (FontTextView) view.findViewById(R.id.tv_date_day);
            tvDateMonth         = (FontTextView) view.findViewById(R.id.tv_date_month);
            tvLocation          = (FontTextView) view.findViewById(R.id.tv_location);
            tvStrtEndTime       = (FontTextView) view.findViewById(R.id.tv_strt_end_time);
            imgEvent            = (RoundedImageView) view.findViewById(R.id.img_event);
            imgPublic           = (ImageView) view.findViewById(R.id.img_public);
            tvHour              = (FontTextView) view.findViewById(R.id.tv_hour);
            tvMint              = (FontTextView) view.findViewById(R.id.tv_mint);
            tvSeconds           = (FontTextView) view.findViewById(R.id.tv_seconds);
            imgIntr1            = (ImageView) view.findViewById(R.id.img_intr1);
            imgIntr2            = (ImageView) view.findViewById(R.id.img_intr2);
            imgIntr3            = (ImageView) view.findViewById(R.id.img_intr3);
            imgAtt1             = (ImageView) view.findViewById(R.id.img_att1);
            imgAtt2             = (ImageView) view.findViewById(R.id.img_att2);
            imgAtt3             = (ImageView) view.findViewById(R.id.img_att3);
            pbEvent             = (ProgressBar) view.findViewById(R.id.pb_event);
            viewInterested      = view.findViewById(R.id.view_interested);
            tvInterested        = (FontTextView) view.findViewById(R.id.tv_interested);
            tvInterestedCount   = (FontTextView) view.findViewById(R.id.tv_interested_count);
            viewAttended        = view.findViewById(R.id.view_attended);
            tvAttended          = (FontTextView) view.findViewById(R.id.tv_attended);
            tvAttendedCount     = (FontTextView) view.findViewById(R.id.tv_attended_count);


        }

    }

    private class CountTimer extends CountDownTimer
    {

        ViewHolder h;
        Date start_time;
        Date end_Time  ;

        public CountTimer(long millisInFuture, long countDownInterval, ViewHolder h, Date startTime, Date endTime) {

            super(millisInFuture, countDownInterval);
            this.h = h;
            this.start_time = startTime;
            this.end_Time = endTime;

        }



        @Override
        public void onTick(long millisUntilFinished) {

            int seconds = (int) (millisUntilFinished / 1000) % 60 ;
            int minutes = (int) ((millisUntilFinished / (1000*60)) % 60);
            int hours   = (int) ((millisUntilFinished / (1000*60*60)) % 24);

            h.tvHour.setText(String.format(formater , hours));
            h.tvMint.setText(String.format   (formater,minutes));
            h.tvSeconds.setText(String.format(formater,seconds));

            try
            {
                int progress = (int) ((millisUntilFinished * 100) / (end_Time.getTime() - start_time.getTime()));
                h.pbEvent.setProgress(progress);
            }
            catch (Exception e)
            {

            }
        }


        @Override
        public void onFinish() {

            h.timerObj.cancel();
            mTimerList.remove(h.timerObj);
            h.tvSeconds.setText(String.format(formater,0));

        }
    }

    public void CancelTimers()
    {
        if (mTimerList.size()>0)
        {
            for (CountDownTimer obj:mTimerList)
            {
                obj.cancel();
            }
        }

        mTimerList.clear();
    }

    private Date GetCurrentTime() throws ParseException {

        String current    = dateFormat.format(new Date());
        Date   currentTime = dateFormat.parse(current);

        return currentTime;
    }




    public void FilterList(String filterTxt)
    {
        if (filterTxt.isEmpty())
            mFilterArray = mOriginalArray;
        else
        {
            mFilterArray = new ArrayList<>();

            for (Events obj : mOriginalArray)
            {
                if (obj.getEvent_name().toLowerCase().contains(filterTxt.toLowerCase()))
                    mFilterArray.add(obj);
            }

            if (mFilterArray.size()==0)
                mFilterArray = mOriginalArray;
        }

        notifyDataSetChanged();
    }

    
    
}
