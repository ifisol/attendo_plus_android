package ifisol.attendoplus.Controller.Adapters;

import java.io.IOException;
import java.util.ArrayList;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.CardView;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.github.siyamed.shapeimageview.RoundedImageView;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONObject;

import fr.arnaudguyon.smartfontslib.FontButton;
import fr.arnaudguyon.smartfontslib.FontTextView;
import ifisol.attendoplus.Global.Globals;
import ifisol.attendoplus.Model.Events;
import ifisol.attendoplus.R;
import ifisol.attendoplus.Utilities.Constants;
import ifisol.attendoplus.Utilities.HttpHelper;
import ifisol.attendoplus.Utilities.Utility;
import ifisol.attendoplus.View.Fragments.AttendanceFragment;
import ifisol.attendoplus.View.Fragments.EventsDetailPagerFragment;
import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.RequestBody;

public class EventsAdapter extends BaseAdapter implements HttpHelper.HttpCallback {

  //  private List<T> objects = new ArrayList<T>();

    private LayoutInflater layoutInflater;
    FragmentActivity mActivity;
    String mEventType;
    public ArrayList<Events> mFilterArray;
    public ArrayList<Events> mOriginalArray;
    Globals mGlobal;
    int position;

    public EventsAdapter(Context context, FragmentActivity activity, String type, ArrayList<Events> events) {

        this.layoutInflater = LayoutInflater.from(context);
        mActivity           = activity;
        mEventType = type;
        this.mFilterArray = events;
        this.mOriginalArray = events;
        mGlobal = (Globals) mActivity.getApplicationContext();
    }

    @Override
    public int getCount() {
        return mFilterArray.size();
    }

    @Override
    public Object getItem(int position) {
        return mFilterArray.get(position);
    }


    public Events getItm(int position) {
        return mFilterArray.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {

                convertView = layoutInflater.inflate(R.layout.events_list_item, null);
                convertView.setTag(new ViewHolder(convertView));

        }
        initializeViews(convertView.getTag(), position);
        return convertView;
    }

    private void initializeViews(Object vh, int pos) {

            ViewHolder h = (ViewHolder) vh;
            InitEvents(pos, h);
        }


    ///////////////////// Events Views /////////////////////////////////

    private void InitEvents(final int pos, ViewHolder h)
    {
        Events obj = mFilterArray.get(pos);

        // Common properties for all events //
        h.tvEventName.setText(obj.getEvent_name());
        h.tvCpd.setText("CPD Points : "+obj.getEvent_cpd());
        if (obj.getHas_exhibitor().equals("yes"))
            h.tvExhibitors.setText("Exhibitors : "+obj.getExhibitor_count());
        else
            h.tvExhibitors.setText("No Exhibitors");
        h.tvDateDay.setText(obj.getEvent_date().split("-")[2]);
        h.tvDateMonth.setText(Utility.GetMonth(obj.getEvent_date().split("-")[1]));
        h.tvLocation.setText(obj.getEvent_street_address()+", "+obj.getEvent_city()+" "+obj.getEvent_country());
        h.tvStrtEndTime.setText(obj.getEvent_start_time()+" - "+obj.getEvent_end_time());
        if (obj.getInterested_guests()>3)
        {
            h.viewInterested.setVisibility(View.VISIBLE);
            h.viewInterested.setTag(pos);
            h.tvCount.setText(String.valueOf(obj.getInterested_guests())+"+");
        }
        else
            h.viewInterested.setVisibility(View.GONE);


        ImageLoader.getInstance().displayImage(Constants.Img_Base_URL+obj.getEvent_photo(),h.imgEvent);
        if (obj.getIs_public_event().equals("yes"))
            h.imgPublic.setVisibility(View.VISIBLE);
        else
            h.imgPublic.setVisibility(View.GONE);



           if (mEventType.equals(Constants.TAG_ATTENDED))
              SetAttendedLbFbIcons(h,obj);
           else
             SetLbFbIcons(h,obj);

        if (mGlobal.mActiveUser.getUser_type().equals(Constants.TAG_GUEST) && mEventType.equals(Constants.TAG_UPCOMING))
        {
            h.viewImgInterest.setVisibility(View.VISIBLE);

            if (obj.getUser_is_interested().equals("yes"))
                h.imgInterested.setImageResource(R.drawable.ic_heart_filled);
            else
                h.imgInterested.setImageResource(R.drawable.ic_heart);
        }
        else
           h.viewImgInterest.setVisibility(View.GONE);





        h.viewInterested.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    Bundle bundle1 = new Bundle();
                    bundle1.putInt("POSITION", 0);
                    bundle1.putSerializable("EVENT", mFilterArray.get(pos));
                    bundle1.putString(Constants.EVENT_TYPE, mEventType);
                    Utility.ReplaceFragment(new AttendanceFragment(), mActivity.getSupportFragmentManager(), bundle1);
            }
        });


        h.viewImgInterest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                position = pos;
                if (mFilterArray.get(pos).getUser_is_interested().equals("yes"))
                    InterestWarning(true);
                else
                    InterestWarning(false);
            }
        });


        h.tvExhibitors.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mFilterArray.get(pos).getHas_exhibitor().equals("yes"))
                {
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.TAG_VIEW_TO_GO, Constants.TAG_EXHIBITOR);
                    bundle.putString(Constants.EVENT_TYPE, mEventType);
                    bundle.putSerializable("EVENT", mFilterArray.get(pos));
                    Utility.ReplaceFragment(new EventsDetailPagerFragment(),mActivity.getSupportFragmentManager(),bundle);
                }
                else
                Utility.ShowAlert(mActivity,"Exhibitors","No Exhibtors participating in this event");
            }
        });


        /*h.tvCpd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utility.ReplaceFragment(new Cpd_MainFragment(),mActivity.getSupportFragmentManager());
            }
        });*/

    }



    private void SetLbFbIcons(ViewHolder h , Events obj) {
        if (obj.getHas_logbook().equals("yes"))
        {
            h.imgLogbook.setImageResource(R.drawable.ic_has_logbook);
            h.tvLogbook.setTextColor(mActivity.getResources().getColor(R.color.black));
        }
        else
        {
            h.imgLogbook.setImageResource(R.drawable.ic_no_logbook);
            h.tvLogbook.setTextColor(mActivity.getResources().getColor(R.color.trans_black3));
        }

        if (obj.getHas_feedback().equals("yes"))
        {
            h.imgFeedback.setImageResource(R.drawable.ic_has_feedback);
            h.tvFeedback.setTextColor(mActivity.getResources().getColor(R.color.black));
        }
        else
        {
            h.imgFeedback.setImageResource(R.drawable.ic_no_feedback);
            h.tvFeedback.setTextColor(mActivity.getResources().getColor(R.color.trans_black3));
        }

    }




    private void SetAttendedLbFbIcons(ViewHolder h, Events obj) {

        if (obj.getHas_logbook().equals("yes"))
        {
            if (obj.getHas_filled_logbook().equals("yes"))
                h.imgLogbook.setImageResource(R.drawable.ic_filled_log_book);
            else
                h.imgLogbook.setImageResource(R.drawable.ic_not_filled_logbook);

            h.tvLogbook.setTextColor(mActivity.getResources().getColor(R.color.black));
        }
        else
        {
            h.imgLogbook.setImageResource(R.drawable.ic_no_logbook);
            h.tvLogbook.setTextColor(mActivity.getResources().getColor(R.color.trans_black3));
        }

        if (obj.getHas_feedback().equals("yes"))
        {
            if (obj.getHas_filled_logbook().equals("yes"))
                h.imgFeedback.setImageResource(R.drawable.ic_filled_feed_back);
            else
                h.imgFeedback.setImageResource(R.drawable.ic_not_filled_feedback);

            h.tvFeedback.setTextColor(mActivity.getResources().getColor(R.color.black));
        }
        else
        {
            h.imgFeedback.setImageResource(R.drawable.ic_no_feedback);
            h.tvFeedback.setTextColor(mActivity.getResources().getColor(R.color.trans_black3));
        }

    }



    protected class ViewHolder{
        private CardView cardView;
        private FontTextView tvEventName;
        private FontTextView tvCpd;
        private FontTextView tvExhibitors;
        private LinearLayout viewDate;
        private FontTextView tvDateDay;
        private FontTextView tvDateMonth;
        private FontTextView tvLocation;
        private FontTextView tvStrtEndTime;
        private View         viewInterested;
        private CircularImageView img1;
        private CircularImageView img2;
        private CircularImageView img3;
        private FontTextView tvCount;
        private RoundedImageView imgEvent;
        private ImageView imgPublic;
        private ImageView imgLogbook;
        private FontTextView tvLogbook;
        private ImageView imgFeedback;
        private FontTextView tvFeedback;
        private ImageView imgInterested;
        private View viewImgInterest;

        public ViewHolder(View view) {
            cardView = (CardView) view.findViewById(R.id.card_view);
            tvEventName = (FontTextView) view.findViewById(R.id.tv_eventName);
            tvCpd = (FontTextView) view.findViewById(R.id.tv_cpd);
            tvExhibitors = (FontTextView) view.findViewById(R.id.tv_exhibitors);
            viewDate = (LinearLayout) view.findViewById(R.id.view_date);
            tvDateDay = (FontTextView) view.findViewById(R.id.tv_date_day);
            tvDateMonth = (FontTextView) view.findViewById(R.id.tv_date_month);
            tvLocation = (FontTextView) view.findViewById(R.id.tv_location);
            tvStrtEndTime = (FontTextView) view.findViewById(R.id.tv_strt_end_time);
            viewInterested = view.findViewById(R.id.view_interested);
            img1 = (CircularImageView) view.findViewById(R.id.img_1);
            img2 = (CircularImageView) view.findViewById(R.id.img_2);
            img3 = (CircularImageView) view.findViewById(R.id.img_3);
            tvCount = (FontTextView) view.findViewById(R.id.tv_count);
            imgEvent = (RoundedImageView) view.findViewById(R.id.img_event);
            imgPublic = (ImageView) view.findViewById(R.id.img_public);
            imgLogbook = (ImageView) view.findViewById(R.id.img_logbook);
            tvLogbook = (FontTextView) view.findViewById(R.id.tv_logbook);
            imgFeedback = (ImageView) view.findViewById(R.id.img_feedback);
            tvFeedback = (FontTextView) view.findViewById(R.id.tv_feedback);
            imgInterested = (ImageView) view.findViewById(R.id.img_interested);
            viewImgInterest = view.findViewById(R.id.view_img_interest);
        }
    }


    public void FilterList(String filterTxt)
    {
        if (filterTxt.isEmpty())
            mFilterArray = mOriginalArray;
        else
        {
            mFilterArray = new ArrayList<>();

            for (Events obj : mOriginalArray)
            {
                if (obj.getEvent_name().toLowerCase().contains(filterTxt.toLowerCase()))
                    mFilterArray.add(obj);
            }

            if (mFilterArray.size()==0)
                mFilterArray = mOriginalArray;
        }

        notifyDataSetChanged();
    }


    public void InterestWarning(final boolean interested) {
        final Dialog dialog = new Dialog(mActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.edit_event_warning);
        DisplayMetrics dm = new DisplayMetrics();
        mActivity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER | Gravity.START);
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        FontTextView tvHeading = (FontTextView) dialog.findViewById(R.id.tv_mainheading);
        FontTextView tvmsg = (FontTextView) dialog.findViewById(R.id.tv_msg1);

        tvHeading.setText(interested?"Not Interested ?":"Interested ?");
        tvmsg.setText(interested?"Are you sure, you are not interested in this event any more?":"Are you sure, you are interested in this event ?");
        FontButton btnOK = (FontButton) dialog.findViewById(R.id.btn_ok);
        btnOK.setText("Yes");
        FontButton btnCancel = (FontButton) dialog.findViewById(R.id.btn_cancel);
        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Interested(mFilterArray.get(position));
                dialog.dismiss();
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dialog.dismiss();
            }
        });
        dialog.show();
    }



    private void Interested(Events mEvent) {
        RequestBody body = new FormBody.Builder()
                .add("user_id", mGlobal.mActiveUser.getUser_id())
                .add("event_id", mEvent.getEvent_id())
                .add("status",mEvent.getUser_is_interested().equals("yes")?"no":"yes").build();
        try {
            HttpHelper.CallApi(mActivity,Constants.Im_interested,body,this,true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onFailure(Call call, IOException e) {
          mActivity.runOnUiThread(new Runnable() {
              @Override
              public void run() {

                  Toast.makeText(mActivity, "Request Timeout", Toast.LENGTH_SHORT).show();
              }
          });
    }

    @Override
    public void onSuccess(Call call, final String response) {

        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                try {
                    JSONObject json = new JSONObject(response);
                    if (json.getString("status").equals("true"))
                    {
                        if(mFilterArray.get(position).getUser_is_interested().equals("no"))
                           mFilterArray.get(position).setUser_is_interested("yes");
                        else
                            mFilterArray.get(position).setUser_is_interested("no");

                        notifyDataSetChanged();
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                    Toast.makeText(mActivity, "Erro ! please try again later", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }


}
