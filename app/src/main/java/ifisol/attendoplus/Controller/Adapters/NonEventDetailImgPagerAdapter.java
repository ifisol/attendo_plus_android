package ifisol.attendoplus.Controller.Adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;

import ifisol.attendoplus.Model.Pictures;
import ifisol.attendoplus.View.Fragments.NonEventImgVPChildFragment;

/**
 * Created by Mehtab Ahmad on 7/28/2016.
 */
public class NonEventDetailImgPagerAdapter extends FragmentStatePagerAdapter {


    Context mContext;

    ArrayList<Pictures> Images;


    public NonEventDetailImgPagerAdapter(FragmentManager fm, Context context, ArrayList<Pictures> images) {

        super(fm);
        mContext=context;
        this.Images = images ;

    }

    @Override
    public Fragment getItem(int position) {

        Fragment fragment = new NonEventImgVPChildFragment();
        Bundle bundle = new Bundle();
        bundle.putString( "IMAGE", Images.get(position).getNon_event_picture());
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getCount() {

        return Images.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {

        return "";
    }


}
