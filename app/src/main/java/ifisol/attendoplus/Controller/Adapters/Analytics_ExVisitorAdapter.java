package ifisol.attendoplus.Controller.Adapters;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.github.siyamed.shapeimageview.RoundedImageView;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.Objects;

import fr.arnaudguyon.smartfontslib.FontTextView;
import ifisol.attendoplus.Model.User;
import ifisol.attendoplus.R;
import ifisol.attendoplus.Utilities.Constants;

public class Analytics_ExVisitorAdapter extends BaseAdapter {

  //  private List<T> objects = new ArrayList<T>();

    private Context context;
    private LayoutInflater layoutInflater;
    FragmentActivity activity;
    ArrayList<User> mVisitorList;

    public Analytics_ExVisitorAdapter(Context context, FragmentActivity act, ArrayList<User> list) {
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
        this.activity=act;
        this.mVisitorList = list;
    }

    @Override
    public int getCount() {
        return mVisitorList.size();
    }

    @Override
    public Object getItem(int position) {
        return mVisitorList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.visitor_list_item, null);
            convertView.setTag(new ViewHolder(convertView));
        }
        initializeViews((ViewHolder) convertView.getTag(), mVisitorList.get(position));

        return convertView;
    }

    private void initializeViews(ViewHolder h, User obj) {

        ImageLoader.getInstance().displayImage(Constants.Img_Base_URL+obj.getPicture(),h.imgUser);
        h.tvUsername.setText(obj.getUser_name());
        h.tvEmail.setText(obj.getEmail());
        h.tvProfession.setText(obj.getProfession());
    }

    protected class ViewHolder {
        private CircularImageView imgUser;
        private FontTextView tvUsername;
        private FontTextView tvEmail;
        private FontTextView tvProfession;

        public ViewHolder(View view) {
            imgUser = (CircularImageView) view.findViewById(R.id.img_user);
            tvUsername = (FontTextView) view.findViewById(R.id.tv_username);
            tvEmail = (FontTextView) view.findViewById(R.id.tv_email);
            tvProfession= (FontTextView) view.findViewById(R.id.tv_profession);


        }
    }
}
