package ifisol.attendoplus.Controller.Adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import ifisol.attendoplus.Model.Events;
import ifisol.attendoplus.R;
import ifisol.attendoplus.Utilities.Constants;
import ifisol.attendoplus.View.Fragments.GuestsFragment;

/**
 * Created by Mehtab Ahmad on 7/28/2016.
 */
public class AttendancePagerAdapter extends FragmentStatePagerAdapter {


    Context mContext;

    String[] tabs;
    int[]  tabIcons;
    int[]  tabActIcons;
    Events event;
    String mEventType;

    public AttendancePagerAdapter(FragmentManager fm, Context context, Events evt, String eventType) {

        super(fm);
        mContext=context;
        event = evt;
        mEventType = eventType;

        tabs =new String[]{Constants.TAG_INTERESTED_GUEST,Constants.TAG_ATTENDED_GUEST};
        tabIcons =new int[]{R.drawable.ic_upcoming,R.drawable.ic_past};
        tabActIcons =new int[]{R.drawable.ic_upcoming_active,R.drawable.ic_past_active};


    }

    @Override
    public Fragment getItem(int position) {

        Fragment fragment = new GuestsFragment();
        Bundle bundle = new Bundle();
        bundle.putString(Constants.USER_TYPE,tabs[position]);
        bundle.putSerializable("EVENT", event);
        bundle.putSerializable(Constants.EVENT_TYPE, mEventType);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getCount() {

        return tabs.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        return tabs[position];
    }


    public View getTabView(int position) {

        // Given you have a custom layout in `res/layout/custom_tab.xml` with a TextView and ImageView
        View v = LayoutInflater.from(mContext).inflate(R.layout.custom_tab_layout, null);

        ImageView img = (ImageView) v.findViewById(R.id.img_tab);

        img.setImageResource(tabIcons[position]);

        return v;
    }

    public int GetActIcon(int pos)
    {
        return tabActIcons[pos];
    }
    public int GetNormalIcon(int pos)
    {
        return tabIcons[pos];
    }
}
