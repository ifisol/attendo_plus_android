package ifisol.attendoplus.Controller.Adapters;

import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import fr.arnaudguyon.smartfontslib.FontTextView;
import ifisol.attendoplus.Model.Exhibitors;
import ifisol.attendoplus.R;

/**
 * Created by Mehtab Ahmad on 12/15/2016.
 */
public class QrListAdapter extends BaseAdapter {

    FragmentActivity mActivity;
    public ArrayList<Exhibitors> mFilterArray;
    ArrayList<Exhibitors> mOriginalArray;

    viewClickListener clickListener;
    private LayoutInflater layoutInflater;
    public QrListAdapter(FragmentActivity activity, ArrayList<Exhibitors> list)
    {

        mActivity = activity;
        mFilterArray = list ;
        mOriginalArray = list ;
        this.layoutInflater = LayoutInflater.from(mActivity);

    }


    @Override
    public int getCount() {

        return mFilterArray.size();
    }

    @Override
    public Object getItem(int position) {

        return mFilterArray.get(position);
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {

            convertView = layoutInflater.inflate(R.layout.qr_list_item, null);
            convertView.setTag(new ViewHolder(convertView));

        }
        initializeViews(convertView.getTag(), position);
        return convertView;
    }


    protected class ViewHolder {
        private ImageView imgEmail;
        private TextView tvRequest;
        private ImageView imgQrStatus;
        private FontTextView tvExhibitorName;

        public ViewHolder(View view) {

            imgEmail = (ImageView) view.findViewById(R.id.img_email);
            tvRequest = (TextView) view.findViewById(R.id.tv_request);
            imgQrStatus = (ImageView) view.findViewById(R.id.img_qr_status);
            tvExhibitorName = (FontTextView) view.findViewById(R.id.tv_exhibitor_name);

        }


    }


        private void initializeViews(Object vh, final int pos) {

            ViewHolder h = (ViewHolder) vh;

            h.tvExhibitorName.setText(mFilterArray.get(pos).getExhibitor_name());
            if(mFilterArray.get(pos).getExhibitor_qr_exists().equalsIgnoreCase("yes"))
            {
                h.imgQrStatus.setImageResource(R.drawable.ic_circle_check);
                h.imgEmail.setVisibility(View.VISIBLE);
                h.tvRequest.setVisibility(View.GONE);
            }
            else
            {
                h.imgQrStatus.setImageResource(R.drawable.ic_circle_uncheck);
                h.imgEmail.setVisibility(View.GONE);
                h.tvRequest.setVisibility(View.VISIBLE);
            }


            h.tvRequest.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickListener.click(v,mFilterArray.get(pos));
                }
            });
            h.imgEmail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickListener.click(v,mFilterArray.get(pos));
                }
            });


        }


    public void setViewClickListener(viewClickListener listener)
    {
        this.clickListener=listener;
    }

    public interface viewClickListener
    {
        public void click(View v, Exhibitors obj);
    }
    public void FilterList(String filterTxt)
    {
        if (filterTxt.isEmpty())
            mFilterArray = mOriginalArray;
        else
        {
            mFilterArray = new ArrayList<>();

            for (Exhibitors obj : mOriginalArray)
            {
                if (obj.getExhibitor_name().toLowerCase().contains(filterTxt.toLowerCase()))
                    mFilterArray.add(obj);
            }

            if (mFilterArray.size()==0)
                mFilterArray = mOriginalArray;
        }

        notifyDataSetChanged();
    }
}

