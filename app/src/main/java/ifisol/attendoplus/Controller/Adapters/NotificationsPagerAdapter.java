package ifisol.attendoplus.Controller.Adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import java.util.ArrayList;

import ifisol.attendoplus.R;
import ifisol.attendoplus.Utilities.Constants;
import ifisol.attendoplus.View.Fragments.ExhibitorDetailFragment;
import ifisol.attendoplus.View.Fragments.ExhibitorParticipatedEventsFragment;
import ifisol.attendoplus.View.Fragments.NotificationsFragment;

/**
 * Created by Mehtab Ahmad on 7/28/2016.
 */
public class NotificationsPagerAdapter extends FragmentStatePagerAdapter {


    Context mContext;

    String[] tabs;
    int[]  tabIcons;
    int[]  tabActIcons;


    public NotificationsPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        mContext=context;
        tabs      = new String[]{"Received","Sent"};
        tabIcons  = new int[]{R.drawable.ic_notification_recieved,R.drawable.ic_notification_sent};
        tabActIcons =new int[]{R.drawable.ic_notification_recieved_active,R.drawable.ic_notification_sent_active};
    }



    @Override
    public Fragment getItem(int position) {
        Fragment fragment = new NotificationsFragment();
            Bundle bundle = new Bundle();
            bundle.putString("NOTIFICATION_TYPE", tabs[position]);
            fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public int getCount() {

        return tabs.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        return tabs[position];
    }

    public View getTabView(int position) {

        // Given you have a custom layout in `res/layout/custom_tab.xml` with a TextView and ImageView
        View v = LayoutInflater.from(mContext).inflate(R.layout.custom_tab_layout, null);

        ImageView img = (ImageView) v.findViewById(R.id.img_tab);

        img.setImageResource(tabIcons[position]);

        return v;
    }

    public int GetActIcon(int pos)
    {
        return tabActIcons[pos];
    }
    public int GetNormalIcon(int pos)
    {
        return tabIcons[pos];
    }

}
