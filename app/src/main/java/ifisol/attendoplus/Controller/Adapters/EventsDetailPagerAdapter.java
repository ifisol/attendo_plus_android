package ifisol.attendoplus.Controller.Adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import java.util.ArrayList;

import ifisol.attendoplus.Global.Globals;
import ifisol.attendoplus.Model.Events;
import ifisol.attendoplus.R;
import ifisol.attendoplus.Utilities.Constants;
import ifisol.attendoplus.View.Fragments.EventDetailFragment;
import ifisol.attendoplus.View.Fragments.ExhibitorsFragment;
import ifisol.attendoplus.View.Fragments.FeedbackwbvFragment;
import ifisol.attendoplus.View.Fragments.LogbookwbvFragment;

/**
 * Created by Mehtab Ahmad on 7/28/2016.
 */
public class
EventsDetailPagerAdapter extends FragmentStatePagerAdapter {


    Context mContext;

    ArrayList<Fragment> mFragList;

    int[] tabIcons;
    String[] tabs;

    String mEventType;
    String mUserType;
    Events mEvent;

    public EventsDetailPagerAdapter(FragmentManager fm, Context context, String type, Events event) {
        super(fm);

        mContext    = context;
        mEventType  = type ;
        mUserType   = ((Globals)mContext.getApplicationContext()).mActiveUser.getUser_type();
        this.mEvent = event;

        mFragList = new ArrayList<>();

        if (mUserType.equals(Constants.TAG_GUEST))
        {
            mFragList.add(new EventDetailFragment());
            mFragList.add(new ExhibitorsFragment());
            tabs =new String[]{"Event Detail","Participated Exhibitors"};
            tabIcons = new int[]{R.drawable.ic_event_details,R.drawable.ic_exhibitor_event_detail};
        }
        else
        {
            tabs =new String[]{"Event Detail","Participated Exhibitors","Event Logbook","Event Feedback"};
            tabIcons = new int[]{R.drawable.ic_event_details,R.drawable.ic_exhibitor_event_detail,R.drawable.ic_logbook,R.drawable.ic_feedback};
            mFragList.add(new EventDetailFragment());
            mFragList.add(new ExhibitorsFragment());
            mFragList.add(new LogbookwbvFragment());
            mFragList.add(new FeedbackwbvFragment());
        }

    }

    @Override
    public Fragment getItem(int position) {

        Fragment fragment = mFragList.get(position);
        Bundle bundle = new Bundle();
        bundle.putString(Constants.EVENT_TYPE, mEventType);
        bundle.putSerializable("EVENT", mEvent);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getCount() {

        return tabs.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        return tabs[position];
    }

    public View getTabView(int position) {

        // Given you have a custom layout in `res/layout/custom_tab.xml` with a TextView and ImageView
        View v = LayoutInflater.from(mContext).inflate(R.layout.custom_tab_layout, null);

        ImageView img = (ImageView) v.findViewById(R.id.img_tab);

        img.setImageResource(tabIcons[position]);

        return v;
    }
}
