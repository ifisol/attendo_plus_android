package ifisol.attendoplus.Controller.Adapters;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import fr.arnaudguyon.smartfontslib.FontTextView;
import ifisol.attendoplus.Model.Event_Types;
import ifisol.attendoplus.R;

/**
 * Created by Mehtab Ahmad on 12/15/2016.
 */
public class EventsTypeAdapter extends RecyclerView.Adapter<EventsTypeAdapter.ViewHolder> {

    Context mContext;
    FragmentActivity mActivity;
    public ArrayList<Event_Types> mFilterArray;
    public ArrayList<Event_Types> mOriginalArray;
    OnItemClicked clickListener;

    public EventsTypeAdapter(Context context, FragmentActivity activity, ArrayList<Event_Types> types)
    {

        mContext  = context;
        mActivity = activity;
        this.mFilterArray = types;
        this.mOriginalArray = types;


    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v     = LayoutInflater.from(mContext).inflate(R.layout.event_type_list_item, null);
        ViewHolder vh =new ViewHolder(v);

        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder h, int position) {

        h.tvTitle.setText(mFilterArray.get(position).getEvent_type_name());

        if (mFilterArray.get(position).isSelected())
            h.tvSelect.setSelected(true);
        else
            h.tvSelect.setSelected(false);
    }

    @Override
    public int getItemCount() {

        return mFilterArray.size();
    }

    public void setOnItemClick(OnItemClicked listener)
    {
        this.clickListener = listener;
    }


    public interface OnItemClicked
    {
        public void onClick(int pos);
    }



    protected class ViewHolder extends RecyclerView.ViewHolder{

        private TextView tvSelect;
        private FontTextView tvTitle;

        public ViewHolder(View view) {
            super(view);
            tvSelect = (TextView) view.findViewById(R.id.tv_select);
            tvTitle = (FontTextView) view.findViewById(R.id.tv_title);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickListener.onClick(getPosition());
                }
            });
        }
    }


    public void FilterList(String filterTxt)
    {
        if (filterTxt.isEmpty())
            mFilterArray = mOriginalArray;
        else
        {
            mFilterArray = new ArrayList<>();

            for (Event_Types type : mOriginalArray)
            {
                if (type.getEvent_type_name().toLowerCase().contains(filterTxt.toLowerCase()))
                    mFilterArray.add(type);
            }

            if (mFilterArray.size()==0)
                mFilterArray = mOriginalArray;
        }

        notifyDataSetChanged();
    }

}
