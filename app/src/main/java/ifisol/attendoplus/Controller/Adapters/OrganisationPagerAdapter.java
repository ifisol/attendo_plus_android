package ifisol.attendoplus.Controller.Adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import java.util.ArrayList;

import ifisol.attendoplus.R;
import ifisol.attendoplus.Utilities.Constants;
import ifisol.attendoplus.View.Fragments.GuestsFragment;
import ifisol.attendoplus.View.Fragments.OrganisationDetailFragment;
import ifisol.attendoplus.View.Fragments.OrganisationEventsFragment;

/**
 * Created by Mehtab Ahmad on 7/28/2016.
 */
public class OrganisationPagerAdapter extends FragmentStatePagerAdapter {


    Context mContext;

    String[] tabs;
    ArrayList<Fragment> mFragList;
    String mUserType;
    int[] tabIcons;
    String orgID;


    public OrganisationPagerAdapter(FragmentManager fm, Context context, String id) {

        super(fm);
        mContext=context;
        mUserType=Constants.USER_TYPE;
        this.orgID=id;

        mFragList = new ArrayList<>();

            mFragList.add(new OrganisationDetailFragment());
            mFragList.add(new GuestsFragment());
            mFragList.add(new OrganisationEventsFragment());
            tabs      = new String[]{"Organisation Detail","Organisation Members",Constants.TAG_ORGANISATION_EVENTS};
           tabIcons =new int[]{R.drawable.ic_event_details,R.drawable.ic_people,R.drawable.ic_events};

    }

    @Override
    public Fragment getItem(int position) {

        Fragment fragment =  mFragList.get(position);
        Bundle bundle = new Bundle();
        bundle.putString("ORGANISATION_ID", orgID);
        if (position==1)
            bundle.putString(Constants.USER_TYPE, tabs[1]);

            fragment.setArguments(bundle);


        return fragment;
    }

    @Override
    public int getCount() {

        return tabs.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        return tabs[position];
    }

    public View getTabView(int position) {

        // Given you have a custom layout in `res/layout/custom_tab.xml` with a TextView and ImageView
        View v = LayoutInflater.from(mContext).inflate(R.layout.custom_tab_layout, null);

        ImageView img = (ImageView) v.findViewById(R.id.img_tab);

        img.setImageResource(tabIcons[position]);

        return v;
    }



}
