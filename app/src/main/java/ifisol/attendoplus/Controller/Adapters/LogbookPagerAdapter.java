package ifisol.attendoplus.Controller.Adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import ifisol.attendoplus.R;
import ifisol.attendoplus.Utilities.Constants;
import ifisol.attendoplus.View.Fragments.LogbookEventsFragment;

/**
 * Created by Mehtab Ahmad on 7/28/2016.
 */
public class LogbookPagerAdapter extends FragmentStatePagerAdapter {


    Context mContext;

    String[] tabs;
    int[] tabIcons;
    int[] tabIconsAct;

    public LogbookPagerAdapter(FragmentManager fm, Context context) {

        super(fm);
        mContext=context;

        tabs =new String[]{Constants.TAG_PENDING_LOG_EVENTS , Constants.TAG_FILLED_LOG_EVENTS};
        tabIcons = new int[]{R.drawable.ic_logbook_pending,R.drawable.ic_logbook_filled};
        tabIconsAct = new int[]{R.drawable.ic_logbook_pending_active,R.drawable.ic_logbook_filled_activie};

    }

    @Override
    public Fragment getItem(int position) {

        Fragment fragment = new LogbookEventsFragment();
        Bundle bundle = new Bundle();
        bundle.putString(Constants.EVENT_TYPE, tabs[position]);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getCount() {

        return tabs.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        return tabs[position];
    }

    public View getTabView(int position) {

        // Given you have a custom layout in `res/layout/custom_tab.xml` with a TextView and ImageView
        View v = LayoutInflater.from(mContext).inflate(R.layout.custom_tab_layout, null);

        ImageView img = (ImageView) v.findViewById(R.id.img_tab);

        img.setImageResource(tabIcons[position]);

        return v;
    }

    public int GetActIcon(int pos)
    {
       return tabIconsAct[pos];
    }
    public int GetNormIcon(int pos)
    {
        return tabIcons[pos];
    }
}
