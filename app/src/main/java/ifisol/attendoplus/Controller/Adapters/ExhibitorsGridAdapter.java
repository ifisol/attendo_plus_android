package ifisol.attendoplus.Controller.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

import fr.arnaudguyon.smartfontslib.FontTextView;
import ifisol.attendoplus.Global.Globals;
import ifisol.attendoplus.Model.Exhibitors;
import ifisol.attendoplus.R;
import ifisol.attendoplus.Utilities.Constants;
import ifisol.attendoplus.View.Fragments.EventQrScannerFragment;
import ifisol.attendoplus.View.Fragments.ExhibitorsFragment;

/**
 * Created by Mehtab Ahmad on 12/15/2016.
 */
public class ExhibitorsGridAdapter extends RecyclerView.Adapter<ExhibitorsGridAdapter.ViewHolder> {
    
    FragmentActivity mActivity;
    Fragment fragment;
    public ArrayList<Exhibitors> mExList;
    String eventType;
    clickListener mListener;

    public ExhibitorsGridAdapter(FragmentActivity activity, Fragment frag, ArrayList<Exhibitors> list)
    {

        mActivity = activity;
        fragment = frag;
        mExList = list ;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = null;
        if (fragment instanceof EventQrScannerFragment)
            v = LayoutInflater.from(mActivity).inflate(R.layout.notification_viewer_list_item, null);
        else
            v = LayoutInflater.from(mActivity).inflate(R.layout.exhibitors_list_item, null);
        ViewHolder vh =new ViewHolder(v);

        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder h, int position) {

        if (fragment instanceof ExhibitorsFragment || fragment instanceof EventQrScannerFragment)
            h.tvSelection.setVisibility(View.GONE);
        else
            h.tvSelection.setVisibility(View.VISIBLE);

        h.tvName.setText(mExList.get(position).getExhibitor_name());
        ImageLoader.getInstance().displayImage(Constants.Img_Base_URL+mExList.get(position).getExhibitor_picture(), h.imgExhibitor);
        if(fragment instanceof ExhibitorsFragment)
            h.tvOrganisation.setText(mExList.get(position).getExhibitor_company());

        if (((Globals)mActivity.getApplicationContext()).mActiveUser.getUser_type().equals(Constants.TAG_GUEST))
        {
           /* if (mExList.get(position).getExhb_scanned_status()!=null && mExList.get(position).getExhb_scanned_status().equals("yes"))
            {
                h.tvSelection.setVisibility(View.VISIBLE);
                h.tvSelection.setIs_selected(true);
            }
            else
                h.tvSelection.setVisibility(View.GONE);*/
        }

        if (mExList.get(position).isIs_selected())
            h.tvSelection.setSelected(true);
        else
            h.tvSelection.setSelected(false);


    }

    @Override
    public int getItemCount() {

        return mExList.size();
    }

    protected class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private CircularImageView imgExhibitor;
        private FontTextView tvSelection;
        private FontTextView tvName;
        private FontTextView tvOrganisation;

        public ViewHolder(View view) {

            super(view);
            imgExhibitor = (CircularImageView) view.findViewById(R.id.img_exhibitor);
            tvSelection = (FontTextView) view.findViewById(R.id.tv_selection);
            tvName = (FontTextView) view.findViewById(R.id.tv_name);
            tvOrganisation = (FontTextView) view.findViewById(R.id.tv_organisation);

            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            if (mListener!=null)
                mListener.onItemClick(getPosition(),mExList.get(getPosition()));
        }
    }

    public void setClickListener(clickListener listener)
    {
        mListener = listener;
    }
    public interface clickListener
    {
        public void onItemClick(int pos, Exhibitors obj);
    }
}
