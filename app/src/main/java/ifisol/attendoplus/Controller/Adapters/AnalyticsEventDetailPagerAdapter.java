package ifisol.attendoplus.Controller.Adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import java.util.ArrayList;

import ifisol.attendoplus.Model.Events;
import ifisol.attendoplus.R;
import ifisol.attendoplus.Utilities.Constants;
import ifisol.attendoplus.View.Fragments.Analytics_FeedbackFragment;
import ifisol.attendoplus.View.Fragments.Analytics_LogbookFragment;
import ifisol.attendoplus.View.Fragments.ExhibitorsFragment;
import ifisol.attendoplus.View.Fragments.GuestsFragment;

/**
 * Created by Mehtab Ahmad on 7/28/2016.
 */
public class AnalyticsEventDetailPagerAdapter extends FragmentStatePagerAdapter {


    Context mContext;

    String[] tabs;
    ArrayList<Fragment> mFragList;
    String mUserType;
    int TabIcons[];
    Events event;


    public AnalyticsEventDetailPagerAdapter(FragmentManager fm, Context context, Events evnt) {

        super(fm);
        mContext=context;
        mUserType=Constants.USER_TYPE;
        this.event = evnt;

        mFragList = new ArrayList<>();

            mFragList.add(new GuestsFragment());
            mFragList.add(new GuestsFragment());
            mFragList.add(new ExhibitorsFragment());
            mFragList.add(new Analytics_LogbookFragment());
            mFragList.add(new Analytics_FeedbackFragment());


            tabs      = new String[]{Constants.TAG_INTERESTED_GUEST,Constants.TAG_ATTENDED_GUEST,"Exhibitor visits","Event Logbook","Event Feedback"};
            TabIcons  = new int[] {R.drawable.ic_interested,R.drawable.ic_attendance,R.drawable.ic_exhibitor,R.drawable.ic_logbook,R.drawable.ic_feedback};


    }

    @Override
    public Fragment getItem(int position) {

        Fragment fragment = mFragList.get(position);

        Bundle bundle = new Bundle();
            bundle.putString(Constants.USER_TYPE, tabs[position]);
            bundle.putString("EVENT_ID", event.getEvent_id());
            bundle.putSerializable("EVENT", event);
            bundle.putString(Constants.EVENT_TYPE, Constants.TAG_ANALYTIC_EVENTS);
            fragment.setArguments(bundle);


        return fragment;
    }

    @Override
    public int getCount() {

        return tabs.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        return tabs[position];
    }

    public View getTabView(int position) {

        // Given you have a custom layout in `res/layout/custom_tab.xml` with a TextView and ImageView
        View v = LayoutInflater.from(mContext).inflate(R.layout.custom_tab_layout, null);

        ImageView img = (ImageView) v.findViewById(R.id.img_tab);

        img.setImageResource(TabIcons[position]);

        return v;
    }
}
