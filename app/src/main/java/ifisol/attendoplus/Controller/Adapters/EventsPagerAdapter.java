package ifisol.attendoplus.Controller.Adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;

import ifisol.attendoplus.Global.Globals;
import ifisol.attendoplus.R;
import ifisol.attendoplus.Utilities.Constants;
import ifisol.attendoplus.View.Fragments.AwaitingEventsFragment;
import ifisol.attendoplus.View.Fragments.PastEventsFragment;
import ifisol.attendoplus.View.Fragments.UpcomingEventsFragment;

/**
 * Created by Mehtab Ahmad on 7/28/2016.
 */
public class EventsPagerAdapter extends FragmentStatePagerAdapter {


    Context mContext;

    String[] tabs;
    String mUserType;
    int[] tabIcons;
    int[] tabActIcons;
    SparseArray<Fragment> registeredFragments = new SparseArray<Fragment>();
    public int currentPos=0;
    ArrayList<Fragment> mFragList = new ArrayList<>();

    public EventsPagerAdapter(FragmentManager fm, Context context) {

        super(fm);
        mContext=context;
        mUserType=((Globals)mContext.getApplicationContext()).mActiveUser.getUser_type();

        if (mUserType.equals(Constants.TAG_GUEST))
        {
            tabs =new String[]{Constants.TAG_UPCOMING,Constants.TAG_ATTENDED};
            tabIcons =new int[]{R.drawable.ic_upcoming,R.drawable.ic_past};
            tabActIcons =new int[]{R.drawable.ic_upcoming_active,R.drawable.ic_past_active};
            mFragList.add(new UpcomingEventsFragment());
            mFragList.add(new PastEventsFragment());

        }
        else if (mUserType.equals(Constants.TAG_ORGANISER))
        {
            tabs =new String[]{Constants.TAG_AWAITING,Constants.TAG_UPCOMING,Constants.TAG_PAST};
            tabIcons =new int[]{R.drawable.ic_awaiting, R.drawable.ic_upcoming,R.drawable.ic_past};
            tabActIcons =new int[]{R.drawable.ic_awaiting_active,R.drawable.ic_upcoming_active,R.drawable.ic_past_active};
            mFragList.add(new AwaitingEventsFragment());
            mFragList.add(new UpcomingEventsFragment());
            mFragList.add(new PastEventsFragment());
        }
    }

    @Override
    public Fragment getItem(int position) {

        Fragment fragment = mFragList.get(position);
        Bundle bundle = new Bundle();
        bundle.putString(Constants.EVENT_TYPE,tabs[position]);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getCount() {

        return tabs.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        return tabs[position];
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment fragment = (Fragment) super.instantiateItem(container, position);
        registeredFragments.put(position, fragment);
        currentPos = position;
        return fragment;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        registeredFragments.remove(position);
        super.destroyItem(container, position, object);
    }



    public Fragment getRegisteredFragment(int position) {
        return registeredFragments.get(position);
    }


    public View getTabView(int position) {

        // Given you have a custom layout in `res/layout/custom_tab.xml` with a TextView and ImageView
        View v = LayoutInflater.from(mContext).inflate(R.layout.custom_tab_layout, null);

        ImageView img = (ImageView) v.findViewById(R.id.img_tab);

        img.setImageResource(tabIcons[position]);

        return v;
    }

    public int GetActIcon(int pos)
    {
        return tabActIcons[pos];
    }
    public int GetNormalIcon(int pos)
    {
        return tabIcons[pos];
    }
}
