package ifisol.attendoplus.Controller.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import fr.arnaudguyon.smartfontslib.FontTextView;
import ifisol.attendoplus.Global.Globals;
import ifisol.attendoplus.Model.Notifications;
import ifisol.attendoplus.R;
import ifisol.attendoplus.Utilities.Constants;

/**
 * Created by Mehtab Ahmad on 1/6/2017.
 */

public class NotificationsAdapter extends BaseExpandableListAdapter {
    private Context context;
    private List<String> listHeaders;
    Globals globals;
    private HashMap<String, ArrayList<Notifications>> dataHM;

    public NotificationsAdapter(Context context, List<String> listHeaders, HashMap<String, ArrayList<Notifications>> listChildData) {
        this.context = context;
        this.listHeaders = listHeaders;
        this.dataHM = listChildData;
        globals = (Globals)context.getApplicationContext();
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this.dataHM.get(this.listHeaders.get(groupPosition))
                .get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {



        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.notification_list_item_child, null);
            convertView.setTag(new ViewHolderChild(convertView));
        }

        InitChildern((ViewHolderChild)convertView.getTag(),childPosition,groupPosition);

        return convertView;
    }



    private void InitChildern(ViewHolderChild h, int childPosition, int groupPosition) {

        Notifications notification = (Notifications) getChild(groupPosition,childPosition);
        ImageLoader.getInstance().displayImage(Constants.Img_Base_URL+notification.getNotification_picture(),h.imgUser);
        h.tvName.setText(notification.getNotification_name());
        h.tvSubject.setText(notification.getNotification_subject());

        if (globals.mActiveUser.getUser_type().equals(Constants.TAG_GUEST))
        {
            if (notification.getNotification_read_status().equals("yes"))
                h.mNotiView.setBackgroundColor(context.getResources().getColor(R.color.white));
            else
                h.mNotiView.setBackgroundColor(context.getResources().getColor(R.color.very_light_grey));
        }


    }





    @Override
    public int getChildrenCount(int groupPosition) {
        return this.dataHM.get(this.listHeaders.get(groupPosition))
                .size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.listHeaders.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this.listHeaders.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.notification_list_header_item, null);
        }

        FontTextView lblListHeader = (FontTextView) convertView.findViewById(R.id.tv_header);
        lblListHeader.setText(headerTitle);

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }








    protected class ViewHolderChild {
        private CircularImageView imgUser;
        private FontTextView tvName;
        private FontTextView tvSubject;
        private View mNotiView;

        public ViewHolderChild(View view) {
            imgUser = (CircularImageView) view.findViewById(R.id.img_user);
            tvName = (FontTextView) view.findViewById(R.id.tv_name);
            tvSubject = (FontTextView) view.findViewById(R.id.tv_subject);
            mNotiView = view.findViewById(R.id.view_notification);
        }
    }





}
