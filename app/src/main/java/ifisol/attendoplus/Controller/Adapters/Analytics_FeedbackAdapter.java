package ifisol.attendoplus.Controller.Adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.hedgehog.ratingbar.RatingBar;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

import fr.arnaudguyon.smartfontslib.FontTextView;
import ifisol.attendoplus.Model.User;
import ifisol.attendoplus.R;
import ifisol.attendoplus.Utilities.Constants;
import ifisol.attendoplus.Utilities.Utility;
import ifisol.attendoplus.View.Fragments.Analytics_FeedbackDetailFragment;

/**
 * Created by Mehtab Ahmad on 12/15/2016.
 */
public class Analytics_FeedbackAdapter extends RecyclerView.Adapter<Analytics_FeedbackAdapter.ViewHolder> {

    Context mContext;
    FragmentActivity mActivity;
    ArrayList<User> mUserList;

    public Analytics_FeedbackAdapter(FragmentActivity activity,ArrayList<User> mList)
    {

        mUserList = mList;
        mActivity = activity;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v     = LayoutInflater.from(mContext).inflate(R.layout.analytics_feedback_list_item, null);
        ViewHolder vh =new ViewHolder(v);

        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder h, int position) {

        User mUser = mUserList.get(position);
        ImageLoader.getInstance().displayImage(Constants.Img_Base_URL+mUser.getPicture(),h.imgUser);
        h.tvUsername.setText(mUser.getUser_name());
        h.tvEmail.setText(mUser.getEmail());
        h.userRating.setStarCount(mUser.getRatings());

    }

    @Override
    public int getItemCount() {

        return mUserList.size();
    }




    protected class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private CircularImageView imgUser;
        private FontTextView      tvUsername;
        private FontTextView      tvEmail;
        private RatingBar         userRating;
        public ViewHolder(View view) {

            super(view);

            imgUser = (CircularImageView) view.findViewById(R.id.img_user);
            tvUsername = (FontTextView) view.findViewById(R.id.tv_username);
            tvEmail = (FontTextView) view.findViewById(R.id.tv_email);
            userRating = (RatingBar) view.findViewById(R.id.user_rating);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            Bundle bundle = new Bundle();
            bundle.putSerializable("USER", mUserList.get(getPosition()));
            Utility.ReplaceFragment(new Analytics_FeedbackDetailFragment(),mActivity.getSupportFragmentManager(),bundle);

        }
    }
}
