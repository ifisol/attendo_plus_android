package ifisol.attendoplus.Controller.Adapters;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

import fr.arnaudguyon.smartfontslib.FontTextView;
import ifisol.attendoplus.Model.User;
import ifisol.attendoplus.R;
import ifisol.attendoplus.Utilities.Constants;
import ifisol.attendoplus.Utilities.Utility;
import ifisol.attendoplus.View.Fragments.Analytics_logbookDetailFragment;

/**
 * Created by Mehtab Ahmad on 12/15/2016.
 */
public class Analytics_LogbookAdapter extends RecyclerView.Adapter<Analytics_LogbookAdapter.ViewHolder> {

    FragmentActivity mActivity;
    ArrayList<User> mLogList;

    public Analytics_LogbookAdapter(FragmentActivity activity, ArrayList<User> mList)
    {
        mActivity = activity;
        mLogList = mList;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v     = LayoutInflater.from(mActivity).inflate(R.layout.analytics_logbook_list_item, null);
        ViewHolder vh =  new ViewHolder(v);

        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder h, int position) {

        User user = mLogList.get(position);
        ImageLoader.getInstance().displayImage(Constants.Img_Base_URL+user.getPicture(),h.imgUser);
        h.tvUsername.setText(user.getUser_name());
        h.tvEmail.setText(user.getEmail());
        h.tvPhone.setText(user.getPhone());

    }

    @Override
    public int getItemCount() {

        return mLogList.size();
    }



    protected class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private CircularImageView imgUser;
        private FontTextView      tvUsername;
        private FontTextView      tvEmail;
        private FontTextView      tvPhone;

        public ViewHolder(View view) {

            super(view);
            imgUser    = (CircularImageView) view.findViewById(R.id.img_user);
            tvUsername = (FontTextView) view.findViewById(R.id.tv_username);
            tvEmail    = (FontTextView) view.findViewById(R.id.tv_email);
            tvPhone    = (FontTextView) view.findViewById(R.id.tv_phone);

            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            Bundle bundle = new Bundle();
            bundle.putSerializable("USER", mLogList.get(getPosition()));
            Utility.ReplaceFragment(new Analytics_logbookDetailFragment(),mActivity.getSupportFragmentManager(),bundle);

        }
    }
}
