package ifisol.attendoplus.Controller.Adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import java.util.ArrayList;

import ifisol.attendoplus.R;
import ifisol.attendoplus.Utilities.Constants;
import ifisol.attendoplus.View.Fragments.AnalyticsEventsFragment;
import ifisol.attendoplus.View.Fragments.Graphs_Fragment;

/**
 * Created by Mehtab Ahmad on 7/28/2016.
 */
public class AnalyticsMainPagerAdapter extends FragmentStatePagerAdapter {


    Context mContext;

    String[] tabs;
    int[] TabNormIcons;
    ArrayList<Fragment> mFragList;
    String mUserType;


    public AnalyticsMainPagerAdapter(FragmentManager fm, Context context) {

        super(fm);
        mContext=context;
        mUserType=Constants.USER_TYPE;

        mFragList = new ArrayList<>();

            mFragList.add(new AnalyticsEventsFragment());
            mFragList.add(new Graphs_Fragment());


           tabs      = new String[]{Constants.TAG_STATISTICS_EVENTS,"Graphs"};
           TabNormIcons = new int[] {R.drawable.ic_stats,R.drawable.ic_graph};



    }

    @Override
    public Fragment getItem(int position) {

        Fragment fragment = mFragList.get(position);
            Bundle bundle = new Bundle();
            bundle.putString(Constants.EVENT_TYPE, tabs[0]);
            fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getCount() {

        return tabs.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        return tabs[position];
    }

    public View getTabView(int position) {

        // Given you have a custom layout in `res/layout/custom_tab.xml` with a TextView and ImageView
        View v = LayoutInflater.from(mContext).inflate(R.layout.custom_tab_layout, null);

        ImageView img = (ImageView) v.findViewById(R.id.img_tab);

        img.setImageResource(TabNormIcons[position]);

        return v;
    }


}
