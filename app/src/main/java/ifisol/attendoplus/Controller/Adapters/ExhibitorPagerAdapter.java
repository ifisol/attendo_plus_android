package ifisol.attendoplus.Controller.Adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import java.util.ArrayList;

import ifisol.attendoplus.R;
import ifisol.attendoplus.Utilities.Constants;
import ifisol.attendoplus.View.Fragments.ExhibitorDetailFragment;
import ifisol.attendoplus.View.Fragments.ExhibitorParticipatedEventsFragment;

/**
 * Created by Mehtab Ahmad on 7/28/2016.
 */
public class ExhibitorPagerAdapter extends FragmentStatePagerAdapter {


    Context mContext;

    String[] tabs;
    ArrayList<Fragment> mFragList;
    String mUserType;
    int[]  tabIcons;
    String ExhibitorID;


    public ExhibitorPagerAdapter(FragmentManager fm, Context context, String ID) {
        super(fm);
        mContext=context;
        mUserType=Constants.USER_TYPE;
        mFragList = new ArrayList<>();
        mFragList.add(new ExhibitorDetailFragment());
        mFragList.add(new ExhibitorParticipatedEventsFragment());
        tabs      = new String[]{"Exhibitor Detail",Constants.TAG_EXHIBITOR_PARTICIPATED_EVENTS};
        tabIcons  = new int[]{R.drawable.ic_event_details,R.drawable.ic_events};
        this.ExhibitorID = ID;
    }



    @Override
    public Fragment getItem(int position) {
        Fragment fragment = mFragList.get(position);
            Bundle bundle = new Bundle();
            bundle.putString("EXHIBITOR_ID", ExhibitorID);
            fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public int getCount() {

        return tabs.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        return tabs[position];
    }

    public View getTabView(int position) {

        // Given you have a custom layout in `res/layout/custom_tab.xml` with a TextView and ImageView
        View v = LayoutInflater.from(mContext).inflate(R.layout.custom_tab_layout, null);

        ImageView img = (ImageView) v.findViewById(R.id.img_tab);

        img.setImageResource(tabIcons[position]);

        return v;
    }



}
