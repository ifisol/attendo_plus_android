package ifisol.attendoplus.Controller.Adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

import fr.arnaudguyon.smartfontslib.FontTextView;
import ifisol.attendoplus.Model.User;
import ifisol.attendoplus.R;
import ifisol.attendoplus.Utilities.Constants;
import ifisol.attendoplus.Utilities.Utility;
import ifisol.attendoplus.View.Fragments.ProfileFragment;

/**
 * Created by Mehtab Ahmad on 12/15/2016.
 */
public class Viewers_RecyclerAdapter extends RecyclerView.Adapter<Viewers_RecyclerAdapter.ViewHolder> {

    Context mContext;
    FragmentActivity mActivity;
    ArrayList<User> mList;

    public Viewers_RecyclerAdapter(Context context, FragmentActivity activity, ArrayList<User> list)
    {

        mContext  = context;
        mActivity = activity;
        mList = list;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v     = LayoutInflater.from(mContext).inflate(R.layout.notification_viewer_list_item, null);
        ViewHolder vh =new ViewHolder(v);

        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder h, int position) {

        User user = mList.get(position);
        ImageLoader.getInstance().displayImage(Constants.Img_Base_URL+user.getPicture(),h.imgUser);
        h.tvName.setText(user.getUser_name());
    }

    @Override
    public int getItemCount() {

        return mList.size();
    }



    protected class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private CircularImageView imgUser;
        private FontTextView tvSelection;
        private FontTextView tvName;
        public ViewHolder(View view) {

            super(view);

            imgUser = (CircularImageView) view.findViewById(R.id.img_exhibitor);
            tvSelection = (FontTextView) view.findViewById(R.id.tv_selection);
            tvName = (FontTextView) view.findViewById(R.id.tv_name);

            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            if (mList.get(getPosition()).getProfile_visibility_status()!=null && mList.get(getPosition()).getProfile_visibility_status().equals("public"))
            {
                Bundle bundle = new Bundle();
                bundle.putString("USER_ID", mList.get(getPosition()).getUser_id());
                bundle.putString("PROFILE_TYPE", "OTHER");
                Utility.ReplaceFragment(new ProfileFragment(),mActivity.getSupportFragmentManager(),bundle);
            }
            else
                Utility.ShowAlert(mActivity,"Private Profile","This user has a private profile. You cannot view private profiles");

        }
    }
}
