package ifisol.attendoplus.Controller.Adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.github.siyamed.shapeimageview.RoundedImageView;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

import fr.arnaudguyon.smartfontslib.FontTextView;
import ifisol.attendoplus.Model.Events;
import ifisol.attendoplus.R;
import ifisol.attendoplus.Utilities.Constants;
import ifisol.attendoplus.Utilities.Utility;
import ifisol.attendoplus.View.Fragments.AttendanceFragment;
import ifisol.attendoplus.View.Fragments.Cpd_MainFragment;
import ifisol.attendoplus.View.Fragments.EventsDetailPagerFragment;

public class ExhibitorEventsAdapter extends BaseAdapter {

    private LayoutInflater layoutInflater;
    FragmentActivity mActivity;
    String mEventType;
    public ArrayList<Events> mFilterArray;
    ArrayList<Events> mOriginalArray;


    public ExhibitorEventsAdapter(Context context, FragmentActivity activity, String type, ArrayList<Events> events) {

        this.layoutInflater = LayoutInflater.from(context);
        mActivity           = activity;
        mEventType = type;
        this.mFilterArray = events;
        this.mOriginalArray = events;}

    @Override
    public int getCount() {
        return mFilterArray.size();
    }

    @Override
    public Object getItem(int position) {
        return mFilterArray.get(position);
    }


    public Events getItm(int position) {
        return mFilterArray.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {


                convertView = layoutInflater.inflate(R.layout.events_list_item, null);
                convertView.setTag(new ViewHolder(convertView));

        }
        initializeViews(convertView.getTag(), position);
        return convertView;
    }

    private void initializeViews(Object vh, int pos) {

            ViewHolder h = (ViewHolder) vh;
            InitEvents(pos, h);

    }



    private void InitEvents(final int pos, ViewHolder h)
    {
        Events obj = mFilterArray.get(pos);

        h.tvEventName.setText(obj.getEvent_name());
        h.tvCpd.setText("Cpd Points : "+obj.getEvent_cpd());
        if (obj.getHas_exhibitor().equals("yes"))
            h.tvExhibitors.setText("Exhibitors : "+obj.getExhibitor_count());
        else
            h.tvExhibitors.setText("No Exhibitors");
        h.tvDateDay.setText(obj.getEvent_date().split("-")[2]);
        h.tvDateMonth.setText(Utility.GetMonth(obj.getEvent_date().split("-")[1]));
        h.tvLocation.setText(obj.getEvent_street_address()+" "+obj.getEvent_city()+" "+obj.getEvent_country());
        h.tvStrtEndTime.setText(obj.getEvent_start_time()+" - "+obj.getEvent_end_time());
        if (obj.getInterested_guests()>3)
        {
            h.viewInterested.setVisibility(View.VISIBLE);
            h.viewInterested.setTag(pos);
            h.tvCount.setText(String.valueOf(obj.getInterested_guests())+"+");
        }
        else
            h.viewInterested.setVisibility(View.GONE);

        ImageLoader.getInstance().displayImage(Constants.Img_Base_URL+obj.getEvent_photo(),h.imgEvent);
        if (obj.getIs_public_event().equals("yes"))
            h.imgPublic.setVisibility(View.VISIBLE);
        else
            h.imgPublic.setVisibility(View.GONE);



            SetLogbookFeedBack(h,obj);




        h.viewInterested.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Bundle bundle1 = new Bundle();
                bundle1.putInt("POSITION", 0);
                bundle1.putSerializable("EVENT", mFilterArray.get(pos));
                bundle1.putString(Constants.EVENT_TYPE, mEventType);
                Utility.ReplaceFragment(new AttendanceFragment(),mActivity.getSupportFragmentManager(),bundle1);
            }
        });


        h.tvExhibitors.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString(Constants.TAG_VIEW_TO_GO, Constants.TAG_EXHIBITOR);
                bundle.putString(Constants.EVENT_TYPE, mEventType);
                bundle.putSerializable("EVENT", mFilterArray.get(pos));
                Utility.ReplaceFragment(new EventsDetailPagerFragment(),mActivity.getSupportFragmentManager(),bundle);
            }
        });


        h.tvCpd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utility.ReplaceFragment(new Cpd_MainFragment(),mActivity.getSupportFragmentManager());
            }
        });

    }



    private void SetLogbookFeedBack(ViewHolder h , Events obj) {
        if (obj.getHas_logbook().equals("yes"))
        {
            h.imgLogbook.setImageResource(R.drawable.ic_has_logbook);
            h.tvLogbook.setTextColor(mActivity.getResources().getColor(R.color.black));
        }
        else
        {
            h.imgLogbook.setImageResource(R.drawable.ic_no_logbook);
            h.tvLogbook.setTextColor(mActivity.getResources().getColor(R.color.trans_black3));
        }

        if (obj.getHas_feedback().equals("yes"))
        {
            h.imgFeedback.setImageResource(R.drawable.ic_has_feedback);
            h.tvFeedback.setTextColor(mActivity.getResources().getColor(R.color.black));
        }
        else
        {
            h.imgFeedback.setImageResource(R.drawable.ic_no_feedback);
            h.tvFeedback.setTextColor(mActivity.getResources().getColor(R.color.trans_black3));
        }

        /*if (obj.getUser_is_interested().equals("yes"))  // This will be visible in only upcoming events of guest
            h.imgInterested.setVisibility(View.VISIBLE);
        else
            h.imgInterested.setVisibility(View.GONE);*/
    }




    protected class ViewHolder{
        private FontTextView tvEventName;
        private FontTextView tvCpd;
        private FontTextView tvExhibitors;
        private LinearLayout viewDate;
        private FontTextView tvDateDay;
        private FontTextView tvDateMonth;
        private FontTextView tvLocation;
        private FontTextView tvStrtEndTime;
        private View         viewInterested;
        private CircularImageView img1;
        private CircularImageView img2;
        private CircularImageView img3;
        private FontTextView tvCount;
        private RoundedImageView imgEvent;
        private ImageView imgPublic;
        private ImageView imgLogbook;
        private FontTextView tvLogbook;
        private ImageView imgFeedback;
        private FontTextView tvFeedback;
        private ImageView imgInterested;
        public ViewHolder(View view) {
            tvEventName = (FontTextView) view.findViewById(R.id.tv_eventName);
            tvCpd = (FontTextView) view.findViewById(R.id.tv_cpd);
            tvExhibitors = (FontTextView) view.findViewById(R.id.tv_exhibitors);
            viewDate = (LinearLayout) view.findViewById(R.id.view_date);
            tvDateDay = (FontTextView) view.findViewById(R.id.tv_date_day);
            tvDateMonth = (FontTextView) view.findViewById(R.id.tv_date_month);
            tvLocation = (FontTextView) view.findViewById(R.id.tv_location);
            tvStrtEndTime = (FontTextView) view.findViewById(R.id.tv_strt_end_time);
            viewInterested = view.findViewById(R.id.view_interested);
            img1 = (CircularImageView) view.findViewById(R.id.img_1);
            img2 = (CircularImageView) view.findViewById(R.id.img_2);
            img3 = (CircularImageView) view.findViewById(R.id.img_3);
            tvCount = (FontTextView) view.findViewById(R.id.tv_count);
            imgEvent = (RoundedImageView) view.findViewById(R.id.img_event);
            imgPublic = (ImageView) view.findViewById(R.id.img_public);
            imgLogbook = (ImageView) view.findViewById(R.id.img_logbook);
            tvLogbook = (FontTextView) view.findViewById(R.id.tv_logbook);
            imgFeedback = (ImageView) view.findViewById(R.id.img_feedback);
            tvFeedback = (FontTextView) view.findViewById(R.id.tv_feedback);
            imgInterested = (ImageView) view.findViewById(R.id.img_interested);
            imgInterested.setVisibility(View.GONE);  // This will be visible in only upcoming events of guest
        }
    }


    public void FilterList(String filterTxt)
    {
        if (filterTxt.isEmpty())
            mFilterArray = mOriginalArray;
        else
        {
            mFilterArray = new ArrayList<>();

            for (Events obj : mOriginalArray)
            {
                if (obj.getEvent_name().toLowerCase().contains(filterTxt.toLowerCase()))
                    mFilterArray.add(obj);
            }

            if (mFilterArray.size()==0)
                mFilterArray = mOriginalArray;
        }

        notifyDataSetChanged();
    }
}
