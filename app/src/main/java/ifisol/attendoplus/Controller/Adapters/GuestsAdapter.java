package ifisol.attendoplus.Controller.Adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

import fr.arnaudguyon.smartfontslib.FontTextView;
import ifisol.attendoplus.Model.Attendees;
import ifisol.attendoplus.Model.User;
import ifisol.attendoplus.R;
import ifisol.attendoplus.Utilities.Constants;

/**
 * Created by Mehtab Ahmad on 12/15/2016.
 */
public class GuestsAdapter extends RecyclerView.Adapter<GuestsAdapter.ViewHolder> {

    Context mContext;
    FragmentActivity mActivity;
    Fragment fragment;
    ArrayList<?> mGuestList;

    ItemClick clickListener;
    Attendees attendees;
    User mUser;

    public GuestsAdapter(Context context, FragmentActivity activity, Fragment frag, ArrayList<?> mList)
    {

        mContext  = context;
        mActivity = activity;
        fragment  = frag;
        mGuestList = mList;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v     = LayoutInflater.from(mContext).inflate(R.layout.attendance_guest_list_item, null);
        ViewHolder vh =new ViewHolder(v);

        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder h, int position) {

        Object user = mGuestList.get(position);

        if (user instanceof Attendees)
        {
            attendees = (Attendees) user;
            ImageLoader.getInstance().displayImage(Constants.Img_Base_URL+attendees.getPicture(),h.imgUser);
            h.tvUsername.setText(attendees.getUser_name());
            h.tvEmail.setText(attendees.getEmail());
            h.tvProfession.setText(attendees.getProfession());
            if (!attendees.getUser_registration_status().equals("auto"))
                h.imgManual.setVisibility(View.VISIBLE);
            else
                h.imgManual.setVisibility(View.GONE);
        }

        else
        {
            mUser = (User) user;
            ImageLoader.getInstance().displayImage(Constants.Img_Base_URL+mUser.getPicture(),h.imgUser);
            h.tvUsername.setText(mUser.getUser_name());
            h.tvEmail.setText(mUser.getEmail());
            h.tvProfession.setText(mUser.getProfession());
            if (!mUser.getUser_registration_status().equals("auto"))
                h.imgManual.setVisibility(View.VISIBLE);
            else
                h.imgManual.setVisibility(View.GONE);
        }




    }

    @Override
    public int getItemCount() {

        return mGuestList.size();
    }

        protected class ViewHolder extends RecyclerView.ViewHolder{


            private ImageView imgManual;
            private CircularImageView imgUser;
            private FontTextView tvUsername;
            private FontTextView tvEmail;
            private FontTextView tvProfession;

            public ViewHolder(View view) {
                super(view);
                imgManual = (ImageView) view.findViewById(R.id.img_manual);
                imgUser = (CircularImageView) view.findViewById(R.id.img_user);
                tvUsername = (FontTextView) view.findViewById(R.id.tv_username);
                tvEmail = (FontTextView) view.findViewById(R.id.tv_email);
                tvProfession = (FontTextView) view.findViewById(R.id.tv_profession);
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        clickListener.onItemClick(getPosition(), mGuestList.get(getPosition()));
                    }
                });
            }
        }


    public void setItemClick(ItemClick click)
    {
        this.clickListener = click ;
    }

    public interface ItemClick
    {
        public void onItemClick(int position, Object user);
    }

}
