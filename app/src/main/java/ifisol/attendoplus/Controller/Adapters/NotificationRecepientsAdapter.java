package ifisol.attendoplus.Controller.Adapters;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

import fr.arnaudguyon.smartfontslib.FontTextView;
import ifisol.attendoplus.Model.NotificationRecepients;
import ifisol.attendoplus.R;
import ifisol.attendoplus.Utilities.Constants;

/**
 * Created by Mehtab Ahmad on 12/15/2016.
 */
public class NotificationRecepientsAdapter extends RecyclerView.Adapter<NotificationRecepientsAdapter.ViewHolder> {

    Context mContext;
    FragmentActivity mActivity;
    public ArrayList<NotificationRecepients> mFilterArray;
    ArrayList<NotificationRecepients> mOriginalArray;

    public NotificationRecepientsAdapter(Context context, ArrayList<NotificationRecepients> mList)
    {

        mContext  = context;
        mFilterArray = mList;
        mOriginalArray = mList;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v     = LayoutInflater.from(mContext).inflate(R.layout.recepient_list_item, null);
        ViewHolder vh =new ViewHolder(v);

        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder h, int position) {

        NotificationRecepients user = mFilterArray.get(position);



            ImageLoader.getInstance().displayImage(Constants.Img_Base_URL+user.getPicture(),h.imgUser);
            h.tvUsername.setText(user.getFirst_name()+" "+user.getLast_name());

        if (user.isSelect())
            h.tvSelect.setSelected(true);
        else
            h.tvSelect.setSelected(false);



    }

    @Override
    public int getItemCount() {

        return mFilterArray.size();
    }

        protected class ViewHolder extends RecyclerView.ViewHolder{



            private CircularImageView imgUser;
            private FontTextView tvUsername;
            private TextView tvSelect;

            public ViewHolder(View view) {
                super(view);
                imgUser = (CircularImageView) view.findViewById(R.id.img_guest);
                tvUsername = (FontTextView) view.findViewById(R.id.tv_name);
                tvSelect = (TextView) view.findViewById(R.id.tv_select);
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mFilterArray.get(getPosition()).isSelect())
                            mFilterArray.get(getPosition()).setSelect(false);
                        else
                            mFilterArray.get(getPosition()).setSelect(true);

                        notifyDataSetChanged();
                    }
                });
            }
        }
    public void FilterList(String filterTxt)
    {
        if (filterTxt.isEmpty())
            mFilterArray = mOriginalArray;
        else
        {
            mFilterArray = new ArrayList<>();

            for (NotificationRecepients recepient : mOriginalArray)
            {
                if (recepient.getFirst_name().toLowerCase().contains(filterTxt.toLowerCase()))
                    mFilterArray.add(recepient);
            }

            if (mFilterArray.size()==0)
                mFilterArray = mOriginalArray;
        }

        notifyDataSetChanged();
    }

}
